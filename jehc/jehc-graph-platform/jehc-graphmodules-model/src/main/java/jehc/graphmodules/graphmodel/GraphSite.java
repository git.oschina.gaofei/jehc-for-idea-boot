package jehc.graphmodules.graphmodel;
import jehc.xtmodules.xtcore.base.BaseEntity;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* graph_site 站点 
* 2020-01-21 10:36:28  邓纯杰
*/
public class GraphSite extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String graph_site_id;/**站点主键**/
	private String graph_site_name;/**名称**/
	private int graph_site_status;/**状态（0关闭1发布）**/
	private String graph_site_mxgraph;/**存储值**/
	private String xt_userinfo_id;/**创建人**/
	private Date cTime;/**创建时间**/
	private Date mTime;/**最后修改时间**/
	private String update_userinfo_id;/**最后修改人**/
	private String content;/**内容**/
	private String background;/**背景颜色**/
	private String gridColor;/**网格颜色**/
	private int showGrid;/**显示网格（0是1否）**/
	private int isDelete;/**是否删除0正常1删除**/
	private int gridSize;//网格大小

	private String createUser;
	private String updateUser;
	public void setGraph_site_id(String graph_site_id){
		this.graph_site_id=graph_site_id;
	}
	public String getGraph_site_id(){
		return graph_site_id;
	}
	public void setGraph_site_name(String graph_site_name){
		this.graph_site_name=graph_site_name;
	}
	public String getGraph_site_name(){
		return graph_site_name;
	}
	public void setGraph_site_status(int graph_site_status){
		this.graph_site_status=graph_site_status;
	}
	public int getGraph_site_status(){
		return graph_site_status;
	}
	public void setGraph_site_mxgraph(String graph_site_mxgraph){
		this.graph_site_mxgraph=graph_site_mxgraph;
	}
	public String getGraph_site_mxgraph(){
		return graph_site_mxgraph;
	}
	public void setXt_userinfo_id(String xt_userinfo_id){
		this.xt_userinfo_id=xt_userinfo_id;
	}
	public String getXt_userinfo_id(){
		return xt_userinfo_id;
	}
	public void setCTime(Date cTime){
		this.cTime=cTime;
	}
	public Date getCTime(){
		return cTime;
	}
	public void setMTime(Date mTime){
		this.mTime=mTime;
	}
	public Date getMTime(){
		return mTime;
	}
	public void setUpdate_userinfo_id(String update_userinfo_id){
		this.update_userinfo_id=update_userinfo_id;
	}
	public String getUpdate_userinfo_id(){
		return update_userinfo_id;
	}
	public void setContent(String content){
		this.content=content;
	}
	public String getContent(){
		return content;
	}
	public void setBackground(String background){
		this.background=background;
	}
	public String getBackground(){
		return background;
	}
	public void setGridColor(String gridColor){
		this.gridColor=gridColor;
	}
	public String getGridColor(){
		return gridColor;
	}
	public void setShowGrid(int showGrid){
		this.showGrid=showGrid;
	}
	public int getShowGrid(){
		return showGrid;
	}
	public void setIsDelete(int isDelete){
		this.isDelete=isDelete;
	}
	public int getIsDelete(){
		return isDelete;
	}
	public int getGridSize() {
		return gridSize;
	}

	public void setGridSize(int gridSize) {
		this.gridSize = gridSize;
	}

	public Date getcTime() {
		return cTime;
	}

	public void setcTime(Date cTime) {
		this.cTime = cTime;
	}

	public Date getmTime() {
		return mTime;
	}

	public void setmTime(Date mTime) {
		this.mTime = mTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
