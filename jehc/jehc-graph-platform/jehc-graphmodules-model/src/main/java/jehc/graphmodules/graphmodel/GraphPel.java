package jehc.graphmodules.graphmodel;

import jehc.xtmodules.xtcore.base.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * desc : 图元管理
 * date : 2020-01-16 09:50:00
 * auth : 邓纯杰
 */
public class GraphPel extends BaseEntity implements Serializable {
    private String graph_pel_id;
    private String graph_pel_title;//标题
    private String graph_pel_key;//键
    private int width;//宽
    private int height;//高
    private String hideLabel;//隐藏Label
    private String hideTitles;//隐藏标题
    private String svgHtml;//SVG（存放SVG文件）
    private String graph_pel_type;//图元类型
    private String graph_pel_deleted;//是否删除0正常1删除
    private int graph_pel_sort;//排序
    private String graph_pel_cell_type;//图元节点类型（0.svg,1.image,2.其它）
    private String graph_pel_category;//图元分类（0其它，1电力，2工业）
    private String graph_pel_status;//状态 0未发布 1已发布

    public String getGraph_pel_key() {
        return graph_pel_key;
    }

    public void setGraph_pel_key(String graph_pel_key) {
        this.graph_pel_key = graph_pel_key;
    }

    private List<GraphPelElement> graphPelElements;

    public String getGraph_pel_id() {
        return graph_pel_id;
    }

    public void setGraph_pel_id(String graph_pel_id) {
        this.graph_pel_id = graph_pel_id;
    }

    public String getGraph_pel_title() {
        return graph_pel_title;
    }

    public void setGraph_pel_title(String graph_pel_title) {
        this.graph_pel_title = graph_pel_title;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getHideTitles() {
        return hideTitles;
    }

    public void setHideTitles(String hideTitles) {
        this.hideTitles = hideTitles;
    }

    public String getSvgHtml() {
        return svgHtml;
    }

    public void setSvgHtml(String svgHtml) {
        this.svgHtml = svgHtml;
    }

    public String getGraph_pel_type() {
        return graph_pel_type;
    }

    public void setGraph_pel_type(String graph_pel_type) {
        this.graph_pel_type = graph_pel_type;
    }

    public String getGraph_pel_deleted() {
        return graph_pel_deleted;
    }

    public void setGraph_pel_deleted(String graph_pel_deleted) {
        this.graph_pel_deleted = graph_pel_deleted;
    }

    public int getGraph_pel_sort() {
        return graph_pel_sort;
    }

    public void setGraph_pel_sort(int graph_pel_sort) {
        this.graph_pel_sort = graph_pel_sort;
    }

    public String getGraph_pel_cell_type() {
        return graph_pel_cell_type;
    }

    public void setGraph_pel_cell_type(String graph_pel_cell_type) {
        this.graph_pel_cell_type = graph_pel_cell_type;
    }

    public String getGraph_pel_category() {
        return graph_pel_category;
    }

    public void setGraph_pel_category(String graph_pel_category) {
        this.graph_pel_category = graph_pel_category;
    }

    public String getGraph_pel_status() {
        return graph_pel_status;
    }

    public void setGraph_pel_status(String graph_pel_status) {
        this.graph_pel_status = graph_pel_status;
    }

    public List<GraphPelElement> getGraphPelElements() {
        return graphPelElements;
    }

    public void setGraphPelElements(List<GraphPelElement> graphPelElements) {
        this.graphPelElements = graphPelElements;
    }
}
