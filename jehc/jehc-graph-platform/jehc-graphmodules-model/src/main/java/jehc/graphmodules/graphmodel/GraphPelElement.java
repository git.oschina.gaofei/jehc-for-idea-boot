package jehc.graphmodules.graphmodel;

import jehc.xtmodules.xtcore.base.BaseEntity;

import java.io.Serializable;
/**
 * desc : 图元节点
 * date : 2020-01-16 09:55:55
 * auth : 邓纯杰
 */
public class GraphPelElement extends BaseEntity implements Serializable {
    private String graph_pel_element_id;
    private String graph_pel_id;//图元id外键
    private String graph_pel_element_categray;//节点类型（如Path,ellipse）
    private String graph_pel_element_attrs;//节点属性（如：{d:"M734,931 l0,2 -28,13 -32,7 -32,4 -36,4 -28,3 -29,2 -31,1 -32,1 -34,1 -35,1 -18,0 -18,0 -36,0 -35,-1 -34,-1 -32,-1 -31,-1 -29,-2 -27,-3 -37,-4 -32,-4 -32,-7 -24,-9 -4,-4 0,-2 0,-785 11,-29 24,-22 25,-15 32,-13 37,-12 27,-7 29,-5 31,-5 32,-5 34,-2 35,-2 18,-1 18,0 36,1 35,2 34,2 32,5 31,5 29,5 28,7 25,8 33,13 28,14 21,16 17,22 4,12 0,6 0,785z",style:"fill:#b2b29b;"}）
    private String fills;//填充颜色
    private String stock;//线条颜色
    private int stroke_width;//线框宽度
    private int graph_pel_element_sort;//排序

    public String getGraph_pel_element_id() {
        return graph_pel_element_id;
    }

    public void setGraph_pel_element_id(String graph_pel_element_id) {
        this.graph_pel_element_id = graph_pel_element_id;
    }

    public String getGraph_pel_id() {
        return graph_pel_id;
    }

    public void setGraph_pel_id(String graph_pel_id) {
        this.graph_pel_id = graph_pel_id;
    }

    public String getGraph_pel_element_categray() {
        return graph_pel_element_categray;
    }

    public void setGraph_pel_element_categray(String graph_pel_element_categray) {
        this.graph_pel_element_categray = graph_pel_element_categray;
    }

    public String getGraph_pel_element_attrs() {
        return graph_pel_element_attrs;
    }

    public void setGraph_pel_element_attrs(String graph_pel_element_attrs) {
        this.graph_pel_element_attrs = graph_pel_element_attrs;
    }

    public String getFills() {
        return fills;
    }

    public void setFills(String fills) {
        this.fills = fills;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public int getStroke_width() {
        return stroke_width;
    }

    public void setStroke_width(int stroke_width) {
        this.stroke_width = stroke_width;
    }

    public int getGraph_pel_element_sort() {
        return graph_pel_element_sort;
    }

    public void setGraph_pel_element_sort(int graph_pel_element_sort) {
        this.graph_pel_element_sort = graph_pel_element_sort;
    }

    @Override
    public String toString() {
        return "GraphPelElement{" +
                "graph_pel_element_id='" + graph_pel_element_id + '\'' +
                ", graph_pel_id='" + graph_pel_id + '\'' +
                ", graph_pel_element_categray='" + graph_pel_element_categray + '\'' +
                ", graph_pel_element_attrs='" + graph_pel_element_attrs + '\'' +
                ", fills='" + fills + '\'' +
                ", stock='" + stock + '\'' +
                ", stroke_width=" + stroke_width +
                ", graph_pel_element_sort=" + graph_pel_element_sort +
                '}';
    }
}
