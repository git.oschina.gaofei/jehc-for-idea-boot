package jehc.graphmodules.graphweb;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.graphmodules.graphdao.GraphPelElementDao;
import jehc.graphmodules.graphmodel.GraphPel;
import jehc.graphmodules.graphservice.GraphPelElementService;
import jehc.graphmodules.graphservice.GraphPelService;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.annotation.NeedLoginUnAuth;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * desc : 图元管理
 * date : 2020-01-16 09:50:00
 * auth : 邓纯杰
 */
@Api(value = "图元管理", description = "图元管理")
@Controller
@RequestMapping("/graphPelController")
public class GraphPelController extends BaseAction {
    @Autowired
    GraphPelService graphPelService;
    @Autowired
    GraphPelElementDao graphPelElementDao;

    /**
     * 列表页面
     * @param graphPel
     * @param request
     * @return
     */
    @ApiOperation(value="列表页面", notes="列表页面")
    @RequestMapping(value="/loadGraphPel",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView loadGraphPel(GraphPel graphPel, HttpServletRequest request){
        return new ModelAndView("pc/graph-view/graph-pel/graph-pel-list");
    }
    /**
     * 查询并分页
     * @param baseSearch
     * @param request
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @ResponseBody
    @RequestMapping(value="/getGraphPelListByCondition",method={RequestMethod.POST,RequestMethod.GET})
    public String getGraphPelListByCondition(BaseSearch baseSearch, HttpServletRequest request){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(condition,request);
        List<GraphPel> graphPelList = graphPelService.getGraphPelListByCondition(condition);
        PageInfo<GraphPel> page = new PageInfo<GraphPel>(graphPelList);
        return outPageBootStr(page,request);
    }

    /**
     * 查询单条记录
     * @param graph_pel_id
     * @param request
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @ResponseBody
    @RequestMapping(value="/getGraphPelById",method={RequestMethod.POST,RequestMethod.GET})
    public String getGraphPelById(String graph_pel_id,HttpServletRequest request){
        GraphPel graphPel = graphPelService.getGraphPelById(graph_pel_id);
        return outDataStr(graphPel);
    }

    /**
     * 添加
     * @param graphPel
     * @return
     */
    @ApiOperation(value="添加", notes="添加")
    @ResponseBody
    @RequestMapping(value="/addGraphPel",method={RequestMethod.POST,RequestMethod.GET})
    public String addGraphPel(GraphPel graphPel){
        int i = 0;
        if(null != graphPel){
            graphPel.setGraph_pel_id(UUID.toUUID());
            i=graphPelService.addGraphPel(graphPel);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改
     * @param graphPel
     * @return
     */
    @ApiOperation(value="修改", notes="修改")
    @ResponseBody
    @RequestMapping(value="/updateGraphPel",method={RequestMethod.POST,RequestMethod.GET})
    public String updateGraphPel(GraphPel graphPel){
        int i = 0;
        if(null != graphPel){
            i=graphPelService.updateGraphPel(graphPel);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param graph_pel_id
     */
    @ApiOperation(value="删除", notes="删除")
    @ResponseBody
    @RequestMapping(value="/delGraphPel",method={RequestMethod.POST,RequestMethod.GET})
    public String delGraphPel(String graph_pel_id){
        int i = 0;
        if(!StringUtil.isEmpty(graph_pel_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("graph_pel_id",graph_pel_id.split(","));
            i=graphPelService.delGraphPel(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 新增页面
     */
    @ApiOperation(value="新增页面", notes="新增页面")
    @RequestMapping(value="/toGraphPelAdd",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView toGraphPelAdd(){
        return new ModelAndView("pc/graph-view/graph-pel/graph-pel-add");
    }
    /**
     * 编辑页面
     * @param graph_pel_id
     */
    @ApiOperation(value="编辑页面", notes="编辑页面")
    @RequestMapping(value="/toGraphPelUpdate",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView toGraphPelUpdate(String graph_pel_id,Model model){
        GraphPel graphPel = graphPelService.getGraphPelById(graph_pel_id);
        model.addAttribute("graphPel", graphPel);
        model.addAttribute("graphPelJSON", outItemsStr(graphPel));
        return new ModelAndView("pc/graph-view/graph-pel/graph-pel-update");
    }
    /**
     * 详情页面
     * @param graph_pel_id
     */
    @ApiOperation(value="详情页面", notes="详情页面")
    @RequestMapping(value="/toGraphPelDetail",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView toGraphPelDetail(String graph_pel_id, Model model){
        GraphPel graphPel = graphPelService.getGraphPelById(graph_pel_id);
        model.addAttribute("graphPel", graphPel);
        return new ModelAndView("pc/graph-view/graph-pel/graph-pel-detail");
    }

    /**
     * 查询全部
     */
    @ApiOperation(value="查询全部", notes="查询全部")
    @ResponseBody
    @NeedLoginUnAuth
    @RequestMapping(value="/getGraphPelList",method={RequestMethod.POST,RequestMethod.GET})
    public String getGraphPelList(String graph_pel_category){
        Map<String,Object> condition = new HashMap<>();
        condition.put("graph_pel_category",graph_pel_category);
        List<GraphPel> graphPelList = graphPelService.getGraphPelListByCondition(condition);
        for(GraphPel graphPel:graphPelList){
            condition = new HashMap<>();
            condition.put("graph_pel_id",graphPel.getGraph_pel_id());
            graphPel.setGraphPelElements(graphPelElementDao.getGraphPelElementListByCondition(condition));
        }
        return outItemsStr(graphPelList);
    }
}
