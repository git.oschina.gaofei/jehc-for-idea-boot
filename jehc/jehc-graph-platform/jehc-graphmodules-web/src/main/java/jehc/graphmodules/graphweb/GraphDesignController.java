package jehc.graphmodules.graphweb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.graphmodules.graphmodel.GraphSite;
import jehc.graphmodules.graphservice.GraphSiteService;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseResponse;
import jehc.xtmodules.xtcore.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Api(value = "拓扑图设计平台", description = "拓扑图设计平台")
@Controller
@RequestMapping("/graphDesignController")
public class GraphDesignController  extends BaseAction {

    @Autowired
    private GraphSiteService graphSiteService;

    /**
     * 设计器页面
     */
    @AuthUneedLogin
    @ApiOperation(value="设计器页面", notes="设计器页面")
    @RequestMapping(value="/toGraphDesign",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView toGraphDesign(String graph_site_id, Model model){
        if(!StringUtil.isEmpty(graph_site_id)){
            GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
            model.addAttribute("graphSite",graphSite);
        }else{
            model.addAttribute("graphSite",new GraphSite());
        }
        return new ModelAndView("pc/graph-view/graph-design/graph-design-list");
    }


    /**
     * 修改
     * @param graphSite
     * @param request
     */
    @ResponseBody
    @RequestMapping(value="/saveGraphSite",method={RequestMethod.POST,RequestMethod.GET})
    public String saveGraphSite(GraphSite graphSite,HttpServletRequest request){
        int i = 0;
        if(null != graphSite){
            //编辑
            if(!StringUtil.isEmpty(graphSite.getGraph_site_id())){
                graphSite.setMTime(new Date());
                graphSite.setUpdate_userinfo_id(getXtUid());
                i=graphSiteService.updateGraphSite(graphSite);
            }else{
                //新增
                graphSite.setGraph_site_id(UUID.toUUID());
                graphSite.setCTime(new Date());
                graphSite.setXt_userinfo_id(getXtUid());
                i=graphSiteService.addGraphSite(graphSite);
            }
        }
        BaseResponse baseResponse = new BaseResponse();
        if(i>0){
            baseResponse.setSucess(true);
            baseResponse.setMsg("保存成功");
            baseResponse.setData(graphSite.getGraph_site_id());
            return outAudStr(baseResponse);
        }else{
            baseResponse.setSucess(false);
            baseResponse.setMsg("保存失败");
            return outAudStr(baseResponse);
        }
    }
}
