package jehc.graphmodules.graphweb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.graphmodules.graphmodel.GraphSite;
import jehc.graphmodules.graphservice.GraphSiteService;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.base.BaseAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Api(value = "拓扑图预览", description = "拓扑图预览")
@Controller
@RequestMapping("/graphPreviewController")
public class GraphPreviewController  extends BaseAction {
    @Autowired
    private GraphSiteService graphSiteService;

    /**
     * 预览页面
     */
    @ApiOperation(value="预览页面", notes="预览页面")
    @AuthUneedLogin
    @RequestMapping(value="/toGraphPreview",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView toGraphPreview(String graph_site_id, Model model){
        if(!StringUtil.isEmpty(graph_site_id)){
            GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
            model.addAttribute("graphSite",graphSite);
        }else{
            model.addAttribute("graphSite",new GraphSite());
        }
        return new ModelAndView("pc/graph-view/graph-preview/graph-preview-list");
    }
}
