package jehc.graphmodules.graphweb;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.annotation.NeedLoginUnAuth;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.graphmodules.graphmodel.GraphSite;
import jehc.graphmodules.graphservice.GraphSiteService;

/**
* 站点 
* 2020-01-21 10:36:28  邓纯杰
*/
@Controller
@RequestMapping("/graphSiteController")
@Api(value = "站点", description = "站点")
public class GraphSiteController extends BaseAction{
	@Autowired
	private GraphSiteService graphSiteService;
	/**
	* 列表页面
	* @param graphSite
	* @param request 
	* @return
	*/
	@AuthUneedLogin
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadGraphSite",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadGraphSite(GraphSite graphSite,HttpServletRequest request){
		return new ModelAndView("pc/graph-view/graph-site/graph-site-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ResponseBody
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@RequestMapping(value="/getGraphSiteListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getGraphSiteListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<GraphSite> graphSiteList = graphSiteService.getGraphSiteListByCondition(condition);
		PageInfo<GraphSite> page = new PageInfo<GraphSite>(graphSiteList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param graph_site_id 
	* @param request 
	*/
	@ResponseBody
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@RequestMapping(value="/getGraphSiteById",method={RequestMethod.POST,RequestMethod.GET})
	public String getGraphSiteById(String graph_site_id,HttpServletRequest request){
		GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
		return outDataStr(graphSite);
	}
	/**
	* 添加
	* @param graphSite 
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addGraphSite",method={RequestMethod.POST,RequestMethod.GET})
	public String addGraphSite(GraphSite graphSite,HttpServletRequest request){
		int i = 0;
		if(null != graphSite){
			graphSite.setGraph_site_id(UUID.toUUID());
			graphSite.setCTime(new Date());
			graphSite.setXt_userinfo_id(getXtUid());
			i=graphSiteService.addGraphSite(graphSite);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param graphSite 
	* @param request 
	*/
	@ResponseBody
	@ApiOperation(value="修改", notes="修改")
	@RequestMapping(value="/updateGraphSite",method={RequestMethod.POST,RequestMethod.GET})
	public String updateGraphSite(GraphSite graphSite,HttpServletRequest request){
		int i = 0;
		if(null != graphSite){
			graphSite.setMTime(new Date());
			graphSite.setUpdate_userinfo_id(getXtUid());
			i=graphSiteService.updateGraphSite(graphSite);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param graph_site_id 
	* @param request 
	*/
	@ResponseBody
	@ApiOperation(value="删除", notes="删除")
	@RequestMapping(value="/delGraphSite",method={RequestMethod.POST,RequestMethod.GET})
	public String delGraphSite(String graph_site_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(graph_site_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("graph_site_id",graph_site_id.split(","));
			i=graphSiteService.delGraphSite(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param graph_site_id 
	* @param request 
	*/
	@ResponseBody
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@RequestMapping(value="/copyGraphSite",method={RequestMethod.POST,RequestMethod.GET})
	public String copyGraphSite(String graph_site_id,HttpServletRequest request){
		int i = 0;
		GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
		if(null != graphSite){
			graphSite.setGraph_site_id(UUID.toUUID());
			i=graphSiteService.addGraphSite(graphSite);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 新增页面
	* @param request 
	*/
	@NeedLoginUnAuth
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toGraphSiteAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toGraphSiteAdd(GraphSite graphSite,HttpServletRequest request){
		return new ModelAndView("pc/graph-view/graph-site/graph-site-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@NeedLoginUnAuth
	@RequestMapping(value="/toGraphSiteUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toGraphSiteUpdate(String graph_site_id,HttpServletRequest request, Model model){
		GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
		model.addAttribute("graphSite", graphSite);
		return new ModelAndView("pc/graph-view/graph-site/graph-site-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@NeedLoginUnAuth
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toGraphSiteDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toGraphSiteDetail(String graph_site_id,HttpServletRequest request, Model model){
		GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
		model.addAttribute("graphSite", graphSite);
		return new ModelAndView("pc/graph-view/graph-site/graph-site-detail");
	}
}
