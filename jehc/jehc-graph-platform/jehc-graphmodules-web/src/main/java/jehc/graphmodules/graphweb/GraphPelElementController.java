package jehc.graphmodules.graphweb;

import io.swagger.annotations.Api;
import jehc.xtmodules.xtcore.base.BaseAction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * desc : 图元节点
 * date : 2020-01-16 09:55:55
 * auth : 邓纯杰
 */
@Api(value = "图元节点", description = "图元节点")
@Controller
@RequestMapping("/graphPelElementController")
public class GraphPelElementController extends BaseAction {

}
