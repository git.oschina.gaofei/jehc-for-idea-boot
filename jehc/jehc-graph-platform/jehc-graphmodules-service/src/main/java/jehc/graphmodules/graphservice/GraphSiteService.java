package jehc.graphmodules.graphservice;
import java.util.List;
import java.util.Map;
import jehc.graphmodules.graphmodel.GraphSite;

/**
* 站点 
* 2020-01-21 10:36:28  邓纯杰
*/
public interface GraphSiteService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<GraphSite> getGraphSiteListByCondition(Map<String, Object> condition);
	/**
	* 查询单条记录
	* @param graph_site_id 
	* @return
	*/
	public GraphSite getGraphSiteById(String graph_site_id);
	/**
	* 添加
	* @param graphSite
	* @return
	*/
	public int addGraphSite(GraphSite graphSite);
	/**
	* 修改
	* @param graphSite
	* @return
	*/
	public int updateGraphSite(GraphSite graphSite);
	/**
	* 修改（根据动态条件）
	* @param graphSite
	* @return
	*/
	public int updateGraphSiteBySelective(GraphSite graphSite);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delGraphSite(Map<String, Object> condition);
	/**
	* 批量添加
	* @param graphSiteList
	* @return
	*/
	public int addBatchGraphSite(List<GraphSite> graphSiteList);
	/**
	* 批量修改
	* @param graphSiteList
	* @return
	*/
	public int updateBatchGraphSite(List<GraphSite> graphSiteList);
	/**
	* 批量修改（根据动态条件）
	* @param graphSiteList
	* @return
	*/
	public int updateBatchGraphSiteBySelective(List<GraphSite> graphSiteList);
}
