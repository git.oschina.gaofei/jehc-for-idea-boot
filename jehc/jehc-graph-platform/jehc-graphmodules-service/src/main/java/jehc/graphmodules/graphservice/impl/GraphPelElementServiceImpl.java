package jehc.graphmodules.graphservice.impl;

import jehc.graphmodules.graphdao.GraphPelElementDao;
import jehc.graphmodules.graphservice.GraphPelElementService;
import jehc.xtmodules.xtcore.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * desc : 图元节点
 * date : 2020-01-16 09:55:55
 * auth : 邓纯杰
 */
@Service("graphPelElementService")
public class GraphPelElementServiceImpl extends BaseService implements GraphPelElementService {
    @Autowired
    GraphPelElementDao graphPelElementDao;
}
