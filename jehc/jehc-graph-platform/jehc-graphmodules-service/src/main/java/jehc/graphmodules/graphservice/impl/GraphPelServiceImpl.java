package jehc.graphmodules.graphservice.impl;

import jehc.graphmodules.graphdao.GraphPelDao;
import jehc.graphmodules.graphdao.GraphPelElementDao;
import jehc.graphmodules.graphmodel.GraphPel;
import jehc.graphmodules.graphmodel.GraphPelElement;
import jehc.graphmodules.graphservice.GraphPelService;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtcore.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * desc : 图元管理
 * date : 2020-01-16 09:50:00
 * auth : 邓纯杰
 */
@Service("graphPelService")
public class GraphPelServiceImpl extends BaseService implements GraphPelService {
    @Autowired
    GraphPelDao graphPelDao;
    @Autowired
    GraphPelElementDao graphPelElementDao;
    /**
     * 分页查询
     * @param condition
     * @return
     */
    public List<GraphPel> getGraphPelListByCondition(Map<String,Object> condition){
        return graphPelDao.getGraphPelListByCondition(condition);
    }

    /**
     * 查询单条记录
     * @param graph_pel_id
     * @return
     */
    public GraphPel getGraphPelById(String graph_pel_id){
        Map<String,Object> condition = new HashMap<>();
        condition.put("graph_pel_id",graph_pel_id);
        GraphPel graphPel = graphPelDao.getGraphPelById(graph_pel_id);
        graphPel.setGraphPelElements(graphPelElementDao.getGraphPelElementListByCondition(condition));
        return graphPel;
    }

    /**
     * 添加
     * @param graphPel
     * @return
     */
    public int addGraphPel(GraphPel graphPel){
        int i = 0;
        try{
            i = graphPelDao.addGraphPel(graphPel);
            if(null != graphPel.getGraphPelElements() && !graphPel.getGraphPelElements().isEmpty()){
                for(GraphPelElement graphPelElement:graphPel.getGraphPelElements()){
                    graphPelElement.setGraph_pel_id(graphPel.getGraph_pel_id());
                    graphPelElement.setGraph_pel_element_id(UUID.toUUID());
                    graphPelElementDao.addGraphPelElement(graphPelElement);
                }
            }
        }catch (Exception e){
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }

        return i;
    }

    /**
     * 修改
     * @param graphPel
     * @return
     */
    public int updateGraphPel(GraphPel graphPel){
        int i = 0;
        try{
            i = graphPelDao.updateGraphPel(graphPel);
            if(!StringUtil.isEmpty(graphPel.getGraph_pel_id())){
                Map<String,Object> condition = new HashMap<>();
                condition.put("graph_pel_id",graphPel.getGraph_pel_id().split(","));
                graphPelElementDao.delGraphPelElements(condition);
            }
            if(null != graphPel.getGraphPelElements() && !graphPel.getGraphPelElements().isEmpty()){
                for(GraphPelElement graphPelElement:graphPel.getGraphPelElements()){
                    graphPelElement.setGraph_pel_id(graphPel.getGraph_pel_id());
                    graphPelElement.setGraph_pel_element_id(UUID.toUUID());
                    graphPelElementDao.addGraphPelElement(graphPelElement);
                }
            }
        }catch (Exception e){
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPel(Map<String,Object> condition){
        int i = 0;
        try{
            i = graphPelDao.delGraphPel(condition);
            graphPelElementDao.delGraphPelElements(condition);
        }catch (Exception e){
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
}
