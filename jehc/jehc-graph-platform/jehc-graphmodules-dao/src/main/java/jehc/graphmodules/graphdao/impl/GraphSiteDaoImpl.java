package jehc.graphmodules.graphdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.graphmodules.graphdao.GraphSiteDao;
import jehc.graphmodules.graphmodel.GraphSite;

/**
* 站点 
* 2020-01-21 10:36:28  邓纯杰
*/
@Repository("graphSiteDao")
public class GraphSiteDaoImpl  extends BaseDaoImpl implements GraphSiteDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	@SuppressWarnings("unchecked")
	public List<GraphSite> getGraphSiteListByCondition(Map<String,Object> condition){
		return (List<GraphSite>)this.getList("getGraphSiteListByCondition",condition);
	}
	/**
	* 查询单条记录
	* @param graph_site_id 
	* @return
	*/
	public GraphSite getGraphSiteById(String graph_site_id){
		return (GraphSite)this.get("getGraphSiteById", graph_site_id);
	}
	/**
	* 添加
	* @param graphSite 
	* @return
	*/
	public int addGraphSite(GraphSite graphSite){
		return this.add("addGraphSite", graphSite);
	}
	/**
	* 修改
	* @param graphSite 
	* @return
	*/
	public int updateGraphSite(GraphSite graphSite){
		return this.update("updateGraphSite", graphSite);
	}
	/**
	* 修改（根据动态条件）
	* @param graphSite 
	* @return
	*/
	public int updateGraphSiteBySelective(GraphSite graphSite){
		return this.update("updateGraphSiteBySelective", graphSite);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delGraphSite(Map<String,Object> condition){
		return this.del("delGraphSite", condition);
	}
	/**
	* 批量添加
	* @param graphSiteList 
	* @return
	*/
	public int addBatchGraphSite(List<GraphSite> graphSiteList){
		return this.add("addBatchGraphSite", graphSiteList);
	}
	/**
	* 批量修改
	* @param graphSiteList 
	* @return
	*/
	public int updateBatchGraphSite(List<GraphSite> graphSiteList){
		return this.update("updateBatchGraphSite", graphSiteList);
	}
	/**
	* 批量修改（根据动态条件）
	* @param graphSiteList 
	* @return
	*/
	public int updateBatchGraphSiteBySelective(List<GraphSite> graphSiteList){
		return this.update("updateBatchGraphSiteBySelective", graphSiteList);
	}
}
