package jehc.graphmodules.graphdao;

import jehc.graphmodules.graphmodel.GraphPelElement;

import java.util.List;
import java.util.Map;

/**
 * desc : 图元节点
 * date : 2020-01-16 09:55:55
 * auth : 邓纯杰
 */
public interface GraphPelElementDao {

    /**
     * 分页查询
     * @param condition
     * @return
     */
    List<GraphPelElement> getGraphPelElementListByCondition(Map<String,Object> condition);

    /**
     * 查询单条记录
     * @param graph_pel_element_id
     * @return
     */
    GraphPelElement getGraphPelElementById(String graph_pel_element_id);

    /**
     * 添加
     * @param graphPelElement
     * @return
     */
    int addGraphPelElement(GraphPelElement graphPelElement);

    /**
     * 修改
     * @param graphPelElement
     * @return
     */
    int updateGraphPelElement(GraphPelElement graphPelElement);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPelElement(Map<String,Object> condition);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPelElements(Map<String,Object> condition);
}
