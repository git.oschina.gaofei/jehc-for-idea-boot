package jehc.graphmodules.graphdao;

import jehc.graphmodules.graphmodel.GraphPel;

import java.util.List;
import java.util.Map;

/**
 * desc : 图元管理
 * date : 2020-01-16 09:50:00
 * auth : 邓纯杰
 */
public interface GraphPelDao {
    /**
     * 分页查询
     * @param condition
     * @return
     */
    List<GraphPel> getGraphPelListByCondition(Map<String,Object> condition);

    /**
     * 查询单条记录
     * @param graph_pel_id
     * @return
     */
    GraphPel getGraphPelById(String graph_pel_id);

    /**
     * 添加
     * @param graphPel
     * @return
     */
    int addGraphPel(GraphPel graphPel);

    /**
     * 修改
     * @param graphPel
     * @return
     */
    int updateGraphPel(GraphPel graphPel);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPel(Map<String,Object> condition);
}
