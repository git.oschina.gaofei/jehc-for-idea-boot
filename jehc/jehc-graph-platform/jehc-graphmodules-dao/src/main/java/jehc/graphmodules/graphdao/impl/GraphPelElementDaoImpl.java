package jehc.graphmodules.graphdao.impl;

import jehc.graphmodules.graphdao.GraphPelElementDao;
import jehc.graphmodules.graphmodel.GraphPelElement;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * desc : 图元节点
 * date : 2020-01-16 09:55:55
 * auth : 邓纯杰
 */
@Repository("graphPelElementDao")
public class GraphPelElementDaoImpl extends BaseDaoImpl implements GraphPelElementDao {
    /**
     * 分页查询
     * @param condition
     * @return
     */
    public List<GraphPelElement> getGraphPelElementListByCondition(Map<String,Object> condition){
        return (List<GraphPelElement>)this.getList("getGraphPelElementListByCondition",condition);
    }

    /**
     * 查询单条记录
     * @param graph_pel_element_id
     * @return
     */
    public GraphPelElement getGraphPelElementById(String graph_pel_element_id){
        return (GraphPelElement)this.get("getGraphPelElementById",graph_pel_element_id);
    }

    /**
     * 添加
     * @param graphPelElement
     * @return
     */
    public int addGraphPelElement(GraphPelElement graphPelElement){
        return this.add("addGraphPelElement",graphPelElement);
    }

    /**
     * 修改
     * @param graphPelElement
     * @return
     */
    public int updateGraphPelElement(GraphPelElement graphPelElement){
        return this.update("updateGraphPelElement",graphPelElement);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPelElement(Map<String,Object> condition){
        return this.del("delGraphPelElement",condition);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPelElements(Map<String,Object> condition){
        return this.del("delGraphPelElements",condition);
    }
}
