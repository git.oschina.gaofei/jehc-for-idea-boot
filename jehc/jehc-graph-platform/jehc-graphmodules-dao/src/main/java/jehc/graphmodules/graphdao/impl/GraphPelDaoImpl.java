package jehc.graphmodules.graphdao.impl;

import jehc.graphmodules.graphdao.GraphPelDao;
import jehc.graphmodules.graphmodel.GraphPel;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * desc : 图元管理
 * date : 2020-01-16 09:50:00
 * auth : 邓纯杰
 */
@Repository("graphPelDao")
public class GraphPelDaoImpl extends BaseDaoImpl implements GraphPelDao {
    /**
     * 分页查询
     * @param condition
     * @return
     */
    public List<GraphPel> getGraphPelListByCondition(Map<String,Object> condition){
        return (List<GraphPel>)this.getList("getGraphPelListByCondition",condition);
    }

    /**
     * 查询单条记录
     * @param graph_pel_id
     * @return
     */
    public GraphPel getGraphPelById(String graph_pel_id){
        return (GraphPel)this.get("getGraphPelById",graph_pel_id);
    }

    /**
     * 添加
     * @param graphPel
     * @return
     */
    public int addGraphPel(GraphPel graphPel){
        return this.add("addGraphPel",graphPel);
    }

    /**
     * 修改
     * @param graphPel
     * @return
     */
    public int updateGraphPel(GraphPel graphPel){
        return this.update("updateGraphPel",graphPel);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPel(Map<String,Object> condition){
        return this.update("delGraphPel",condition);
    }
}
