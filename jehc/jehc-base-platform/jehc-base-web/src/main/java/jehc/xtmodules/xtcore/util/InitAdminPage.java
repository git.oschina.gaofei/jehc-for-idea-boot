package jehc.xtmodules.xtcore.util;

import jehc.xtmodules.xtmodel.XtDyRemind;

public class InitAdminPage {
    private String menuList;
    private String adminMenuList;

    private String basePath;

    private XtDyRemind xtDyRemind;

    private String mini;
    private String classshrink;

    private String colorTheme;

    public String getMenuList() {
        return menuList;
    }

    public void setMenuList(String menuList) {
        this.menuList = menuList;
    }

    public String getAdminMenuList() {
        return adminMenuList;
    }

    public void setAdminMenuList(String adminMenuList) {
        this.adminMenuList = adminMenuList;
    }

    public XtDyRemind getXtDyRemind() {
        return xtDyRemind;
    }

    public void setXtDyRemind(XtDyRemind xtDyRemind) {
        this.xtDyRemind = xtDyRemind;
    }

    public String getMini() {
        return mini;
    }

    public void setMini(String mini) {
        this.mini = mini;
    }

    public String getClassshrink() {
        return classshrink;
    }

    public void setClassshrink(String classshrink) {
        this.classshrink = classshrink;
    }

    public String getColorTheme() {
        return colorTheme;
    }

    public void setColorTheme(String colorTheme) {
        this.colorTheme = colorTheme;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
