package jehc.xtmodules.xtcore.util.quartz;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.quartz.task.SolrDataimportTask;
import jehc.xtmodules.xtcore.util.quartz.task.SolrFullimportTask;
import jehc.xtmodules.xtcore.util.quartz.task.XtDataAuthorityTask;
import jehc.xtmodules.xtcore.util.quartz.task.XtDbinfoBackTask;
import jehc.xtmodules.xtcore.util.quartz.task.XtEhcacheTask;
import jehc.xtmodules.xtcore.util.quartz.task.XtMonitorTask;
import jehc.xtmodules.xtdao.XtQuartzLogDao;
import jehc.xtmodules.xtmodel.XtQuartzLog;
import org.springframework.beans.factory.annotation.Autowired;

public class QuartzJobFactory extends BaseService implements Job {
	@Autowired
	XtQuartzLogDao xtQuartzLogDao;
	public void execute(JobExecutionContext context) throws JobExecutionException {
		XtQuartzLog xtQuartzLog = new XtQuartzLog();
		ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("scheduleJob");
		String name = scheduleJob.getJobName();
		String id = scheduleJob.getJobId();
		String des = scheduleJob.getDesc();
		xtQuartzLog.setXt_quartz_log_ctime(getSimpleDateFormat());
		xtQuartzLog.setXt_quartz_log_id(UUID.toUUID());
		xtQuartzLog.setXt_quartz_log_name(name);
		xtQuartzLog.setXt_quartz_log_content(des);
		xtQuartzLog.setXt_quartz_log_key(id);
		xtQuartzLog.setXt_quartz_log_flag(scheduleJob.getJobStatus());
		String targetMethod = scheduleJob.getTargetMethod();
		String targetClass = scheduleJob.getTargetClass();
		if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Xt_DbinfoBack_Task")){
			XtDbinfoBackTask xtDbinfoBackTask = new XtDbinfoBackTask();
			xtDbinfoBackTask.service();
		}else if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Xt_Ehcache_Task")){
			XtEhcacheTask xtEhcacheTask = new XtEhcacheTask();
			xtEhcacheTask.service();
		}else if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Xt_Monitor_Task")){
			XtMonitorTask xtMonitorTask = new XtMonitorTask();
			xtMonitorTask.service();
		}else if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Solr_dataimportTask")){
			SolrDataimportTask solrDataimportTask = new SolrDataimportTask();
			solrDataimportTask.service();
		}else if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Solr_fullimportTask")){
			SolrFullimportTask solrFullimportTask = new SolrFullimportTask();
			solrFullimportTask.service();
		}else if(null != targetMethod && null != targetClass && targetMethod.equals("service") && targetClass.equals("xtCore.util.quartz.task.Xt_Data_Authority_Task")){
			XtDataAuthorityTask xtDataAuthorityTask = new XtDataAuthorityTask();
			xtDataAuthorityTask.service();
		}
		xtQuartzLog.setXt_quartz_log_etime(getSimpleDateFormat());
		xtQuartzLogDao.addXtQuartzLog(xtQuartzLog);
	}
}