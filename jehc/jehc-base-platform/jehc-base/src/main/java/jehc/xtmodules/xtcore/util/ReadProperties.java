package jehc.xtmodules.xtcore.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContextEvent;

import jehc.xtmodules.xtcore.allutils.AllUtils;
import jehc.xtmodules.xtcore.util.constant.PathConstant;
import org.springframework.util.ClassUtils;

/**
 * 读取properties文件
 * @author邓纯杰
 *
 */
public class ReadProperties {
	/**
	 * 读取平台配置文件
	 * @param event
	 * @return
	 */
	public static Map<String, Object> readProperties(ServletContextEvent event){
		Map<String, Object> map = new HashMap<String, Object>();
		map = PropertisUtil.resultProperties(PathConstant.ZN_PROPERTIES_PATH);
		return map;
	}
	/**
	 * 读取提示信息文件
	 * @param event
	 * @return
	 */
	public static Map<String, Object> readMessageProperties(ServletContextEvent event){
		Map<String, Object> map = new HashMap<String, Object>();
		map = PropertisUtil.resultProperties(PathConstant.MESSAGE_PROPERTIES_PATH);
		return map;
	}
	
	/**
	 * 读取平台Config配置文件
	 * @param event
	 * @return
	 */
	public static Map<String, Object> readConfigProperties(ServletContextEvent event){
		Map<String, Object> map = new HashMap<String, Object>();
		map = PropertisUtil.resultProperties(PathConstant.CONFIG_PROPERTIES_PATH);
		return map;
	}
	/**
	 * 读取平台Config配置文件
	 * @param key
	 * @return
	 */
	public static String readConfigProperties(String key){
		Properties prop = new Properties();
        FileInputStream fis;
		try {
			fis = new FileInputStream(PathConstant.CONFIG_PROPERTIES_PATH);
			try {
				prop.load(fis);
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return prop.getProperty(key);
	}
}
