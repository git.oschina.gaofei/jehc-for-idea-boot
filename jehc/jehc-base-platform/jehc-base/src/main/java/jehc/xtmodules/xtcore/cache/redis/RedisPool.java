package jehc.xtmodules.xtcore.cache.redis;
import org.springframework.stereotype.Repository;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * RedisPool
 * @author 邓纯杰
 *
 */
@Repository
public class RedisPool {
	private JedisPoolConfig config; 
    private String serverIp;
    private int port;
    private JedisPool pool;

    public void init() {
        pool = new JedisPool(config, serverIp, port, 4000);
    }

    public Jedis getInstance() {
        return pool.getResource();
    }

    public void returnResource(Jedis jedis) {
        pool.returnResource(jedis);
    }

    public void returnBrokenResource(Jedis jedis){
        pool.returnBrokenResource(jedis);
    }

    public JedisPoolConfig getConfig() {
        return config;
    }

    public void setConfig(JedisPoolConfig config) {
        this.config = config;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    /**
     * 关闭
     * @param jedis
     * @return
     */
    public Boolean close(Jedis jedis){
        try {
            if (jedis != null) {
                jedis.close();
            }
            return true;
        } catch (Exception e) {
           return false;
        }
    }
}
