package jehc.xtmodules.xtcore.util;

import org.springframework.stereotype.Repository;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InitIpConfig {
    private String ipApi;

    public String getIpApi() {
        return ipApi;
    }

    public void setIpApi(String ipApi) {
        this.ipApi = ipApi;
    }

    public static Map<String,Object> dataIpApi = new ConcurrentHashMap<String, Object>();


    public void init() {
        dataIpApi.put("ipApi",ipApi);
    }

    public Map<String,Object> getDataIpApi(){
        return dataIpApi;
    }
}
