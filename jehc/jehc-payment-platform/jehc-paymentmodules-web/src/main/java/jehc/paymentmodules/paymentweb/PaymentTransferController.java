package jehc.paymentmodules.paymentweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.paymentmodules.paymentmodel.PaymentTransfer;
import jehc.paymentmodules.paymentservice.PaymentTransferService;

/**
* 转账 
* 2018-07-24 15:31:30  邓纯杰
*/
@Api(value = "转账", description = "转账")
@Controller
@RequestMapping("/paymentTransferController")
public class PaymentTransferController extends BaseAction{
	@Autowired
	private PaymentTransferService paymentTransferService;
	/**
	* 列表页面
	* @param paymentTransfer
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadPaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadPaymentTransfer(PaymentTransfer paymentTransfer,HttpServletRequest request){
		return new ModelAndView("pc/payment-view/payment-transfer/payment-transfer-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getPaymentTransferListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getPaymentTransferListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<PaymentTransfer> paymentTransferList = paymentTransferService.getPaymentTransferListByCondition(condition);
		PageInfo<PaymentTransfer> page = new PageInfo<PaymentTransfer>(paymentTransferList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getPaymentTransferById",method={RequestMethod.POST,RequestMethod.GET})
	public String getPaymentTransferById(String id,HttpServletRequest request){
		PaymentTransfer paymentTransfer = paymentTransferService.getPaymentTransferById(id);
		return outDataStr(paymentTransfer);
	}
	/**
	* 添加
	* @param paymentTransfer
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addPaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public String addPaymentTransfer(PaymentTransfer paymentTransfer,HttpServletRequest request){
		int i = 0;
		if(null != paymentTransfer){
			paymentTransfer.setId(UUID.toUUID());
			i=paymentTransferService.addPaymentTransfer(paymentTransfer);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param paymentTransfer
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updatePaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public String updatePaymentTransfer(PaymentTransfer paymentTransfer,HttpServletRequest request){
		int i = 0;
		if(null != paymentTransfer){
			i=paymentTransferService.updatePaymentTransfer(paymentTransfer);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delPaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public String delPaymentTransfer(String id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=paymentTransferService.delPaymentTransfer(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyPaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public String copyPaymentTransfer(String id,HttpServletRequest request){
		int i = 0;
		PaymentTransfer paymentTransfer = paymentTransferService.getPaymentTransferById(id);
		if(null != paymentTransfer && !"".equals(paymentTransfer)){
			paymentTransfer.setId(UUID.toUUID());
			i=paymentTransferService.addPaymentTransfer(paymentTransfer);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportPaymentTransfer",method={RequestMethod.POST,RequestMethod.GET})
	public void exportPaymentTransfer(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toPaymentTransferAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toPaymentTransferAdd(PaymentTransfer paymentTransfer,HttpServletRequest request){
		return new ModelAndView("pc/payment-view/payment-transfer/payment-transfer-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toPaymentTransferUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toPaymentTransferUpdate(String id,HttpServletRequest request, Model model){
		PaymentTransfer paymentTransfer = paymentTransferService.getPaymentTransferById(id);
		model.addAttribute("paymentTransfer", paymentTransfer);
		return new ModelAndView("pc/payment-view/payment-transfer/payment-transfer-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toPaymentTransferDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toPaymentTransferDetail(String id,HttpServletRequest request, Model model){
		PaymentTransfer paymentTransfer = paymentTransferService.getPaymentTransferById(id);
		model.addAttribute("paymentTransfer", paymentTransfer);
		return new ModelAndView("pc/payment-view/payment-transfer/payment-transfer-detail");
	}
}
