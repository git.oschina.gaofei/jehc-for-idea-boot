package jehc.config;

import jehc.xtmodules.xtcore.util.constant.PathConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations={PathConstant.BASE_SPRING_PATH})
public class ConfigBean {
    public static void main(String[] args)
    {
        SpringApplication.run(ConfigBean.class, args);
    }

}