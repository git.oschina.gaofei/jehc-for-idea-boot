package jehc.crmmodules.crmweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.crmmodules.crmmodel.CrmCustomer;
import jehc.crmmodules.crmmodel.CrmInvoice;
import jehc.crmmodules.crmservice.CrmCustomerService;
import jehc.crmmodules.crmservice.CrmInvoiceService;
import jehc.lcmodules.lcmodel.LcApply;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.annotation.NeedLoginUnAuth;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtDataDictionary;

/**
* 客户基础资料 
* 2018-06-27 13:45:48  邓纯杰
*/
@Api(value = "客户基础资料", description = "客户基础资料")
@Controller
@RequestMapping("/crmCustomerController")
public class CrmCustomerController extends BaseAction{
	@Autowired
	private CrmCustomerService crmCustomerService;
	@Autowired
	CrmInvoiceService crmInvoiceService;
	/**
	* 列表页面
	* @param crmCustomer
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCrmCustomer",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCrmCustomer(CrmCustomer crmCustomer,HttpServletRequest request){
		return new ModelAndView("pc/crm-view/crm-customer/crm-customer-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCrmCustomerListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCrmCustomerListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		dataAuthForXtUID(request,"createUser", condition);
		List<CrmCustomer> crmCustomerList = crmCustomerService.getCrmCustomerListByCondition(condition);
		PageInfo<CrmCustomer> page = new PageInfo<CrmCustomer>(crmCustomerList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param customerId 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCrmCustomerById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCrmCustomerById(String customerId,HttpServletRequest request){
		CrmCustomer crmCustomer = crmCustomerService.getCrmCustomerById(customerId);
		return outDataStr(crmCustomer);
	}

	/**
	 * 添加
	 * @param crmCustomer
	 * @param crmInvoice
	 * @param request
	 * @return
	 */
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCrmCustomer",method={RequestMethod.POST,RequestMethod.GET})
	public String addCrmCustomer(CrmCustomer crmCustomer,CrmInvoice crmInvoice,HttpServletRequest request){
		int i = 0;
		if(null != crmCustomer){
			crmCustomer.setCustomerId(UUID.toUUID());
			crmCustomer.setCdate(getDate());
			crmCustomer.setCreateUser(getXtUid());
			i=crmCustomerService.addCrmCustomer(crmCustomer,crmInvoice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 修改
	 * @param crmCustomer
	 * @param crmInvoice
	 * @param request
	 * @return
	 */
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCrmCustomer",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCrmCustomer(CrmCustomer crmCustomer,CrmInvoice crmInvoice,HttpServletRequest request){
		int i = 0;
		if(null != crmCustomer){
			crmCustomer.setMdate(getDate());
			crmCustomer.setModifyUser(getXtUid());
			i=crmCustomerService.updateCrmCustomer(crmCustomer,crmInvoice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param customerId 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCrmCustomer",method={RequestMethod.POST,RequestMethod.GET})
	public String delCrmCustomer(String customerId,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(customerId)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("customerId",customerId.split(","));
			i=crmCustomerService.delCrmCustomer(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCrmCustomer",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCrmCustomer(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCrmCustomerAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCrmCustomerAdd(CrmCustomer crmCustomer,HttpServletRequest request){
		return new ModelAndView("pc/crm-view/crm-customer/crm-customer-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCrmCustomerUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCrmCustomerUpdate(String customerId,HttpServletRequest request, Model model){
		CrmCustomer crmCustomer = crmCustomerService.getCrmCustomerById(customerId);
		model.addAttribute("crmCustomer", crmCustomer);
		model.addAttribute("crmInvoice", crmInvoiceService.getCrmInvoiceSingleByCustomerId(customerId));
		model.addAttribute("crmCustomerJSON", outItemsStr(crmCustomer));
		return new ModelAndView("pc/crm-view/crm-customer/crm-customer-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCrmCustomerDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCrmCustomerDetail(String customerId,HttpServletRequest request, Model model){
		CrmCustomer crmCustomer = crmCustomerService.getCrmCustomerById(customerId);
		model.addAttribute("crmCustomer", crmCustomer);
		model.addAttribute("crmInvoice", crmInvoiceService.getCrmInvoiceSingleByCustomerId(customerId));
		model.addAttribute("crmCustomerJSON", outItemsStr(crmCustomer));
		return new ModelAndView("pc/crm-view/crm-customer/crm-customer-detail");
	}
	
	/**
	 * 查询所属行业
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询所属行业", notes="查询所属行业")
	@ResponseBody
	@RequestMapping(value="/getIndustryIdList",method={RequestMethod.POST,RequestMethod.GET})
	@NeedLoginUnAuth
	public String getIndustryIdList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("industryId");
		return outComboDataStr(xtDataDictionaryList);
	}
	/**
	 * 查询公司规模
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询公司规模", notes="查询公司规模")
	@ResponseBody
	@RequestMapping(value="/getScaleId",method={RequestMethod.POST,RequestMethod.GET})
	@NeedLoginUnAuth
	public String getSCaleIdList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList= CommonUtils.getXtDataDictionaryCache("scaleId");
		return outComboDataStr(xtDataDictionaryList);
	}
	/**
	 * 查询年龄结构
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询年龄结构", notes="查询年龄结构")
	@ResponseBody
	@RequestMapping(value="/getAgeScope",method={RequestMethod.POST,RequestMethod.GET})
	@NeedLoginUnAuth
	public String getAgeScopeList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList= CommonUtils.getXtDataDictionaryCache("ageScope");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 分配客户
	 * @param customerId
	 * @param xt_userinfo_id
	 * @return
	 */
	@ApiOperation(value="分配客户", notes="分配客户")
	@ResponseBody
	@RequestMapping(value="/updateCToUser",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCToUser(String customerId,String xt_userinfo_id){
		int i = 0;
		if(!StringUtil.isEmpty(customerId) && !StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("customerId", customerId.split(","));
			map.put("createUser", xt_userinfo_id);
			map.put("mdate", getDate());
			map.put("modifyUser", getXtUid());
			i = crmCustomerService.updateCToUser(map);
			if(i>0){
				return outAudStr(true);
			}else{
				return outAudStr(false);
			}
		}else{
			return outAudStr(true,"未能获取到用户编号或客户编号");
		}
	}
	
	/**
	 * 客户等级申请
	 * @param lcApply
	 * @param customerId
	 * @return
	 */
	@ApiOperation(value="客户等级申请", notes="客户等级申请")
	@ResponseBody
	@RequestMapping(value="/addCrmLevelApply",method={RequestMethod.POST,RequestMethod.GET})
	public String addCrmLevelApply(LcApply lcApply,String customerId){
		int i = 0;
		if(null != lcApply){
			CrmCustomer crmCustomer = new CrmCustomer();
			crmCustomer.setCustomerId(customerId);
			i = crmCustomerService.addCrmLevelApply(crmCustomer, lcApply);
			if(i>0){
				return outAudStr(true);
			}else{
				return outAudStr(false);
			}
		}else{
			return outAudStr(true,"未能获取到用户编号或客户编号");
		}
	}
	
	/**
	* 审批详情页面
	* @param request 
	*/
	@ApiOperation(value="审批详情页面", notes="审批详情页面")
	@NeedLoginUnAuth
	@RequestMapping(value="/toCrmCustomerApply",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCrmCustomerApply(String lc_apply_model_biz_id,HttpServletRequest request, Model model){
		CrmCustomer crmCustomer = crmCustomerService.getCrmCustomerById(lc_apply_model_biz_id);
		model.addAttribute("crmCustomer", crmCustomer);
		model.addAttribute("crmInvoice", crmInvoiceService.getCrmInvoiceSingleByCustomerId(lc_apply_model_biz_id));
		model.addAttribute("crmCustomerJSON", outItemsStr(crmCustomer));
		return new ModelAndView("pc/crm-view/crm-customer/crm-customer-apply");
	}
}
