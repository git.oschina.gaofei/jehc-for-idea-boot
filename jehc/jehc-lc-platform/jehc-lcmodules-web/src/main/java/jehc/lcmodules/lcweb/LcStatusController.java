package jehc.lcmodules.lcweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.lcmodules.lcmodel.LcStatus;
import jehc.lcmodules.lcservice.LcStatusService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 流程状态 
* 2016-05-04 14:13:34  邓纯杰
*/
@Api(value = "流程状态", description = "流程状态")
@Controller
@RequestMapping("/lcStatusController")
public class LcStatusController extends BaseAction{
	@Autowired
	private LcStatusService lcStatusService;
	/**
	* 列表页面
	* @param lcStatus
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadLcStatus(LcStatus lcStatus,HttpServletRequest request){
		return new ModelAndView("pc/lc-view/lc-status/lc-status-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getLcStatusListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcStatusListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<LcStatus> lcStatusList = lcStatusService.getLcStatusListByCondition(condition);
		PageInfo<LcStatus> page = new PageInfo<LcStatus>(lcStatusList);
		return outPageBootStr(page,request);
	}
	/**
	* 获取对象
	* @param lc_status_id 
	* @param request 
	*/
	@ApiOperation(value="获取对象", notes="获取对象")
	@ResponseBody
	@RequestMapping(value="/getLcStatusById",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcStatusById(String lc_status_id,HttpServletRequest request){
		LcStatus lc_Status = lcStatusService.getLcStatusById(lc_status_id);
		return outDataStr(lc_Status);
	}
	/**
	* 添加
	* @param lcStatus
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public String addLcStatus(LcStatus lcStatus,HttpServletRequest request){
		int i = 0;
		if(null != lcStatus){
			lcStatus.setXt_userinfo_id(CommonUtils.getXtUid());
			lcStatus.setLc_status_ctime(CommonUtils.getSimpleDateFormat());
			lcStatus.setLc_status_id(UUID.toUUID());
			i=lcStatusService.addLcStatus(lcStatus);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcStatus
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public String updateLcStatus(LcStatus lcStatus,HttpServletRequest request){
		int i = 0;
		if(null != lcStatus){
			lcStatus.setXt_userinfo_id(getXtUid());
			lcStatus.setLc_status_mtime(CommonUtils.getSimpleDateFormat());
			i=lcStatusService.updateLcStatus(lcStatus);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_status_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public String delLcStatus(String lc_status_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(lc_status_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_status_id",lc_status_id.split(","));
			i=lcStatusService.delLcStatus(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param lc_status_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public String copyLcStatus(String lc_status_id,HttpServletRequest request){
		int i = 0;
		LcStatus lcStatus = lcStatusService.getLcStatusById(lc_status_id);
		if(null != lcStatus){
			lcStatus.setLc_status_id(UUID.toUUID());
			i=lcStatusService.addLcStatus(lcStatus);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportLcStatus",method={RequestMethod.POST,RequestMethod.GET})
	public void exportLcStatus(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 读取数据
	* @param lcStatus
	* @param request 
	*/
	@ApiOperation(value="读取数据", notes="读取数据")
	@ResponseBody
	@RequestMapping(value="/getLcStatusList",method={RequestMethod.POST,RequestMethod.GET})
	public List<LcStatus> getLcStatusList(LcStatus lcStatus,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<LcStatus> lcStatusList = lcStatusService.getLcStatusListByCondition(condition);
		return lcStatusList;
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toLcStatusAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toLcStatusAdd(LcStatus lcStatus,HttpServletRequest request){
		return new ModelAndView("pc/lc-view/lc-status/lc-status-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toLcStatusUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toLcStatusUpdate(String lc_status_id,HttpServletRequest request, Model model){
		LcStatus lcStatus = lcStatusService.getLcStatusById(lc_status_id);
		model.addAttribute("lcStatus", lcStatus);
		return new ModelAndView("pc/lc-view/lc-status/lc-status-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toLcStatusDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toLcStatusDetail(String lc_status_id,HttpServletRequest request, Model model){
		LcStatus lcStatus = lcStatusService.getLcStatusById(lc_status_id);
		model.addAttribute("lcStatus", lcStatus);
		return new ModelAndView("pc/lc-view/lc-status/lc-status-detail");
	}
}
