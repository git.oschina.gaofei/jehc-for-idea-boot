package jehc.lcmodules.lcweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.lcmodules.lcmodel.LcApply;
import jehc.lcmodules.lcservice.LcApplyService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 流程申请 
* 2017-01-08 14:55:11  邓纯杰
*/
@Api(value = "流程申请", description = "流程申请")
@Controller
@RequestMapping("/lcApplyController")
public class LcApplyController extends BaseAction{
	@Autowired
	private LcApplyService lcApplyService;
	/**
	* 列表页面
	* @param lcApply
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadLcApply(LcApply lcApply,HttpServletRequest request){
		return new ModelAndView("pc/lc-view/lc-apply/lc-apply-list");
	}
	/**
	* 查询并分页
	* @param lcApply
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getLcApplyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcApplyListByCondition(LcApply lcApply,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		condition.put("lc_apply_title",request.getParameter("lc_apply_title"));
		condition.put("xt_userinfo_id",getXtUid());
		List<LcApply> lcApplyList = lcApplyService.getLcApplyListByCondition(condition);
		PageInfo<LcApply> page = new PageInfo<LcApply>(lcApplyList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param lc_apply_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getLcApplyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcApplyById(String lc_apply_id,HttpServletRequest request){
		LcApply lcApply = lcApplyService.getLcApplyById(lc_apply_id);
		return outDataStr(lcApply);
	}
	/**
	* 添加
	* @param lcApply
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public String addLcApply(LcApply lcApply,HttpServletRequest request){
		int i = 0;
		if(null != lcApply){
			lcApply.setLc_apply_id(UUID.toUUID());
			i=lcApplyService.addLcApply(lcApply);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcApply
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public String updateLcApply(LcApply lcApply,HttpServletRequest request){
		int i = 0;
		if(null != lcApply){
			i=lcApplyService.updateLcApply(lcApply);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_apply_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public String delLcApply(String lc_apply_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(lc_apply_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_apply_id",lc_apply_id.split(","));
			i=lcApplyService.delLcApply(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param lc_apply_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public String copyLcApply(String lc_apply_id,HttpServletRequest request){
		int i = 0;
		LcApply lcApply = lcApplyService.getLcApplyById(lc_apply_id);
		if(null != lcApply){
			lcApply.setLc_apply_id(UUID.toUUID());
			i=lcApplyService.addLcApply(lcApply);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportLcApply",method={RequestMethod.POST,RequestMethod.GET})
	public void exportLcApply(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
