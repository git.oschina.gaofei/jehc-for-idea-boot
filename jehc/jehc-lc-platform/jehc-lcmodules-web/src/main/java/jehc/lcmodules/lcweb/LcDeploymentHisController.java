package jehc.lcmodules.lcweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.lcmodules.lcmodel.LcDeploymentHis;
import jehc.lcmodules.lcservice.LcDeploymentHisService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 流程部署历史记录 
* 2016-12-22 13:02:01  邓纯杰
*/
@Api(value = "流程部署历史记录", description = "流程部署历史记录")
@Controller
@RequestMapping("/lcDeploymentHisController")
public class LcDeploymentHisController extends BaseAction{
	@Autowired
	private LcDeploymentHisService lcDeploymentHisService;
	/**
	* 列表页面
	* @param lcDeploymentHis
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadLcDeploymentHis(LcDeploymentHis lcDeploymentHis,HttpServletRequest request,Model model){
		model.addAttribute("lc_Deployment_His",lcDeploymentHis);
		return new ModelAndView("pc/lc-view/lc-deployment-his/lc-deployment-his-list");
	}
	/**
	* 查询并分页
	* @param lcDeploymentHis
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getLcDeploymentHisListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcDeploymentHisListByCondition(LcDeploymentHis lcDeploymentHis,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		condition.put("lc_process_id", lcDeploymentHis.getLc_process_id());
		List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
		PageInfo<LcDeploymentHis> page = new PageInfo<LcDeploymentHis>(lcDeploymentHisList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getLcDeploymentHisById",method={RequestMethod.POST,RequestMethod.GET})
	public String getLcDeploymentHisById(String id,HttpServletRequest request){
		LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisById(id);
		return outDataStr(lcDeploymentHis);
	}
	/**
	* 添加
	* @param lcDeploymentHis
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public String addLcDeploymentHis(LcDeploymentHis lcDeploymentHis,HttpServletRequest request){
		int i = 0;
		if(null != lcDeploymentHis){
			lcDeploymentHis.setId(UUID.toUUID());
			i=lcDeploymentHisService.addLcDeploymentHis(lcDeploymentHis);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcDeploymentHis
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public String updateLcDeploymentHis(LcDeploymentHis lcDeploymentHis,HttpServletRequest request){
		int i = 0;
		if(null != lcDeploymentHis){
			i=lcDeploymentHisService.updateLcDeploymentHis(lcDeploymentHis);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public String delLcDeploymentHis(String id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=lcDeploymentHisService.delLcDeploymentHis(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public String copyLcDeploymentHis(String id,HttpServletRequest request){
		int i = 0;
		LcDeploymentHis lc_Deployment_His = lcDeploymentHisService.getLcDeploymentHisById(id);
		if(null != lc_Deployment_His && !"".equals(lc_Deployment_His)){
			lc_Deployment_His.setId(UUID.toUUID());
			i=lcDeploymentHisService.addLcDeploymentHis(lc_Deployment_His);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportLcDeploymentHis",method={RequestMethod.POST,RequestMethod.GET})
	public void exportLcDeploymentHis(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
