package jehc.lcmodules.lcdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.lcmodules.lcdao.LcApprovalDao;
import jehc.lcmodules.lcmodel.LcApproval;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 工作流批审表 
* 2017-01-08 17:06:33  邓纯杰
*/
@Repository("lcApprovalDao")
public class LcApprovalDaoImpl  extends BaseDaoImpl implements LcApprovalDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApproval> getLcApprovalListByCondition(Map<String,Object> condition){
		return (List<LcApproval>)this.getList("getLcApprovalListByCondition",condition);
	}
	/**
	* 查询对象
	* @param lc_approval_id 
	* @return
	*/
	public LcApproval getLcApprovalById(String lc_approval_id){
		return (LcApproval)this.get("getLcApprovalById", lc_approval_id);
	}
	/**
	* 添加
	* @param lcApproval
	* @return
	*/
	public int addLcApproval(LcApproval lcApproval){
		return this.add("addLcApproval", lcApproval);
	}
	/**
	* 修改
	* @param lcApproval
	* @return
	*/
	public int updateLcApproval(LcApproval lcApproval){
		return this.update("updateLcApproval", lcApproval);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApproval(Map<String,Object> condition){
		return this.del("delLcApproval", condition);
	}
	/**
	* 批量添加
	* @param lcApprovalList
	* @return
	*/
	public int addBatchLcApproval(List<LcApproval> lcApprovalList){
		return this.add("addBatchLcApproval", lcApprovalList);
	}
	/**
	* 批量修改
	* @param lcApprovalList
	* @return
	*/
	public int updateBatchLcApproval(List<LcApproval> lcApprovalList){
		return this.update("updateBatchLcApproval", lcApprovalList);
	}
}
