package jehc.lcmodules.lcdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.lcmodules.lcdao.LcApplyDao;
import jehc.lcmodules.lcmodel.LcApply;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 流程申请 
* 2017-01-08 14:55:10  邓纯杰
*/
@Repository("lcApplyDao")
public class LcApplyDaoImpl  extends BaseDaoImpl implements LcApplyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApply> getLcApplyListByCondition(Map<String,Object> condition){
		return (List<LcApply>)this.getList("getLcApplyListByCondition",condition);
	}
	/**
	* 查询对象
	* @param lc_apply_id 
	* @return
	*/
	public LcApply getLcApplyById(String lc_apply_id){
		return (LcApply)this.get("getLcApplyById", lc_apply_id);
	}
	/**
	* 添加
	* @param lcApply
	* @return
	*/
	public int addLcApply(LcApply lcApply){
		return this.add("addLcApply", lcApply);
	}
	/**
	* 修改
	* @param lcApply
	* @return
	*/
	public int updateLcApply(LcApply lcApply){
		return this.update("updateLcApply", lcApply);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApply(Map<String,Object> condition){
		return this.del("delLcApply", condition);
	}
	/**
	* 批量添加
	* @param lcApplyList
	* @return
	*/
	public int addBatchLcApply(List<LcApply> lcApplyList){
		return this.add("addBatchLcApply", lcApplyList);
	}
	/**
	* 批量修改
	* @param lcApplyList
	* @return
	*/
	public int updateBatchLcApply(List<LcApply> lcApplyList){
		return this.update("updateBatchLcApply", lcApplyList);
	}
	/**
	 * 根据实例编号查找集合
	 * @param condition
	 * @return
	 */
	public List<LcApply> getLcApplyList(Map<String,Object> condition){
		return (List<LcApply>)this.getList("getLcApplyList", condition);
	}
}
