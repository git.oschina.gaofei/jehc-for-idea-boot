package jehc.lcmodules.lcdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.lcmodules.lcdao.LcDeploymentHisDao;
import jehc.lcmodules.lcmodel.LcDeploymentHis;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 流程部署历史记录 
* 2016-12-22 13:02:01  邓纯杰
*/
@Repository("lcDeploymentHisDao")
public class LcDeploymentHisDaoImpl extends BaseDaoImpl implements LcDeploymentHisDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcDeploymentHis> getLcDeploymentHisListByCondition(Map<String,Object> condition){
		return (List<LcDeploymentHis>)this.getList("getLcDeploymentHisListByCondition",condition);
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LcDeploymentHis getLcDeploymentHisById(String id){
		return (LcDeploymentHis)this.get("getLcDeploymentHisById", id);
	}
	/**
	* 添加
	* @param lcDeploymentHis
	* @return
	*/
	public int addLcDeploymentHis(LcDeploymentHis lcDeploymentHis){
		return this.add("addLcDeploymentHis", lcDeploymentHis);
	}
	/**
	* 修改
	* @param lcDeploymentHis
	* @return
	*/
	public int updateLcDeploymentHis(LcDeploymentHis lcDeploymentHis){
		return this.update("updateLcDeploymentHis", lcDeploymentHis);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcDeploymentHis(Map<String,Object> condition){
		return this.del("delLcDeploymentHis", condition);
	}
	/**
	* 批量添加
	* @param lcDeploymentHisList
	* @return
	*/
	public int addBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList){
		return this.add("addBatchLcDeploymentHis", lcDeploymentHisList);
	}
	/**
	* 批量修改
	* @param lcDeploymentHisList
	* @return
	*/
	public int updateBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList){
		return this.update("updateBatchLcDeploymentHis", lcDeploymentHisList);
	}
	
	/**
	 * 查询唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisUnique(Map<String,Object> condition){
		return (LcDeploymentHis)this.get("getLcDeploymentHisUnique", condition);
	}
	
	/**
	 * 查询最新唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisNewUnique(Map<String,Object> condition){
		List<LcDeploymentHis> his = (List<LcDeploymentHis>)this.getList("getLcDeploymentHisNewUnique",condition);
		if(null != his && !his.isEmpty()){
			return his.get(0);
		}else{
			return new LcDeploymentHis();
		}
	}
}
