package jehc.lcmodules.lcservice;
import java.util.List;
import java.util.Map;

import jehc.lcmodules.lcmodel.LcStatus;

/**
* 流程状态 
* 2016-05-04 14:13:34  邓纯杰
*/
public interface LcStatusService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcStatus> getLcStatusListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param lc_status_id 
	* @return
	*/
	public LcStatus getLcStatusById(String lc_status_id);
	/**
	* 添加
	* @param lcStatus
	* @return
	*/
	public int addLcStatus(LcStatus lcStatus);
	/**
	* 修改
	* @param lcStatus
	* @return
	*/
	public int updateLcStatus(LcStatus lcStatus);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcStatus(Map<String,Object> condition);
}
