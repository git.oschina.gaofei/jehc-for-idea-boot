package jehc.lcmodules.lcservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.lcmodules.lcdao.LcApprovalDao;
import jehc.lcmodules.lcmodel.LcApproval;
import jehc.lcmodules.lcservice.LcApprovalService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* 工作流批审
* 2017-01-08 17:06:34  邓纯杰
*/
@Service("lcApprovalService")
public class LcApprovalServiceImpl extends BaseService implements LcApprovalService{
	@Autowired
	private LcApprovalDao lcApprovalDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApproval> getLcApprovalListByCondition(Map<String,Object> condition){
		try{
			return lcApprovalDao.getLcApprovalListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_approval_id 
	* @return
	*/
	public LcApproval getLcApprovalById(String lc_approval_id){
		try{
			return lcApprovalDao.getLcApprovalById(lc_approval_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcApproval
	* @return
	*/
	public int addLcApproval(LcApproval lcApproval){
		int i = 0;
		try {
			i = lcApprovalDao.addLcApproval(lcApproval);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcApproval
	* @return
	*/
	public int updateLcApproval(LcApproval lcApproval){
		int i = 0;
		try {
			i = lcApprovalDao.updateLcApproval(lcApproval);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApproval(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcApprovalDao.delLcApproval(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param lcApprovalList
	* @return
	*/
	public int addBatchLcApproval(List<LcApproval> lcApprovalList){
		int i = 0;
		try {
			i = lcApprovalDao.addBatchLcApproval(lcApprovalList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param lcApprovalList
	* @return
	*/
	public int updateBatchLcApproval(List<LcApproval> lcApprovalList){
		int i = 0;
		try {
			i = lcApprovalDao.updateBatchLcApproval(lcApprovalList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
