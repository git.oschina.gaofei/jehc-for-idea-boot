package jehc.lcmodules.lcservice;
import java.util.List;
import java.util.Map;

import jehc.lcmodules.lcmodel.LcApply;

/**
* 流程申请 
* 2017-01-08 14:55:11  邓纯杰
*/
public interface LcApplyService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApply> getLcApplyListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param lc_apply_id 
	* @return
	*/
	public LcApply getLcApplyById(String lc_apply_id);
	/**
	* 添加
	* @param lcApply
	* @return
	*/
	public int addLcApply(LcApply lcApply);
	/**
	* 修改
	* @param lcApply
	* @return
	*/
	public int updateLcApply(LcApply lcApply);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApply(Map<String,Object> condition);
	/**
	* 批量添加
	* @param lcApplyList
	* @return
	*/
	public int addBatchLcApply(List<LcApply> lcApplyList);
	/**
	* 批量修改
	* @param lcApplyList
	* @return
	*/
	public int updateBatchLcApply(List<LcApply> lcApplyList);
	/**
	 * 根据实例编号查找集合
	 * @param condition
	 * @return
	 */
	public List<LcApply> getLcApplyList(Map<String,Object> condition);
}
