package jehc.lcmodules.lcservice;
import java.util.List;
import java.util.Map;

import jehc.lcmodules.lcmodel.LcDeploymentHis;

/**
* 流程部署历史记录 
* 2016-12-22 13:02:01  邓纯杰
*/
public interface LcDeploymentHisService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcDeploymentHis> getLcDeploymentHisListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LcDeploymentHis getLcDeploymentHisById(String id);
	/**
	* 添加
	* @param lcDeploymentHis
	* @return
	*/
	public int addLcDeploymentHis(LcDeploymentHis lcDeploymentHis);
	/**
	* 修改
	* @param lcDeploymentHis
	* @return
	*/
	public int updateLcDeploymentHis(LcDeploymentHis lcDeploymentHis);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcDeploymentHis(Map<String,Object> condition);
	/**
	* 批量添加
	* @param lcDeploymentHisList
	* @return
	*/
	public int addBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList);
	/**
	* 批量修改
	* @param lcDeploymentHisList
	* @return
	*/
	public int updateBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList);
	
	/**
	 * 查询唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisUnique(Map<String,Object> condition);
	
	/**
	 * 查询最新唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisNewUnique(Map<String,Object> condition);
}
