package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProduct;
import jehc.bmodules.bservice.BProductService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础_产品 
* 2016-01-08 21:03:47  邓纯杰
*/
@Api(value = "基础_产品", description = "基础_产品")
@Controller
@RequestMapping("/bProductController")
public class BProductController extends BaseAction{
	@Autowired
	private BProductService bProductService;
	/**
	* 列表页面
	* @param bProduct
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProduct(BProduct bProduct,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-product/b-product-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBProductListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BProduct> bProductList = bProductService.getBProductListByCondition(condition);
		PageInfo<BProduct> page = new PageInfo<BProduct>(bProductList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_product_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBProductById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductById(String b_product_id,HttpServletRequest request){
		BProduct bProduct = bProductService.getBProductById(b_product_id);
		return outDataStr(bProduct);
	}
	/**
	* 添加
	* @param bProduct
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String addBProduct(BProduct bProduct,HttpServletRequest request){
		int i = 0;
		String b_product_features = request.getParameter("b_product_features");
		if(null != bProduct){
			bProduct.setB_product_id(UUID.toUUID());
			bProduct.setXt_userinfo_id(getXtUid());
			bProduct.setB_product_ctime(getDate());
			bProduct.setB_product_features(b_product_features);
			i=bProductService.addBProduct(bProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bProduct
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBProduct(BProduct bProduct,HttpServletRequest request){
		int i = 0;
		String b_product_features = request.getParameter("b_product_features");
		if(null != bProduct){
			bProduct.setXt_userinfo_id(getXtUid());
			bProduct.setB_product_mtime(getDate());
			bProduct.setB_product_features(b_product_features);
			i=bProductService.updateBProduct(bProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_product_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String delBProduct(String b_product_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_product_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_product_id",b_product_id.split(","));
			i=bProductService.delBProduct(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_product_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBProduct(String b_product_id,HttpServletRequest request){
		int i = 0;
		BProduct bProduct = bProductService.getBProductById(b_product_id);
		if(null != bProduct){
			bProduct.setB_product_id(UUID.toUUID());
			i=bProductService.addBProduct(bProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBProduct",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBProduct(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBProductAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductAdd(BProduct bProduct,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-product/b-product-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBProductUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductUpdate(String b_product_id,HttpServletRequest request, Model model){
		BProduct bProduct = bProductService.getBProductById(b_product_id);
		model.addAttribute("bProduct", bProduct);
		return new ModelAndView("pc/b-view/b-product/b-product-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBProductDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductDetail(String b_product_id,HttpServletRequest request, Model model){
		BProduct bProduct = bProductService.getBProductById(b_product_id);
		model.addAttribute("bProduct", bProduct);
		return new ModelAndView("pc/b-view/b-product/b-product-detail");
	}
}
