package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BStock;
import jehc.bmodules.bservice.BStockService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础库存 
* 2016-01-27 14:28:46  邓纯杰
*/
@Api(value = "基础库存", description = "基础库存")
@Controller
@RequestMapping("/bStockController")
public class BStockController extends BaseAction{
	@Autowired
	private BStockService bStockService;
	/**
	* 列表页面
	* @param bStock
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBStock",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBStock(BStock bStock,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-stock/b-stock-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBStockListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBStockListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BStock> bStockList = bStockService.getBStockListByCondition(condition);
		PageInfo<BStock> page = new PageInfo<BStock>(bStockList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_stock_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBStockById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBStockById(String b_stock_id,HttpServletRequest request){
		BStock b_Stock = bStockService.getBStockById(b_stock_id);
		return outDataStr(b_Stock);
	}
	/**
	* 添加
	* @param bStock
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBStock",method={RequestMethod.POST,RequestMethod.GET})
	public String addBStock(BStock bStock,HttpServletRequest request){
		int i = 0;
		if(null != bStock){
			bStock.setB_stock_id(UUID.toUUID());
			i=bStockService.addBStock(bStock);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bStock
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBStock",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBStock(BStock bStock,HttpServletRequest request){
		int i = 0;
		if(null != bStock){
			i=bStockService.updateBStock(bStock);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_stock_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBStock",method={RequestMethod.POST,RequestMethod.GET})
	public String delBStock(String b_stock_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_stock_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_stock_id",b_stock_id.split(","));
			i=bStockService.delBStock(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_stock_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBStock",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBStock(String b_stock_id,HttpServletRequest request){
		int i = 0;
		BStock bStock = bStockService.getBStockById(b_stock_id);
		if(null != bStock){
			bStock.setB_stock_id(UUID.toUUID());
			i=bStockService.addBStock(bStock);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBStock",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBStock(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBStockAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBStockAdd(BStock bStock,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-stock/b-stock-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBStockUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBStockUpdate(String b_stock_id,HttpServletRequest request, Model model){
		BStock bStock = bStockService.getBStockById(b_stock_id);
		model.addAttribute("bStock", bStock);
		return new ModelAndView("pc/b-view/b-stock/b-stock-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBStockDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBStockDetail(String b_stock_id,HttpServletRequest request, Model model){
		BStock bStock = bStockService.getBStockById(b_stock_id);
		model.addAttribute("bStock", bStock);
		return new ModelAndView("pc/b-view/b-stock/b-stock-detail");
	}
}
