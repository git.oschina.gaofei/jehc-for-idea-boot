package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jehc.bmodules.bmodel.BOrderDetail;
import jehc.bmodules.bservice.BOrderDetailService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础订单详细 
* 2016-01-27 13:59:04  邓纯杰
*/
@Api(value = "基础订单详细", description = "基础订单详细")
@Controller
@RequestMapping("/bOrderDetailController")
public class BOrderDetailController extends BaseAction{
	@Autowired
	private BOrderDetailService bOrderDetailService;
	/**
	* 列表页面
	* @param bOrderDetail
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBOrderDetail(BOrderDetail bOrderDetail,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-order-detail/b-order-detail-list");
	}

	/**
	 * 查询并分页
	 * @param bOrderDetail
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBOrderDetailListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBOrderDetailListByCondition(BOrderDetail bOrderDetail,BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("b_order_id", bOrderDetail.getB_order_id());
		List<BOrderDetail> b_Order_DetailList = bOrderDetailService.getBOrderDetailListByCondition(condition);
		return outItemsStr(b_Order_DetailList);
	}
	/**
	* 查询单条记录
	* @param b_order_detail_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBOrderDetailById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBOrderDetailById(String b_order_detail_id,HttpServletRequest request){
		BOrderDetail b_Order_Detail = bOrderDetailService.getBOrderDetailById(b_order_detail_id);
		return outDataStr(b_Order_Detail);
	}
	/**
	* 添加
	* @param bOrderDetail
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String addBOrderDetail(BOrderDetail bOrderDetail,HttpServletRequest request){
		int i = 0;
		if(null != bOrderDetail){
			bOrderDetail.setB_order_detail_id(UUID.toUUID());
			i=bOrderDetailService.addBOrderDetail(bOrderDetail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bOrderDetail
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBOrderDetail(BOrderDetail bOrderDetail,HttpServletRequest request){
		int i = 0;
		if(null != bOrderDetail){
			i=bOrderDetailService.updateBOrderDetail(bOrderDetail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_order_detail_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String delBOrderDetail(String b_order_detail_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_order_detail_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_order_detail_id",b_order_detail_id.split(","));
			i=bOrderDetailService.delBOrderDetail(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_order_detail_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBOrderDetail(String b_order_detail_id,HttpServletRequest request){
		int i = 0;
		BOrderDetail bOrderDetail = bOrderDetailService.getBOrderDetailById(b_order_detail_id);
		if(null != bOrderDetail){
			bOrderDetail.setB_order_detail_id(UUID.toUUID());
			i=bOrderDetailService.addBOrderDetail(bOrderDetail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBOrderDetail",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBOrderDetail(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
