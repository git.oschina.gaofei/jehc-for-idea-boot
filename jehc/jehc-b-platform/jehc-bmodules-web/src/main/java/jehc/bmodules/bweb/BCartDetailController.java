package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jehc.bmodules.bmodel.BCartDetail;
import jehc.bmodules.bservice.BCartDetailService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础购物车明细 
* 2016-01-27 13:52:21  邓纯杰
*/
@Api(value = "基础购物车明细", description = "基础购物车明细")
@Controller
@RequestMapping("/bCartDetailController")
public class BCartDetailController extends BaseAction{
	@Autowired
	private BCartDetailService bCartDetailService;
	/**
	* 列表页面
	* @param bCartDetail
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBCartDetail(BCartDetail bCartDetail,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-cart-detail/b-cart-detail-list");
	}

	/**
	 * 查询并分页
	 * @param bCartDetail
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBCartDetailListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBCartDetailListByCondition(BCartDetail bCartDetail,BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("b_cart_id", bCartDetail.getB_cart_id());
		List<BCartDetail> b_Cart_DetailList = bCartDetailService.getBCartDetailListByCondition(condition);
		return outItemsStr(b_Cart_DetailList);
	}
	/**
	* 查询单条记录
	* @param b_cart_detail_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBCartDetailById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBCartDetailById(String b_cart_detail_id,HttpServletRequest request){
		BCartDetail b_Cart_Detail = bCartDetailService.getBCartDetailById(b_cart_detail_id);
		return outDataStr(b_Cart_Detail);
	}
	/**
	* 添加
	* @param bCartDetail
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String addBCartDetail(BCartDetail bCartDetail,HttpServletRequest request){
		int i = 0;
		if(null != bCartDetail){
			bCartDetail.setB_cart_detail_id(UUID.toUUID());
			i=bCartDetailService.addBCartDetail(bCartDetail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bCartDetail
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBCartDetail(BCartDetail bCartDetail,HttpServletRequest request){
		int i = 0;
		if(null != bCartDetail){
			i=bCartDetailService.updateBCartDetail(bCartDetail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_cart_detail_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String delBCartDetail(String b_cart_detail_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_cart_detail_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_cart_detail_id",b_cart_detail_id.split(","));
			i=bCartDetailService.delBCartDetail(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_cart_detail_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBCartDetail(String b_cart_detail_id,HttpServletRequest request){
		int i = 0;
		BCartDetail b_Cart_Detail = bCartDetailService.getBCartDetailById(b_cart_detail_id);
		if(null != b_Cart_Detail){
			b_Cart_Detail.setB_cart_detail_id(UUID.toUUID());
			i=bCartDetailService.addBCartDetail(b_Cart_Detail);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBCartDetail",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBCartDetail(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
