package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BOrder;
import jehc.bmodules.bservice.BOrderService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础订单 
* 2016-01-27 13:55:11  邓纯杰
*/
@Api(value = "基础订单", description = "基础订单")
@Controller
@RequestMapping("/bOrderController")
public class BOrderController extends BaseAction{
	@Autowired
	private BOrderService bOrderService;
	/**
	* 列表页面
	* @param bOrder
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBOrder(BOrder bOrder,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-order/b-order-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBOrderListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBOrderListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BOrder> bOrderList = bOrderService.getBOrderListByCondition(condition);
		PageInfo<BOrder> page = new PageInfo<BOrder>(bOrderList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_order_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBOrderById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBOrderById(String b_order_id,HttpServletRequest request){
		BOrder b_Order = bOrderService.getBOrderById(b_order_id);
		return outDataStr(b_Order);
	}
	/**
	* 添加
	* @param bOrder
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public String addBOrder(BOrder bOrder,HttpServletRequest request){
		int i = 0;
		if(null != bOrder){
			bOrder.setB_order_id(UUID.toUUID());
			i=bOrderService.addBOrder(bOrder);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bOrder
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBOrder(BOrder bOrder,HttpServletRequest request){
		int i = 0;
		if(null != bOrder){
			i=bOrderService.updateBOrder(bOrder);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_order_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public String delBOrder(String b_order_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_order_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_order_id",b_order_id.split(","));
			i=bOrderService.delBOrder(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_order_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBOrder(String b_order_id,HttpServletRequest request){
		int i = 0;
		BOrder bOrder = bOrderService.getBOrderById(b_order_id);
		if(null != bOrder){
			bOrder.setB_order_id(UUID.toUUID());
			i=bOrderService.addBOrder(bOrder);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBOrder",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBOrder(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
