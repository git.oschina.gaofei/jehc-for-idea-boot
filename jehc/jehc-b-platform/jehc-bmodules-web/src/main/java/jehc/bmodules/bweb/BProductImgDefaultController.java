package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProductImgDefault;
import jehc.bmodules.bservice.BProductImgDefaultService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础商品默认图片 
* 2016-01-09 09:06:38  邓纯杰
*/
@Api(value = "基础商品默认图片", description = "基础商品默认图片")
@Controller
@RequestMapping("/bProductImgDefaultController")
public class BProductImgDefaultController extends BaseAction{
	@Autowired
	private BProductImgDefaultService bProductImgDefaultService;
	/**
	* 列表页面
	* @param bProductImgDefault
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProductImgDefault(BProductImgDefault bProductImgDefault,HttpServletRequest request,Model model){
		model.addAttribute("b_product_id", bProductImgDefault.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-img-default/b-product-img-default-list");
	}
	
	/**
	* DataGrid页面
	* @param bProductImgDefault
	* @param request 
	* @return
	*/
	@ApiOperation(value="DataGrid页面", notes="DataGrid页面")
	@RequestMapping(value="/loadBProductImgDefaultDataGrid",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProductImgDefaultDataGrid(BProductImgDefault bProductImgDefault,HttpServletRequest request,Model model){
		model.addAttribute("b_product_id", bProductImgDefault.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-img-default/b-product-img-default-datagrid");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBProductImgDefaultListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductImgDefaultListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		String jehcimg_base_url = CommonUtils.getXtPathCache("jehcimg_base_url").get(0).getXt_path();
		List<BProductImgDefault> bProductImgDefaultList = bProductImgDefaultService.getBProductImgDefaultListByCondition(condition);
		for(int i = 0; i < bProductImgDefaultList.size(); i++){
			bProductImgDefaultList.get(i).setJehcimg_base_url(jehcimg_base_url);
			bProductImgDefaultList.get(i).setJehcimg_base_path_url(jehcimg_base_url+bProductImgDefaultList.get(i).getXt_attachmentPath());
		}
		PageInfo<BProductImgDefault> page = new PageInfo<BProductImgDefault>(bProductImgDefaultList);
		return outPageBootStr(page,request);
	}
	
	/**
	* 查询单条记录
	* @param b_product_img_default_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBProductImgDefaultById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductImgDefaultById(String b_product_img_default_id,HttpServletRequest request){
		BProductImgDefault b_Product_Img_Default = bProductImgDefaultService.getBProductImgDefaultById(b_product_img_default_id);
		return outDataStr(b_Product_Img_Default);
	}
	/**
	* 添加
	* @param bProductImgDefault
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String addBProductImgDefault(BProductImgDefault bProductImgDefault,HttpServletRequest request){
		int i = 0;
		if(null != bProductImgDefault){
			bProductImgDefault.setB_product_img_default_id(UUID.toUUID());
			bProductImgDefault.setB_product_img_ctime(getDate());
			bProductImgDefault.setXt_userinfo_id(getXtUid());
			i=bProductImgDefaultService.addBProductImgDefault(bProductImgDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bProductImgDefault
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBProductImgDefault(BProductImgDefault bProductImgDefault,HttpServletRequest request){
		int i = 0;
		if(null != bProductImgDefault){
			bProductImgDefault.setB_product_img_mtime(getDate());
			bProductImgDefault.setXt_userinfo_id(getXtUid());
			i=bProductImgDefaultService.updateBProductImgDefault(bProductImgDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_product_img_default_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String delBProductImgDefault(String b_product_img_default_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_product_img_default_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_product_img_default_id",b_product_img_default_id.split(","));
			i=bProductImgDefaultService.delBProductImgDefault(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_product_img_default_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBProductImgDefault(String b_product_img_default_id,HttpServletRequest request){
		int i = 0;
		BProductImgDefault bProductImgDefault = bProductImgDefaultService.getBProductImgDefaultById(b_product_img_default_id);
		if(null != bProductImgDefault){
			bProductImgDefault.setB_product_img_default_id(UUID.toUUID());
			i=bProductImgDefaultService.addBProductImgDefault(bProductImgDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBProductImgDefault",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBProductImgDefault(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBProductImgDefaultAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgDefaultAdd(BProductImgDefault bProductImgDefault,HttpServletRequest request, Model model){
		model.addAttribute("b_product_id", bProductImgDefault.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-img-default/b-product-img-default-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBProductImgDefaultUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgDefaultUpdate(String b_product_img_default_id,HttpServletRequest request, Model model){
		BProductImgDefault bProductImgDefault = bProductImgDefaultService.getBProductImgDefaultById(b_product_img_default_id);
		model.addAttribute("bProductImgDefault", bProductImgDefault);
		return new ModelAndView("pc/b-view/b-product-img-default/b-product-img-default-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBProductImgDefaultDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgDefaultDetail(String b_product_img_default_id,HttpServletRequest request, Model model){
		BProductImgDefault bProductImgDefault = bProductImgDefaultService.getBProductImgDefaultById(b_product_img_default_id);
		model.addAttribute("bProductImgDefault", bProductImgDefault);
		return new ModelAndView("pc/b-view/b-product-img-default/b-product-img-default-detail");
	}
}
