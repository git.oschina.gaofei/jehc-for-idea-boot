package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProductColorDefault;
import jehc.bmodules.bservice.BProductColorDefaultService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础商品默认颜色 
* 2016-01-19 15:38:05  邓纯杰
*/
@Api(value = "基础商品默认颜色", description = "基础商品默认颜色")
@Controller
@RequestMapping("/bProductColorDefaultController")
public class BProductColorDefaultController extends BaseAction{
	@Autowired
	private BProductColorDefaultService bProductColorDefaultService;
	/**
	* 列表页面
	* @param bProductColorDefault
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProductColorDefault(BProductColorDefault bProductColorDefault,HttpServletRequest request,Model model){
		model.addAttribute("b_product_id", bProductColorDefault.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-color-default/b-product-color-default-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBProductColorDefaultListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductColorDefaultListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		String jehcimg_base_url = CommonUtils.getXtPathCache("jehcimg_base_url").get(0).getXt_path();
		List<BProductColorDefault> bProductColorDefaultList = bProductColorDefaultService.getBProductColorDefaultListByCondition(condition);
		for(int i = 0; i < bProductColorDefaultList.size(); i++){
			bProductColorDefaultList.get(i).setJehcimg_base_url(jehcimg_base_url);
		}
		PageInfo<BProductColorDefault> page = new PageInfo<BProductColorDefault>(bProductColorDefaultList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_product_color_default_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBProductColorDefaultById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductColorDefaultById(String b_product_color_default_id,HttpServletRequest request){
		BProductColorDefault bProductColorDefault = bProductColorDefaultService.getBProductColorDefaultById(b_product_color_default_id);
		return outDataStr(bProductColorDefault);
	}
	/**
	* 添加
	* @param bProductColorDefault
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String addBProductColorDefault(BProductColorDefault bProductColorDefault,HttpServletRequest request){
		int i = 0;
		if(null != bProductColorDefault){
			bProductColorDefault.setB_product_color_default_id(UUID.toUUID());
			bProductColorDefault.setB_product_color_default_ctime(getDate());
			bProductColorDefault.setXt_userinfo_id(CommonUtils.getXtUid());
			i=bProductColorDefaultService.addBProductColorDefault(bProductColorDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bProductColorDefault
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBProductColorDefault(BProductColorDefault bProductColorDefault,HttpServletRequest request){
		int i = 0;
		if(null != bProductColorDefault){
			bProductColorDefault.setB_product_color_default_mtime(getDate());
			i=bProductColorDefaultService.updateBProductColorDefault(bProductColorDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_product_color_default_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String delBProductColorDefault(String b_product_color_default_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_product_color_default_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_product_color_default_id",b_product_color_default_id.split(","));
			i=bProductColorDefaultService.delBProductColorDefault(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_product_color_default_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBProductColorDefault(String b_product_color_default_id,HttpServletRequest request){
		int i = 0;
		BProductColorDefault bProductColorDefault = bProductColorDefaultService.getBProductColorDefaultById(b_product_color_default_id);
		if(null != bProductColorDefault){
			bProductColorDefault.setB_product_color_default_id(UUID.toUUID());
			bProductColorDefault.setB_product_color_default_ctime(getDate());
			i=bProductColorDefaultService.addBProductColorDefault(bProductColorDefault);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBProductColorDefault",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBProductColorDefault(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBProductColorDefaultAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorDefaultAdd(BProductColorDefault bProductColorDefault,HttpServletRequest request, Model model){
		model.addAttribute("b_product_id", bProductColorDefault.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-color-default/b-product-color-default-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBProductColorDefaultUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorDefaultUpdate(String b_product_color_default_id,HttpServletRequest request, Model model){
		BProductColorDefault bProductColorDefault = bProductColorDefaultService.getBProductColorDefaultById(b_product_color_default_id);
		model.addAttribute("bProductColorDefault", bProductColorDefault);
		return new ModelAndView("pc/b-view/b-product-color-default/b-product-color-default-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBProductColorDefaultDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorDefaultDetail(String b_product_color_default_id,HttpServletRequest request, Model model){
		BProductColorDefault bProductColorDefault = bProductColorDefaultService.getBProductColorDefaultById(b_product_color_default_id);
		model.addAttribute("bProductColorDefault", bProductColorDefault);
		return new ModelAndView("pc/b-view/b-product-color-default/b-product-color-default-detail");
	}
}
