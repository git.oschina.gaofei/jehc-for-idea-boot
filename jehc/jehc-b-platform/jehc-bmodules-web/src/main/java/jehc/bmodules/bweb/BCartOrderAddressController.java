package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BCartOrderAddress;
import jehc.bmodules.bservice.BCartOrderAddressService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础购物车订单常用配送地址 
* 2016-02-22 21:17:25  邓纯杰
*/
@Api(value = "基础购物车订单常用配送地址", description = "基础购物车订单常用配送地址")
@Controller
@RequestMapping("/bCartOrderAddressController")
public class BCartOrderAddressController extends BaseAction{
	@Autowired
	private BCartOrderAddressService bCartOrderAddressService;
	/**
	* 列表页面
	* @param bCartOrderAddress
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBCartOrderAddress(BCartOrderAddress bCartOrderAddress,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-cart-order-address/b-cart-order-address-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBCartOrderAddressListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBCartOrderAddressListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BCartOrderAddress> bCartOrderAddressList = bCartOrderAddressService.getBCartOrderAddressListByCondition(condition);
		PageInfo<BCartOrderAddress> page = new PageInfo<BCartOrderAddress>(bCartOrderAddressList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_cart_order_address_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBCartOrderAddressById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBCartOrderAddressById(String b_cart_order_address_id,HttpServletRequest request){
		BCartOrderAddress bCartOrderAddress = bCartOrderAddressService.getBCartOrderAddressById(b_cart_order_address_id);
		return outDataStr(bCartOrderAddress);
	}
	/**
	* 添加
	* @param bCartOrderAddress
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String addBCartOrderAddress(BCartOrderAddress bCartOrderAddress,HttpServletRequest request){
		int i = 0;
		if(null != bCartOrderAddress){
			bCartOrderAddress.setB_cart_order_address_id(UUID.toUUID());
			i=bCartOrderAddressService.addBCartOrderAddress(bCartOrderAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bCartOrderAddress
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBCartOrderAddress(BCartOrderAddress bCartOrderAddress,HttpServletRequest request){
		int i = 0;
		if(null != bCartOrderAddress){
			i=bCartOrderAddressService.updateBCartOrderAddress(bCartOrderAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_cart_order_address_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String delBCartOrderAddress(String b_cart_order_address_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_cart_order_address_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_cart_order_address_id",b_cart_order_address_id.split(","));
			i=bCartOrderAddressService.delBCartOrderAddress(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_cart_order_address_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBCartOrderAddress(String b_cart_order_address_id,HttpServletRequest request){
		int i = 0;
		BCartOrderAddress bCartOrderAddress = bCartOrderAddressService.getBCartOrderAddressById(b_cart_order_address_id);
		if(null != bCartOrderAddress){
			bCartOrderAddress.setB_cart_order_address_id(UUID.toUUID());
			i=bCartOrderAddressService.addBCartOrderAddress(bCartOrderAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBCartOrderAddress",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBCartOrderAddress(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
