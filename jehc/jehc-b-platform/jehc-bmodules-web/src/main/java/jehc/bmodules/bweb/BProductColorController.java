package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProductColor;
import jehc.bmodules.bservice.BProductColorService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础商品商户所选颜色 
* 2016-07-02 16:54:11  邓纯杰
*/
@Api(value = "基础商品商户所选颜色", description = "基础商品商户所选颜色")
@Controller
@RequestMapping("/bProductColorController")
public class BProductColorController extends BaseAction{
	@Autowired
	private BProductColorService bProductColorService;
	/**
	* 列表页面
	* @param bProductColor
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProductColor(BProductColor bProductColor,HttpServletRequest request,Model model){
		model.addAttribute("b_product_id", bProductColor.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-color/b-product-color-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBProductColorListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductColorListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BProductColor> bProductColorList = bProductColorService.getBProductColorListByCondition(condition);
		PageInfo<BProductColor> page = new PageInfo<BProductColor>(bProductColorList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_product_color_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBProductColorById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductColorById(String b_product_color_id,HttpServletRequest request){
		BProductColor bProductColor = bProductColorService.getBProductColorById(b_product_color_id);
		return outDataStr(bProductColor);
	}
	/**
	* 添加
	* @param bProductColor
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public String addBProductColor(BProductColor bProductColor,HttpServletRequest request){
		int i = 0;
		if(null != bProductColor){
			bProductColor.setB_product_color_id(UUID.toUUID());
			bProductColor.setXt_userinfo_id(getXtUid());
			bProductColor.setB_product_color_ctime(getDate());
			i=bProductColorService.addBProductColor(bProductColor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bProductColor
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBProductColor(BProductColor bProductColor,HttpServletRequest request){
		int i = 0;
		if(null != bProductColor){
			bProductColor.setXt_userinfo_id(getXtUid());
			bProductColor.setB_product_color_mtime(getDate());
			i=bProductColorService.updateBProductColor(bProductColor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_product_color_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public String delBProductColor(String b_product_color_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_product_color_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_product_color_id",b_product_color_id.split(","));
			i=bProductColorService.delBProductColor(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_product_color_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBProductColor(String b_product_color_id,HttpServletRequest request){
		int i = 0;
		BProductColor bProductColor = bProductColorService.getBProductColorById(b_product_color_id);
		if(null != bProductColor){
			bProductColor.setB_product_color_id(UUID.toUUID());
			i=bProductColorService.addBProductColor(bProductColor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBProductColor",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBProductColor(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBProductColorAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorAdd(BProductColor bProductColor,HttpServletRequest request, Model model){
		model.addAttribute("b_product_id", bProductColor.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-color/b-product-color-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBProductColorUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorUpdate(String b_product_color_id,HttpServletRequest request, Model model){
		BProductColor bProductColor = bProductColorService.getBProductColorById(b_product_color_id);
		model.addAttribute("bProductColor", bProductColor);
		return new ModelAndView("pc/b-view/b-product-color/b-product-color-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBProductColorDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductColorDetail(String b_product_color_id,HttpServletRequest request, Model model){
		BProductColor bProductColor = bProductColorService.getBProductColorById(b_product_color_id);
		model.addAttribute("bProductColor", bProductColor);
		return new ModelAndView("pc/b-view/b-product-color/b-product-color-detail");
	}
}
