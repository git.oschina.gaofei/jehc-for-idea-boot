package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BSellerAccount;
import jehc.bmodules.bservice.BSellerAccountService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础卖家账号 
* 2016-02-18 17:07:37  邓纯杰
*/
@Api(value = "基础卖家账号", description = "基础卖家账号")
@Controller
@RequestMapping("/bSellerAccountController")
public class BSellerAccountController extends BaseAction{
	@Autowired
	private BSellerAccountService bSellerAccountService;
	/**
	* 列表页面
	* @param bSellerAccount
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBSellerAccount(BSellerAccount bSellerAccount,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-seller-account/b-seller-account-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerAccountListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerAccountListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSellerAccount> bSellerAccountList = bSellerAccountService.getBSellerAccountListByCondition(condition);
		PageInfo<BSellerAccount> page = new PageInfo<BSellerAccount>(bSellerAccountList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_seller_account_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBSellerAccountById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerAccountById(String b_seller_account_id,HttpServletRequest request){
		BSellerAccount bSellerAccount = bSellerAccountService.getBSellerAccountById(b_seller_account_id);
		return outDataStr(bSellerAccount);
	}
	/**
	* 添加
	* @param bSellerAccount
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String addBSellerAccount(BSellerAccount bSellerAccount,HttpServletRequest request){
		int i = 0;
		if(null != bSellerAccount){
			bSellerAccount.setB_seller_account_id(UUID.toUUID());
			i=bSellerAccountService.addBSellerAccount(bSellerAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bSellerAccount
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBSellerAccount(BSellerAccount bSellerAccount,HttpServletRequest request){
		int i = 0;
		if(null != bSellerAccount){
			i=bSellerAccountService.updateBSellerAccount(bSellerAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_seller_account_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String delBSellerAccount(String b_seller_account_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_seller_account_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_seller_account_id",b_seller_account_id.split(","));
			i=bSellerAccountService.delBSellerAccount(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_seller_account_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBSellerAccount(String b_seller_account_id,HttpServletRequest request){
		int i = 0;
		BSellerAccount bSellerAccount = bSellerAccountService.getBSellerAccountById(b_seller_account_id);
		if(null != bSellerAccount){
			bSellerAccount.setB_seller_account_id(UUID.toUUID());
			i=bSellerAccountService.addBSellerAccount(bSellerAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBSellerAccount",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBSellerAccount(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
