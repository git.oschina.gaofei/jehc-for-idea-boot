package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BMemberAddress;
import jehc.bmodules.bservice.BMemberAddressService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础会员常用地址 
* 2016-02-22 16:44:23  邓纯杰
*/
@Api(value = "基础会员常用地址", description = "基础会员常用地址")
@Controller
@RequestMapping("/bMemberAddressController")
public class BMemberAddressController extends BaseAction{
	@Autowired
	private BMemberAddressService bMemberAddressService;

	/**
	 * 列表页面
	 * @param b_member_id
	 * @param bMemberAddress
	 * @param request
	 * @return
	 */
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBMemberAddress(String b_member_id ,BMemberAddress bMemberAddress,HttpServletRequest request){
		request.setAttribute("b_member_id", b_member_id);
		return new ModelAndView("pc/b-view/b-member-address/b-member-address-list");
	}
	/**
	* 查询并分页
	* @param b_member_id
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBMemberAddressListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAddressListByCondition(String b_member_id,BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("b_member_id", b_member_id);
		commonHPager(condition,request);
		List<BMemberAddress> bMemberAddressList = bMemberAddressService.getBMemberAddressListByCondition(condition);
		PageInfo<BMemberAddress> page = new PageInfo<BMemberAddress>(bMemberAddressList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_member_address_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBMemberAddressById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAddressById(String b_member_address_id,HttpServletRequest request){
		BMemberAddress bMemberAddress = bMemberAddressService.getBMemberAddressById(b_member_address_id);
		return outDataStr(bMemberAddress);
	}
	/**
	* 添加
	* @param bMemberAddress
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String addBMemberAddress(BMemberAddress bMemberAddress,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAddress){
            bMemberAddress.setB_member_address_ctime(CommonUtils.getSimpleDateFormat());
			bMemberAddress.setB_member_address_id(UUID.toUUID());
			i=bMemberAddressService.addBMemberAddress(bMemberAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bMemberAddress
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBMemberAddress(BMemberAddress bMemberAddress,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAddress){
			bMemberAddress.setB_member_address_mtime(CommonUtils.getSimpleDateFormat());
			i=bMemberAddressService.updateBMemberAddress(bMemberAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_member_address_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String delBMemberAddress(String b_member_address_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_member_address_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_member_address_id",b_member_address_id.split(","));
			i=bMemberAddressService.delBMemberAddress(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_member_address_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBMemberAddress(String b_member_address_id,HttpServletRequest request){
		int i = 0;
		BMemberAddress bMemberAddress = bMemberAddressService.getBMemberAddressById(b_member_address_id);
		if(null != bMemberAddress){
			bMemberAddress.setB_member_address_id(UUID.toUUID());
			i=bMemberAddressService.addBMemberAddress(bMemberAddress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBMemberAddress",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBMemberAddress(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
