package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProductPrice;
import jehc.bmodules.bmodel.BSellerProduct;
import jehc.bmodules.bservice.BProductPriceService;
import jehc.bmodules.bservice.BSellerProductService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 卖家商品 
* 2016-02-18 17:20:35  邓纯杰
*/
@Api(value = "卖家商品", description = "卖家商品")
@Controller
@RequestMapping("/bSellerProductController")
public class BSellerProductController extends BaseAction{
	@Autowired
	private BSellerProductService bSellerProductService;
	@Autowired
	private BProductPriceService bProductPriceService;

	/**
	 * 列表页面
	 * @param b_seller_id
	 * @param bSellerProduct
	 * @param request
	 * @return
	 */
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBSellerProduct(String b_seller_id,BSellerProduct bSellerProduct,HttpServletRequest request){
		request.setAttribute("b_seller_id", b_seller_id);
		return new ModelAndView("pc/b-view/b-seller-product/b-seller-product-list");
	}

	/**
	 * 查询并分页
	 * @param b_seller_id
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerProductListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerProductListByCondition(String b_seller_id,BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSellerProduct> bSellerProductList = bSellerProductService.getBSellerProductListByCondition(condition);
		PageInfo<BSellerProduct> page = new PageInfo<BSellerProduct>(bSellerProductList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_seller_product_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBSellerProductById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerProductById(String b_seller_product_id,HttpServletRequest request){
		BSellerProduct b_Seller_Product = bSellerProductService.getBSellerProductById(b_seller_product_id);
		return outDataStr(b_Seller_Product);
	}

	/**
	 * 添加
	 * @param bSellerProduct
	 * @param bProductPrice
	 * @param request
	 * @return
	 */
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String addBSellerProduct(BSellerProduct bSellerProduct,BProductPrice bProductPrice,HttpServletRequest request){
		int i = 0;
		if(null != bSellerProduct){
			bSellerProduct.setB_seller_product_id(UUID.toUUID());
			bProductPrice.setB_product_price_id(UUID.toUUID());
			bProductPrice.setB_seller_product_id(bProductPrice.getB_seller_product_id());
			i=bSellerProductService.addBSellerProduct(bSellerProduct,bProductPrice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 修改
	 * @param bSellerProduct
	 * @param bProductPrice
	 * @param request
	 * @return
	 */
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBSellerProduct(BSellerProduct bSellerProduct,BProductPrice bProductPrice,HttpServletRequest request){
		int i = 0;
		if(null != bSellerProduct && !"".equals(bSellerProduct)){
			i=bSellerProductService.updateBSellerProduct(bSellerProduct,bProductPrice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_seller_product_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String delBSellerProduct(String b_seller_product_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_seller_product_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_seller_product_id",b_seller_product_id.split(","));
			i=bSellerProductService.delBSellerProduct(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_seller_product_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBSellerProduct(String b_seller_product_id,HttpServletRequest request){
		int i = 0;
		BSellerProduct bSellerProduct = bSellerProductService.getBSellerProductById(b_seller_product_id);
		if(null != bSellerProduct){
			bSellerProduct.setB_seller_product_id(UUID.toUUID());
			BProductPrice bProductPrice = bProductPriceService.getBProductPriceByBSellerProductId(b_seller_product_id);
			i=bSellerProductService.addBSellerProduct(bSellerProduct,bProductPrice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBSellerProduct",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBSellerProduct(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}

	/**
	 * 查询库存使用商户商品并分页
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询库存使用商户商品并分页", notes="查询库存使用商户商品并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerProductStockListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerProductStockListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSellerProduct> b_Seller_ProductList = bSellerProductService.getBSellerProductStockListByCondition(condition);
		PageInfo<BSellerProduct> page = new PageInfo<BSellerProduct>(b_Seller_ProductList);
		return outPageBootStr(page, request);
	}
}
