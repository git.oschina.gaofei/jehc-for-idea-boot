package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BSeller;
import jehc.bmodules.bservice.BSellerService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础卖家 
* 2016-01-08 22:54:00  邓纯杰
*/
@Api(value = "基础卖家", description = "基础卖家")
@Controller
@RequestMapping("/bSellerController")
public class BSellerController extends BaseAction{
	@Autowired
	private BSellerService bSellerService;
	/**
	* 列表页面
	* @param bSeller
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBSeller(BSeller bSeller,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-seller/b-seller-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSeller> bSellerList = bSellerService.getBSellerListByCondition(condition);
		PageInfo<BSeller> page = new PageInfo<BSeller>(bSellerList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_seller_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBSellerById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerById(String b_seller_id,HttpServletRequest request){
		BSeller bSeller = bSellerService.getBSellerById(b_seller_id);
		return outDataStr(bSeller);
	}
	/**
	* 添加
	* @param bSeller
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public String addBSeller(BSeller bSeller,HttpServletRequest request){
		int i = 0;
		if(null != bSeller){
			bSeller.setB_seller_id(UUID.toUUID());
			bSeller.setXt_userinfo_id(getXtUid());
			bSeller.setB_seller_ctime(getDate());
			i=bSellerService.addBSeller(bSeller);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bSeller
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBSeller(BSeller bSeller,HttpServletRequest request){
		int i = 0;
		if(null != bSeller){
			bSeller.setB_seller_mtime(getDate());
			bSeller.setXt_userinfo_id(getXtUid());
			i=bSellerService.updateBSeller(bSeller);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_seller_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public String delBSeller(String b_seller_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_seller_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_seller_id",b_seller_id.split(","));
			i=bSellerService.delBSeller(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_seller_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBSeller(String b_seller_id,HttpServletRequest request){
		int i = 0;
		BSeller bSeller = bSellerService.getBSellerById(b_seller_id);
		if(null != bSeller){
			bSeller.setB_seller_id(UUID.toUUID());
			i=bSellerService.addBSeller(bSeller);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBSeller",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBSeller(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBSellerAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBSellerAdd(BSeller bSeller,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-seller/b-seller-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBSellerUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBSellerUpdate(String b_seller_id,HttpServletRequest request, Model model){
		BSeller bSeller = bSellerService.getBSellerById(b_seller_id);
		model.addAttribute("bSeller", bSeller);
		return new ModelAndView("pc/b-view/b-seller/b-seller-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBSellerDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBSellerDetail(String b_seller_id,HttpServletRequest request, Model model){
		BSeller bSeller = bSellerService.getBSellerById(b_seller_id);
		model.addAttribute("bSeller", bSeller);
		return new ModelAndView("pc/b-view/b-seller/b-seller-detail");
	}
}
