package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BMemberAccount;
import jehc.bmodules.bservice.BMemberAccountService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础会员余额账户 
* 2016-03-24 20:30:14  邓纯杰
*/
@Api(value = "基础会员余额账户", description = "基础会员余额账户")
@Controller
@RequestMapping("/bMemberAccountController")
public class BMemberAccountController extends BaseAction{
	@Autowired
	private BMemberAccountService bMemberAccountService;
	/**
	* 列表页面
	* @param bMemberAccount
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBMemberAccount(BMemberAccount bMemberAccount,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-member-account/b-member-account-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBMemberAccountListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAccountListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BMemberAccount> bMemberAccountList = bMemberAccountService.getBMemberAccountListByCondition(condition);
		PageInfo<BMemberAccount> page = new PageInfo<BMemberAccount>(bMemberAccountList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_member_account_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBMemberAccountById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAccountById(String b_member_account_id,HttpServletRequest request){
		BMemberAccount b_Member_Account = bMemberAccountService.getBMemberAccountById(b_member_account_id);
		return outDataStr(b_Member_Account);
	}
	/**
	* 添加
	* @param bMemberAccount
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String addBMemberAccount(BMemberAccount bMemberAccount,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAccount){
			bMemberAccount.setB_member_account_id(UUID.toUUID());
			i=bMemberAccountService.addBMemberAccount(bMemberAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bMemberAccount
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBMemberAccount(BMemberAccount bMemberAccount,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAccount){
			i=bMemberAccountService.updateBMemberAccount(bMemberAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_member_account_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String delBMemberAccount(String b_member_account_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_member_account_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_member_account_id",b_member_account_id.split(","));
			i=bMemberAccountService.delBMemberAccount(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_member_account_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBMemberAccount(String b_member_account_id,HttpServletRequest request){
		int i = 0;
		BMemberAccount bMemberAccount = bMemberAccountService.getBMemberAccountById(b_member_account_id);
		if(null != bMemberAccount){
			bMemberAccount.setB_member_account_id(UUID.toUUID());
			i=bMemberAccountService.addBMemberAccount(bMemberAccount);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBMemberAccount",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBMemberAccount(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
