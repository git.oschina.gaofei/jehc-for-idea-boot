package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BSellerExpress;
import jehc.bmodules.bservice.BSellerExpressService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础卖家快递 
* 2016-02-18 17:14:52  邓纯杰
*/
@Api(value = "基础卖家快递", description = "基础卖家快递")
@Controller
@RequestMapping("/bSellerExpressController")
public class BSellerExpressController extends BaseAction{
	@Autowired
	private BSellerExpressService bSellerExpressService;

	/**
	 * 列表页面
	 * @param b_seller_id
	 * @param bSellerExpress
	 * @param request
	 * @return
	 */
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBSellerExpress(String b_seller_id ,BSellerExpress bSellerExpress,HttpServletRequest request){
		request.setAttribute("b_seller_id", b_seller_id);
		return new ModelAndView("pc/b-view/b-seller-express/b-seller-express-list");
	}

	/**
	 * 查询并分页
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerExpressListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerExpressListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSellerExpress> b_Seller_ExpressList = bSellerExpressService.getBSellerExpressListByCondition(condition);
		PageInfo<BSellerExpress> page = new PageInfo<BSellerExpress>(b_Seller_ExpressList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_seller_express_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBSellerExpressById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerExpressById(String b_seller_express_id,HttpServletRequest request){
		BSellerExpress b_Seller_Express = bSellerExpressService.getBSellerExpressById(b_seller_express_id);
		return outDataStr(b_Seller_Express);
	}
	/**
	* 添加
	* @param bSellerExpress
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public String addBSellerExpress(BSellerExpress bSellerExpress,HttpServletRequest request){
		int i = 0;
		if(null != bSellerExpress){
			bSellerExpress.setB_seller_express_id(UUID.toUUID());
			bSellerExpress.setB_seller_express_ctime(CommonUtils.getSimpleDateFormat());
			i=bSellerExpressService.addBSellerExpress(bSellerExpress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bSellerExpress
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBSellerExpress(BSellerExpress bSellerExpress,HttpServletRequest request){
		int i = 0;
		if(null != bSellerExpress){
			bSellerExpress.setB_seller_express_mtime(CommonUtils.getSimpleDateFormat());
			i=bSellerExpressService.updateBSellerExpress(bSellerExpress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_seller_express_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public String delBSellerExpress(String b_seller_express_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_seller_express_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_seller_express_id",b_seller_express_id.split(","));
			i=bSellerExpressService.delBSellerExpress(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_seller_express_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBSellerExpress(String b_seller_express_id,HttpServletRequest request){
		int i = 0;
		BSellerExpress bSellerExpress = bSellerExpressService.getBSellerExpressById(b_seller_express_id);
		if(null != bSellerExpress){
			bSellerExpress.setB_seller_express_id(UUID.toUUID());
			i=bSellerExpressService.addBSellerExpress(bSellerExpress);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBSellerExpress",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBSellerExpress(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
