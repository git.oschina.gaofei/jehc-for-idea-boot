package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BWarehouse;
import jehc.bmodules.bservice.BWarehouseService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础仓库 
* 2016-01-27 14:21:55  邓纯杰
*/
@Api(value = "基础仓库", description = "基础仓库")
@Controller
@RequestMapping("/bWarehouseController")
public class BWarehouseController extends BaseAction{
	@Autowired
	private BWarehouseService bWarehouseService;
	/**
	* 列表页面
	* @param bWarehouse
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBWarehouse(BWarehouse bWarehouse,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-warehouse/b-warehouse-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBWarehouseListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBWarehouseListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BWarehouse> bWarehouseList = bWarehouseService.getBWarehouseListByCondition(condition);
		PageInfo<BWarehouse> page = new PageInfo<BWarehouse>(bWarehouseList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_warehouse_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBWarehouseById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBWarehouseById(String b_warehouse_id,HttpServletRequest request){
		BWarehouse bWarehouse = bWarehouseService.getBWarehouseById(b_warehouse_id);
		return outDataStr(bWarehouse);
	}
	/**
	* 添加
	* @param bWarehouse
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public String addBWarehouse(BWarehouse bWarehouse,HttpServletRequest request){
		int i = 0;
		if(null != bWarehouse){
			bWarehouse.setB_warehouse_id(UUID.toUUID());
			bWarehouse.setXt_userinfo_id(getXtUid());
			bWarehouse.setB_warehouse_ctime(getDate());
			i=bWarehouseService.addBWarehouse(bWarehouse);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bWarehouse
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/updateBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBWarehouse(BWarehouse bWarehouse,HttpServletRequest request){
		int i = 0;
		if(null != bWarehouse){
			bWarehouse.setXt_userinfo_id(getXtUid());
			bWarehouse.setB_warehouse_mtime(getDate());
			i=bWarehouseService.updateBWarehouse(bWarehouse);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_warehouse_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public String delBWarehouse(String b_warehouse_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_warehouse_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_warehouse_id",b_warehouse_id.split(","));
			i=bWarehouseService.delBWarehouse(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_warehouse_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBWarehouse(String b_warehouse_id,HttpServletRequest request){
		int i = 0;
		BWarehouse bWarehouse = bWarehouseService.getBWarehouseById(b_warehouse_id);
		if(null != bWarehouse){
			bWarehouse.setB_warehouse_id(UUID.toUUID());
			i=bWarehouseService.addBWarehouse(bWarehouse);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBWarehouse",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBWarehouse(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBWarehouseAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseAdd(BWarehouse bWarehouse,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-warehouse/b-warehouse-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBWarehouseUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseUpdate(String b_warehouse_id,HttpServletRequest request, Model model){
		BWarehouse bWarehouse = bWarehouseService.getBWarehouseById(b_warehouse_id);
		model.addAttribute("bWarehouse", bWarehouse);
		return new ModelAndView("pc/b-view/b-warehouse/b-warehouse-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBWarehouseDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseDetail(String b_warehouse_id,HttpServletRequest request, Model model){
		BWarehouse bWarehouse = bWarehouseService.getBWarehouseById(b_warehouse_id);
		model.addAttribute("bWarehouse", bWarehouse);
		return new ModelAndView("pc/b-view/b-warehouse/b-warehouse-detail");
	}
}
