package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BMember;
import jehc.bmodules.bservice.BMemberService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础会员 
* 2016-01-08 22:35:34  邓纯杰
*/
@Api(value = "基础会员", description = "基础会员")
@Controller
@RequestMapping("/bMemberController")
public class BMemberController extends BaseAction{
	@Autowired
	private BMemberService bMemberService;
	/**
	* 列表页面
	* @param bMember
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBMember",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBMember(BMember bMember,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-member/b-member-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBMemberListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BMember> bMemberList = bMemberService.getBMemberListByCondition(condition);
		PageInfo<BMember> page = new PageInfo<BMember>(bMemberList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_member_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBMemberById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberById(String b_member_id,HttpServletRequest request){
		BMember b_Member = bMemberService.getBMemberById(b_member_id);
		return outDataStr(b_Member);
	}
	/**
	* 添加
	* @param bMember
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBMember",method={RequestMethod.POST,RequestMethod.GET})
	public String addBMember(BMember bMember,HttpServletRequest request){
		int i = 0;
		if(null != bMember){
			bMember.setB_member_id(UUID.toUUID());
			bMember.setB_member_ctime(getDate());
			i=bMemberService.addBMember(bMember);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bMember
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBMember",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBMember(BMember bMember,HttpServletRequest request){
		int i = 0;
		if(null != bMember){
			bMember.setB_member_mtime(getDate());
			i=bMemberService.updateBMember(bMember);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_member_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBMember",method={RequestMethod.POST,RequestMethod.GET})
	public String delBMember(String b_member_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_member_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_member_id",b_member_id.split(","));
			i=bMemberService.delBMember(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_member_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBMember",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBMember(String b_member_id,HttpServletRequest request){
		int i = 0;
		BMember bMember = bMemberService.getBMemberById(b_member_id);
		if(null != bMember){
			bMember.setB_member_id(UUID.toUUID());
			i=bMemberService.addBMember(bMember);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBMember",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBMember(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBMemberAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBMemberAdd(BMember bMember,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-member/b-member-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBMemberUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBMemberUpdate(String b_member_id,HttpServletRequest request, Model model){
		BMember bMember = bMemberService.getBMemberById(b_member_id);
		model.addAttribute("bMember", bMember);
		return new ModelAndView("pc/b-view/b-member/b-member-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBMemberDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBMemberDetail(String b_member_id,HttpServletRequest request, Model model){
		BMember bMember = bMemberService.getBMemberById(b_member_id);
		model.addAttribute("bMember", bMember);
		return new ModelAndView("pc/b-view/b-member/b-member-detail");
	}
}
