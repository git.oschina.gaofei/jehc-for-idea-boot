package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BProductImg;
import jehc.bmodules.bservice.BProductImgService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础商品图片 
* 2016-07-07 20:50:43  邓纯杰
*/
@Api(value = "基础商品图片", description = "基础商品图片")
@Controller
@RequestMapping("/bProductImgController")
public class BProductImgController extends BaseAction{
	@Autowired
	private BProductImgService bProductImgService;
	/**
	* 列表页面
	* @param bProductImg
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBProductImg(BProductImg bProductImg,HttpServletRequest request,Model model){
		model.addAttribute("b_product_id", bProductImg.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-img/b-product-img-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBProductImgListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductImgListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BProductImg> bProductImgList = bProductImgService.getBProductImgListByCondition(condition);
		PageInfo<BProductImg> page = new PageInfo<BProductImg>(bProductImgList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_product_img_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBProductImgById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBProductImgById(String b_product_img_id,HttpServletRequest request){
		BProductImg b_Product_Img = bProductImgService.getBProductImgById(b_product_img_id);
		return outDataStr(b_Product_Img);
	}
	/**
	* 添加
	* @param bProductImg
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public String addBProductImg(BProductImg bProductImg,HttpServletRequest request){
		int i = 0;
		if(null != bProductImg){
			bProductImg.setB_product_img_id(UUID.toUUID());
			bProductImg.setXt_userinfo_id(getXtUid());
			bProductImg.setB_product_img_ctime(getDate());
			i=bProductImgService.addBProductImg(bProductImg);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bProductImg
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBProductImg(BProductImg bProductImg,HttpServletRequest request){
		int i = 0;
		if(null != bProductImg){
			bProductImg.setXt_userinfo_id(getXtUid());
			bProductImg.setB_product_img_mtime(getDate());
			i=bProductImgService.updateBProductImg(bProductImg);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_product_img_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public String delBProductImg(String b_product_img_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_product_img_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_product_img_id",b_product_img_id.split(","));
			i=bProductImgService.delBProductImg(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_product_img_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBProductImg(String b_product_img_id,HttpServletRequest request){
		int i = 0;
		BProductImg bProductImg = bProductImgService.getBProductImgById(b_product_img_id);
		if(null != bProductImg){
			bProductImg.setB_product_img_id(UUID.toUUID());
			i=bProductImgService.addBProductImg(bProductImg);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBProductImg",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBProductImg(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBProductImgAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgAdd(BProductImg bProductImg,HttpServletRequest request, Model model){
		model.addAttribute("b_product_id", bProductImg.getB_product_id());
		return new ModelAndView("pc/b-view/b-product-img/b-product-img-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBProductImgUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgUpdate(String b_product_img_id,HttpServletRequest request, Model model){
		BProductImg bProductImg = bProductImgService.getBProductImgById(b_product_img_id);
		model.addAttribute("bProductImg", bProductImg);
		return new ModelAndView("pc/b-view/b-product-img/b-product-img-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBProductImgDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBProductImgDetail(String b_product_img_id,HttpServletRequest request, Model model){
		BProductImg bProductImg = bProductImgService.getBProductImgById(b_product_img_id);
		model.addAttribute("bProductImg", bProductImg);
		return new ModelAndView("pc/b-view/b-product-img/b-product-img-detail");
	}
}
