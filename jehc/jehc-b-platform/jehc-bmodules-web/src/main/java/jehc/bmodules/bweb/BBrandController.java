package jehc.bmodules.bweb;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BBrand;
import jehc.bmodules.bservice.BBrandService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础-品牌 
* 2016-01-05 12:46:23  邓纯杰
*/
@Api(value = "品牌", description = "基础品牌")
@Controller
@RequestMapping("/bBrandController")
public class BBrandController extends BaseAction{
	@Autowired
	private BBrandService bBrandService;
	/**
	* 列表页面
	* @param bBrand
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBBrand(BBrand bBrand,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-brand/b-brand-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBBrandListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBBrandListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BBrand> b_BrandList = bBrandService.getBBrandListByCondition(condition);
		PageInfo<BBrand> page = new PageInfo<BBrand>(b_BrandList);
		return outPageBootStr(page,request);
	}
	
	/**
	* 查询所有品牌集合
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询所有品牌集合", notes="查询所有品牌集合")
	@ResponseBody
	@RequestMapping(value="/getBBrandList",method={RequestMethod.POST,RequestMethod.GET})
	public List<BBrand> getBBrandList(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		List<BBrand> bBrandList = bBrandService.getBBrandListByCondition(condition);
		return bBrandList;
	}
	
	/**
	* 查询单条记录
	* @param b_brand_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBBrandById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBBrandById(String b_brand_id,HttpServletRequest request){
		BBrand b_Brand = bBrandService.getBBrandById(b_brand_id);
		return outDataStr(b_Brand);
	}
	/**
	* 添加
	* @param bBrand
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public String addBBrand(BBrand bBrand,HttpServletRequest request){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int i = 0;
		if(null != bBrand){
			bBrand.setB_brand_id(UUID.toUUID());
			bBrand.setB_brand_ctime(getDate());
			bBrand.setXt_userinfo_id(CommonUtils.getXtUid());
			i=bBrandService.addBBrand(bBrand);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bBrand
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBBrand(BBrand bBrand,HttpServletRequest request){
		int i = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(null != bBrand){
			bBrand.setB_brand_mtime(getDate());
			bBrand.setXt_userinfo_id(CommonUtils.getXtUid());
			i=bBrandService.updateBBrand(bBrand);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_brand_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public String delBBrand(String b_brand_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_brand_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_brand_id",b_brand_id.split(","));
			i=bBrandService.delBBrand(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_brand_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBBrand(String b_brand_id,HttpServletRequest request){
		int i = 0;
		BBrand b_Brand = bBrandService.getBBrandById(b_brand_id);
		if(null != b_Brand && !"".equals(b_Brand)){
			b_Brand.setB_brand_id(UUID.toUUID());
			i=bBrandService.addBBrand(b_Brand);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBBrand",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBBrand(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBBrandAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBBrandAdd(BBrand bBrand,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-brand/b-brand-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBBrandUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBBrandUpdate(String b_brand_id,HttpServletRequest request, Model model){
		BBrand bBrand = bBrandService.getBBrandById(b_brand_id);
		model.addAttribute("bBrand", bBrand);
		return new ModelAndView("pc/b-view/b-brand/b-brand-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBBrandDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBBrandDetail(String b_brand_id,HttpServletRequest request, Model model){
		BBrand bBrand = bBrandService.getBBrandById(b_brand_id);
		model.addAttribute("bBrand", bBrand);
		return new ModelAndView("pc/b-view/b-brand/b-brand-detail");
	}
}
