package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BSellerContacts;
import jehc.bmodules.bservice.BSellerContactsService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础卖家联系人 
* 2016-02-18 17:11:48  邓纯杰
*/
@Api(value = "基础卖家联系人", description = "基础卖家联系人")
@Controller
@RequestMapping("/bSellerContactsController")
public class BSellerContactsController extends BaseAction{
	@Autowired
	private BSellerContactsService bSellerContactsService;
	/**
	* 列表页面
	* @param bSellerContacts
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBSellerContacts(BSellerContacts bSellerContacts,HttpServletRequest request,String b_seller_id){
		request.setAttribute("b_seller_id", b_seller_id);
		return new ModelAndView("pc/b-view/b-seller-contacts/b-seller-contacts-list");
	}

	/**
	 * 查询并分页
	 * @param b_seller_id
	 * @param baseSearch
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBSellerContactsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerContactsListByCondition(String b_seller_id,BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BSellerContacts> bSellerContactsList = bSellerContactsService.getBSellerContactsListByCondition(condition);
		PageInfo<BSellerContacts> page = new PageInfo<BSellerContacts>(bSellerContactsList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_seller_contacts_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBSellerContactsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBSellerContactsById(String b_seller_contacts_id,HttpServletRequest request){
		BSellerContacts bSellerContacts = bSellerContactsService.getBSellerContactsById(b_seller_contacts_id);
		return outDataStr(bSellerContacts);
	}
	/**
	* 添加
	* @param bSellerContacts
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public String addBSellerContacts(BSellerContacts bSellerContacts,HttpServletRequest request){
		int i = 0;
		if(null != bSellerContacts){
			bSellerContacts.setB_seller_contacts_id(UUID.toUUID());
			i=bSellerContactsService.addBSellerContacts(bSellerContacts);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bSellerContacts
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBSellerContacts(BSellerContacts bSellerContacts,HttpServletRequest request){
		int i = 0;
		if(null != bSellerContacts){
			i=bSellerContactsService.updateBSellerContacts(bSellerContacts);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_seller_contacts_id 
	* @param request 
	*/
    @ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public String delBSellerContacts(String b_seller_contacts_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_seller_contacts_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_seller_contacts_id",b_seller_contacts_id.split(","));
			i=bSellerContactsService.delBSellerContacts(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_seller_contacts_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBSellerContacts(String b_seller_contacts_id,HttpServletRequest request){
		int i = 0;
		BSellerContacts bSellerContacts = bSellerContactsService.getBSellerContactsById(b_seller_contacts_id);
		if(null != bSellerContacts){
			bSellerContacts.setB_seller_contacts_id(UUID.toUUID());
			i=bSellerContactsService.addBSellerContacts(bSellerContacts);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBSellerContacts",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBSellerContacts(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
