package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BMemberAccountProprietary;
import jehc.bmodules.bservice.BMemberAccountProprietaryService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础专有账户 
* 2016-03-24 20:33:38  邓纯杰
*/
@Api(value = "基础专有账户", description = "基础专有账户")
@Controller
@RequestMapping("/bMemberAccountProprietaryController")
public class BMemberAccountProprietaryController extends BaseAction{
	@Autowired
	private BMemberAccountProprietaryService bMemberAccountProprietaryService;
	/**
	* 列表页面
	* @param bMemberAccountProprietary
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBMemberAccountProprietary(BMemberAccountProprietary bMemberAccountProprietary,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-member-account-proprietary/b-member-account-proprietary-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBMemberAccountProprietaryListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAccountProprietaryListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BMemberAccountProprietary> bMemberAccountProprietaryList = bMemberAccountProprietaryService.getBMemberAccountProprietaryListByCondition(condition);
		PageInfo<BMemberAccountProprietary> page = new PageInfo<BMemberAccountProprietary>(bMemberAccountProprietaryList);
		return outPageStr(page,request);
	}

	/**
	 * 查询对象 会员编号 或者专有账户编号
	 * @param b_member_account_proprietary_id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询对象 会员编号 或者专有账户编号", notes="查询对象 会员编号 或者专有账户编号")
	@ResponseBody
	@RequestMapping(value="/getBMemberAccountProprietaryById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBMemberAccountProprietaryById(String b_member_account_proprietary_id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("b_member_account_proprietary_id", b_member_account_proprietary_id);
		BMemberAccountProprietary bMemberAccountProprietary = bMemberAccountProprietaryService.getBMemberAccountProprietaryById(condition);
		return outDataStr(bMemberAccountProprietary);
	}
	/**
	* 添加
	* @param bMemberAccountProprietary
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public String addBMemberAccountProprietary(BMemberAccountProprietary bMemberAccountProprietary,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAccountProprietary){
			bMemberAccountProprietary.setB_member_account_proprietary_id(UUID.toUUID());
			i=bMemberAccountProprietaryService.addBMemberAccountProprietary(bMemberAccountProprietary);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bMemberAccountProprietary
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBMemberAccountProprietary(BMemberAccountProprietary bMemberAccountProprietary,HttpServletRequest request){
		int i = 0;
		if(null != bMemberAccountProprietary){
			i=bMemberAccountProprietaryService.updateBMemberAccountProprietary(bMemberAccountProprietary);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_member_account_proprietary_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public String delBMemberAccountProprietary(String b_member_account_proprietary_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_member_account_proprietary_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_member_account_proprietary_id",b_member_account_proprietary_id.split(","));
			i=bMemberAccountProprietaryService.delBMemberAccountProprietary(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_member_account_proprietary_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBMemberAccountProprietary(String b_member_account_proprietary_id,HttpServletRequest request){
		int i = 0;
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("b_member_account_proprietary_id", b_member_account_proprietary_id);
		BMemberAccountProprietary bMemberAccountProprietary = bMemberAccountProprietaryService.getBMemberAccountProprietaryById(condition);
		if(null != bMemberAccountProprietary){
			bMemberAccountProprietary.setB_member_account_proprietary_id(UUID.toUUID());
			i=bMemberAccountProprietaryService.addBMemberAccountProprietary(bMemberAccountProprietary);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBMemberAccountProprietary",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBMemberAccountProprietary(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
