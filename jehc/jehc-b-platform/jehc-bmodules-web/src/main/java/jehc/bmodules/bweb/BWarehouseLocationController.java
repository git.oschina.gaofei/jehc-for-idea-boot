package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BWarehouse;
import jehc.bmodules.bmodel.BWarehouseLocation;
import jehc.bmodules.bservice.BWarehouseLocationService;
import jehc.bmodules.bservice.BWarehouseService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础仓库库位 
* 2016-01-27 14:25:28  邓纯杰
*/
@Api(value = "基础仓库库位", description = "基础仓库库位")
@Controller
@RequestMapping("/bWarehouseLocationController")
public class BWarehouseLocationController extends BaseAction{
	@Autowired
	private BWarehouseLocationService bWarehouseLocationService;
	@Autowired
	private BWarehouseService bWarehouseService;

	/**
	 * 查询页面
	 * @param bWarehouse
	 * @param request
	 * @param model
	 * @return
	 */
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBWarehouseLocation(BWarehouse bWarehouse,HttpServletRequest request, Model model){
		model.addAttribute("bWarehouse", bWarehouse=bWarehouseService.getBWarehouseById(bWarehouse.getB_warehouse_id()));
		return new ModelAndView("pc/b-view/b-warehouse-location/b-warehouse-location-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBWarehouseLocationListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBWarehouseLocationListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BWarehouseLocation> bWarehouseLocationList = bWarehouseLocationService.getBWarehouseLocationListByCondition(condition);
		PageInfo<BWarehouseLocation> page = new PageInfo<BWarehouseLocation>(bWarehouseLocationList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_warehouse_location_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBWarehouseLocationById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBWarehouseLocationById(String b_warehouse_location_id,HttpServletRequest request){
		BWarehouseLocation bWarehouseLocation = bWarehouseLocationService.getBWarehouseLocationById(b_warehouse_location_id);
		return outDataStr(bWarehouseLocation);
	}
	/**
	* 添加
	* @param bWarehouseLocation
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public String addBWarehouseLocation(BWarehouseLocation bWarehouseLocation,HttpServletRequest request){
		int i = 0;
		if(null != bWarehouseLocation){
			bWarehouseLocation.setB_warehouse_location_id(UUID.toUUID());
			i=bWarehouseLocationService.addBWarehouseLocation(bWarehouseLocation);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bWarehouseLocation
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBWarehouseLocation(BWarehouseLocation bWarehouseLocation,HttpServletRequest request){
		int i = 0;
		if(null != bWarehouseLocation){
			i=bWarehouseLocationService.updateBWarehouseLocation(bWarehouseLocation);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_warehouse_location_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public String delBWarehouseLocation(String b_warehouse_location_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_warehouse_location_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_warehouse_location_id",b_warehouse_location_id.split(","));
			i=bWarehouseLocationService.delBWarehouseLocation(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_warehouse_location_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBWarehouseLocation(String b_warehouse_location_id,HttpServletRequest request){
		int i = 0;
		BWarehouseLocation bWarehouseLocation = bWarehouseLocationService.getBWarehouseLocationById(b_warehouse_location_id);
		if(null != bWarehouseLocation){
			bWarehouseLocation.setB_warehouse_location_id(UUID.toUUID());
			i=bWarehouseLocationService.addBWarehouseLocation(bWarehouseLocation);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBWarehouseLocation",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBWarehouseLocation(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBWarehouseLocationAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseLocationAdd(BWarehouse bWarehouse,HttpServletRequest request, Model model){
		model.addAttribute("bWarehouse", bWarehouse=bWarehouseService.getBWarehouseById(bWarehouse.getB_warehouse_id()));
		return new ModelAndView("pc/b-view/b-warehouse-location/b-warehouse-location-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBWarehouseLocationUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseLocationUpdate(String b_warehouse_location_id,HttpServletRequest request, Model model){
		BWarehouseLocation bWarehouseLocation = bWarehouseLocationService.getBWarehouseLocationById(b_warehouse_location_id);
		model.addAttribute("bWarehouseLocation", bWarehouseLocation);
		return new ModelAndView("pc/b-view/b-warehouse-location/b-warehouse-location-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBWarehouseLocationDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBWarehouseLocationDetail(String b_warehouse_location_id,HttpServletRequest request, Model model){
		BWarehouseLocation bWarehouseLocation = bWarehouseLocationService.getBWarehouseLocationById(b_warehouse_location_id);
		model.addAttribute("bWarehouseLocation", bWarehouseLocation);
		return new ModelAndView("pc/b-view/b-warehouse-location/b-warehouse-location-detail");
	}
}
