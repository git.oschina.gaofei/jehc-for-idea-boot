package jehc.bmodules.bweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.bmodules.bmodel.BFriendshipLink;
import jehc.bmodules.bservice.BFriendshipLinkService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 基础友情链接 
* 2016-01-10 12:35:06  邓纯杰
*/
@Api(value = "基础友情链接", description = "基础友情链接")
@Controller
@RequestMapping("/bFriendshipLinkController")
public class BFriendshipLinkController extends BaseAction{
	@Autowired
	private BFriendshipLinkService bFriendshipLinkService;
	/**
	* 列表页面
	* @param bFriendshipLink
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadBFriendshipLink(BFriendshipLink bFriendshipLink,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-friendship-link/b-friendship-link-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getBFriendshipLinkListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getBFriendshipLinkListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<BFriendshipLink> bFriendshipLinkList = bFriendshipLinkService.getBFriendshipLinkListByCondition(condition);
		PageInfo<BFriendshipLink> page = new PageInfo<BFriendshipLink>(bFriendshipLinkList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param b_friendship_link_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getBFriendshipLinkById",method={RequestMethod.POST,RequestMethod.GET})
	public String getBFriendshipLinkById(String b_friendship_link_id,HttpServletRequest request){
		BFriendshipLink b_Friendship_Link = bFriendshipLinkService.getBFriendshipLinkById(b_friendship_link_id);
		return outDataStr(b_Friendship_Link);
	}
	/**
	* 添加
	* @param bFriendshipLink
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public String addBFriendshipLink(BFriendshipLink bFriendshipLink,HttpServletRequest request){
		int i = 0;
		if(null != bFriendshipLink){
			bFriendshipLink.setB_friendship_link_id(UUID.toUUID());
			bFriendshipLink.setB_friendship_link_ctime(getDate());
			bFriendshipLink.setXt_userinfo_id(getXtUid());
			i=bFriendshipLinkService.addBFriendshipLink(bFriendshipLink);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param bFriendshipLink
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public String updateBFriendshipLink(BFriendshipLink bFriendshipLink,HttpServletRequest request){
		int i = 0;
		if(null != bFriendshipLink){
			bFriendshipLink.setXt_userinfo_id(getXtUid());
			bFriendshipLink.setB_friendship_link_mtime(getDate());
			i=bFriendshipLinkService.updateBFriendshipLink(bFriendshipLink);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param b_friendship_link_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public String delBFriendshipLink(String b_friendship_link_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(b_friendship_link_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_friendship_link_id",b_friendship_link_id.split(","));
			i=bFriendshipLinkService.delBFriendshipLink(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param b_friendship_link_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public String copyBFriendshipLink(String b_friendship_link_id,HttpServletRequest request){
		int i = 0;
		BFriendshipLink bFriendshipLink = bFriendshipLinkService.getBFriendshipLinkById(b_friendship_link_id);
		if(null != bFriendshipLink){
			bFriendshipLink.setB_friendship_link_id(UUID.toUUID());
			i=bFriendshipLinkService.addBFriendshipLink(bFriendshipLink);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportBFriendshipLink",method={RequestMethod.POST,RequestMethod.GET})
	public void exportBFriendshipLink(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toBFriendshipLinkAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBFriendshipLinkAdd(BFriendshipLink bFriendshipLink,HttpServletRequest request){
		return new ModelAndView("pc/b-view/b-friendship-link/b-friendship-link-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toBFriendshipLinkUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBFriendshipLinkUpdate(String b_friendship_link_id,HttpServletRequest request, Model model){
		BFriendshipLink bFriendshipLink = bFriendshipLinkService.getBFriendshipLinkById(b_friendship_link_id);
		model.addAttribute("bFriendshipLink", bFriendshipLink);
		return new ModelAndView("pc/b-view/b-friendship-link/b-friendship-link-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toBFriendshipLinkDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toBFriendshipLinkDetail(String b_friendship_link_id,HttpServletRequest request, Model model){
		BFriendshipLink bFriendshipLink = bFriendshipLinkService.getBFriendshipLinkById(b_friendship_link_id);
		model.addAttribute("bFriendshipLink", bFriendshipLink);
		return new ModelAndView("pc/b-view/b-friendship-link/b-friendship-link-detail");
	}
}
