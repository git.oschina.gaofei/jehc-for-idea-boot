package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BProductImgDefaultDao;
import jehc.bmodules.bmodel.BProductImgDefault;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础商品默认图片 
* 2016-01-09 09:06:38  邓纯杰
*/
@Repository("bProductImgDefaultDao")
public class BProductImgDefaultDaoImpl  extends BaseDaoImpl implements BProductImgDefaultDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BProductImgDefault> getBProductImgDefaultListByCondition(Map<String,Object> condition){
		return (List<BProductImgDefault>)this.getList("getBProductImgDefaultListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_product_img_default_id 
	* @return
	*/
	public BProductImgDefault getBProductImgDefaultById(String b_product_img_default_id){
		return (BProductImgDefault)this.get("getBProductImgDefaultById", b_product_img_default_id);
	}
	/**
	* 添加
	* @param bProductImgDefault
	* @return
	*/
	public int addBProductImgDefault(BProductImgDefault bProductImgDefault){
		return this.add("addBProductImgDefault", bProductImgDefault);
	}
	/**
	* 修改
	* @param bProductImgDefault
	* @return
	*/
	public int updateBProductImgDefault(BProductImgDefault bProductImgDefault){
		return this.update("updateBProductImgDefault", bProductImgDefault);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBProductImgDefault(Map<String,Object> condition){
		return this.del("delBProductImgDefault", condition);
	}
}
