package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BSellerContactsDao;
import jehc.bmodules.bmodel.BSellerContacts;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础卖家联系人 
* 2016-02-18 17:11:48  邓纯杰
*/
@Repository("bSellerContactsDao")
public class BSellerContactsDaoImpl  extends BaseDaoImpl implements BSellerContactsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BSellerContacts> getBSellerContactsListByCondition(Map<String,Object> condition){
		return (List<BSellerContacts>)this.getList("getBSellerContactsListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_seller_contacts_id 
	* @return
	*/
	public BSellerContacts getBSellerContactsById(String b_seller_contacts_id){
		return (BSellerContacts)this.get("getBSellerContactsById", b_seller_contacts_id);
	}
	/**
	* 添加
	* @param bSellerContacts
	* @return
	*/
	public int addBSellerContacts(BSellerContacts bSellerContacts){
		return this.add("addBSellerContacts", bSellerContacts);
	}
	/**
	* 修改
	* @param bSellerContacts
	* @return
	*/
	public int updateBSellerContacts(BSellerContacts bSellerContacts){
		return this.update("updateBSellerContacts", bSellerContacts);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBSellerContacts(Map<String,Object> condition){
		return this.del("delBSellerContacts", condition);
	}
}
