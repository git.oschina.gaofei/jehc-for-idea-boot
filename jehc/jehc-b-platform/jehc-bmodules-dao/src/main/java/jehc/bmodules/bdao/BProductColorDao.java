package jehc.bmodules.bdao;
import java.util.List;
import java.util.Map;

import jehc.bmodules.bmodel.BProductColor;

/**
* 基础商品商户所选颜色
* 待采用SKU
* 2016-07-02 16:54:11  邓纯杰
*/
public interface BProductColorDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BProductColor> getBProductColorListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param b_product_color_id 
	* @return
	*/
	public BProductColor getBProductColorById(String b_product_color_id);
	/**
	* 添加
	* @param bProductColor
	* @return
	*/
	public int addBProductColor(BProductColor bProductColor);
	/**
	* 修改
	* @param bProductColor
	* @return
	*/
	public int updateBProductColor(BProductColor bProductColor);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBProductColor(Map<String,Object> condition);
}
