package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BMemberAccountDao;
import jehc.bmodules.bmodel.BMemberAccount;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础会员余额账户 
* 2016-03-24 20:30:14  邓纯杰
*/
@Repository("bMemberAccountDao")
public class BMemberAccountDaoImpl  extends BaseDaoImpl implements BMemberAccountDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BMemberAccount> getBMemberAccountListByCondition(Map<String,Object> condition){
		return (List<BMemberAccount>)this.getList("getBMemberAccountListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_member_account_id 
	* @return
	*/
	public BMemberAccount getBMemberAccountById(String b_member_account_id){
		return (BMemberAccount)this.get("getBMemberAccountById", b_member_account_id);
	}
	/**
	* 添加
	* @param bMemberAccount
	* @return
	*/
	public int addBMemberAccount(BMemberAccount bMemberAccount){
		return this.add("addBMemberAccount", bMemberAccount);
	}
	/**
	* 修改
	* @param bMemberAccount
	* @return
	*/
	public int updateBMemberAccount(BMemberAccount bMemberAccount){
		return this.update("updateBMemberAccount", bMemberAccount);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBMemberAccount(Map<String,Object> condition){
		return this.del("delBMemberAccount", condition);
	}
}
