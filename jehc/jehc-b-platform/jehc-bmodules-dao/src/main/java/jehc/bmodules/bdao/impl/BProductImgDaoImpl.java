package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BProductImgDao;
import jehc.bmodules.bmodel.BProductImg;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础商品图片 （待采用SKU）
* 2016-07-07 20:50:43  邓纯杰
*/
@Repository("bProductImgDao")
public class BProductImgDaoImpl  extends BaseDaoImpl implements BProductImgDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BProductImg> getBProductImgListByCondition(Map<String,Object> condition){
		return (List<BProductImg>)this.getList("getBProductImgListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_product_img_id 
	* @return
	*/
	public BProductImg getBProductImgById(String b_product_img_id){
		return (BProductImg)this.get("getBProductImgById", b_product_img_id);
	}
	/**
	* 添加
	* @param bProductImg
	* @return
	*/
	public int addBProductImg(BProductImg bProductImg){
		return this.add("addBProductImg", bProductImg);
	}
	/**
	* 修改
	* @param bProductImg
	* @return
	*/
	public int updateBProductImg(BProductImg bProductImg){
		return this.update("updateBProductImg", bProductImg);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBProductImg(Map<String,Object> condition){
		return this.del("delBProductImg", condition);
	}
}
