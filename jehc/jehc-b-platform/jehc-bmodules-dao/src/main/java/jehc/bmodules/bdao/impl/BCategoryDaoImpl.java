package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BCategoryDao;
import jehc.bmodules.bmodel.BCategory;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础-品类 
* 2016-01-04 21:24:03  邓纯杰
*/
@Repository("bCategoryDao")
public class BCategoryDaoImpl  extends BaseDaoImpl implements BCategoryDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BCategory> getBCategoryListByCondition(Map<String,Object> condition){
		return (List<BCategory>)this.getList("getBCategoryListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_category_id 
	* @return
	*/
	public BCategory getBCategoryById(String b_category_id){
		return (BCategory)this.get("getBCategoryById", b_category_id);
	}
	/**
	* 添加
	* @param bCategory
	* @return
	*/
	public int addBCategory(BCategory bCategory){
		return this.add("addBCategory", bCategory);
	}
	/**
	* 修改
	* @param bCategory
	* @return
	*/
	public int updateBCategory(BCategory bCategory){
		return this.update("updateBCategory", bCategory);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBCategory(Map<String,Object> condition){
		return this.del("delBCategory", condition);
	}
	/**
	 * 读取所有数据根据条件
	 * @param condition
	 * @return
	 */
	public List<BCategory> getBCategoryListAllByCondition(Map<String,Object> condition){
		return (List<BCategory>)this.getList("getBCategoryListAllByCondition",condition);
	}
	
	/**
	 * 根据条件查找集合（前端提供）
	 * @param condition
	 * @return
	 */
	public List<BCategory> getBCategoryListForFrontByCondition(Map<String,Object> condition){
		return (List<BCategory>)this.getList("getBCategoryListForFrontByCondition",condition);
	}
}
