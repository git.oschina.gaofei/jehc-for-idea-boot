package jehc.bmodules.bdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.bmodules.bdao.BOrderDao;
import jehc.bmodules.bmodel.BOrder;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 基础订单 
* 2016-01-27 13:55:11  邓纯杰
*/
@Repository("bOrderDao")
public class BOrderDaoImpl  extends BaseDaoImpl implements BOrderDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BOrder> getBOrderListByCondition(Map<String,Object> condition){
		return (List<BOrder>)this.getList("getBOrderListByCondition",condition);
	}
	/**
	* 查询对象
	* @param b_order_id 
	* @return
	*/
	public BOrder getBOrderById(String b_order_id){
		return (BOrder)this.get("getBOrderById", b_order_id);
	}
	/**
	* 添加
	* @param bOrder
	* @return
	*/
	public int addBOrder(BOrder bOrder){
		return this.add("addBOrder", bOrder);
	}
	/**
	* 修改
	* @param bOrder
	* @return
	*/
	public int updateBOrder(BOrder bOrder){
		return this.update("updateBOrder", bOrder);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBOrder(Map<String,Object> condition){
		return this.del("delBOrder", condition);
	}
	/**
	 * 更新订单类型
	 * @param bOrder
	 */
	public int updateBOrderType(BOrder bOrder){
		return this.update("updateBOrderType", bOrder);
	}
}
