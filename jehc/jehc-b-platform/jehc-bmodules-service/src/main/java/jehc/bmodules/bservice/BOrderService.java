package jehc.bmodules.bservice;
import java.util.List;
import java.util.Map;

import jehc.bmodules.bmodel.BOrder;

/**
* 基础订单 
* 2016-01-27 13:55:11  邓纯杰
*/
public interface BOrderService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BOrder> getBOrderListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param b_order_id 
	* @return
	*/
	public BOrder getBOrderById(String b_order_id);
	/**
	* 添加
	* @param bOrder
	* @return
	*/
	public int addBOrder(BOrder bOrder);
	/**
	* 修改
	* @param bOrder
	* @return
	*/
	public int updateBOrder(BOrder bOrder);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBOrder(Map<String,Object> condition);
	/**
	 * 更新订单类型
	 * @param bOrder
	 */
	public int updateBOrderType(BOrder bOrder);
}
