package jehc.bmodules.bservice;
import java.util.List;
import java.util.Map;

import jehc.bmodules.bmodel.BCart;
import jehc.bmodules.bmodel.BCartDetail;
import jehc.bmodules.bmodel.BCartOrderAddress;

/**
* 基础购物车 
* 2016-01-27 13:36:04  邓纯杰
*/
public interface BCartService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BCart> getBCartListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param b_cart_id 
	* @return
	*/
	public BCart getBCartById(String b_cart_id);

	/**
	 * 添加
	 * @param bCart
	 * @param bCartDetailList
	 * @param bCartOrderAddress
	 * @return
	 */
	public int addBCart(BCart bCart,List<BCartDetail> bCartDetailList,BCartOrderAddress bCartOrderAddress);

	/**
	 * 修改
	 * @param bCart
	 * @param bCartDetailList
	 * @param bCartOrderAddress
	 * @return
	 */
	public int updateBCart(BCart bCart,List<BCartDetail> bCartDetailList,BCartOrderAddress bCartOrderAddress);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBCart(Map<String,Object> condition);
	/**
	 * 根据购物车编号获取购物车订单地址集合编号
	 * @param condition
	 * @return
	 */
	public List<String> getBCartOrderAddressIdByCondition(Map<String,Object> condition);
	/**
	 * 根据购物车编号集合
	 * @param condition
	 * @return
	 */
	public List<BCart> getBCartList(Map<String,Object> condition);
	
	/**
	 * 单个购物车转订单转换
	 * @param b_cart_id
	 * @return
	 */
	public int singleBCartTBOrderPoulators(String b_cart_id);
	/**
	 * 批量购物车转订单转换
	 * @param b_cart_id
	 * @return
	 */
	public int batchBCartTBOrderPoulators(String b_cart_id);
}
