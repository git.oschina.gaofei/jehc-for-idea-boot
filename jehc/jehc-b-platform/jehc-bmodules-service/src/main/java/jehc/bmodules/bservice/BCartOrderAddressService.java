package jehc.bmodules.bservice;
import java.util.List;
import java.util.Map;

import jehc.bmodules.bmodel.BCartOrderAddress;

/**
* 基础购物车订单常用配送地址 
* 2016-02-22 21:17:25  邓纯杰
*/
public interface BCartOrderAddressService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BCartOrderAddress> getBCartOrderAddressListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param b_cart_order_address_id 
	* @return
	*/
	public BCartOrderAddress getBCartOrderAddressById(String b_cart_order_address_id);
	/**
	* 添加
	* @param bCartOrderAddress
	* @return
	*/
	public int addBCartOrderAddress(BCartOrderAddress bCartOrderAddress);
	/**
	* 修改
	* @param bCartOrderAddress
	* @return
	*/
	public int updateBCartOrderAddress(BCartOrderAddress bCartOrderAddress);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBCartOrderAddress(Map<String,Object> condition);
}
