package jehc.bmodules.bservice;
import java.util.List;
import java.util.Map;

import jehc.bmodules.bmodel.BMemberAccountProprietary;

/**
* 基础专有账户 
* 2016-03-24 20:33:38  邓纯杰
*/
public interface BMemberAccountProprietaryService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BMemberAccountProprietary> getBMemberAccountProprietaryListByCondition(Map<String,Object> condition);
	/**
	* 查询对象 会员编号 或者专有账户编号
	* @param condition 
	* @return
	*/
	public BMemberAccountProprietary getBMemberAccountProprietaryById(Map<String, Object> condition);
	/**
	* 添加
	* @param bMemberAccountProprietary
	* @return
	*/
	public int addBMemberAccountProprietary(BMemberAccountProprietary bMemberAccountProprietary);
	/**
	* 修改
	* @param bMemberAccountProprietary
	* @return
	*/
	public int updateBMemberAccountProprietary(BMemberAccountProprietary bMemberAccountProprietary);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBMemberAccountProprietary(Map<String,Object> condition);
}
