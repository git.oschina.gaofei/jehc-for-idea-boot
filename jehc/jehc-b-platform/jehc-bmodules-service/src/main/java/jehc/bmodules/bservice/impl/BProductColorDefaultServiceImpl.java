package jehc.bmodules.bservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.bmodules.bdao.BProductColorDefaultDao;
import jehc.bmodules.bmodel.BProductColorDefault;
import jehc.bmodules.bservice.BProductColorDefaultService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* 基础商品默认颜色 
* 2016-01-19 15:38:05  邓纯杰
*/
@Service("bProductColorDefaultService")
public class BProductColorDefaultServiceImpl extends BaseService implements BProductColorDefaultService{
	@Autowired
	private BProductColorDefaultDao bProductColorDefaultDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BProductColorDefault> getBProductColorDefaultListByCondition(Map<String,Object> condition){
		try{
			return bProductColorDefaultDao.getBProductColorDefaultListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param b_product_color_default_id 
	* @return
	*/
	public BProductColorDefault getBProductColorDefaultById(String b_product_color_default_id){
		try{
			return bProductColorDefaultDao.getBProductColorDefaultById(b_product_color_default_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param bProductColorDefault
	* @return
	*/
	public int addBProductColorDefault(BProductColorDefault bProductColorDefault){
		int i = 0;
		try {
			i = bProductColorDefaultDao.addBProductColorDefault(bProductColorDefault);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param bProductColorDefault
	* @return
	*/
	public int updateBProductColorDefault(BProductColorDefault bProductColorDefault){
		int i = 0;
		try {
			i = bProductColorDefaultDao.updateBProductColorDefault(bProductColorDefault);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBProductColorDefault(Map<String,Object> condition){
		int i = 0;
		try {
			i = bProductColorDefaultDao.delBProductColorDefault(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
