package jehc.bmodules.bservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.bmodules.bdao.BMemberAddressDao;
import jehc.bmodules.bmodel.BMemberAddress;
import jehc.bmodules.bservice.BMemberAddressService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* 基础会员常用地址 
* 2016-02-22 16:44:23  邓纯杰
*/
@Service("bMemberAddressService")
public class BMemberAddressServiceImpl extends BaseService implements BMemberAddressService{
	@Autowired
	private BMemberAddressDao bMemberAddressDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BMemberAddress> getBMemberAddressListByCondition(Map<String,Object> condition){
		try{
			return bMemberAddressDao.getBMemberAddressListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param b_member_address_id 
	* @return
	*/
	public BMemberAddress getBMemberAddressById(String b_member_address_id){
		try{
			return bMemberAddressDao.getBMemberAddressById(b_member_address_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param bMemberAddress
	* @return
	*/
	public int addBMemberAddress(BMemberAddress bMemberAddress){
		int i = 0;
		try {
			i = bMemberAddressDao.addBMemberAddress(bMemberAddress);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param bMemberAddress
	* @return
	*/
	public int updateBMemberAddress(BMemberAddress bMemberAddress){
		int i = 0;
		try {
			i = bMemberAddressDao.updateBMemberAddress(bMemberAddress);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBMemberAddress(Map<String,Object> condition){
		int i = 0;
		try {
			i = bMemberAddressDao.delBMemberAddress(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
