package jehc.bmodules.bservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.bmodules.bdao.BWarehouseLocationDao;
import jehc.bmodules.bmodel.BWarehouseLocation;
import jehc.bmodules.bservice.BWarehouseLocationService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* 基础仓库库位 
* 2016-01-27 14:25:28  邓纯杰
*/
@Service("bWarehouseLocationService")
public class BWarehouseLocationServiceImpl extends BaseService implements BWarehouseLocationService{
	@Autowired
	private BWarehouseLocationDao bWarehouseLocationDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BWarehouseLocation> getBWarehouseLocationListByCondition(Map<String,Object> condition){
		try{
			return bWarehouseLocationDao.getBWarehouseLocationListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param b_warehouse_location_id 
	* @return
	*/
	public BWarehouseLocation getBWarehouseLocationById(String b_warehouse_location_id){
		try{
			return bWarehouseLocationDao.getBWarehouseLocationById(b_warehouse_location_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param bWarehouseLocation
	* @return
	*/
	public int addBWarehouseLocation(BWarehouseLocation bWarehouseLocation){
		int i = 0;
		try {
			i = bWarehouseLocationDao.addBWarehouseLocation(bWarehouseLocation);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param bWarehouseLocation
	* @return
	*/
	public int updateBWarehouseLocation(BWarehouseLocation bWarehouseLocation){
		int i = 0;
		try {
			i = bWarehouseLocationDao.updateBWarehouseLocation(bWarehouseLocation);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBWarehouseLocation(Map<String,Object> condition){
		int i = 0;
		try {
			i = bWarehouseLocationDao.delBWarehouseLocation(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
