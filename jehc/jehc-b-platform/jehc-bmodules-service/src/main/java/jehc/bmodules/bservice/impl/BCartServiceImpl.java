package jehc.bmodules.bservice.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.bmodules.bdao.BCartDao;
import jehc.bmodules.bmodel.BCart;
import jehc.bmodules.bmodel.BCartDetail;
import jehc.bmodules.bmodel.BCartOrderAddress;
import jehc.bmodules.bmodel.BOrder;
import jehc.bmodules.bmodel.BOrderDetail;
import jehc.bmodules.bservice.BCartService;
import jehc.bmodules.bservice.BCartDetailService;
import jehc.bmodules.bservice.BCartOrderAddressService;
import jehc.bmodules.bservice.BOrderService;
import jehc.bmodules.bservice.BOrderDetailService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtcore.util.UUID;

/**
* 基础购物车 
* 2016-01-27 13:36:04  邓纯杰
*/
@Service("bCartService")
public class BCartServiceImpl extends BaseService implements BCartService{
	@Autowired
	private BCartDao bCartDao;
	@Autowired
	private BCartDetailService bCartDetailService;
	@Autowired
	private BCartOrderAddressService bCartOrderAddressService;
	
	@Autowired
	private BOrderService bOrderService;
	@Autowired
	private BOrderDetailService bOrderDetailService;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<BCart> getBCartListByCondition(Map<String,Object> condition){
		try{
			return bCartDao.getBCartListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param b_cart_id 
	* @return
	*/
	public BCart getBCartById(String b_cart_id){
		try{
			return bCartDao.getBCartById(b_cart_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 添加
	 * @param bCart
	 * @param bCartDetailList
	 * @param bCartOrderAddress
	 * @return
	 */
	public int addBCart(BCart bCart,List<BCartDetail> bCartDetailList,BCartOrderAddress bCartOrderAddress){
		int i = 0;
		try {
			bCartOrderAddressService.addBCartOrderAddress(bCartOrderAddress);
			bCart.setB_cart_order_address_id(bCartOrderAddress.getB_cart_order_address_id());
			bCartDao.addBCart(bCart);
			for(int j = 0; j < bCartDetailList.size(); j++){
				bCartDetailService.addBCartDetail(bCartDetailList.get(j));
			}
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 修改
	 * @param bCart
	 * @param bCartDetailList
	 * @param bCartOrderAddress
	 * @return
	 */
	public int updateBCart(BCart bCart,List<BCartDetail> bCartDetailList,BCartOrderAddress bCartOrderAddress){
		int i = 0;
		try {
			bCartOrderAddressService.updateBCartOrderAddress(bCartOrderAddress);
			bCartDao.updateBCart(bCart);
			for(int j = 0; j < bCartDetailList.size(); j++){
				bCartDetailList.get(j).setB_cart_id(bCart.getB_cart_id());
				if(null != bCartDetailList.get(j)  && null != bCartDetailList.get(j).getB_cart_detail_id()){
					bCartDetailList.get(j).setB_cart_detail_mtime(getSimpleDateFormat());
					bCartDetailService.updateBCartDetail(bCartDetailList.get(j));
				}else{
					bCartDetailList.get(j).setB_cart_detail_id(UUID.toUUID());
					bCartDetailList.get(j).setB_cart_detail_ctime(getSimpleDateFormat());
					bCartDetailService.addBCartDetail(bCartDetailList.get(j));
				}
			}
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delBCart(Map<String,Object> condition){
		int i = 0;
		try {
			List<String> b_cart_order_address_idList = bCartDao.getBCartOrderAddressIdByCondition(condition);
			String b_cart_order_address_id ="";
			for(int j =0; j < b_cart_order_address_idList.size();j++){
				if(null != b_cart_order_address_id && !"".equals(b_cart_order_address_id)){
					b_cart_order_address_id = b_cart_order_address_id+","+b_cart_order_address_idList.get(j);
				}else{
					b_cart_order_address_id = b_cart_order_address_idList.get(j);
				}
			}
			bCartDao.delBCart(condition);
			bCartDetailService.delBCartDetailByBCartId(condition);
			condition = new HashMap<String, Object>();
			condition.put("b_cart_order_address_id",b_cart_order_address_id.split(","));
			i = bCartOrderAddressService.delBCartOrderAddress(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 根据购物车编号获取购物车订单地址集合编号
	 * @param condition
	 * @return
	 */
	public List<String> getBCartOrderAddressIdByCondition(Map<String,Object> condition){
		try{
			return bCartDao.getBCartOrderAddressIdByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 根据购物车编号集合
	 * @param condition
	 * @return
	 */
	public List<BCart> getBCartList(Map<String,Object> condition){
		try{
			return bCartDao.getBCartList(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 单个购物车转订单转换
	 * @param b_cart_id
	 * @return
	 */
	public int singleBCartTBOrderPoulators(String b_cart_id){
		int i = 0;
		try {
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_cart_id", b_cart_id);
			BOrder bOrder = new BOrder();
			BCart b_cart = bCartDao.getBCartById(b_cart_id);
			List<BCartDetail> bCartDetailList = bCartDetailService.getBCartDetailListByCondition(condition);
			BCartOrderAddress bCartOrderAddress = bCartOrderAddressService.getBCartOrderAddressById(b_cart.getB_cart_order_address_id());
			bCartOrderAddress.setB_cart_order_address_id(UUID.toUUID());
			bOrder.setB_cart_order_address_id(b_cart.getB_cart_order_address_id());
			bOrder.setB_order_name("购物车转订单");
			bOrder.setB_invoice_id(b_cart.getB_invoice_id());
			bOrder.setB_order_ctime(getSimpleDateFormat());
			bOrder.setB_order_from(b_cart.getB_cart_from());
			bOrder.setB_order_id(UUID.toUUID());
			bOrder.setB_order_key(b_cart.getB_cart_orderkey());
			bOrder.setB_order_remark(b_cart.getB_cart_remark());
			bOrder.setB_order_sessionid(b_cart.getB_cart_sessionid());
			bOrder.setB_order_status("0");/**正常订单**/
			bOrder.setB_order_total_number(b_cart.getB_cart_total_number());
			bOrder.setB_order_total_price(b_cart.getB_cart_total_price());
			bOrder.setB_cart_order_address_id(bCartOrderAddress.getB_cart_order_address_id());
			bOrder.setB_member_id(b_cart.getB_member_id());
			bOrder.setB_order_type("0");/**待付订单**/
			bCartOrderAddressService.addBCartOrderAddress(bCartOrderAddress);
			bOrderService.addBOrder(bOrder);
			for(int j = 0; j < bCartDetailList.size(); j++){
				BCartDetail bCartDetail = bCartDetailList.get(j);
				BOrderDetail bOrderDetail = new BOrderDetail();
				bOrderDetail.setB_order_detail_id(UUID.toUUID());
				bOrderDetail.setB_order_detail_ctime(getSimpleDateFormat());
				bOrderDetail.setB_order_detail_discount(bCartDetail.getB_cart_detail_discount());
				bOrderDetail.setB_order_detail_number(bCartDetail.getB_cart_detail_number());
				bOrderDetail.setB_order_detail_price(bCartDetail.getB_cart_detail_price());
				bOrderDetail.setB_product_id(bCartDetail.getB_product_id());
				bOrderDetail.setB_seller_id(bCartDetail.getB_seller_id());
				bOrderDetail.setB_order_id(bOrder.getB_order_id());
				bOrderDetailService.addBOrderDetail(bOrderDetail);
			}
			condition.put("b_cart_id", b_cart_id.split(","));
			//删除
			delBCart(condition);
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 批量购物车转订单转换
	 * @param b_cart_id
	 * @return
	 */
	public int batchBCartTBOrderPoulators(String b_cart_id){
		int i = 0;
		try {
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("b_cart_id", b_cart_id.split(","));
			List<BCart> bCartList = bCartDao.getBCartListByCondition(condition);
			for(BCart bCart:bCartList){
				BOrder bOrder = new BOrder();
				condition.put("b_cart_id", bCart.getB_cart_id());
				List<BCartDetail> bCartDetailList = bCartDetailService.getBCartDetailListByCondition(condition);
				BCartOrderAddress bCartOrderAddress = bCartOrderAddressService.getBCartOrderAddressById(bCart.getB_cart_order_address_id());
				bCartOrderAddress.setB_cart_order_address_id(UUID.toUUID());
				bOrder.setB_cart_order_address_id(bCart.getB_cart_order_address_id());
				bOrder.setB_order_name("购物车转订单");
				bOrder.setB_invoice_id(bCart.getB_invoice_id());
				bOrder.setB_order_ctime(getSimpleDateFormat());
				bOrder.setB_order_from(bCart.getB_cart_from());
				bOrder.setB_order_id(UUID.toUUID());
				bOrder.setB_order_key(bCart.getB_cart_orderkey());
				bOrder.setB_order_remark(bCart.getB_cart_remark());
				bOrder.setB_order_sessionid(bCart.getB_cart_sessionid());
				bOrder.setB_order_status("0");/**正常订单**/
				bOrder.setB_order_total_number(bCart.getB_cart_total_number());
				bOrder.setB_order_total_price(bCart.getB_cart_total_price());
				bOrder.setB_cart_order_address_id(bCartOrderAddress.getB_cart_order_address_id());
				bOrder.setB_member_id(bCart.getB_member_id());
				bOrder.setB_order_type("0");/**待付订单**/
				bCartOrderAddressService.addBCartOrderAddress(bCartOrderAddress);
				bOrderService.addBOrder(bOrder);
				for(int j = 0; j < bCartDetailList.size(); j++){
					BCartDetail bCartDetail = bCartDetailList.get(j);
					BOrderDetail bOrderDetail = new BOrderDetail();
					bOrderDetail.setB_order_detail_id(UUID.toUUID());
					bOrderDetail.setB_order_detail_ctime(getSimpleDateFormat());
					bOrderDetail.setB_order_detail_discount(bCartDetail.getB_cart_detail_discount());
					bOrderDetail.setB_order_detail_number(bCartDetail.getB_cart_detail_number());
					bOrderDetail.setB_order_detail_price(bCartDetail.getB_cart_detail_price());
					bOrderDetail.setB_product_id(bCartDetail.getB_product_id());
					bOrderDetail.setB_seller_id(bCartDetail.getB_seller_id());
					bOrderDetail.setB_order_id(bOrder.getB_order_id());
					bOrderDetailService.addBOrderDetail(bOrderDetail);
				}
			}
			condition.put("b_cart_id", b_cart_id.split(","));
			//删除
			delBCart(condition);
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
