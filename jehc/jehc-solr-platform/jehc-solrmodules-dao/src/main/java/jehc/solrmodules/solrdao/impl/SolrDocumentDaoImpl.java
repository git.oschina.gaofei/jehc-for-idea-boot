package jehc.solrmodules.solrdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.solrmodules.solrdao.SolrDocumentDao;
import jehc.solrmodules.solrmodel.SolrDocument;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* solr文档 
* 2015-12-23 09:38:59  邓纯杰
*/
@Repository("solrDocumentDao")
public class SolrDocumentDaoImpl  extends BaseDaoImpl implements SolrDocumentDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrDocument> getSolrDocumentListByCondition(Map<String,Object> condition){
		return (List<SolrDocument>)this.getList("getSolrDocumentListByCondition",condition);
	}
	/**
	* 查询对象
	* @param solr_document_id 
	* @return
	*/
	public SolrDocument getSolrDocumentById(String solr_document_id){
		return (SolrDocument)this.get("getSolrDocumentById", solr_document_id);
	}
	/**
	* 添加
	* @param solrDocument
	* @return
	*/
	public int addSolrDocument(SolrDocument solrDocument){
		return this.add("addSolrDocument", solrDocument);
	}
	/**
	* 修改
	* @param solrDocument
	* @return
	*/
	public int updateSolrDocument(SolrDocument solrDocument){
		return this.update("updateSolrDocument", solrDocument);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrDocument(Map<String,Object> condition){
		return this.del("delSolrDocument", condition);
	}
}
