package jehc.solrmodules.solrdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.solrmodules.solrdao.SolrSchemaTempletDao;
import jehc.solrmodules.solrmodel.SolrSchemaTemplet;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* solr schema 模板 
* 2016-07-01 13:14:46  邓纯杰
*/
@Repository("solrSchemaTempletDao")
public class SolrSchemaTempletDaoImpl  extends BaseDaoImpl implements SolrSchemaTempletDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrSchemaTemplet> getSolrSchemaTempletListByCondition(Map<String,Object> condition){
		return (List<SolrSchemaTemplet>)this.getList("getSolrSchemaTempletListByCondition",condition);
	}
	/**
	* 查询对象
	* @param solr_schema_templet_id 
	* @return
	*/
	public SolrSchemaTemplet getSolrSchemaTempletById(String solr_schema_templet_id){
		return (SolrSchemaTemplet)this.get("getSolrSchemaTempletById", solr_schema_templet_id);
	}
	/**
	* 添加
	* @param solrSchemaTemplet
	* @return
	*/
	public int addSolrSchemaTemplet(SolrSchemaTemplet solrSchemaTemplet){
		return this.add("addSolrSchemaTemplet", solrSchemaTemplet);
	}
	/**
	* 修改
	* @param solrSchemaTemplet
	* @return
	*/
	public int updateSolrSchemaTemplet(SolrSchemaTemplet solrSchemaTemplet){
		return this.update("updateSolrSchemaTemplet", solrSchemaTemplet);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrSchemaTemplet(Map<String,Object> condition){
		return this.del("delSolrSchemaTemplet", condition);
	}
}
