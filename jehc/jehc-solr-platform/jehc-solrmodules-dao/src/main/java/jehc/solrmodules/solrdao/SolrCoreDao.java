package jehc.solrmodules.solrdao;
import java.util.List;
import java.util.Map;

import jehc.solrmodules.solrmodel.SolrCore;

/**
* 全文检索多实例配置 
* 2015-12-15 13:07:24  邓纯杰
*/
public interface SolrCoreDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrCore> getSolrCoreListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param solr_core_id 
	* @return
	*/
	public SolrCore getSolrCoreById(String solr_core_id);
	/**
	* 添加
	* @param solrCore
	* @return
	*/
	public int addSolrCore(SolrCore solrCore);
	/**
	* 修改
	* @param solrCore
	* @return
	*/
	public int updateSolrCore(SolrCore solrCore);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrCore(Map<String,Object> condition);
}
