package jehc.solrmodules.solrdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.solrmodules.solrdao.SolrFiledCopyDao;
import jehc.solrmodules.solrmodel.SolrFiledCopy;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* Solr字段拷贝配置 
* 2016-11-14 22:20:39  邓纯杰
*/
@Repository("solrFiledCopyDao")
public class SolrFiledCopyDaoImpl  extends BaseDaoImpl implements SolrFiledCopyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrFiledCopy> getSolrFiledCopyListByCondition(Map<String,Object> condition){
		return (List<SolrFiledCopy>)this.getList("getSolrFiledCopyListByCondition",condition);
	}
	/**
	* 查询对象
	* @param solr_filed_copy_id 
	* @return
	*/
	public SolrFiledCopy getSolrFiledCopyById(String solr_filed_copy_id){
		return (SolrFiledCopy)this.get("getSolrFiledCopyById", solr_filed_copy_id);
	}
	/**
	* 添加
	* @param solrFiledCopy
	* @return
	*/
	public int addSolrFiledCopy(SolrFiledCopy solrFiledCopy){
		return this.add("addSolrFiledCopy", solrFiledCopy);
	}
	/**
	* 修改
	* @param solrFiledCopy
	* @return
	*/
	public int updateSolrFiledCopy(SolrFiledCopy solrFiledCopy){
		return this.update("updateSolrFiledCopy", solrFiledCopy);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrFiledCopy(Map<String,Object> condition){
		return this.del("delSolrFiledCopy", condition);
	}
	/**
	* 批量添加
	* @param solrFiledCopyList
	* @return
	*/
	public int addBatchSolrFiledCopy(List<SolrFiledCopy> solrFiledCopyList){
		return this.add("addBatchSolrFiledCopy", solrFiledCopyList);
	}
	/**
	* 批量修改
	* @param solrFiledCopyList
	* @return
	*/
	public int updateBatchSolrFiledCopy(List<SolrFiledCopy> solrFiledCopyList){
		return this.update("updateBatchSolrFiledCopy", solrFiledCopyList);
	}
}
