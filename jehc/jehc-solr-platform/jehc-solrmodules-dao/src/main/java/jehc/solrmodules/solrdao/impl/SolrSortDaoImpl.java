package jehc.solrmodules.solrdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.solrmodules.solrdao.SolrSortDao;
import jehc.solrmodules.solrmodel.SolrSort;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* solr排序 
* 2016-07-08 23:49:58  邓纯杰
*/
@Repository("solrSortDao")
public class SolrSortDaoImpl  extends BaseDaoImpl implements SolrSortDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrSort> getSolrSortListByCondition(Map<String,Object> condition){
		return (List<SolrSort>)this.getList("getSolrSortListByCondition",condition);
	}
	/**
	* 查询对象
	* @param solr_sort_id 
	* @return
	*/
	public SolrSort getSolrSortById(String solr_sort_id){
		return (SolrSort)this.get("getSolrSortById", solr_sort_id);
	}
	/**
	* 添加
	* @param solrSort
	* @return
	*/
	public int addSolrSort(SolrSort solrSort){
		return this.add("addSolrSort", solrSort);
	}
	/**
	* 修改
	* @param solrSort
	* @return
	*/
	public int updateSolrSort(SolrSort solrSort){
		return this.update("updateSolrSort", solrSort);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrSort(Map<String,Object> condition){
		return this.del("delSolrSort", condition);
	}
	
	/**
	 * 根据实例编号查找其下面排序字段
	 * @param condition
	 * @return
	 */
	public List<SolrSort> getSolrSortList(Map<String,Object> condition){
		return (List<SolrSort>)this.getList("getSolrSortList",condition);
	}
}
