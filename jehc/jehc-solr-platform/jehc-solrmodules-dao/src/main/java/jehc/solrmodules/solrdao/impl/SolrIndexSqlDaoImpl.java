package jehc.solrmodules.solrdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.solrmodules.solrdao.SolrIndexSqlDao;
import jehc.solrmodules.solrmodel.SolrIndexSql;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;

/**
* 索引SQL查询 
* 2015-12-23 09:42:26  邓纯杰
*/
@Repository("solrIndexSqlDao")
public class SolrIndexSqlDaoImpl  extends BaseDaoImpl implements SolrIndexSqlDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrIndexSql> getSolrIndexSqlListByCondition(Map<String,Object> condition){
		return (List<SolrIndexSql>)this.getList("getSolrIndexSqlListByCondition",condition);
	}
	/**
	* 查询对象
	* @param solr_index_sql_id 
	* @return
	*/
	public SolrIndexSql getSolrIndexSqlById(String solr_index_sql_id){
		return (SolrIndexSql)this.get("getSolrIndexSqlById", solr_index_sql_id);
	}
	/**
	* 添加
	* @param solrIndexSql
	* @return
	*/
	public int addSolrIndexSql(SolrIndexSql solrIndexSql){
		return this.add("addSolrIndexSql", solrIndexSql);
	}
	/**
	* 修改
	* @param solrIndexSql
	* @return
	*/
	public int updateSolrIndexSql(SolrIndexSql solrIndexSql){
		return this.update("updateSolrIndexSql", solrIndexSql);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrIndexSql(Map<String,Object> condition){
		return this.del("delSolrIndexSql", condition);
	}
}
