package jehc.solrmodules.solrservice.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.solrmodules.solrdao.SolrCoreDao;
import jehc.solrmodules.solrdao.SolrUrlDao;
import jehc.solrmodules.solrmodel.SolrCore;
import jehc.solrmodules.solrmodel.SolrDocument;
import jehc.solrmodules.solrmodel.SolrFiledCopy;
import jehc.solrmodules.solrmodel.SolrIndex;
import jehc.solrmodules.solrmodel.SolrUrl;
import jehc.solrmodules.solrservice.SolrCoreService;
import jehc.solrmodules.solrservice.SolrDocumentService;
import jehc.solrmodules.solrservice.SolrFiledCopyService;
import jehc.solrmodules.solrservice.SolrIndexService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.solr.utils.SolrUtil;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtcore.util.UUID;

/**
* 全文检索多实例配置 
* 2015-12-15 13:07:24  邓纯杰
*/
@Service("solrCoreService")
public class SolrCoreServiceImpl extends BaseService implements SolrCoreService{
	@Autowired
	private SolrCoreDao solrCoreDao;
	@Autowired
	private SolrUrlDao solrUrlDao;
	@Autowired
	private SolrDocumentService solrDocumentService;
	@Autowired
	private SolrIndexService solrIndexService;
	@Autowired
	private SolrFiledCopyService solrFiledCopyService;
	
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrCore> getSolrCoreListByCondition(Map<String,Object> condition){
		try{
			return solrCoreDao.getSolrCoreListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param solr_core_id 
	* @return
	*/
	public SolrCore getSolrCoreById(String solr_core_id){
		try{
			return solrCoreDao.getSolrCoreById(solr_core_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 添加
	 * @param solrCore
	 * @param solrDocument
	 * @param solrIndexList
	 * @return
	 */
	public int addSolrCore(SolrCore solrCore,SolrDocument solrDocument,List<SolrIndex> solrIndexList){
		int i = 0;
		try {
			SolrUrl solr_url =solrUrlDao.getSolrUrlById(solrCore.getSolr_url_id());
			if(null == solr_url){
				throw new ExceptionUtil("未获取到SOlr_URL对象");
			}else{
				String solr_url_url = solr_url.getSolr_url_url();
				/**添加实例**/
				solrCoreDao.addSolrCore(solrCore);
				for(int l = 0; l < solrIndexList.size(); l++){
					SolrIndex solrIndex = solrIndexList.get(l);
					solrIndex.setSolr_core_id(solrCore.getSolr_core_id());
					/**添加索引**/
					solrIndexService.addSolrIndex(solrIndex);
				}
				solrDocument.setSolr_core_id(solrCore.getSolr_core_id());
				/**添加文档**/
				i = solrDocumentService.addSolrDocument(solrDocument);
				SolrUtil.createCore(solr_url_url, solrCore,solrDocument,solrIndexList);
			}
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 修改
	 * @param solrCore
	 * @param solrDocument
	 * @param solrIndexList
	 * @return
	 */
	public int updateSolrCore(SolrCore solrCore,SolrDocument solrDocument,List<SolrIndex> solrIndexList){
		int i = 0;
		try {
			SolrUrl solr_url =solrUrlDao.getSolrUrlById(solrCore.getSolr_url_id());
			String solr_url_url = solr_url.getSolr_url_url();
			/**修改实例**/
			solrCoreDao.updateSolrCore(solrCore);
			for(int l = 0; l < solrIndexList.size(); l++){
				SolrIndex solrIndex = solrIndexList.get(l);
				if(!StringUtil.isEmpty(solrIndex.getSolr_index_id())){
					/**更新索引**/
					solrIndex.setSolr_core_id(solrCore.getSolr_core_id());
					solrIndexService.updateSolrIndex(solrIndex);
				}else{
					solrIndex.setSolr_core_id(solrCore.getSolr_core_id());
					solrIndex.setSolr_index_id(UUID.toUUID());
					/**添加索引**/
					solrIndexService.addSolrIndex(solrIndex);
				}
			}
			/**修改文档**/
			i = solrDocumentService.updateSolrDocument(solrDocument);
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_core_id", solrCore.getSolr_core_id());
			List<SolrFiledCopy> solrFiledCopyList = solrFiledCopyService.getSolrFiledCopyListByCondition(condition);
			SolrUtil.updateCore(solr_url_url, solrCore,solrDocument,solrIndexList,solrFiledCopyList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrCore(Map<String,Object> condition){
		int i = 0;
		try {
			i = solrCoreDao.delSolrCore(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
