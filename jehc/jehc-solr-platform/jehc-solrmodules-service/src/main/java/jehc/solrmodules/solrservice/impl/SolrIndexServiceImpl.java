package jehc.solrmodules.solrservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.solrmodules.solrdao.SolrIndexDao;
import jehc.solrmodules.solrmodel.SolrIndex;
import jehc.solrmodules.solrservice.SolrIndexService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* 索引字段表 
* 2015-12-23 09:32:01  邓纯杰
*/
@Service("solrIndexService")
public class SolrIndexServiceImpl extends BaseService implements SolrIndexService{
	@Autowired
	private SolrIndexDao solrIndexDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrIndex> getSolrIndexListByCondition(Map<String,Object> condition){
		try{
			return solrIndexDao.getSolrIndexListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param solr_index_id 
	* @return
	*/
	public SolrIndex getSolrIndexById(String solr_index_id){
		try{
			return solrIndexDao.getSolrIndexById(solr_index_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param solrIndex
	* @return
	*/
	public int addSolrIndex(SolrIndex solrIndex){
		int i = 0;
		try {
			i = solrIndexDao.addSolrIndex(solrIndex);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param solrIndex
	* @return
	*/
	public int updateSolrIndex(SolrIndex solrIndex){
		int i = 0;
		try {
			i = solrIndexDao.updateSolrIndex(solrIndex);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrIndex(Map<String,Object> condition){
		int i = 0;
		try {
			i = solrIndexDao.delSolrIndex(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
