package jehc.solrmodules.solrservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.solrmodules.solrdao.SolrSortDao;
import jehc.solrmodules.solrmodel.SolrSort;
import jehc.solrmodules.solrservice.SolrSortService;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;

/**
* solr排序 
* 2016-07-08 23:49:58  邓纯杰
*/
@Service("solrSortService")
public class SolrSortServiceImpl extends BaseService implements SolrSortService{
	@Autowired
	private SolrSortDao solrSortDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<SolrSort> getSolrSortListByCondition(Map<String,Object> condition){
		try{
			return solrSortDao.getSolrSortListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param solr_sort_id 
	* @return
	*/
	public SolrSort getSolrSortById(String solr_sort_id){
		try{
			return solrSortDao.getSolrSortById(solr_sort_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param solrSort
	* @return
	*/
	public int addSolrSort(SolrSort solrSort){
		int i = 0;
		try {
			i = solrSortDao.addSolrSort(solrSort);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param solrSort
	* @return
	*/
	public int updateSolrSort(SolrSort solrSort){
		int i = 0;
		try {
			i = solrSortDao.updateSolrSort(solrSort);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delSolrSort(Map<String,Object> condition){
		int i = 0;
		try {
			i = solrSortDao.delSolrSort(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 根据实例编号查找其下面排序字段
	 * @param condition
	 * @return
	 */
	public List<SolrSort> getSolrSortList(Map<String,Object> condition){
		try{
			return solrSortDao.getSolrSortList(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
