package jehc.solrmodules.solrweb;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jehc.solrmodules.solrmodel.SolrEntity;
import jehc.solrmodules.solrmodel.SolrIndexSql;
import jehc.solrmodules.solrmodel.SolrIndexSqlFiled;
import jehc.solrmodules.solrservice.SolrEntityService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtmodel.XtDataDictionary;

/**
 * SOLR实体
 * @author邓纯杰
 *
 */
@Api(value = "SOLR实体", description = "SOLR实体")
@Controller
@RequestMapping("/solrEntityController")
public class SolrEntityController extends BaseAction{
	@Autowired
	private SolrEntityService solrEntityService;
	/**
	* 查询单个实体
	* @param solr_entity_id 
	* @param request 
	*/
	@ApiOperation(value="查询单个实体", notes="查询单个实体")
	@ResponseBody
	@RequestMapping(value="/getSolrEntityById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrEntityById(String solr_entity_id,HttpServletRequest request){
		if(null != solr_entity_id && !"".equals(solr_entity_id)){
			List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("solrTransformer");
			String solr_entity_transformer_text="";
			SolrEntity solrEntity = solrEntityService.getSolrEntityById(solr_entity_id);
			for(int i = 0; i < xtDataDictionaryList.size(); i++){
				XtDataDictionary xtDataDictionary = xtDataDictionaryList.get(i);
				if(null != solrEntity.getSolr_entity_transformer() && !"".equals(solrEntity.getSolr_entity_transformer()) && solrEntity.getSolr_entity_transformer().indexOf(xtDataDictionary.getXt_data_dictionary_id())>=0){
					if(null != solr_entity_transformer_text && !"".equals(solr_entity_transformer_text)){
						solr_entity_transformer_text = solr_entity_transformer_text+",【"+xtDataDictionary.getXt_data_dictionary_name()+"】";
					}else{
						solr_entity_transformer_text = "【"+xtDataDictionary.getXt_data_dictionary_name()+"】";
					}
				}
			}
			solrEntity.setSolr_entity_transformer_text(solr_entity_transformer_text);
			return outDataStr(solrEntity);
		}else{
			throw new RuntimeException("未能获取到solr_entity_id");
		}
	}
	/**
	* 添加
	* @param solrEntity
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrEntity",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrEntity(SolrEntity solrEntity,String solrIndexSqlJSON,String solrIndexSqlFiledJSON,HttpServletRequest request){
		int i = 0;
		solrEntity.setXt_userinfo_id(CommonUtils.getXtUid());
		solrEntity.setSolr_entity_ctime(getDate());
		List<SolrIndexSql> solrIndexSqlList = commonSolrIndexSqlList(solrIndexSqlJSON);
		List<SolrIndexSqlFiled> solrIndexSqlFiledList = commonSolrIndexSqlFiledJSON(solrIndexSqlFiledJSON);
		for(int j = 0; j < solrIndexSqlList.size(); j++){
			solrIndexSqlList.get(j).setSolr_index_ctime(getDate());
			solrIndexSqlList.get(j).setSolr_index_sql_id(UUID.toUUID());
			solrIndexSqlList.get(j).setXt_userinfo_id(CommonUtils.getXtUid());
		}
		for(int j = 0; j < solrIndexSqlFiledList.size(); j++){
			solrIndexSqlFiledList.get(j).setSolr_index_sql_filed_id(UUID.toUUID());
			solrIndexSqlFiledList.get(j).setSolr_index_sql_filed_ctime(getDate());
			solrIndexSqlFiledList.get(j).setXt_userinfo_id(CommonUtils.getXtUid());
		}
		i=solrEntityService.addSolrEntity(solrEntity,solrIndexSqlList,solrIndexSqlFiledList);
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 修改
	 * @param solrEntity
	 * @param solrIndexSqlJSON
	 * @param solrIndexSqlFiledJSON
	 * @param request
	 * @return
	 */
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrEntity",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrEntity(SolrEntity solrEntity,String solrIndexSqlJSON,String solrIndexSqlFiledJSON,HttpServletRequest request){
		int i = 0;
		solrEntity.setXt_userinfo_id(CommonUtils.getXtUid());
		solrEntity.setSolr_entity_mtime(getDate());
		List<SolrIndexSql> solrIndexSqlList = commonSolrIndexSqlList(solrIndexSqlJSON);
		List<SolrIndexSqlFiled> solrIndexSqlFiledList = commonSolrIndexSqlFiledJSON(solrIndexSqlFiledJSON);
		for(int j = 0; j < solrIndexSqlList.size(); j++){
			if(null == solrIndexSqlList.get(j).getSolr_index_sql_id() || "".equals(solrIndexSqlList.get(j).getSolr_index_sql_id())){
				solrIndexSqlList.get(j).setSolr_index_ctime(getDate());
				
			}else{
				solrIndexSqlList.get(j).setSolr_index_mtime(getDate());
			}
			solrIndexSqlList.get(j).setXt_userinfo_id(CommonUtils.getXtUid());
		}
		for(int j = 0; j < solrIndexSqlFiledList.size(); j++){
			if(StringUtil.isEmpty(solrIndexSqlFiledList.get(j).getSolr_index_sql_filed_id())){
				solrIndexSqlFiledList.get(j).setSolr_index_sql_filed_ctime(getDate());
			}else{
				solrIndexSqlFiledList.get(j).setSolr_index_sql_filed_mtime(getDate());
			}
			solrIndexSqlFiledList.get(j).setXt_userinfo_id(CommonUtils.getXtUid());
		}
		i=solrEntityService.updateSolrEntity(solrEntity,solrIndexSqlList,solrIndexSqlFiledList);
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	 * 返回查询SQL集合
	 * @param solrIndexSqlJSON
	 * @return
	 */
	private List<SolrIndexSql> commonSolrIndexSqlList(String solrIndexSqlJSON){
		try {
			solrIndexSqlJSON = URLDecoder.decode(solrIndexSqlJSON, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		List<SolrIndexSql> solrIndexSqlList = new ArrayList<SolrIndexSql>();
		JSONArray arr = JSONArray.fromObject(solrIndexSqlJSON);
		Object[] o = arr.toArray();
		for(Object obj:o){ 
		    if (obj instanceof JSONObject) {
		    	JSONObject json = (JSONObject)obj; 
		    	SolrIndexSql solrIndexSql = new SolrIndexSql();
		    	Object solr_index_sql_id = json.get("solr_index_sql_id");
		    	Object solr_index_sql_query = json.get("solr_index_sql_query");
		    	Object solr_index_sql_type = json.get("solr_index_sql_type");
		    	Object solr_entity_id = json.get("solr_entity_id");
		    	if(null != solr_index_sql_id){
					solrIndexSql.setSolr_index_sql_id((String)solr_index_sql_id);
				}
		    	if(null != solr_index_sql_query){
					solrIndexSql.setSolr_index_sql_query((String)solr_index_sql_query);
		    	}
		    	if(null != solr_index_sql_type){
					solrIndexSql.setSolr_index_sql_type((String)solr_index_sql_type);
		    	}
		    	if(null != solr_entity_id){
					solrIndexSql.setSolr_entity_id((String)solr_entity_id);
		    	}
				solrIndexSqlList.add(solrIndexSql);
		    }
		}
		return solrIndexSqlList;
	}
	
	/**
	 * 返回SQL结果字段
	 * @param solrIndexSqlFiledJSON
	 * @return
	 */
	private List<SolrIndexSqlFiled> commonSolrIndexSqlFiledJSON(String solrIndexSqlFiledJSON){
		try {
			solrIndexSqlFiledJSON = URLDecoder.decode(solrIndexSqlFiledJSON, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		List<SolrIndexSqlFiled> solrIndexSqlFiledList = new ArrayList<SolrIndexSqlFiled>();
		JSONArray arr = JSONArray.fromObject(solrIndexSqlFiledJSON);
		Object[] o = arr.toArray();
		for(Object obj:o){ 
		    if (obj instanceof JSONObject) {
		    	JSONObject json = (JSONObject)obj; 
		    	SolrIndexSqlFiled solrIndexSqlFiled = new SolrIndexSqlFiled();
		    	Object solr_index_sql_filed_id = json.get("solr_index_sql_filed_id");
		    	Object solr_index_sql_filed_name = json.get("solr_index_sql_filed_name");
		    	Object solr_index_sql_filed_zh = json.get("solr_index_sql_filed_zh");
		    	Object solr_index_filed_name = json.get("solr_index_filed_name");
		    	Object solr_entity_id = json.get("solr_entity_id");
		    	if(null != solr_index_sql_filed_id){
					solrIndexSqlFiled.setSolr_index_sql_filed_id((String)solr_index_sql_filed_id);
				}
		    	if(null != solr_index_sql_filed_name){
					solrIndexSqlFiled.setSolr_index_sql_filed_name((String)solr_index_sql_filed_name);
		    	}
		    	if(null != solr_index_sql_filed_zh){
					solrIndexSqlFiled.setSolr_index_sql_filed_zh((String)solr_index_sql_filed_zh);
		    	}
		    	if(null != solr_index_filed_name){
					solrIndexSqlFiled.setSolr_index_filed_name((String)solr_index_filed_name);
		    	}
		    	if(null != solr_entity_id){
					solrIndexSqlFiled.setSolr_entity_id((String)solr_entity_id);
		    	}
				solrIndexSqlFiledList.add(solrIndexSqlFiled);
		    }
		}
		return solrIndexSqlFiledList;
	}
}
