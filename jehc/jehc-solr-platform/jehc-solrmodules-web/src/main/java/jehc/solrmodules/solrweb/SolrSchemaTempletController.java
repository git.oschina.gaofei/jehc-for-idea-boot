package jehc.solrmodules.solrweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrSchemaTemplet;
import jehc.solrmodules.solrservice.SolrSchemaTempletService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* solr schema 模板 
* 2016-07-01 13:14:46  邓纯杰
*/
@Api(value = "solr schema 模板", description = "solr schema 模板")
@Controller
@RequestMapping("/solrSchemaTempletController")
public class SolrSchemaTempletController extends BaseAction{
	@Autowired
	private SolrSchemaTempletService solrSchemaTempletService;
	/**
	* 列表页面
	* @param solrSchemaTemplet
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrSchemaTemplet(SolrSchemaTemplet solrSchemaTemplet,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-schema-templet/solr-schema-templet-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrSchemaTempletListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrSchemaTempletListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<SolrSchemaTemplet> solrSchemaTempletList = solrSchemaTempletService.getSolrSchemaTempletListByCondition(condition);
		PageInfo<SolrSchemaTemplet> page = new PageInfo<SolrSchemaTemplet>(solrSchemaTempletList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param solr_schema_templet_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrSchemaTempletById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrSchemaTempletById(String solr_schema_templet_id,HttpServletRequest request){
		SolrSchemaTemplet solrSchemaTemplet = solrSchemaTempletService.getSolrSchemaTempletById(solr_schema_templet_id);
		return outDataStr(solrSchemaTemplet);
	}
	/**
	* 添加
	* @param solrSchemaTemplet
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrSchemaTemplet(SolrSchemaTemplet solrSchemaTemplet,HttpServletRequest request){
		int i = 0;
		if(null != solrSchemaTemplet){
			solrSchemaTemplet.setXt_userinfo_id(getXtUid());
			solrSchemaTemplet.setSolr_schema_templet_content(request.getParameter("solr_schema_templet_content"));
			solrSchemaTemplet.setSolr_schema_templet_ctime(getDate());
			solrSchemaTemplet.setSolr_schema_templet_id(UUID.toUUID());
			i=solrSchemaTempletService.addSolrSchemaTemplet(solrSchemaTemplet);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrSchemaTemplet
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrSchemaTemplet(SolrSchemaTemplet solrSchemaTemplet,HttpServletRequest request){
		int i = 0;
		if(null != solrSchemaTemplet){
			solrSchemaTemplet.setXt_userinfo_id(getXtUid());
			solrSchemaTemplet.setSolr_schema_templet_content(request.getParameter("solr_schema_templet_content"));
			solrSchemaTemplet.setSolr_schema_templet_mtime(getDate());
			i=solrSchemaTempletService.updateSolrSchemaTemplet(solrSchemaTemplet);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_schema_templet_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrSchemaTemplet(String solr_schema_templet_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_schema_templet_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_schema_templet_id",solr_schema_templet_id.split(","));
			i=solrSchemaTempletService.delSolrSchemaTemplet(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_schema_templet_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrSchemaTemplet(String solr_schema_templet_id,HttpServletRequest request){
		int i = 0;
		SolrSchemaTemplet solrSchemaTemplet = solrSchemaTempletService.getSolrSchemaTempletById(solr_schema_templet_id);
		if(null != solrSchemaTemplet){
			solrSchemaTemplet.setSolr_schema_templet_id(UUID.toUUID());
			i=solrSchemaTempletService.addSolrSchemaTemplet(solrSchemaTemplet);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrSchemaTemplet",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrSchemaTemplet(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 集合
	* @param solrSchemaTemplet
	* @param request 
	*/
	@ApiOperation(value="集合", notes="集合")
	@ResponseBody
	@RequestMapping(value="/getSolrSchemaTempletList",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrSchemaTempletList(SolrSchemaTemplet solrSchemaTemplet,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("solr_schema_templet_status", 0);
		List<SolrSchemaTemplet> solrSchemaTempletList = solrSchemaTempletService.getSolrSchemaTempletListByCondition(condition);
		return outComboDataStr(solrSchemaTempletList);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toSolrSchemaTempletAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrSchemaTempletAdd(SolrSchemaTemplet solrSchemaTemplet,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-schema-templet/solr-schema-templet-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toSolrSchemaTempletUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrSchemaTempletUpdate(String solr_schema_templet_id,HttpServletRequest request, Model model){
		SolrSchemaTemplet solrSchemaTemplet = solrSchemaTempletService.getSolrSchemaTempletById(solr_schema_templet_id);
		model.addAttribute("solrSchemaTemplet", solrSchemaTemplet);
		return new ModelAndView("pc/solr-view/solr-schema-templet/solr-schema-templet-update");
	}
	/**
	* 明细页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toSolrSchemaTempletDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrSchemaTempletDetail(String solr_schema_templet_id,HttpServletRequest request, Model model){
		SolrSchemaTemplet solrSchemaTemplet = solrSchemaTempletService.getSolrSchemaTempletById(solr_schema_templet_id);
		model.addAttribute("solrSchemaTemplet", solrSchemaTemplet);
		return new ModelAndView("pc/solr-view/solr-schema-templet/solr-schema-templet-detail");
	}
}
