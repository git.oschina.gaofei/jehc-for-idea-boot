package jehc.solrmodules.solrweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrSort;
import jehc.solrmodules.solrservice.SolrSortService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* solr排序 
* 2016-07-08 23:49:58  邓纯杰
*/
@Api(value = "solr排序", description = "solr排序")
@Controller
@RequestMapping("/solrSortController")
public class SolrSortController extends BaseAction{
	@Autowired
	private SolrSortService solrSortService;
	/**
	* 列表页面
	* @param solrSort
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrSort(SolrSort solrSort,HttpServletRequest request,Model model){
		String solr_index_id = request.getParameter("solr_index_id");
		model.addAttribute("solr_index_id", solr_index_id);
		return new ModelAndView("pc/solr-view/solr-sort/solr-sort-list");
	}
	/**
	* 查询并分页
	* @param solrSort
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrSortListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrSortListByCondition(SolrSort solrSort,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		condition.put("solr_index_id", solrSort.getSolr_index_id());
		condition.put("solr_sort_name",request.getParameter("solr_sort_name"));
		List<SolrSort> solr_SortList = solrSortService.getSolrSortListByCondition(condition);
		PageInfo<SolrSort> page = new PageInfo<SolrSort>(solr_SortList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param solr_sort_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrSortById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrSortById(String solr_sort_id,HttpServletRequest request){
		SolrSort solrSort = solrSortService.getSolrSortById(solr_sort_id);
		return outDataStr(solrSort);
	}
	/**
	* 添加
	* @param solrSort
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrSort(SolrSort solrSort,HttpServletRequest request){
		int i = 0;
		if(null != solrSort){
			solrSort.setSolr_sort_id(UUID.toUUID());
			solrSort.setSolr_sort_ctime(getDate());
			solrSort.setXt_userinfo_id(getXtUid());
			i=solrSortService.addSolrSort(solrSort);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrSort
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrSort(SolrSort solrSort,HttpServletRequest request){
		int i = 0;
		if(null != solrSort){
			solrSort.setSolr_sort_mtime(getDate());
			solrSort.setXt_userinfo_id(getXtUid());
			i=solrSortService.updateSolrSort(solrSort);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_sort_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrSort(String solr_sort_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_sort_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_sort_id",solr_sort_id.split(","));
			i=solrSortService.delSolrSort(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_sort_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrSort(String solr_sort_id,HttpServletRequest request){
		int i = 0;
		SolrSort solr_Sort = solrSortService.getSolrSortById(solr_sort_id);
		if(null != solr_Sort && !"".equals(solr_Sort)){
			solr_Sort.setSolr_sort_id(UUID.toUUID());
			i=solrSortService.addSolrSort(solr_Sort);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrSort",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrSort(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
