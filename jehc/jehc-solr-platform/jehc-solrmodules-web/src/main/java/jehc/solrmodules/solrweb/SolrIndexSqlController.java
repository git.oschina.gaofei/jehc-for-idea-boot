package jehc.solrmodules.solrweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jehc.solrmodules.solrmodel.SolrIndexSql;
import jehc.solrmodules.solrservice.SolrIndexSqlService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 索引SQL查询 
* 2015-12-23 09:42:26  邓纯杰
*/
@Api(value = "索引SQL查询", description = "索引SQL查询")
@Controller
@RequestMapping("/solrIndexSqlController")
@Scope("prototype")
public class SolrIndexSqlController extends BaseAction{
	@Autowired
	private SolrIndexSqlService solrIndexSqlService;
	/**
	* 列表页面
	* @param solrIndexSql
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrIndexSql(SolrIndexSql solrIndexSql,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-index-sql/solr-index-sql-list");
	}
	/**
	* 查询并分页
	* @param solrIndexSql
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexSqlListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexSqlListByCondition(SolrIndexSql solrIndexSql,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(null != solrIndexSql && !StringUtil.isEmpty(solrIndexSql.getSolr_entity_id())){
			condition.put("solr_entity_id", solrIndexSql.getSolr_entity_id());
		}else{
			condition.put("solr_entity_id", 0);
		}
		List<SolrIndexSql> solrIndexSqlList = solrIndexSqlService.getSolrIndexSqlListByCondition(condition);
		return outItemsStr(solrIndexSqlList);
	}
	/**
	* 查询单条记录
	* @param solr_index_sql_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexSqlById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexSqlById(String solr_index_sql_id,HttpServletRequest request){
		SolrIndexSql solrIndexSql = solrIndexSqlService.getSolrIndexSqlById(solr_index_sql_id);
		return outDataStr(solrIndexSql);
	}
	/**
	* 添加
	* @param solrIndexSql
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrIndexSql(SolrIndexSql solrIndexSql,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexSql){
			solrIndexSql.setSolr_index_sql_id(UUID.toUUID());
			i=solrIndexSqlService.addSolrIndexSql(solrIndexSql);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrIndexSql
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrIndexSql(SolrIndexSql solrIndexSql,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexSql){
			i=solrIndexSqlService.updateSolrIndexSql(solrIndexSql);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_index_sql_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrIndexSql(String solr_index_sql_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_index_sql_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_index_sql_id",solr_index_sql_id.split(","));
			i=solrIndexSqlService.delSolrIndexSql(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_index_sql_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrIndexSql(String solr_index_sql_id,HttpServletRequest request){
		int i = 0;
		SolrIndexSql solr_Index_Sql = solrIndexSqlService.getSolrIndexSqlById(solr_index_sql_id);
		if(null != solr_Index_Sql && !"".equals(solr_Index_Sql)){
			solr_Index_Sql.setSolr_index_sql_id(UUID.toUUID());
			i=solrIndexSqlService.addSolrIndexSql(solr_Index_Sql);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrIndexSql",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrIndexSql(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
