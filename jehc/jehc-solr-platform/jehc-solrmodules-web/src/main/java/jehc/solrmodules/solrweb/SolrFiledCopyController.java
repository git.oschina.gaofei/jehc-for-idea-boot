package jehc.solrmodules.solrweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrFiledCopy;
import jehc.solrmodules.solrservice.SolrFiledCopyService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 字段拷贝配置
* 2016-11-15 12:49:27  邓纯杰
*/
@Api(value = "字段拷贝配置", description = "字段拷贝配置")
@Controller
@RequestMapping("/solrFiledCopyController")
public class SolrFiledCopyController extends BaseAction{
	@Autowired
	private SolrFiledCopyService solrFiledCopyService;
	/**
	* 列表页面
	* @param solrFiledCopy
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrFiledCopy(SolrFiledCopy solrFiledCopy,HttpServletRequest request){
		request.setAttribute("solr_core_id", solrFiledCopy.getSolr_core_id());
		return new ModelAndView("pc/solr-view/solr-filed-copy/solr-filed-copy-list");
	}

	/**
	 * 查询并分页
	 * @param solrFiledCopy
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrFiledCopyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrFiledCopyListByCondition(SolrFiledCopy solrFiledCopy,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("solr_core_id", solrFiledCopy.getSolr_core_id());
		condition.put("solr_filed_copy_source_id", solrFiledCopy.getSolr_filed_copy_source_id());
		condition.put("solr_filed_copy_dest_id", solrFiledCopy.getSolr_filed_copy_dest_id());
		condition.put("solr_filed_copy_remark", solrFiledCopy.getSolr_filed_copy_remark());
		commonHPager(condition,request);
		List<SolrFiledCopy> solrFiledCopyList = solrFiledCopyService.getSolrFiledCopyListByCondition(condition);
		PageInfo<SolrFiledCopy> page = new PageInfo<SolrFiledCopy>(solrFiledCopyList);
		return outPageStr(page,request);
	}

	/**
	* 查询单个
	* @param solr_filed_copy_id
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrFiledCopyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrFiledCopyById(String solr_filed_copy_id){
		return outDataStr(solrFiledCopyService.getSolrFiledCopyById(solr_filed_copy_id));
	}

	/**
	* 添加
	* @param solrFiledCopy
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrFiledCopy(SolrFiledCopy solrFiledCopy){
		int i = 0;
		if(null != solrFiledCopy){
			solrFiledCopy.setSolr_filed_copy_id(UUID.toUUID());
			solrFiledCopy.setXt_userinfo_id(getXtUid());
			solrFiledCopy.setSolr_filed_copy_ctime(getDate());
			i=solrFiledCopyService.addSolrFiledCopy(solrFiledCopy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 修改
	* @param solrFiledCopy
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrFiledCopy(SolrFiledCopy solrFiledCopy){
		int i = 0;
		if(null != solrFiledCopy){
			solrFiledCopy.setXt_userinfo_id(getXtUid());
			solrFiledCopy.setSolr_filed_copy_mtime(getDate());
			i=solrFiledCopyService.updateSolrFiledCopy(solrFiledCopy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param solr_filed_copy_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrFiledCopy(String solr_filed_copy_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_filed_copy_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_filed_copy_id",solr_filed_copy_id.split(","));
			i=solrFiledCopyService.delSolrFiledCopy(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 复制一行并生成记录
	* @param solr_filed_copy_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrFiledCopy(String solr_filed_copy_id,HttpServletRequest request){
		int i = 0;
		SolrFiledCopy solr_Filed_Copy = solrFiledCopyService.getSolrFiledCopyById(solr_filed_copy_id);
		if(null != solr_Filed_Copy && !"".equals(solr_Filed_Copy)){
			solr_Filed_Copy.setSolr_filed_copy_id(UUID.toUUID());
			i=solrFiledCopyService.addSolrFiledCopy(solr_Filed_Copy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrFiledCopy",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrFiledCopy(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
