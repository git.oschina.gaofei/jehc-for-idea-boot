package jehc.solrmodules.solrweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrIndex;
import jehc.solrmodules.solrservice.SolrIndexService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 索引字段表 
* 2015-12-23 09:32:01  邓纯杰
*/
@Api(value = "索引字段表", description = "索引字段表")
@Controller
@RequestMapping("/solrIndexController")
@Scope("prototype")
public class SolrIndexController extends BaseAction{
	@Autowired
	private SolrIndexService solrIndexService;
	/**
	* 列表页面
	* @param solrIndex
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrIndex(SolrIndex solrIndex,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-index/solr-index-list");
	}
	/**
	* 查询并分页
	* @param solrIndex
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexListByCondition(SolrIndex solrIndex,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		condition.put("solr_core_id", solrIndex.getSolr_core_id());
		List<SolrIndex> solrIndexList = solrIndexService.getSolrIndexListByCondition(condition);
		PageInfo<SolrIndex> page = new PageInfo<SolrIndex>(solrIndexList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param solr_index_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexById(String solr_index_id,HttpServletRequest request){
		SolrIndex solrIndex = solrIndexService.getSolrIndexById(solr_index_id);
		return outDataStr(solrIndex);
	}
	/**
	* 添加
	* @param solrIndex
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrIndex(SolrIndex solrIndex,HttpServletRequest request){
		int i = 0;
		if(null != solrIndex){
			solrIndex.setSolr_index_id(UUID.toUUID());
			i=solrIndexService.addSolrIndex(solrIndex);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrIndex
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/updateSolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrIndex(SolrIndex solrIndex,HttpServletRequest request){
		int i = 0;
		if(null != solrIndex){
			i=solrIndexService.updateSolrIndex(solrIndex);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_index_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrIndex(String solr_index_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_index_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_index_id",solr_index_id.split(","));
			i=solrIndexService.delSolrIndex(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_index_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrIndex(String solr_index_id,HttpServletRequest request){
		int i = 0;
		SolrIndex solr_Index = solrIndexService.getSolrIndexById(solr_index_id);
		if(null != solr_Index && !"".equals(solr_Index)){
			solr_Index.setSolr_index_id(UUID.toUUID());
			i=solrIndexService.addSolrIndex(solr_Index);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrIndex",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrIndex(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 查询索引
	* @param solr_core_id
	* @param request 
	*/
	@ApiOperation(value="查询索引", notes="查询索引")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexList",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexList(String solr_core_id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("solr_core_id", solr_core_id);
		List<SolrIndex> solrIndexList = solrIndexService.getSolrIndexListByCondition(condition);
		return outItemsStr(solrIndexList);
	}
}
