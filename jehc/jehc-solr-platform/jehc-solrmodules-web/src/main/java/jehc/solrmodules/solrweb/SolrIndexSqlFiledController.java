package jehc.solrmodules.solrweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jehc.solrmodules.solrmodel.SolrIndexSqlFiled;
import jehc.solrmodules.solrservice.SolrIndexSqlFiledService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* SQL查询结果返回字段 
* 2015-12-23 09:44:02  邓纯杰
*/
@Api(value = "SQL查询结果返回字段", description = "SQL查询结果返回字段")
@Controller
@RequestMapping("/solrIndexSqlFiledController")
@Scope("prototype")
public class SolrIndexSqlFiledController extends BaseAction{
	@Autowired
	private SolrIndexSqlFiledService solrIndexSqlFiledService;
	/**
	* 列表页面
	* @param solrIndexSqlFiled
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrIndexSqlFiled(SolrIndexSqlFiled solrIndexSqlFiled,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-index-sql-filed/solr-index-sql-filed-list");
	}
	/**
	* 查询并分页
	* @param solrIndexSqlFiled
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexSqlFiledListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexSqlFiledListByCondition(SolrIndexSqlFiled solrIndexSqlFiled,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(null != solrIndexSqlFiled && !StringUtil.isEmpty(solrIndexSqlFiled.getSolr_entity_id())){
			condition.put("solr_entity_id", solrIndexSqlFiled.getSolr_entity_id());
		}else{
			condition.put("solr_entity_id", 0);
		}
		List<SolrIndexSqlFiled> solrIndexSqlFiledList = solrIndexSqlFiledService.getSolrIndexSqlFiledListByCondition(condition);
		return outItemsStr(solrIndexSqlFiledList);
	}
	/**
	* 查询单条记录
	* @param solr_index_sql_filed_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexSqlFiledById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexSqlFiledById(String solr_index_sql_filed_id,HttpServletRequest request){
		SolrIndexSqlFiled solrIndexSqlFiled = solrIndexSqlFiledService.getSolrIndexSqlFiledById(solr_index_sql_filed_id);
		return outDataStr(solrIndexSqlFiled);
	}
	/**
	* 添加
	* @param solrIndexSqlFiled
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrIndexSqlFiled(SolrIndexSqlFiled solrIndexSqlFiled,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexSqlFiled){
			solrIndexSqlFiled.setSolr_index_sql_filed_id(UUID.toUUID());
			i=solrIndexSqlFiledService.addSolrIndexSqlFiled(solrIndexSqlFiled);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrIndexSqlFiled
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrIndexSqlFiled(SolrIndexSqlFiled solrIndexSqlFiled,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexSqlFiled){
			i=solrIndexSqlFiledService.updateSolrIndexSqlFiled(solrIndexSqlFiled);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_index_sql_filed_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrIndexSqlFiled(String solr_index_sql_filed_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_index_sql_filed_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_index_sql_filed_id",solr_index_sql_filed_id.split(","));
			i=solrIndexSqlFiledService.delSolrIndexSqlFiled(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_index_sql_filed_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrIndexSqlFiled(String solr_index_sql_filed_id,HttpServletRequest request){
		int i = 0;
		SolrIndexSqlFiled solr_Index_Sql_Filed = solrIndexSqlFiledService.getSolrIndexSqlFiledById(solr_index_sql_filed_id);
		if(null != solr_Index_Sql_Filed && !"".equals(solr_Index_Sql_Filed)){
			solr_Index_Sql_Filed.setSolr_index_sql_filed_id(UUID.toUUID());
			i=solrIndexSqlFiledService.addSolrIndexSqlFiled(solr_Index_Sql_Filed);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrIndexSqlFiled",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrIndexSqlFiled(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
