package jehc.solrmodules.solrweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrIndexAttribute;
import jehc.solrmodules.solrservice.SolrIndexAttributeService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 索引更多属性 
* 2016-07-10 22:19:49  邓纯杰
*/
@Api(value = "索引更多属性", description = "索引更多属性")
@Controller
@RequestMapping("/solrIndexAttributeController")
public class SolrIndexAttributeController extends BaseAction{
	@Autowired
	private SolrIndexAttributeService solrIndexAttributeService;
	/**
	* 列表页面
	* @param solrIndexAttribute
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrIndexAttribute(SolrIndexAttribute solrIndexAttribute,HttpServletRequest request,Model model){
		String solr_index_id = request.getParameter("solr_index_id");
		model.addAttribute("solr_index_id", solr_index_id);
		return new ModelAndView("pc/solr-view/solr-index-attribute/solr-index-attribute-list");
	}
	/**
	* 查询并分页
	* @param solrIndexAttribute
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexAttributeListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexAttributeListByCondition(SolrIndexAttribute solrIndexAttribute,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		List<SolrIndexAttribute> solrIndexAttributeList = solrIndexAttributeService.getSolrIndexAttributeListByCondition(condition);
		PageInfo<SolrIndexAttribute> page = new PageInfo<SolrIndexAttribute>(solrIndexAttributeList);
		return outPageStr(page,request);
	}
	/**
	* 查询单个
	* @param solr_index_attribute_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getSolrIndexAttributeById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrIndexAttributeById(String solr_index_attribute_id,HttpServletRequest request){
		SolrIndexAttribute solrIndexAttribute = solrIndexAttributeService.getSolrIndexAttributeById(solr_index_attribute_id);
		return outDataStr(solrIndexAttribute);
	}
	/**
	* 添加
	* @param solrIndexAttribute
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addSolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrIndexAttribute(SolrIndexAttribute solrIndexAttribute,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexAttribute){
			solrIndexAttribute.setSolr_index_attribute_id(UUID.toUUID());
			i=solrIndexAttributeService.addSolrIndexAttribute(solrIndexAttribute);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param solrIndexAttribute
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrIndexAttribute(SolrIndexAttribute solrIndexAttribute,HttpServletRequest request){
		int i = 0;
		if(null != solrIndexAttribute){
			i=solrIndexAttributeService.updateSolrIndexAttribute(solrIndexAttribute);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param solr_index_attribute_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrIndexAttribute(String solr_index_attribute_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_index_attribute_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_index_attribute_id",solr_index_attribute_id.split(","));
			i=solrIndexAttributeService.delSolrIndexAttribute(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_index_attribute_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrIndexAttribute(String solr_index_attribute_id,HttpServletRequest request){
		int i = 0;
		SolrIndexAttribute solr_Index_Attribute = solrIndexAttributeService.getSolrIndexAttributeById(solr_index_attribute_id);
		if(null != solr_Index_Attribute && !"".equals(solr_Index_Attribute)){
			solr_Index_Attribute.setSolr_index_attribute_id(UUID.toUUID());
			i=solrIndexAttributeService.addSolrIndexAttribute(solr_Index_Attribute);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrIndexAttribute",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrIndexAttribute(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
