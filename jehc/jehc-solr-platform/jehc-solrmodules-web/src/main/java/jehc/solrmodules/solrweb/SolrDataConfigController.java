package jehc.solrmodules.solrweb;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.solrmodules.solrmodel.SolrDataConfig;
import jehc.solrmodules.solrservice.SolrDataConfigService;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;

/**
* 模板配置
* 2016-07-02 10:09:05  邓纯杰
*/
@Api(value = "模板配置", description = "模板配置")
@Controller
@RequestMapping("/solrDataConfigController")
public class SolrDataConfigController extends BaseAction{
	@Autowired
	private SolrDataConfigService solrDataConfigService;

	/**
	 * 列表页面
	 * @param solrDataConfig
	 * @param request
	 * @return
	 */
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadSolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadSolrDataConfig(SolrDataConfig solrDataConfig,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-data-config/solr-data-config-list");
	}
	/**
	* 查询模板配置集合并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询模板配置集合并分页", notes="查询模板配置集合并分页")
	@ResponseBody
	@RequestMapping(value="/getSolrDataConfigListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrDataConfigListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<SolrDataConfig> solrDataConfigList = solrDataConfigService.getSolrDataConfigListByCondition(condition);
		PageInfo<SolrDataConfig> page = new PageInfo<SolrDataConfig>(solrDataConfigList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单个模板配置
	* @param solr_data_config_id 
	* @param request 
	*/
	@ApiOperation(value="查询单个模板配置", notes="查询单个模板配置")
	@ResponseBody
	@RequestMapping(value="/getSolrDataConfigById",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrDataConfigById(String solr_data_config_id,HttpServletRequest request){
		SolrDataConfig solrDataConfig = solrDataConfigService.getSolrDataConfigById(solr_data_config_id);
		return outDataStr(solrDataConfig);
	}
	/**
	* 添加模板配置
	* @param solrDataConfig
	* @param request 
	*/
	@ApiOperation(value="添加模板配置", notes="添加模板配置")
	@ResponseBody
	@RequestMapping(value="/addSolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public String addSolrDataConfig(SolrDataConfig solrDataConfig,HttpServletRequest request){
		int i = 0;
		if(null != solrDataConfig){
			solrDataConfig.setSolr_data_config_id(UUID.toUUID());
			solrDataConfig.setSolr_data_config_ctime(getDate());
			solrDataConfig.setXt_userinfo_id(getXtUid());
			solrDataConfig.setSolr_data_config_datasource(request.getParameter("solr_data_config_datasource"));
			solrDataConfig.setSolr_data_config_content(request.getParameter("solr_data_config_content"));
			i=solrDataConfigService.addSolrDataConfig(solrDataConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改模板配置
	* @param solrDataConfig
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateSolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public String updateSolrDataConfig(SolrDataConfig solrDataConfig,HttpServletRequest request){
		int i = 0;
		if(null != solrDataConfig){
			solrDataConfig.setSolr_data_config_mtime(getDate());
			solrDataConfig.setXt_userinfo_id(getXtUid());
			solrDataConfig.setSolr_data_config_datasource(request.getParameter("solr_data_config_datasource"));
			solrDataConfig.setSolr_data_config_content(request.getParameter("solr_data_config_content"));
			i=solrDataConfigService.updateSolrDataConfig(solrDataConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除模板配置
	* @param solr_data_config_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delSolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public String delSolrDataConfig(String solr_data_config_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(solr_data_config_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("solr_data_config_id",solr_data_config_id.split(","));
			i=solrDataConfigService.delSolrDataConfig(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param solr_data_config_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copySolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public String copySolrDataConfig(String solr_data_config_id,HttpServletRequest request){
		int i = 0;
		SolrDataConfig solrDataConfig = solrDataConfigService.getSolrDataConfigById(solr_data_config_id);
		if(null != solrDataConfig){
			solrDataConfig.setSolr_data_config_id(UUID.toUUID());
			i=solrDataConfigService.addSolrDataConfig(solrDataConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportSolrDataConfig",method={RequestMethod.POST,RequestMethod.GET})
	public void exportSolrDataConfig(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 查询集合
	* @param solrDataConfig
	* @param request 
	*/
	@ApiOperation(value="查询集合", notes="查询集合")
	@ResponseBody
	@RequestMapping(value="/getSolrDataConfigList",method={RequestMethod.POST,RequestMethod.GET})
	public String getSolrDataConfigList(SolrDataConfig solrDataConfig,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("solr_data_config_status", 0);
		List<SolrDataConfig> solr_Data_ConfigList = solrDataConfigService.getSolrDataConfigListByCondition(condition);
		return outComboDataStr(solr_Data_ConfigList);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toSolrDataConfigAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrDataConfigAdd(SolrDataConfig solrDataConfig,HttpServletRequest request){
		return new ModelAndView("pc/solr-view/solr-data-config/solr-data-config-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toSolrDataConfigUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrDataConfigUpdate(String solr_data_config_id,HttpServletRequest request, Model model){
		SolrDataConfig solrDataConfig = solrDataConfigService.getSolrDataConfigById(solr_data_config_id);
		model.addAttribute("solrDataConfig", solrDataConfig);
		return new ModelAndView("pc/solr-view/solr-data-config/solr-data-config-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toSolrDataConfigDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toSolrDataConfigDetail(String solr_data_config_id,HttpServletRequest request, Model model){
		SolrDataConfig solrDataConfig = solrDataConfigService.getSolrDataConfigById(solr_data_config_id);
		model.addAttribute("solrDataConfig", solrDataConfig);
		return new ModelAndView("pc/solr-view/solr-data-config/solr-data-config-detail");
	}
}
