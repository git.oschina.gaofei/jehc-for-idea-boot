package jehc.xtmodules.xtweb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.annotation.NeedLoginUnAuth;
import jehc.xtmodules.xtcore.util.InitAdminPage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseHttpSessionEntity;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.md5.MD5;
import jehc.xtmodules.xtcore.util.AdminTree;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.IndexTree;
import jehc.xtmodules.xtcore.util.SortList;
import jehc.xtmodules.xtcore.util.constant.SessionConstant;
import jehc.xtmodules.xtmodel.XtDyRemind;
import jehc.xtmodules.xtmodel.XtMenuinfo;
import jehc.xtmodules.xtmodel.XtMessage;
import jehc.xtmodules.xtmodel.XtNotifyReceiver;
import jehc.xtmodules.xtmodel.XtUserinfo;
import jehc.xtmodules.xtservice.XtKnowledgeService;
import jehc.xtmodules.xtservice.XtLoginLogsService;
import jehc.xtmodules.xtservice.XtMenuinfoService;
import jehc.xtmodules.xtservice.XtMessageService;
import jehc.xtmodules.xtservice.XtNoticeService;
import jehc.xtmodules.xtservice.XtNotifyReceiverService;
import jehc.xtmodules.xtservice.XtUserinfoService;

/**
 * 加载首页
 * @author邓纯杰
 *
 */
@Api(value = "加载首页", description = "加载首页")
@Controller
@Scope("prototype")
public class XtIndexController extends BaseAction{
	@Autowired
	private XtNotifyReceiverService xtNotifyReceiverService;
	@Autowired
	private XtMessageService xtMessageService;
	@Autowired
	private XtMenuinfoService xtMenuinfoService;
	@Autowired
	private XtUserinfoService xtUserinfoService;
    @Autowired
    private XtNoticeService xtNoticeService;
    @Autowired
    private XtLoginLogsService xtLoginLogsService;
    @Autowired
    private XtKnowledgeService xtKnowledgeService;
	/**
	 * 载入初始化页面（风格二------------Admin风格）
	 * @return
	 */
	@ApiOperation(value="载入初始化页面", notes="载入初始化页面")
	@NeedLoginUnAuth
	@RequestMapping(value="/index.html",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView admin() {
		return new ModelAndView("pc/xt-view/xt-index/xt-index");
	}
	
	/**
	 * 封装通过
	 * @param condition
	 */
	public void commonM(Map<String, Object> condition,HttpServletRequest request){
		if(!CommonUtils.isAdmin()){
			HttpSession session = request.getSession(false);
			if(null != session){
				BaseHttpSessionEntity baseHttpSessionEntity = (BaseHttpSessionEntity) session.getAttribute(SessionConstant.BASE_HTTP_SESSION);
				String xt_role_id = baseHttpSessionEntity.getXT_ROLE_ID();
				if(!StringUtils.isEmpty(xt_role_id)){
					condition.put("xt_role_id", xt_role_id.split(","));
				}else{
					condition.put("xt_role_id", "-1".split(","));
				}
			}else{
				condition.put("xt_role_id", "-1".split(","));
			}
		}
	}

	/**
	 * 载入桌面
	 * @param request
	 * @return
	 */
	@ApiOperation(value="载入桌面", notes="载入桌面")
	@NeedLoginUnAuth
	@RequestMapping(value="/dashboard.html",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadDesk(HttpServletRequest request,Model model) {
		Map<String,Object> condition = new HashMap<String,Object>();
		commonHPager(condition,request);
		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
		model.addAttribute("xtNotifyCount",page.getTotal());
		
		//公告数
		model.addAttribute("xtNoticeCount", xtNoticeService.getXtNoticeCountByCondition(condition));
		model.addAttribute("xtNoticeList", xtNoticeService.getXtNoticeListByCondition(condition));
		//个人登录次数
		condition.put("xt_userinfo_id", getXtUid());
		model.addAttribute("xtLoginLogsCount", xtLoginLogsService.getXtLoginLogsCount(condition));
		//平台知识库数
		model.addAttribute("xtKnowledgeCount", xtKnowledgeService.getXtKnowledgeCount(condition));
		
		condition = new HashMap<String,Object>();
		//处理短消息
		condition.put("type", "1");
		condition.put("to_id", getXtUid());
		condition.put("isread", 0);
		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> xtMessage = new PageInfo<XtMessage>(xt_MessageList);
		model.addAttribute("xtMessageList", xtMessage.getList());
		return new ModelAndView("pc/xt-view/xt-dashboard/xt-dashboard");
	}
	
	
	/**
	 * 载入异常页面
	 * @param request
	 * @return
	 */
	@ApiOperation(value="载入异常页面", notes="载入异常页面")
	@AuthUneedLogin
	@RequestMapping(value="/error.html",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadError(HttpServletRequest request) {
		return new ModelAndView("pc/xt-view/xt-error/xt-error");
	} 
	
	
	/**
	 * 载入忘记密码即找回密码页面
	 * @param request
	 * @return
	 */
	@ApiOperation(value="载入忘记密码即找回密码页面", notes="载入忘记密码即找回密码页面")
	@AuthUneedLogin
	@RequestMapping(value="/forget.html",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadForgetPwd(HttpServletRequest request) {
		return new ModelAndView("pc/xt-view/xt-forgetpwd/xt-forgetpwd");
	} 
	
	/**
	 * 修改密码
	 * @param request
	 * @return
	 */
	@ApiOperation(value="修改密码", notes="修改密码")
	@ResponseBody
	@RequestMapping(value="/updatePwd",method={RequestMethod.POST,RequestMethod.GET})
	public String updatePwd(HttpServletRequest request){
		String oldPwd = request.getParameter("oldPwd");
		String newPwd = request.getParameter("newPwd");
		XtUserinfo xtUserinfo = CommonUtils.getXtU();
		MD5 md5 = new MD5();
		if(null != oldPwd && !"".equals(oldPwd)){
			oldPwd = md5.getMD5ofStr(oldPwd.trim());
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_passWord", oldPwd);
			condition.put("xt_userinfo_name", xtUserinfo.getXt_userinfo_name());
			if(null != xtUserinfoService.getXtUserinfoByUP(condition)){
				condition = new HashMap<String, Object>();
				condition.put("xt_userinfo_passWord", md5.getMD5ofStr(newPwd.trim()));
				condition.put("xt_userinfo_id", xtUserinfo.getXt_userinfo_id());
				int i = xtUserinfoService.updatePwd(condition);
				if(i > 0){
					request.getSession(false).invalidate();
					return outAudStr(true, "密码修改成功,请重新登录系统!");
				}else{
					return outAudStr(false, "密码修改失败!");
				}
			}else{
				return outAudStr(false, "原密码错误!");
			}
		}
		return outAudStr(false, "原密码为空!");
	}
	
	/**
	 * 解锁
	 * @param request
	 * @return
	 */
	@ApiOperation(value="解锁", notes="解锁")
	@ResponseBody
	@RequestMapping(value="/validatePassword",method={RequestMethod.POST,RequestMethod.GET})
	public String validatePassword(HttpServletRequest request){
		String password = request.getParameter("password");
		XtUserinfo xtUserinfo = CommonUtils.getXtU();
		MD5 md5 = new MD5();
		password = md5.getMD5ofStr(password.trim());
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_userinfo_passWord", password);
		condition.put("xt_userinfo_name", xtUserinfo.getXt_userinfo_name());
		if(null != xtUserinfoService.getXtUserinfoByUP(condition)){
			return outAudStr(true,"1");
		}else{
			return outAudStr(true,"2");
		}
	}
	
	/**
	 * 发送至黑名单页面
	 * @param request
	 */
	@ApiOperation(value="发送至黑名单页面", notes="发送至黑名单页面")
	@AuthUneedLogin
	@RequestMapping(value="/illegal",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView illegal(HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-illegal/xt-illegal");
	}

    /**
     * 发送至无操作权限页面
     * @param request
     */
    @ApiOperation(value="发送至无操作权限页面", notes="发送至无操作权限页面")
    @AuthUneedLogin
    @RequestMapping(value="/Unpermission.html",method={RequestMethod.POST,RequestMethod.GET})
    public ModelAndView unpermission(HttpServletRequest request){
        return new ModelAndView("pc/xt-view/xt-no-role/xt-no-role");
    }

	/**
	 * 发送至SESSION失效页面
	 * @param request
	 */
	@ApiOperation(value="发送至SESSION失效页面", notes="发送至SESSION失效页面")
	@AuthUneedLogin
	@RequestMapping(value="/Sessiontimeout.html",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView sessiontimeout(HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-session/xt-session");
	}

	/**
	 * 初始化主页面
	 * @param baseSearch
	 * @param model
	 * @param request
	 * @return
	 */
	@ApiOperation(value="初始化主页面", notes="初始化主页面")
	@ResponseBody
	@NeedLoginUnAuth
	@RequestMapping(value="/init",method={RequestMethod.POST,RequestMethod.GET})
	public InitAdminPage init(BaseSearch baseSearch,Model model,HttpServletRequest request){
		InitAdminPage initAdminPage = new InitAdminPage();
		Map<String, Object> condition = new HashMap<String, Object>();
		commonM(condition,request);
		List<XtMenuinfo> xtMenuinfoList = xtMenuinfoService.getXtMenuinListForRole(condition);
		SortList<XtMenuinfo> sortList = new SortList<XtMenuinfo>();
		sortList.Sort(xtMenuinfoList, "xt_menuinfo_sort", "asc");
		IndexTree tree = new IndexTree(xtMenuinfoList);
		initAdminPage.setMenuList(tree.buildTree(false));
		AdminTree adminTree = new AdminTree(xtMenuinfoList);
		initAdminPage.setAdminMenuList(adminTree.buildTree());

		XtDyRemind xtDyRemind = new XtDyRemind();
		//处理通知
		condition = baseSearch.convert();
		commonHPager(condition,request);
		condition.put("xt_userinfo_id", getXtUid());
		condition.put("receive_status",0);
		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
		xtDyRemind.setXtNotifyReceiverList(page.getList());
		//处理短消息
		condition.put("type", "1");
		condition.put("to_id", getXtUid());
		condition.put("isread", 0);
		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> xtMessage = new PageInfo<XtMessage>(xt_MessageList);
		xtDyRemind.setXtMessageList(xtMessage.getList());

		initAdminPage.setXtDyRemind(xtDyRemind);

        initAdminPage.setBasePath(request.getContextPath());
//		//处理主题
//		String colorTheme="dark";
//		String mini = "NO";
//		String classshrink = "NO";
//		Cookie cookies[]=request.getCookies();
//		Cookie cookieC=null;
//		if(null != cookies){
//			for(int i=0;i < cookies.length;i++){
//				if((SessionConstant.THEME_CLASS).equals(cookies[i].getName())){
//					cookieC=cookies[i];
//					break;
//				}
//			}
//		}
//		if(null != cookieC){
//			if(cookieC.getValue().indexOf("whiteClass")>=0 || cookieC.getValue().indexOf("Whitemini")>=0){
//				colorTheme = "light";
//			}
//			if(cookieC.getValue().indexOf("miniClass")>=0){
//				mini = "YES";
//				if(cookieC.getValue().indexOf("Classshrink")>=0){
//					classshrink = "YES";
//				}
//			}
//		}
//		initAdminPage.setColorTheme(colorTheme);
//		initAdminPage.setClassshrink(classshrink);
//		initAdminPage.setMini(mini);
		return initAdminPage;
	}
}
