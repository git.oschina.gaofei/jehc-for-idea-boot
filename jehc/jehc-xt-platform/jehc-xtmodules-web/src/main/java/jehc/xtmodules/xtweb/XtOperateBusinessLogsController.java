package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtOperateBusinessLogs;
import jehc.xtmodules.xtservice.XtOperateBusinessLogsService;

/**
* 平台业务操作日志
* 2016-09-16 16:39:20  邓纯杰
*/
@Api(value = "平台业务操作日志", description = "平台业务操作日志")
@Controller
@RequestMapping("/xtOperateBusinessLogsController")
public class XtOperateBusinessLogsController extends BaseAction{
	@Autowired
	private XtOperateBusinessLogsService xtOperateBusinessLogsService;
	/**
	* 列表页面
	* @param xtOperateBusinessLogs
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtOperateBusinessLogs(XtOperateBusinessLogs xtOperateBusinessLogs,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-operate-business-logs/xt-operate-business-logs-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtOperateBusinessLogsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtOperateBusinessLogsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtOperateBusinessLogs> xt_Operate_Business_LogsList = xtOperateBusinessLogsService.getXtOperateBusinessLogsListByCondition(condition);
		PageInfo<XtOperateBusinessLogs> page = new PageInfo<XtOperateBusinessLogs>(xt_Operate_Business_LogsList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_operate_business_logs_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtOperateBusinessLogsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtOperateBusinessLogsById(String xt_operate_business_logs_id,HttpServletRequest request){
		XtOperateBusinessLogs xtOperateBusinessLogs = xtOperateBusinessLogsService.getXtOperateBusinessLogsById(xt_operate_business_logs_id);
		return outDataStr(xtOperateBusinessLogs);
	}
	/**
	* 添加
	* @param xtOperateBusinessLogs
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtOperateBusinessLogs(XtOperateBusinessLogs xtOperateBusinessLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtOperateBusinessLogs){
			xtOperateBusinessLogs.setXt_operate_b_logs_id(UUID.toUUID());
			i=xtOperateBusinessLogsService.putXtOperateBusinessLogs(xtOperateBusinessLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtOperateBusinessLogs
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtOperateBusinessLogs(XtOperateBusinessLogs xtOperateBusinessLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtOperateBusinessLogs){
			i=xtOperateBusinessLogsService.updateXtOperateBusinessLogs(xtOperateBusinessLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_operate_business_logs_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtOperateBusinessLogs(String xt_operate_business_logs_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_operate_business_logs_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_operate_b_logs_id",xt_operate_business_logs_id.split(","));
			i=xtOperateBusinessLogsService.delXtOperateBusinessLogs(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_operate_business_logs_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtOperateBusinessLogs(String xt_operate_business_logs_id,HttpServletRequest request){
		int i = 0;
		XtOperateBusinessLogs xtOperateBusinessLogs = xtOperateBusinessLogsService.getXtOperateBusinessLogsById(xt_operate_business_logs_id);
		if(null != xtOperateBusinessLogs){
			xtOperateBusinessLogs.setXt_operate_b_logs_id(UUID.toUUID());
			i=xtOperateBusinessLogsService.putXtOperateBusinessLogs(xtOperateBusinessLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtOperateBusinessLogs",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtOperateBusinessLogs(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
    @ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtOperateBusinessLogsDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtOperateBusinessLogsDetail(String xt_operate_business_logs_id,HttpServletRequest request, Model model){
		XtOperateBusinessLogs xtOperateBusinessLogs = xtOperateBusinessLogsService.getXtOperateBusinessLogsById(xt_operate_business_logs_id);
		model.addAttribute("xtOperateBusinessLogs", xtOperateBusinessLogs);
		return new ModelAndView("pc/xt-view/xt-operate-business-logs/xt-operate-business-logs-detail");
	}
}
