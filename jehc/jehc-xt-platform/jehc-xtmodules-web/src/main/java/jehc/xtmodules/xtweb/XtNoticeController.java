package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtNotice;
import jehc.xtmodules.xtservice.XtNoticeService;

/**
* 平台公告 
* 2016-06-18 15:45:40  邓纯杰
*/
@Api(value = "平台公告", description = "平台公告")
@Controller
@RequestMapping("/xtNoticeController")
public class XtNoticeController extends BaseAction{
	@Autowired
	private XtNoticeService xtNoticeService;
	/**
	* 列表页面
	* @param xtNotice
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtNotice(XtNotice xtNotice,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-notice/xt-notice-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtNoticeListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtNoticeListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		dataAuthForXtUID(request,"xt_userinfo_id", condition);
		List<XtNotice> xtNoticeList = xtNoticeService.getXtNoticeListByCondition(condition);
		PageInfo<XtNotice> page = new PageInfo<XtNotice>(xtNoticeList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_notice_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtNoticeById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtNoticeById(String xt_notice_id,HttpServletRequest request){
		XtNotice xtNotice = xtNoticeService.getXtNoticeById(xt_notice_id);
		return outDataStr(xtNotice);
	}
	/**
	* 添加
	* @param xtNotice
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtNotice(@Valid XtNotice xtNotice,BindingResult bindingResult,HttpServletRequest request){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtNotice){
			xtNotice.setXt_notice_id(UUID.toUUID());
			xtNotice.setXt_userinfo_id(getXtUid());
			xtNotice.setXt_createTime(getSimpleDateFormat());
			i=xtNoticeService.addXtNotice(xtNotice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtNotice
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtNotice(@Valid XtNotice xtNotice,BindingResult bindingResult,HttpServletRequest request){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtNotice){
			i=xtNoticeService.updateXtNotice(xtNotice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_notice_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtNotice(String xt_notice_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_notice_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_notice_id",xt_notice_id.split(","));
			i=xtNoticeService.delXtNotice(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_notice_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtNotice(String xt_notice_id,HttpServletRequest request){
		int i = 0;
		XtNotice xtNotice = xtNoticeService.getXtNoticeById(xt_notice_id);
		if(null != xtNotice ){
			xtNotice.setXt_notice_id(UUID.toUUID());
			i=xtNoticeService.addXtNotice(xtNotice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtNotice",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtNotice(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtNoticeAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtNoticeAdd(XtNotice xtNotice,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-notice/xt-notice-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtNoticeUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtNoticeUpdate(String xt_notice_id,HttpServletRequest request, Model model){
		XtNotice xtNotice = xtNoticeService.getXtNoticeById(xt_notice_id);
		model.addAttribute("xtNotice", xtNotice);
		return new ModelAndView("pc/xt-view/xt-notice/xt-notice-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtNoticeDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtNoticeDetail(String xt_notice_id,HttpServletRequest request, Model model){
		XtNotice xtNotice = xtNoticeService.getXtNoticeById(xt_notice_id);
		model.addAttribute("xtNotice", xtNotice);
		return new ModelAndView("pc/xt-view/xt-notice/xt-notice-detail");
	}
}
