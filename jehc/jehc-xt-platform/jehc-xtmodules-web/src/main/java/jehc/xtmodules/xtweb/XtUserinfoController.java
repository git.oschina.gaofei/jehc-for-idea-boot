package jehc.xtmodules.xtweb;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.base.BaseHttpSessionEntity;
import jehc.xtmodules.xtcore.util.constant.SessionConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.annotation.AuthNeedLogin;
import jehc.xtmodules.xtcore.annotation.NeedLoginUnAuth;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.base.BaseTreeGridEntity;
import jehc.xtmodules.xtcore.md5.MD5;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtDataDictionary;
import jehc.xtmodules.xtmodel.XtUR;
import jehc.xtmodules.xtmodel.XtUserinfo;
import jehc.xtmodules.xtservice.XtURService;
import jehc.xtmodules.xtservice.XtUserinfoService;

/**
* 员工信息
* 2015-07-30 21:41:20  邓纯杰
*/
@Api(value = "员工信息", description = "员工信息")
@Controller
@RequestMapping("/xtUserinfoController")
@Scope("prototype")
public class XtUserinfoController extends BaseAction{
	@Autowired
	private XtUserinfoService xtUserinfoService;
	@Autowired
	private XtURService xtURService;
	/**
	* 列表页面
	* @param xtUserinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtUserinfo(XtUserinfo xtUserinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-userinfo/xt-userinfo-list");
	}
	/**
	* 上传页面
	* @param xtUserinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="上传页面", notes="上传页面")
	@RequestMapping(value="/loadXtUserinfoUpLoad",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtUserinfoUpLoad(XtUserinfo xtUserinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-userinfo/xt-userinfo-upload");
	}
	
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	 * @throws UnsupportedEncodingException 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoListByCondition(BaseSearch baseSearch,HttpServletRequest request) throws UnsupportedEncodingException{
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtUserinfo> xtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(xtUserinfoList);
		return outPageBootStr(page,request);
	}
	
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	 * @throws UnsupportedEncodingException 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoListForLcByCondition",method={RequestMethod.POST,RequestMethod.GET})
	@NeedLoginUnAuth
	public String getXtUserinfoListForLcByCondition(BaseSearch baseSearch,HttpServletRequest request) throws UnsupportedEncodingException{
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtUserinfo> xtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(xtUserinfoList);
		return outPageStr(page,request);
	}
	
	/**
	 * 查询删除用户
	 * @param baseSearch
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value="查询删除用户", notes="查询删除用户")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoDeletedListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoDeletedListByCondition(BaseSearch baseSearch,HttpServletRequest request) throws UnsupportedEncodingException{
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtUserinfo> xtUserinfoList = xtUserinfoService.getXtUserinfoDeletedListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(xtUserinfoList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_userinfo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoById(String xt_userinfo_id,HttpServletRequest request){
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		return outDataStr(xtUserinfo);
	}
	/**
	* 添加
	* @param xtUserinfo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtUserinfo(XtUserinfo xtUserinfo,HttpServletRequest request){
		int i = 0;
		MD5 md5 = new MD5();
		if(null != xtUserinfo){
			xtUserinfo.setXt_userinfo_id(UUID.toUUID());
			xtUserinfo.setXt_userinfo_passWord(md5.getMD5ofStr(getXtConstantCache("XtUserinfoDefaultPwd").getXt_constantValue()));
			i=xtUserinfoService.addXtUserinfo(xtUserinfo);
		}
		if(i>0){
			aBLogs("用户控制层", "添加", "添加用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "添加", "添加用户失败");
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtUserinfo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtUserinfo(XtUserinfo xtUserinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtUserinfo){
			i=xtUserinfoService.updateXtUserinfo(xtUserinfo);
		}
		if(i>0){
			aBLogs("用户控制层", "修改", "修改用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "修改", "修改用户失败");
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_userinfo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtUserinfo(String xt_userinfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id.split(","));
			i=xtUserinfoService.delXtUserinfo(condition);
		}
		if(i>0){
			aBLogs("用户控制层", "删除", "删除用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "删除", "删除用户失败");
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_userinfo_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtUserinfo(String xt_userinfo_id,HttpServletRequest request){
		int i = 0;
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		if(null != xtUserinfo){
			xtUserinfo.setXt_userinfo_id(UUID.toUUID());
			i=xtUserinfoService.addXtUserinfo(xtUserinfo);
		}
		if(i>0){
			aBLogs("用户控制层", "复制一行并生成记录", "复制一行并生成记录用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "复制一行并生成记录", "复制一行并生成记录用户失败");
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtUserinfo(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 查询名族
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询名族", notes="查询名族")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoNationList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoNationList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("xt_userinfo_nation");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 查询性别
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询性别", notes="查询性别")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoSexList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoSexList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("gender");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 查询文化程度
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询文化程度", notes="查询文化程度")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoHighestDegreeList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoHighestDegreeList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("xt_userinfo_highestDegree");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 查询工作年限
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询工作年限", notes="查询工作年限")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoWorkYearList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoWorkYearList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("xt_userinfo_workYear");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 查询是否已婚
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询是否已婚", notes="查询是否已婚")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoIsmarriedList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoIsmarriedList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("xt_userinfo_ismarried");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 查询用户状态
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="查询用户状态", notes="查询用户状态")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoStateList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoStateList(HttpServletRequest request,HttpServletResponse response){
		List<XtDataDictionary> xtDataDictionaryList = CommonUtils.getXtDataDictionaryCache("xt_userinfo_state");
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 判断用户名即登陆账号是否重复
	 * @param xt_userinfo_name
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="判断用户名即登陆账号是否重复", notes="判断用户名即登陆账号是否重复")
	@ResponseBody
	@RequestMapping(value="/validateUser",method={RequestMethod.POST,RequestMethod.GET})
	public String validateUser(String xt_userinfo_name,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(!StringUtil.isEmpty(xt_userinfo_name)){
			condition.put("xt_userinfo_name", xt_userinfo_name);
			int i = xtUserinfoService.validateUser(condition);
			if(i > 0){
				return outAudStr(true,"1");
			}else{
				return outAudStr(true,"0");
			}
		}else{
			return outAudStr(false,"用户名参数未获取!验证失败!");
		}
	}
	
	/**
	* 恢复数据
	* @param xt_userinfo_id 
	* @param request 
	*/
	@ApiOperation(value="恢复数据", notes="恢复数据")
	@ResponseBody
	@RequestMapping(value="/recoverXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String recoverXtUserinfo(String xt_userinfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id.split(","));
			i=xtUserinfoService.recoverXtUserinfo(condition);
		}
		if(i>0){
			aBLogs("用户控制层", "恢复用户", "恢复用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "恢复用户", "恢复用户失败");
			return outAudStr(false);
		}
	}
	
	/**
	 * 查询角色权限
	 * @param xt_userinfo_id
	 * @return
	 */
	@ApiOperation(value="查询角色权限", notes="查询角色权限")
	@ResponseBody
	@RequestMapping(value="/getXtRoleinfoListByUserinfoId",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtRoleinfoListByUserinfoId(String xt_userinfo_id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_userinfo_id", xt_userinfo_id);
		commonHPager(condition,request);
		List<XtUR> xtURList = xtURService.getXtRoleinfoListByUserinfoId(condition);
		PageInfo<XtUR> page = new PageInfo<XtUR>(xtURList);
		return outPageBootStr(page,request);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getChattingUserinfoList",method={RequestMethod.POST,RequestMethod.GET})
	public String getChattingUserinfoList(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtUserinfo> XtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		for(int i = 0; i < XtUserinfoList.size(); i++){
			XtUserinfo Xt_Userinfo = XtUserinfoList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(Xt_Userinfo.getXt_userinfo_id());
			BaseTreeGridEntity.setPid("0");
			BaseTreeGridEntity.setText(Xt_Userinfo.getXt_userinfo_realName());
			BaseTreeGridEntity.setContent("");
			BaseTreeGridEntity.setLeaf(true);
			BaseTreeGridEntity.setIcon("../deng/images/icons/employee_manager.png");
			list.add(BaseTreeGridEntity);
		}
		return outStr(BaseTreeGridEntity.buildTree(list,false));
	}

	/**
	 * 重置密码
	 * @param xt_userinfo_id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="重置密码", notes="重置密码")
	@ResponseBody
	@RequestMapping(value="/resetXtUserinfoPwd",method={RequestMethod.POST,RequestMethod.GET})
	public String resetXtUserinfoPwd(String xt_userinfo_id,String xt_userinfo_name,String xt_userinfo_realName,HttpServletRequest request){
		int i = 0;
		MD5 md5 = new MD5();
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id);
			condition.put("xt_userinfo_passWord", md5.getMD5ofStr(getXtConstantCache("XtUserinfoDefaultPwd").getXt_constantValue()));
			i=xtUserinfoService.updatePwd(condition);
		}
		if(i>0){
			aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+xt_userinfo_name+"】用户姓名：【"+xt_userinfo_realName+"】成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+xt_userinfo_name+"】用户姓名：【"+xt_userinfo_realName+"】失败");
			return outAudStr(false);
		}
	}
	
	/**
	 * 查询集合
	 * @param xt_userinfo_id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询集合", notes="查询集合")
	@ResponseBody
	@RequestMapping(value="/getXtUserinfoList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUserinfoList(String xt_userinfo_id,HttpServletRequest request){
		List<XtUserinfo> list = new ArrayList<XtUserinfo>();
		if(null != xt_userinfo_id && !"".equals(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id", xt_userinfo_id.split(","));
			list = xtUserinfoService.getXtUserinfoList(condition);
		}
		return  outItemsStr(list);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtUserinfoAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUserinfoAdd(XtUserinfo xtUserinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-userinfo/xt-userinfo-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtUserinfoUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUserinfoUpdate(String xt_userinfo_id,HttpServletRequest request, Model model){
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		model.addAttribute("xtUserinfo", xtUserinfo);
		return new ModelAndView("pc/xt-view/xt-userinfo/xt-userinfo-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtUserinfoDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUserinfoDetail(String xt_userinfo_id,HttpServletRequest request, Model model){
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		model.addAttribute("xtUserinfo", xtUserinfo);
		return new ModelAndView("pc/xt-view/xt-userinfo/xt-userinfo-detail");
	}
	
	/**
	 * 个人中心页面
	 * @param model
	 * @return
	 */
	@ApiOperation(value="个人中心页面", notes="个人中心页面")
	@RequestMapping(value="/loadMyXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadMyXtUserinfo(Model model){
		return new ModelAndView("pc/xt-view/xt-userinfo/my-xt-userinfo");
	}

	/**
	 * 个人信息
	 * @param request
	 */
	@ApiOperation(value="个人中心页面", notes="个人中心页面")
	@ResponseBody
	@RequestMapping(value="/myinfo",method={RequestMethod.POST,RequestMethod.GET})
	public XtUserinfo myinfo(HttpServletRequest request){
		BaseHttpSessionEntity baseHttpSessionEntity = (BaseHttpSessionEntity)request.getSession().getAttribute(SessionConstant.BASE_HTTP_SESSION);
		return baseHttpSessionEntity.getXTUSERINFO();
	}
	
	/**
	* 修改（个人中心）
	* @param xt_Userinfo
	* @param request 
	*/
	@ApiOperation(value="修改（个人中心）", notes="修改（个人中心）")
	@ResponseBody
	@NeedLoginUnAuth
	@RequestMapping(value="/updateMyXtUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateMyXtUserinfo(XtUserinfo xt_Userinfo,HttpServletRequest request){
		int i = 0;
		XtUserinfo xtUserinfo = getXtU();
		if(null == xtUserinfo){
			throw new ExceptionUtil("未能获取到当前用户！");
		}
		if(null != xt_Userinfo && !"".equals(xt_Userinfo)){
			xtUserinfo.setXt_userinfo_phone(xt_Userinfo.getXt_userinfo_phone());
			xtUserinfo.setXt_userinfo_mobile(xt_Userinfo.getXt_userinfo_mobile());
			xtUserinfo.setXt_userinfo_ortherTel(xt_Userinfo.getXt_userinfo_ortherTel());
			xtUserinfo.setXt_userinfo_qq(xt_Userinfo.getXt_userinfo_qq());
			xtUserinfo.setXt_userinfo_email(xt_Userinfo.getXt_userinfo_email());
			xtUserinfo.setXt_userinfo_remark(xt_Userinfo.getXt_userinfo_remark());
			xtUserinfo.setXt_userinfo_address(xt_Userinfo.getXt_userinfo_address());
			i=xtUserinfoService.updateXtUserinfo(xtUserinfo);
		}
		if(i>0){
			aBLogs("用户控制层", "修改", "修改用户成功");
			return outAudStr(true);
		}else{
			aBLogs("用户控制层", "修改", "修改用户失败");
			return outAudStr(false);
		}
	}
}
