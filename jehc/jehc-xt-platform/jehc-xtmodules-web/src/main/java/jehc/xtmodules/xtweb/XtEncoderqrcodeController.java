package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtEncoderqrcode;
import jehc.xtmodules.xtservice.XtEncoderqrcodeService;

/**
* 平台二维码 
* 2016-04-05 21:06:53  邓纯杰
*/
@Api(value = "平台二维码", description = "平台二维码")
@Controller
@RequestMapping("/xtEncoderqrcodeController")
public class XtEncoderqrcodeController extends BaseAction{
	@Autowired
	private XtEncoderqrcodeService xtEncoderqrcodeService;
	/**
	* 列表页面
	* @param xtEncoderqrcode
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-encoderqrcode/xt-encoderqrcode-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtEncoderqrcodeListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtEncoderqrcodeListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		String jehcsources_base_url = CommonUtils.getXtPathCache("jehcsources_base_url").get(0).getXt_path();
		List<XtEncoderqrcode> xtEncoderqrcodeList = xtEncoderqrcodeService.getXtEncoderqrcodeListByCondition(condition);
		for(int i = 0; i < xtEncoderqrcodeList.size(); i++){
			xtEncoderqrcodeList.get(i).setJehcsources_base_url(jehcsources_base_url);
		}
		PageInfo<XtEncoderqrcode> page = new PageInfo<XtEncoderqrcode>(xtEncoderqrcodeList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_encoderqrcode_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtEncoderqrcodeById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtEncoderqrcodeById(String xt_encoderqrcode_id,HttpServletRequest request){
		XtEncoderqrcode xtEncoderqrcode = xtEncoderqrcodeService.getXtEncoderqrcodeById(xt_encoderqrcode_id);
		return outDataStr(xtEncoderqrcode);
	}
	/**
	* 添加
	* @param xtEncoderqrcode
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode,HttpServletRequest request){
		int i = 0;
		if(null != xtEncoderqrcode){
			xtEncoderqrcode.setXt_encoderqrcode_id(UUID.toUUID());
			xtEncoderqrcode.setXt_encoderqrcode_ctime(CommonUtils.getSimpleDateFormat());
			xtEncoderqrcode.setXt_userinfo_id(CommonUtils.getXtUid());
			i=xtEncoderqrcodeService.addXtEncoderqrcode(xtEncoderqrcode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtEncoderqrcode
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode,HttpServletRequest request){
		int i = 0;
		if(null != xtEncoderqrcode){
            xtEncoderqrcode.setXt_userinfo_id(CommonUtils.getXtUid());
            xtEncoderqrcode.setXt_encoderqrcode_mtime(CommonUtils.getSimpleDateFormat());
			i=xtEncoderqrcodeService.updateXtEncoderqrcode(xtEncoderqrcode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_encoderqrcode_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtEncoderqrcode(String xt_encoderqrcode_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_encoderqrcode_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_encoderqrcode_id",xt_encoderqrcode_id.split(","));
			i=xtEncoderqrcodeService.delXtEncoderqrcode(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_encoderqrcode_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtEncoderqrcode(String xt_encoderqrcode_id,HttpServletRequest request){
		int i = 0;
		XtEncoderqrcode xtEncoderqrcode = xtEncoderqrcodeService.getXtEncoderqrcodeById(xt_encoderqrcode_id);
		if(null != xtEncoderqrcode){
			xtEncoderqrcode.setXt_encoderqrcode_id(UUID.toUUID());
			i=xtEncoderqrcodeService.addXtEncoderqrcode(xtEncoderqrcode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtEncoderqrcode",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtEncoderqrcode(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtEncoderqrcodeAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtEncoderqrcodeAdd(XtEncoderqrcode xtEncoderqrcode,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-encoderqrcode/xt-encoderqrcode-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtEncoderqrcodeUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtEncoderqrcodeUpdate(String xt_encoderqrcode_id,HttpServletRequest request, Model model){
		XtEncoderqrcode xtEncoderqrcode = xtEncoderqrcodeService.getXtEncoderqrcodeById(xt_encoderqrcode_id);
		model.addAttribute("xtEncoderqrcode", xtEncoderqrcode);
		return new ModelAndView("pc/xt-view/xt-encoderqrcode/xt-encoderqrcode-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtEncoderqrcodeDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtEncoderqrcodeDetail(String xt_encoderqrcode_id,HttpServletRequest request, Model model){
		XtEncoderqrcode xtEncoderqrcode = xtEncoderqrcodeService.getXtEncoderqrcodeById(xt_encoderqrcode_id);
		model.addAttribute("xtEncoderqrcode", xtEncoderqrcode);
		return new ModelAndView("pc/xt-view/xt-encoderqrcode/xt-encoderqrcode-detail");
	}
}
