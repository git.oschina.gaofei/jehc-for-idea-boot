package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtMonitorMem;
import jehc.xtmodules.xtservice.XtMonitorMemService;

/**
* 服务器内存 
* 2016-03-04 12:52:52  邓纯杰
*/
@Api(value = "服务器内存", description = "服务器内存")
@Controller
@RequestMapping("/xtMonitorMemController")
@Scope("prototype")
public class XtMonitorMemController extends BaseAction{
	@Autowired
	private XtMonitorMemService xtMonitorMemService;
	/**
	* 列表页面
	* @param xtMonitorMem
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtMonitorMem(XtMonitorMem xtMonitorMem,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-monitor-mem/xt-monitor-mem-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorMemListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorMemListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtMonitorMem> xtMonitorMemList = xtMonitorMemService.getXtMonitorMemListByCondition(condition);
		PageInfo<XtMonitorMem> page = new PageInfo<XtMonitorMem>(xtMonitorMemList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_monitor_mem_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorMemById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorMemById(String xt_monitor_mem_id,HttpServletRequest request){
		XtMonitorMem xtMonitorMem = xtMonitorMemService.getXtMonitorMemById(xt_monitor_mem_id);
		return outDataStr(xtMonitorMem);
	}
	/**
	* 添加
	* @param xtMonitorMem
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtMonitorMem(XtMonitorMem xtMonitorMem,HttpServletRequest request){
		int i = 0;
		if(null != xtMonitorMem){
			xtMonitorMem.setXt_monitor_mem_id(UUID.toUUID());
			i=xtMonitorMemService.addXtMonitorMem(xtMonitorMem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtMonitorMem
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtMonitorMem(XtMonitorMem xtMonitorMem,HttpServletRequest request){
		int i = 0;
		if(null != xtMonitorMem){
			i=xtMonitorMemService.updateXtMonitorMem(xtMonitorMem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_monitor_mem_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtMonitorMem(String xt_monitor_mem_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_monitor_mem_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_monitor_mem_id",xt_monitor_mem_id.split(","));
			i=xtMonitorMemService.delXtMonitorMem(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_monitor_mem_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtMonitorMem(String xt_monitor_mem_id,HttpServletRequest request){
		int i = 0;
		XtMonitorMem xtMonitorMem = xtMonitorMemService.getXtMonitorMemById(xt_monitor_mem_id);
		if(null != xtMonitorMem){
			xtMonitorMem.setXt_monitor_mem_id(UUID.toUUID());
			i=xtMonitorMemService.addXtMonitorMem(xtMonitorMem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtMonitorMem",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtMonitorMem(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}

	/**
	 * 当前内存剩余量
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="当前内存剩余量", notes="当前内存剩余量")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorMemCurrSy",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorMemCurrSy(HttpServletRequest request,HttpServletResponse response){
		JSONArray jsonArray = new JSONArray();  
		Map<String, Object> model = new HashMap<String, Object>();
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		List<XtMonitorMem> xtMonitorMemList = xtMonitorMemService.getXtMonitorMemListByCondition(condition);
		for(int i = 0; i < xtMonitorMemList.size(); i++){
			XtMonitorMem xtMonitorMem = xtMonitorMemList.get(i);
			model.put("xt_monitor_memTime", xtMonitorMem.getXt_monitor_memTime());
			model.put("xt_monitor_memCurrSy", (new Integer(xtMonitorMem.getXt_monitor_memCurrSy()))/(1024*1024));
			jsonArray.add(model);
		}
		return outItemsStr(jsonArray);
	}
	/**
	 * 内存图表页面
	 * @return
	 */
	@ApiOperation(value="内存图表页面", notes="内存图表页面")
	@RequestMapping(value="/loadXtMonitorMemChart",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtMonitorMemChart(){
		return new ModelAndView("pc/xt-view/xt-monitor-mem/xt-monitor-mem-chart");
	}
}
