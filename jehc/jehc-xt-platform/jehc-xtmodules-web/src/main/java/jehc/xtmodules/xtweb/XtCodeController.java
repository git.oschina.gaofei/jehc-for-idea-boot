package jehc.xtmodules.xtweb;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jehc.xtmodules.xtcore.base.BaseAction;

/**
 * code
 * @author 邓纯杰
 *
 */
@Api(value = "Code Control", description = "Code Control")
@Controller
@RequestMapping("/xtCodeController")
public class XtCodeController extends BaseAction{
	/**
	* 操作页面
	* @param request 
	* @return
	*/
	@ApiOperation(value="操作页面", notes="操作页面")
	@RequestMapping(value="/loadXtCode",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtCode(HttpServletRequest request,Model model){
		return new ModelAndView("pc/xt-view/xt-code/xt-code-list");
	} 
	
	/**
	* 编辑器页面
	* @param request 
	* @return
	*/
	@ApiOperation(value="编辑器页面", notes="编辑器页面")
	@RequestMapping(value="/loadXtCodeJsEditor",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtCodeJsEditor(HttpServletRequest request,Model model){
		return new ModelAndView("pc/xt-view/xt-code/xt-codeJsEditor");
	}

	/**
	 * 运行
	 * @param jsRunContent
	 * @param model
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value="运行", notes="运行")
	@RequestMapping(value="/jsRun",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView jsRun(String jsRunContent,Model model) throws UnsupportedEncodingException{
		model.addAttribute("jsRunContent", jsRunContent);
		return new ModelAndView("pc/xt-view/xt-code/xt-code-run");
	} 
}
