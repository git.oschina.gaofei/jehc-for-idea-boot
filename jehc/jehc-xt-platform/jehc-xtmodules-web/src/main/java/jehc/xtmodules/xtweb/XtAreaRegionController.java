package jehc.xtmodules.xtweb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseZTreeEntity;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtAreaRegion;
import jehc.xtmodules.xtservice.XtAreaRegionService;

/**
* 行政区划表 
* 2017-05-04 14:54:34  邓纯杰
*/
@Api(value = "行政区划表", description = "行政区划表")
@Controller
@RequestMapping("/xtAreaRegionController")
@Scope("prototype")
public class XtAreaRegionController extends BaseAction{
	@Autowired
	private XtAreaRegionService xtAreaRegionService;
	/**
	* 列表页面
	* @param xtAreaRegion
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtAreaRegion(XtAreaRegion xtAreaRegion,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-area-region/xt-area-region-list");
	}
	/**
	* 查询并分页
	* @param request
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtAreaRegionListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAreaRegionListByCondition(HttpServletRequest request){
		String expanded = request.getParameter("expanded");
		String singleClickExpand = request.getParameter("singleClickExpand");
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		Map<String,Object> condition = new HashMap<String,Object>();
		List<XtAreaRegion> xt_Area_RegionList = xtAreaRegionService.getXtAreaRegionListByCondition(condition);
		for(int i = 0; i < xt_Area_RegionList.size(); i++){
			XtAreaRegion xtAreaRegion = xt_Area_RegionList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtAreaRegion.getID());
			BaseZTreeEntity.setPid(""+xtAreaRegion.getPARENT_ID());
			BaseZTreeEntity.setText(xtAreaRegion.getNAME());
			BaseZTreeEntity.setTempObject("行政编码："+xtAreaRegion.getCODE()+"<br>行政级别："+xtAreaRegion.getREGION_LEVEL());
			BaseZTreeEntity.setContent(xtAreaRegion.getNAME_EN());
			BaseZTreeEntity.setIntegerappend("经度："+xtAreaRegion.getLONGITUDE()+"<br>纬度："+xtAreaRegion.getLATITUDE());
			if(("true").equals(expanded)){
				if(xtAreaRegion.getREGION_LEVEL() == 0){
					BaseZTreeEntity.setExpanded(true);
				}else{
					BaseZTreeEntity.setExpanded(false);
				}
			}else{
				BaseZTreeEntity.setExpanded(false);
			}
			if("true".equals(singleClickExpand)){
				BaseZTreeEntity.setSingleClickExpand(true);
			}else{
				BaseZTreeEntity.setSingleClickExpand(false);
			}
			BaseZTreeEntity.setLeaf(true);
			list.add(BaseZTreeEntity);
		}
		return outStr(BaseZTreeEntity.buildTree(list,false));
	}
	/**
	* 查询单条记录
	* @param ID 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtAreaRegionById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAreaRegionById(String ID,HttpServletRequest request){
		XtAreaRegion xtAreaRegion = xtAreaRegionService.getXtAreaRegionById(ID);
		return outDataStr(xtAreaRegion);
	}
	/**
	* 添加
	* @param xtAreaRegion
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtAreaRegion(XtAreaRegion xtAreaRegion,HttpServletRequest request){
		int i = 0;
		if(null != xtAreaRegion){
			xtAreaRegion.setID(UUID.toUUID());
			i=xtAreaRegionService.addXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtAreaRegion
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtAreaRegion(XtAreaRegion xtAreaRegion,HttpServletRequest request){
		int i = 0;
		if(null != xtAreaRegion){
			i=xtAreaRegionService.updateXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param ID 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtAreaRegion(String ID,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(ID)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("ID",ID.split(","));
			i=xtAreaRegionService.delXtAreaRegion(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param ID 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtAreaRegion(String ID,HttpServletRequest request){
		int i = 0;
		XtAreaRegion xtAreaRegion = xtAreaRegionService.getXtAreaRegionById(ID);
		if(null != xtAreaRegion){
			xtAreaRegion.setID(UUID.toUUID());
			i=xtAreaRegionService.addXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtAreaRegion",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtAreaRegion(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 查询省份
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询省份", notes="查询省份")
	@ResponseBody
	@RequestMapping(value="/getPList",method={RequestMethod.POST,RequestMethod.GET})
	@AuthUneedLogin
	public String getPList(HttpServletRequest request){
		List<XtAreaRegion> list = getXtAreaRegionCache(null);
		return outItemsStr(list);
	}
	
	/**
	 * 查询城市
	 * @param parentId
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询城市", notes="查询城市")
	@ResponseBody
	@RequestMapping(value="/getCList",method={RequestMethod.POST,RequestMethod.GET})
	@AuthUneedLogin
	public String getCList(String parentId,HttpServletRequest request){
		List<XtAreaRegion> list = new ArrayList<XtAreaRegion>();
		if(StringUtil.isEmpty(parentId)){
//			throw new ExceptionUtil("未能获取到省份编号");
			return outItemsStr(list);
		}
		list = getXtAreaRegionCache(parentId);
		return outItemsStr(list);
	}
	
	/**
	 * 查询区县
	 * @param parentId
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询区县", notes="查询区县")
	@ResponseBody
	@RequestMapping(value="/getDList",method={RequestMethod.POST,RequestMethod.GET})
	@AuthUneedLogin
	public String getDList(String parentId,HttpServletRequest request){
		List<XtAreaRegion> list = new ArrayList<XtAreaRegion>();
		if(StringUtil.isEmpty(parentId)){
//			throw new ExceptionUtil("未能获取到城市编号");
			return outItemsStr(list);
		}
		list = getXtAreaRegionCache(parentId);
		return outItemsStr(list);
	}
}
