package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtDbinfo;
import jehc.xtmodules.xtservice.XtDbinfoService;

/**
* 数据库信息
* 2015-09-30 16:37:40  邓纯杰
*/
@Api(value = "数据库信息", description = "数据库信息")
@Controller
@RequestMapping("/xtDbinfoController")
public class XtDbinfoController extends BaseAction{
	@Autowired
	private XtDbinfoService xtDbinfoService;
	/**
	* 列表页面
	* @param xtDbinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtDbinfo(XtDbinfo xtDbinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-dbinfo/xt-dbinfo-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtDbinfoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDbinfoListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonPager(condition,request);
		List<XtDbinfo> xt_DbinfoList = xtDbinfoService.getXtDbinfoListByCondition(condition);
		PageInfo<XtDbinfo> page = new PageInfo<XtDbinfo>(xt_DbinfoList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_dbinfo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtDbinfoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDbinfoById(String xt_dbinfo_id,HttpServletRequest request){
		XtDbinfo xtDbinfo = xtDbinfoService.getXtDbinfoById(xt_dbinfo_id);
		return outDataStr(xtDbinfo);
	}
	/**
	* 添加
	* @param xtDbinfo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtDbinfo(XtDbinfo xtDbinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtDbinfo){
			xtDbinfo.setXt_dbinfo_id(UUID.toUUID());
			i=xtDbinfoService.addXtDbinfo(xtDbinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtDbinfo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtDbinfo(XtDbinfo xtDbinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtDbinfo){
			i=xtDbinfoService.updateXtDbinfo(xtDbinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_dbinfo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtDbinfo(String xt_dbinfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_dbinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_dbinfo_id",xt_dbinfo_id.split(","));
			i=xtDbinfoService.delXtDbinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_dbinfo_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtDbinfo(String xt_dbinfo_id,HttpServletRequest request){
		int i = 0;
		XtDbinfo xtDbinfo = xtDbinfoService.getXtDbinfoById(xt_dbinfo_id);
		if(null != xtDbinfo){
			xtDbinfo.setXt_dbinfo_id(UUID.toUUID());
			i=xtDbinfoService.addXtDbinfo(xtDbinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtDbinfo",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtDbinfo(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtDbinfoAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbinfoAdd(XtDbinfo xtDbinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-dbinfo/xt-dbinfo-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtDbinfoUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbinfoUpdate(String xt_dbinfo_id,HttpServletRequest request, Model model){
		XtDbinfo xtDbinfo = xtDbinfoService.getXtDbinfoById(xt_dbinfo_id);
		model.addAttribute("xtDbinfo", xtDbinfo);
		return new ModelAndView("pc/xt-view/xt-dbinfo/xt-dbinfo-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtDbinfoDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbinfoDetail(String xt_dbinfo_id,HttpServletRequest request, Model model){
		XtDbinfo xtDbinfo = xtDbinfoService.getXtDbinfoById(xt_dbinfo_id);
		model.addAttribute("xtDbinfo", xtDbinfo);
		return new ModelAndView("pc/xt-view/xt-dbinfo/xt-dbinfo-detail");
	}
}
