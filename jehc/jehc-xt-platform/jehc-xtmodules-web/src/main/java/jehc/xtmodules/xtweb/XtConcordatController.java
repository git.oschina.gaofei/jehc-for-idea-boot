package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtConcordat;
import jehc.xtmodules.xtservice.XtConcordatService;

/**
* 合同管理 
* 2015-05-24 08:39:49  邓纯杰
*/
@Api(value = "合同管理", description = "合同管理")
@Controller
@RequestMapping("/xtConcordatController")
public class XtConcordatController extends BaseAction{
	@Autowired
	private XtConcordatService xtConcordatService;
	/**
	* 列表页面
	* @param xtConcordat
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtConcordat(XtConcordat xtConcordat,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-concordat/xt-concordat-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtConcordatListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtConcordatListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtConcordat> xtConcordatList = xtConcordatService.getXtConcordatListByCondition(condition);
		PageInfo<XtConcordat> page = new PageInfo<XtConcordat>(xtConcordatList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_concordat_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtConcordatById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtConcordatById(String xt_concordat_id,HttpServletRequest request){
		XtConcordat xtConcordat = xtConcordatService.getXtConcordatById(xt_concordat_id);
		return outDataStr(xtConcordat);
	}
	/**
	* 添加
	* @param xtConcordat
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtConcordat(XtConcordat xtConcordat,HttpServletRequest request){
		int i = 0;
		if(null != xtConcordat){
			xtConcordat.setXt_concordat_id(UUID.toUUID());
			i=xtConcordatService.addXtConcordat(xtConcordat);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtConcordat
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtConcordat(XtConcordat xtConcordat,HttpServletRequest request){
		int i = 0;
		if(null != xtConcordat){
			i=xtConcordatService.updateXtConcordat(xtConcordat);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_concordat_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtConcordat(String xt_concordat_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtils.isEmpty(xt_concordat_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_concordat_id",xt_concordat_id.split(","));
			i=xtConcordatService.delXtConcordat(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_concordat_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtConcordat(String xt_concordat_id,HttpServletRequest request){
		int i = 0;
		XtConcordat xt_Concordat = xtConcordatService.getXtConcordatById(xt_concordat_id);
		if(null != xt_Concordat && !"".equals(xt_Concordat)){
			xt_Concordat.setXt_concordat_id(UUID.toUUID());
			i=xtConcordatService.addXtConcordat(xt_Concordat);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtConcordat",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtConcordat(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtConcordatAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConcordatAdd(XtConcordat xtConcordat,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-concordat/xt-concordat-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtConcordatUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConcordatUpdate(String xt_concordat_id,HttpServletRequest request, Model model){
		XtConcordat xtConcordat = xtConcordatService.getXtConcordatById(xt_concordat_id);
		model.addAttribute("xtConcordat", xtConcordat);
		return new ModelAndView("pc/xt-view/xt-concordat/xt-concordat-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtConcordatDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConcordatDetail(String xt_concordat_id,HttpServletRequest request, Model model){
		XtConcordat xtConcordat = xtConcordatService.getXtConcordatById(xt_concordat_id);
		model.addAttribute("xtConcordat", xtConcordat);
		return new ModelAndView("pc/xt-view/xt-concordat/xt-concordat-detail");
	}
}
