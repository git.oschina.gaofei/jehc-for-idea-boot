package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtQuartz;
import jehc.xtmodules.xtservice.XtQuartzService;

/**
* 任务调度配置信息
* 2015-10-29 16:50:03  邓纯杰
*/
@Api(value = "任务调度配置信息", description = "任务调度配置信息")
@Controller
@RequestMapping("/xtQuartzController")
public class XtQuartzController extends BaseAction{
	@Autowired
	private XtQuartzService xtQuartzService;
	/**
	* 列表页面
	* @param xtQuartz
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtQuartz(XtQuartz xtQuartz,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-quartz/xt-quartz-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtQuartzListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtQuartzListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtQuartz> xtQuartzList = xtQuartzService.getXtQuartzListByCondition(condition);
		PageInfo<XtQuartz> page = new PageInfo<XtQuartz>(xtQuartzList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtQuartzById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtQuartzById(String id,HttpServletRequest request){
		XtQuartz xtQuartz = xtQuartzService.getXtQuartzById(id);
		return outDataStr(xtQuartz);
	}
	/**
	* 添加
	* @param xtQuartz
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtQuartz(XtQuartz xtQuartz,HttpServletRequest request){
		int i = 0;
		if(null != xtQuartz){
			xtQuartz.setId(UUID.toUUID());
			i=xtQuartzService.addXtQuartz(xtQuartz);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtQuartz
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtQuartz(XtQuartz xtQuartz,HttpServletRequest request){
		int i = 0;
		if(null != xtQuartz){
			i=xtQuartzService.updateXtQuartz(xtQuartz);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtQuartz(String id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=xtQuartzService.delXtQuartz(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtQuartz(String id,HttpServletRequest request){
		int i = 0;
		XtQuartz xtQuartz = xtQuartzService.getXtQuartzById(id);
		if(null != xtQuartz){
			xtQuartz.setId(UUID.toUUID());
			i=xtQuartzService.addXtQuartz(xtQuartz);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtQuartz",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtQuartz(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtQuartzAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtQuartzAdd(XtQuartz xtQuartz,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-quartz/xt-quartz-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtQuartzUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtQuartzUpdate(String id,HttpServletRequest request, Model model){
		XtQuartz xtQuartz = xtQuartzService.getXtQuartzById(id);
		model.addAttribute("xtQuartz", xtQuartz);
		return new ModelAndView("pc/xt-view/xt-quartz/xt-quartz-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtQuartzDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtQuartzDetail(String id,HttpServletRequest request, Model model){
		XtQuartz xtQuartz = xtQuartzService.getXtQuartzById(id);
		model.addAttribute("xtQuartz", xtQuartz);
		return new ModelAndView("pc/xt-view/xt-quartz/xt-quartz-detail");
	}
}
