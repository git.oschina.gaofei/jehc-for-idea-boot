package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtUnit;
import jehc.xtmodules.xtservice.XtUnitService;

/**
* 商品(产品)单位
* 2015-09-30 14:16:39  邓纯杰
*/
@Api(value = "商品(产品)单位", description = "商品(产品)单位")
@Controller
@RequestMapping("/xtUnitController")
public class XtUnitController extends BaseAction{
	@Autowired
	private XtUnitService xtUnitService;
	/**
	* 列表页面
	* @param xtUnit
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtUnit(XtUnit xtUnit,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-unit/xt-unit-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtUnitListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUnitListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtUnit> xtUnitList = xtUnitService.getXtUnitListByCondition(condition);
		PageInfo<XtUnit> page = new PageInfo<XtUnit>(xtUnitList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_unit_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtUnitById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtUnitById(String xt_unit_id,HttpServletRequest request){
		XtUnit xtUnit = xtUnitService.getXtUnitById(xt_unit_id);
		return outDataStr(xtUnit);
	}
	/**
	* 添加
	* @param xtUnit
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtUnit(XtUnit xtUnit,HttpServletRequest request){
		int i = 0;
		if(null != xtUnit){
			xtUnit.setXt_unit_id(UUID.toUUID());
			i=xtUnitService.addXtUnit(xtUnit);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtUnit
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtUnit(XtUnit xtUnit,HttpServletRequest request){
		int i = 0;
		if(null != xtUnit){
			i=xtUnitService.updateXtUnit(xtUnit);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_unit_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtUnit(String xt_unit_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_unit_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_unit_id",xt_unit_id.split(","));
			i=xtUnitService.delXtUnit(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_unit_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtUnit(String xt_unit_id,HttpServletRequest request){
		int i = 0;
		XtUnit xtUnit = xtUnitService.getXtUnitById(xt_unit_id);
		if(null != xtUnit){
			xtUnit.setXt_unit_id(UUID.toUUID());
			i=xtUnitService.addXtUnit(xtUnit);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtUnit",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtUnit(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtUnitAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUnitAdd(XtUnit xtUnit,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-unit/xt-unit-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtUnitUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUnitUpdate(String xt_unit_id,HttpServletRequest request, Model model){
		XtUnit xtUnit = xtUnitService.getXtUnitById(xt_unit_id);
		model.addAttribute("xtUnit", xtUnit);
		return new ModelAndView("pc/xt-view/xt-unit/xt-unit-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtUnitDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtUnitDetail(String xt_unit_id,HttpServletRequest request, Model model){
		XtUnit xtUnit = xtUnitService.getXtUnitById(xt_unit_id);
		model.addAttribute("xtUnit", xtUnit);
		return new ModelAndView("pc/xt-view/xt-unit/xt-unit-detail");
	}
}
