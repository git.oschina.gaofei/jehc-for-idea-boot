package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtGeneratorForbidtable;
import jehc.xtmodules.xtservice.XtGeneratorForbidtableService;

/**
* 禁止使用代码生成器生成的表信息 
* 2016-09-26 10:55:48  邓纯杰
*/
@Api(value = "禁止使用代码生成器生成的表信息", description = "禁止使用代码生成器生成的表信息")
@Controller
@RequestMapping("/xtGeneratorForbidtableController")
public class XtGeneratorForbidtableController extends BaseAction{
	@Autowired
	private XtGeneratorForbidtableService xtGeneratorForbidtableService;
	/**
	* 列表页面
	* @param xtGeneratorForbidtable
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtGeneratorForbidtable(XtGeneratorForbidtable xtGeneratorForbidtable,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-generator-forbidtable/xt-generator-forbidtable-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtGeneratorForbidtableListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtGeneratorForbidtableListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtGeneratorForbidtable> xt_Generator_ForbidtableList = xtGeneratorForbidtableService.getXtGeneratorForbidtableListByCondition(condition);
		PageInfo<XtGeneratorForbidtable> page = new PageInfo<XtGeneratorForbidtable>(xt_Generator_ForbidtableList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_generator_forbidtable_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtGeneratorForbidtableById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtGeneratorForbidtableById(String xt_generator_forbidtable_id,HttpServletRequest request){
		XtGeneratorForbidtable xtGeneratorForbidtable = xtGeneratorForbidtableService.getXtGeneratorForbidtableById(xt_generator_forbidtable_id);
		return outDataStr(xtGeneratorForbidtable);
	}
	/**
	* 添加
	* @param xtGeneratorForbidtable
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtGeneratorForbidtable(XtGeneratorForbidtable xtGeneratorForbidtable,HttpServletRequest request){
		int i = 0;
		if(null != xtGeneratorForbidtable){
			xtGeneratorForbidtable.setXt_generator_f_id(UUID.toUUID());
			xtGeneratorForbidtable.setXt_generator_f_ctime(getSimpleDateFormat());
			xtGeneratorForbidtable.setXt_userinfo_id(getXtUid());
			i=xtGeneratorForbidtableService.addXtGeneratorForbidtable(xtGeneratorForbidtable);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtGeneratorForbidtable
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtGeneratorForbidtable(XtGeneratorForbidtable xtGeneratorForbidtable,HttpServletRequest request){
		int i = 0;
		if(null != xtGeneratorForbidtable){
			xtGeneratorForbidtable.setXt_generator_f_mtime(getSimpleDateFormat());
			xtGeneratorForbidtable.setXt_userinfo_id(getXtUid());
			i=xtGeneratorForbidtableService.updateXtGeneratorForbidtable(xtGeneratorForbidtable);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_generator_forbidtable_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtGeneratorForbidtable(String xt_generator_forbidtable_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_generator_forbidtable_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_generator_f_id",xt_generator_forbidtable_id.split(","));
			i=xtGeneratorForbidtableService.delXtGeneratorForbidtable(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_generator_forbidtable_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtGeneratorForbidtable(String xt_generator_forbidtable_id,HttpServletRequest request){
		int i = 0;
		XtGeneratorForbidtable xtGeneratorForbidtable= xtGeneratorForbidtableService.getXtGeneratorForbidtableById(xt_generator_forbidtable_id);
		if(null != xtGeneratorForbidtable){
			xtGeneratorForbidtable.setXt_generator_f_id(UUID.toUUID());
			i=xtGeneratorForbidtableService.addXtGeneratorForbidtable(xtGeneratorForbidtable);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtGeneratorForbidtable",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtGeneratorForbidtable(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtGeneratorForbidtableAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtGeneratorForbidtableAdd(XtGeneratorForbidtable xtGeneratorForbidtable,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-generator-forbidtable/xt-generator-forbidtable-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtGeneratorForbidtableUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtGeneratorForbidtableUpdate(String xt_generator_forbidtable_id,HttpServletRequest request, Model model){
		XtGeneratorForbidtable xtGeneratorForbidtable = xtGeneratorForbidtableService.getXtGeneratorForbidtableById(xt_generator_forbidtable_id);
		model.addAttribute("xtGeneratorForbidtable", xtGeneratorForbidtable);
		return new ModelAndView("pc/xt-view/xt-generator-forbidtable/xt-generator-forbidtable-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtGeneratorForbidtableDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtGeneratorForbidtableDetail(String xt_generator_forbidtable_id,HttpServletRequest request, Model model){
		XtGeneratorForbidtable xtGeneratorForbidtable = xtGeneratorForbidtableService.getXtGeneratorForbidtableById(xt_generator_forbidtable_id);
		model.addAttribute("xtGeneratorForbidtable", xtGeneratorForbidtable);
		return new ModelAndView("pc/xt-view/xt-generator-forbidtable/xt-generator-forbidtable-detail");
	}
}
