package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtLoginLogs;
import jehc.xtmodules.xtservice.XtLoginLogsService;

/**
* 登录日志 
* 2015-05-24 08:17:40  邓纯杰
*/
@Api(value = "登录日志", description = "登录日志")
@Controller
@RequestMapping("/xtLoginLogsController")
public class XtLoginLogsController extends BaseAction{
	@Autowired
	private XtLoginLogsService xtLoginLogsService;
	/**
	* 列表页面
	* @param xtLoginLogs
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtLoginLogs(XtLoginLogs xtLoginLogs,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-login-logs/xt-login-logs-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtLoginLogsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtLoginLogsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		String flag = request.getParameter("flag");
		if(null != flag){
			condition.put("xt_userinfo_id", CommonUtils.getXtUid());
		}
		commonHPager(condition,request);
		List<XtLoginLogs> xtLoginLogsList = xtLoginLogsService.getXtLoginLogsListByCondition(condition);
		PageInfo<XtLoginLogs> page = new PageInfo<XtLoginLogs>(xtLoginLogsList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_login_log_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtLoginLogsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtLoginLogsById(String xt_login_log_id,HttpServletRequest request){
		XtLoginLogs xtLoginLogs = xtLoginLogsService.getXtLoginLogsById(xt_login_log_id);
		return outDataStr(xtLoginLogs);
	}
	/**
	* 添加
	* @param xtLoginLogs
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtLoginLogs(XtLoginLogs xtLoginLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtLoginLogs){
			xtLoginLogs.setXt_login_log_id(UUID.toUUID());
			i=xtLoginLogsService.addXtLoginLogs(xtLoginLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtLoginLogs
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtLoginLogs(XtLoginLogs xtLoginLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtLoginLogs){
			i=xtLoginLogsService.updateXtLoginLogs(xtLoginLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_login_log_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtLoginLogs(String xt_login_log_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_login_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_login_log_id",xt_login_log_id.split(","));
			i=xtLoginLogsService.delXtLoginLogs(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 统计出每个人登录的次数
	 * @param request
	 * @return
	 */
	@ApiOperation(value="统计出每个人登录的次数", notes="统计出每个人登录的次数")
	@ResponseBody
	@RequestMapping(value="/getGroupXtLoginLogsList",method={RequestMethod.POST,RequestMethod.GET})
	public String getGroupXtLoginLogsList(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtLoginLogs> list = xtLoginLogsService.getGroupXtLoginLogsList(condition);
		return outItemsStr(list);
	}
	/**
	* 复制一行并生成记录
	* @param xt_login_log_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtLoginLogs(String xt_login_log_id,HttpServletRequest request){
		int i = 0;
		XtLoginLogs xtLoginLogs = xtLoginLogsService.getXtLoginLogsById(xt_login_log_id);
		if(null != xtLoginLogs){
			xtLoginLogs.setXt_login_log_id(UUID.toUUID());
			i=xtLoginLogsService.addXtLoginLogs(xtLoginLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtLoginLogs",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtLoginLogs(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtLoginLogsDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtLoginLogsDetail(String xt_login_log_id,HttpServletRequest request, Model model){
		XtLoginLogs xtLoginLogs = xtLoginLogsService.getXtLoginLogsById(xt_login_log_id);
		model.addAttribute("xtLoginLogs", xtLoginLogs);
		return new ModelAndView("pc/xt-view/xt-login-logs/xt-login-logs-detail");
	}
}
