package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtStartStopLog;
import jehc.xtmodules.xtservice.XtStartStopLogService;

/**
* 服务器启动与关闭日志
* 2015-10-31 21:28:14  邓纯杰
*/
@Api(value = "服务器启动与关闭日志", description = "服务器启动与关闭日志")
@Controller
@RequestMapping("/xtStartStopLogController")
public class XtStartStopLogController extends BaseAction{
	@Autowired
	private XtStartStopLogService xtStartStopLogService;
	/**
	* 列表页面
	* @param xtStartStopLog
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtStartStopLog(XtStartStopLog xtStartStopLog,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-start-stop-log/xt-start-stop-log-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtStartStopLogListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtStartStopLogListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtStartStopLog> xtStartStopLogList = xtStartStopLogService.getXtStartStopLogListByCondition(condition);
		PageInfo<XtStartStopLog> page = new PageInfo<XtStartStopLog>(xtStartStopLogList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_start_stop_log_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtStartStopLogById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtStartStopLogById(String xt_start_stop_log_id,HttpServletRequest request){
		XtStartStopLog xtStartStopLog = xtStartStopLogService.getXtStartStopLogById(xt_start_stop_log_id);
		return outDataStr(xtStartStopLog);
	}
	/**
	* 添加
	* @param xtStartStopLog
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtStartStopLog(XtStartStopLog xtStartStopLog,HttpServletRequest request){
		int i = 0;
		if(null != xtStartStopLog){
			xtStartStopLog.setXt_start_stop_log_id(UUID.toUUID());
			i=xtStartStopLogService.addXtStartStopLog(xtStartStopLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtStartStopLog
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtStartStopLog(XtStartStopLog xtStartStopLog,HttpServletRequest request){
		int i = 0;
		if(null != xtStartStopLog){
			i=xtStartStopLogService.updateXtStartStopLog(xtStartStopLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_start_stop_log_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtStartStopLog(String xt_start_stop_log_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_start_stop_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_start_stop_log_id",xt_start_stop_log_id.split(","));
			i=xtStartStopLogService.delXtStartStopLog(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_start_stop_log_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtStartStopLog(String xt_start_stop_log_id,HttpServletRequest request){
		int i = 0;
		XtStartStopLog xtStartStopLog = xtStartStopLogService.getXtStartStopLogById(xt_start_stop_log_id);
		if(null != xtStartStopLog){
			xtStartStopLog.setXt_start_stop_log_id(UUID.toUUID());
			i=xtStartStopLogService.addXtStartStopLog(xtStartStopLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtStartStopLog",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtStartStopLog(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtStartStopLogDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtStartStopLogDetail(String xt_start_stop_log_id,HttpServletRequest request, Model model){
		XtStartStopLog xtStartStopLog = xtStartStopLogService.getXtStartStopLogById(xt_start_stop_log_id);
		model.addAttribute("xtStartStopLog", xtStartStopLog);
		return new ModelAndView("pc/xt-view/xt-start-stop-log/xt-start-stop-log-detail");
	}
}
