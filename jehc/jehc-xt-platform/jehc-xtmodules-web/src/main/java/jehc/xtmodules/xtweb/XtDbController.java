package jehc.xtmodules.xtweb;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtmodel.XtDb;
import jehc.xtmodules.xtmodel.XtDbinfo;
import jehc.xtmodules.xtservice.XtDbService;
/**
 * 数据库备份
 * @author 邓纯杰
 *
 */
@Api(value = "数据库备份", description = "数据库备份")
@Controller
@RequestMapping("/xtDbController")
public class XtDbController extends BaseAction {
	@Autowired
	private XtDbService xtDbService;
	
	/**
	* 列表页面
	* @param xtDbinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtDb",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtDbinfo(XtDbinfo xtDbinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-db/xt-db-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtDbListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDbListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonPager(condition,request);
		List<XtDb> xtDbinfoList = xtDbService.getXtDbListByCondition(condition);
		PageInfo<XtDb> page = new PageInfo<XtDb>(xtDbinfoList);
		return outPageBootStr(page,request);
	}
}
