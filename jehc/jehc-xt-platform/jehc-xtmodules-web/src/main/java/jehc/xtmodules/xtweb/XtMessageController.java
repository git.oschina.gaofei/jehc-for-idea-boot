package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtMessage;
import jehc.xtmodules.xtservice.XtMessageService;

/**
* 短消息 
* 2016-10-20 17:49:40  邓纯杰
*/
@Api(value = "短消息", description = "短消息")
@Controller
@RequestMapping("/xtMessageController")
public class XtMessageController extends BaseAction{
	@Autowired
	private XtMessageService xtMessageService;
	/**
	* 列表页面
	* @param xtMessage
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtMessage(XtMessage xtMessage,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-message/xt-message-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtMessageListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMessageListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		if("0".equals(condition.get("type"))){//我发送的消息
			condition.put("from_id", getXtUid());
		}else if("1".equals(condition.get("type"))){
			condition.put("to_id", getXtUid());
		}else{
			condition.put("to_id", "-1");
		}
		commonHPager(condition,request);
		List<XtMessage> xtMessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> page = new PageInfo<XtMessage>(xtMessageList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_message_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtMessageById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMessageById(String xt_message_id,String type,HttpServletRequest request){
		if("1".equals(type)){
			Map<String, Object> condition = new HashMap<String,Object>();
			condition.put("isread", 1);
			condition.put("readtime", getDate());
			condition.put("xt_message_id", xt_message_id);
			xtMessageService.updateRead(condition);
		}
		XtMessage xtMessage = xtMessageService.getXtMessageById(xt_message_id);
		return outDataStr(xtMessage);
	}
	/**
	* 添加
	* @param xtMessage
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtMessage(@Valid XtMessage xtMessage,BindingResult bindingResult,HttpServletRequest request){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtMessage){
			xtMessage.setXt_message_id(UUID.toUUID());
			xtMessage.setCtime(getDate());
			xtMessage.setFrom_id(getXtUid());
			i=xtMessageService.addXtMessage(xtMessage);
		}
		if(i>0){
			return outAudStr(true,"发送成功");
		}else{
			return outAudStr(false,"发送失败");
		}
	}
	/**
	* 修改
	* @param xtMessage
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtMessage(XtMessage xtMessage,HttpServletRequest request){
		int i = 0;
		if(null != xtMessage){
			i=xtMessageService.updateXtMessage(xtMessage);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_message_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtMessage(String xt_message_id,String type ,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_message_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_message_id",xt_message_id.split(","));
			condition.put("type",type);
			i=xtMessageService.delXtMessage(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_message_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtMessage(String xt_message_id,HttpServletRequest request){
		int i = 0;
		XtMessage xtMessage = xtMessageService.getXtMessageById(xt_message_id);
		if(null != xtMessage){
			xtMessage.setXt_message_id(UUID.toUUID());
			i=xtMessageService.addXtMessage(xtMessage);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtMessage",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtMessage(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 更新已读状态
	 * @param request
	 * @return
	 */
	@ApiOperation(value="更新已读状态", notes="更新已读状态")
	@ResponseBody
	@RequestMapping(value="/updateRead",method={RequestMethod.POST,RequestMethod.GET})
	public String updateRead(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("isread", 1);
		condition.put("from_id", getXtUid());
		condition.put("to_id", request.getParameter("to_id"));
		int i = xtMessageService.updateRead(condition);
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 查询历史数据并分页
	* @param xt_Message
	* @param request 
	*/
	@ApiOperation(value="查询历史数据并分页", notes="查询历史数据并分页")
	@ResponseBody
	@RequestMapping(value="/getXtMessageHisListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMessageHisListByCondition(XtMessage xt_Message,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		condition.put("to_id", xt_Message.getTo_id());
		condition.put("from_id", getXtUid());
		List<XtMessage> xtMessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> page = new PageInfo<XtMessage>(xtMessageList);
		condition = new HashMap<String, Object>();
		condition.put("to_id", xt_Message.getTo_id());
		condition.put("from_id", getXtUid());
		condition.put("isread", 1);
		xtMessageService.updateRead(condition);
		return outPageStr(page,request);
	}
	
	/**
	 * 查询未读消息个数
	 * @param xtMessage
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询未读消息个数", notes="查询未读消息个数")
	@ResponseBody
	@RequestMapping(value="/getXtMessageUnReadCount",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMessageUnReadCount(XtMessage xtMessage,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("to_id", getXtUid());
		condition.put("isread", 0);
		List<XtMessage> xtMessageList = xtMessageService.getXtMessageCountByCondition(condition);
		return outItemsStr(xtMessageList);
	}


	/**
	 * 新增页面
	 * @param xtMessage
	 * @param request
	 * @return
	 */
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtMessageAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtMessageAdd(XtMessage xtMessage,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-message/xt-message-add");
	}

	/**
	 * 编辑页面
	 * @param xt_message_id
	 * @param request
	 * @param model
	 * @return
	 */
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtMessageUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtMessageUpdate(String xt_message_id,HttpServletRequest request, Model model){
		XtMessage xtMessage = xtMessageService.getXtMessageById(xt_message_id);
		model.addAttribute("xtMessage", xtMessage);
		return new ModelAndView("pc/xt-view/xt-message/xt-message-update");
	}

	/**
	 * 详情页面
	 * @param xt_message_id
	 * @param type
	 * @param request
	 * @param model
	 * @return
	 */
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtMessageDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtMessageDetail(String xt_message_id,String type,HttpServletRequest request, Model model){
		if("1".equals(type)){
			Map<String, Object> condition = new HashMap<String,Object>();
			condition.put("isread", 1);
			condition.put("readtime", getDate());
			condition.put("xt_message_id", xt_message_id);
			xtMessageService.updateRead(condition);
		}
		XtMessage xtMessage = xtMessageService.getXtMessageById(xt_message_id);
		model.addAttribute("xtMessage", xtMessage);
		return new ModelAndView("pc/xt-view/xt-message/xt-message-detail");
	}
}
