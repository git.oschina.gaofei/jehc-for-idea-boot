package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtQuartzLog;
import jehc.xtmodules.xtservice.XtQuartzLogService;

/**
* 调度器日志 
* 2016-05-25 20:16:23  邓纯杰
*/
@Api(value = "调度器日志", description = "调度器日志")
@Controller
@RequestMapping("/xtQuartzLogController")
public class XtQuartzLogController extends BaseAction{
	@Autowired
	private XtQuartzLogService xtQuartzLogService;
	/**
	* 列表页面
	* @param xtQuartzLog
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtQuartzLog(XtQuartzLog xtQuartzLog,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-quartz-log/xt-quartz-log-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtQuartzLogListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtQuartzLogListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition =  baseSearch.convert();
		commonHPager(condition,request);
		List<XtQuartzLog> xtQuartzLogList = xtQuartzLogService.getXtQuartzLogListByCondition(condition);
		PageInfo<XtQuartzLog> page = new PageInfo<XtQuartzLog>(xtQuartzLogList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_quartz_log_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtQuartzLogById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtQuartzLogById(String xt_quartz_log_id,HttpServletRequest request){
		XtQuartzLog xtQuartzLog = xtQuartzLogService.getXtQuartzLogById(xt_quartz_log_id);
		return outDataStr(xtQuartzLog);
	}
	/**
	* 添加
	* @param xtQuartzLog
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtQuartzLog(XtQuartzLog xtQuartzLog,HttpServletRequest request){
		int i = 0;
		if(null != xtQuartzLog){
			xtQuartzLog.setXt_quartz_log_id(UUID.toUUID());
			i=xtQuartzLogService.addXtQuartzLog(xtQuartzLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtQuartzLog
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtQuartzLog(XtQuartzLog xtQuartzLog,HttpServletRequest request){
		int i = 0;
		if(null != xtQuartzLog){
			i=xtQuartzLogService.updateXtQuartzLog(xtQuartzLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_quartz_log_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtQuartzLog(String xt_quartz_log_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_quartz_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_quartz_log_id",xt_quartz_log_id.split(","));
			i=xtQuartzLogService.delXtQuartzLog(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_quartz_log_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtQuartzLog(String xt_quartz_log_id,HttpServletRequest request){
		int i = 0;
		XtQuartzLog xtQuartzLog = xtQuartzLogService.getXtQuartzLogById(xt_quartz_log_id);
		if(null != xtQuartzLog && !"".equals(xtQuartzLog)){
			xtQuartzLog.setXt_quartz_log_id(UUID.toUUID());
			i=xtQuartzLogService.addXtQuartzLog(xtQuartzLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtQuartzLog",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtQuartzLog(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtQuartzLogDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtQuartzLogDetail(String xt_quartz_log_id,HttpServletRequest request, Model model){
		XtQuartzLog xtQuartzLog = xtQuartzLogService.getXtQuartzLogById(xt_quartz_log_id);
		model.addAttribute("xtQuartzLog", xtQuartzLog);
		return new ModelAndView("pc/xt-view/xt-quartz-log/xt-quartz-log-detail");
	}
}
