package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtServiceCenterParameter;
import jehc.xtmodules.xtservice.XtServiceCenterParameterService;

/**
* 服务中心参数 
* 2017-03-27 12:32:04  邓纯杰
*/
@Api(value = "服务中心参数", description = "服务中心参数")
@Controller
@RequestMapping("/xtServiceCenterParameterController")
public class XtServiceCenterParameterController extends BaseAction{
	@Autowired
	private XtServiceCenterParameterService xtServiceCenterParameterService;
	/**
	* 列表页面
	* @param xtServiceCenterParameter
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-service-center-parameter/xt-service-center-parameter-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtServiceCenterParameterListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtServiceCenterParameterListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		condition.put("xt_service_center_status",request.getParameter("xt_service_center_status"));
		condition.put("xt_service_center_type",request.getParameter("xt_service_center_type"));
		condition.put("xt_service_center_name",request.getParameter("xt_service_center_name"));
		condition.put("xt_service_center_id",request.getParameter("xt_service_center_id"));
		List<XtServiceCenterParameter> xtServiceCenterParameterList = xtServiceCenterParameterService.getXtServiceCenterParameterListByCondition(condition);
		PageInfo<XtServiceCenterParameter> page = new PageInfo<XtServiceCenterParameter>(xtServiceCenterParameterList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_service_center_parameter_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtServiceCenterParameterById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtServiceCenterParameterById(String xt_service_center_parameter_id,HttpServletRequest request){
		XtServiceCenterParameter xtServiceCenterParameter = xtServiceCenterParameterService.getXtServiceCenterParameterById(xt_service_center_parameter_id);
		return outDataStr(xtServiceCenterParameter);
	}
	/**
	* 添加
	* @param xtServiceCenterParameter
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter,HttpServletRequest request){
		int i = 0;
		if(null != xtServiceCenterParameter){
			xtServiceCenterParameter.setXt_service_center_parameter_id(UUID.toUUID());
			i=xtServiceCenterParameterService.addXtServiceCenterParameter(xtServiceCenterParameter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtServiceCenterParameter
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter,HttpServletRequest request){
		int i = 0;
		if(null != xtServiceCenterParameter){
			i=xtServiceCenterParameterService.updateXtServiceCenterParameter(xtServiceCenterParameter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_service_center_parameter_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtServiceCenterParameter(String xt_service_center_parameter_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_service_center_parameter_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_service_center_parameter_id",xt_service_center_parameter_id.split(","));
			i=xtServiceCenterParameterService.delXtServiceCenterParameter(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_service_center_parameter_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtServiceCenterParameter(String xt_service_center_parameter_id,HttpServletRequest request){
		int i = 0;
		XtServiceCenterParameter xtServiceCenterParameter = xtServiceCenterParameterService.getXtServiceCenterParameterById(xt_service_center_parameter_id);
		if(null != xtServiceCenterParameter){
			xtServiceCenterParameter.setXt_service_center_parameter_id(UUID.toUUID());
			i=xtServiceCenterParameterService.addXtServiceCenterParameter(xtServiceCenterParameter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtServiceCenterParameter",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtServiceCenterParameter(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
