package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtFunctioninfoRight;
import jehc.xtmodules.xtservice.XtFunctioninfoRightService;

/**
* 功能分配
* 2016-10-08 17:38:19  邓纯杰
*/
@Api(value = "功能分配", description = "功能分配")
@Controller
@RequestMapping("/xtFunctioninfoRightController")
public class XtFunctioninfoRightController extends BaseAction{
	@Autowired
	private XtFunctioninfoRightService xtFunctioninfoRightService;
	/**
	* 列表页面
	* @param xtFunctioninfoRight
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-functioninfo-right/xt-functioninfo-right-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtFunctioninfoRightListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtFunctioninfoRightListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtFunctioninfoRight> xtFunctioninfoRightList = xtFunctioninfoRightService.getXtFunctioninfoRightListByCondition(condition);
		PageInfo<XtFunctioninfoRight> page = new PageInfo<XtFunctioninfoRight>(xtFunctioninfoRightList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_functioninfo_right_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtFunctioninfoRightById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtFunctioninfoRightById(String xt_functioninfo_right_id,HttpServletRequest request){
		XtFunctioninfoRight xtFunctioninfoRight = xtFunctioninfoRightService.getXtFunctioninfoRightById(xt_functioninfo_right_id);
		return outDataStr(xtFunctioninfoRight);
	}
	/**
	* 添加
	* @param xtFunctioninfoRight
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight,HttpServletRequest request){
		int i = 0;
		if(null != xtFunctioninfoRight){
			xtFunctioninfoRight.setXt_functioninfo_right_id(UUID.toUUID());
			i=xtFunctioninfoRightService.addXtFunctioninfoRight(xtFunctioninfoRight);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtFunctioninfoRight
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight,HttpServletRequest request){
		int i = 0;
		if(null != xtFunctioninfoRight){
			i=xtFunctioninfoRightService.updateXtFunctioninfoRight(xtFunctioninfoRight);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_functioninfo_right_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtFunctioninfoRight(String xt_functioninfo_right_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_functioninfo_right_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_functioninfo_right_id",xt_functioninfo_right_id.split(","));
			i=xtFunctioninfoRightService.delXtFunctioninfoRight(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_functioninfo_right_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtFunctioninfoRight(String xt_functioninfo_right_id,HttpServletRequest request){
		int i = 0;
		XtFunctioninfoRight xtFunctioninfoRight = xtFunctioninfoRightService.getXtFunctioninfoRightById(xt_functioninfo_right_id);
		if(null != xt_functioninfo_right_id){
			xtFunctioninfoRight.setXt_functioninfo_right_id(UUID.toUUID());
			i=xtFunctioninfoRightService.addXtFunctioninfoRight(xtFunctioninfoRight);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtFunctioninfoRight",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtFunctioninfoRight(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
