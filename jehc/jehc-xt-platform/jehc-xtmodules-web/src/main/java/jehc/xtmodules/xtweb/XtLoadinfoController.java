package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.DateUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtLoadinfo;
import jehc.xtmodules.xtservice.XtLoadinfoService;

/**
* 页面加载信息、
* 2015-09-30 15:30:38  邓纯杰
*/
@Api(value = "页面加载信息", description = "页面加载信息")
@Controller
@RequestMapping("/xtLoadinfoController")
public class XtLoadinfoController extends BaseAction{
	@Autowired
	private XtLoadinfoService xtLoadinfoService;
	/**
	* 列表页面
	* @param xtLoadinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtLoadinfo(XtLoadinfo xtLoadinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-loadinfo/xt-loadinfo-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtLoadinfoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtLoadinfoListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtLoadinfo> xtLoadinfoList = xtLoadinfoService.getXtLoadinfoListByCondition(condition);
		PageInfo<XtLoadinfo> page = new PageInfo<XtLoadinfo>(xtLoadinfoList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_loadinfo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtLoadinfoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtLoadinfoById(String xt_loadinfo_id,HttpServletRequest request){
		XtLoadinfo xtLoadinfo = xtLoadinfoService.getXtLoadinfoById(xt_loadinfo_id);
		return outDataStr(xtLoadinfo);
	}
	/**
	* 添加
	* @param xtLoadinfo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtLoadinfo(XtLoadinfo xtLoadinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtLoadinfo){
			xtLoadinfo.setXt_loadinfo_time((int)(Long.parseLong(xtLoadinfo.getXt_loadinfo_endtime())-Long.parseLong(xtLoadinfo.getXt_loadinfo_begtime())));
			xtLoadinfo.setXt_loadinfo_begtime(DateUtils.convert(Long.parseLong(xtLoadinfo.getXt_loadinfo_begtime())));
			xtLoadinfo.setXt_loadinfo_endtime(DateUtils.convert(Long.parseLong(xtLoadinfo.getXt_loadinfo_endtime())));
			xtLoadinfo.setXt_loadinfo_id(UUID.toUUID());
			xtLoadinfo.setXt_userinfo_id(CommonUtils.getXtUid());
			i=xtLoadinfoService.addXtLoadinfo(xtLoadinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtLoadinfo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtLoadinfo(XtLoadinfo xtLoadinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtLoadinfo){
			i=xtLoadinfoService.updateXtLoadinfo(xtLoadinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_loadinfo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtLoadinfo(String xt_loadinfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_loadinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_loadinfo_id",xt_loadinfo_id.split(","));
			i=xtLoadinfoService.delXtLoadinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_loadinfo_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtLoadinfo(String xt_loadinfo_id,HttpServletRequest request){
		int i = 0;
		XtLoadinfo xtLoadinfo = xtLoadinfoService.getXtLoadinfoById(xt_loadinfo_id);
		if(null != xtLoadinfo){
			xtLoadinfo.setXt_loadinfo_id(UUID.toUUID());
			i=xtLoadinfoService.addXtLoadinfo(xtLoadinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtLoadinfo",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtLoadinfo(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 分组统计
	 * @return
	 */
	@ApiOperation(value="分组统计", notes="分组统计")
	@ResponseBody
	@RequestMapping(value="/getXtLoadingGroupList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtLoadingGroupList(){
		JSONArray jsonArray = new JSONArray();  
		Map<String, Object> model = new HashMap<String, Object>();
		List<XtLoadinfo> xtLoadinfoList = xtLoadinfoService.getXtLoadingGroupList();
		for(int i = 0; i < xtLoadinfoList.size(); i++){
			XtLoadinfo xtLoadinfo = xtLoadinfoList.get(i);
			model.put("menuTitle", xtLoadinfo.getXt_loadinfo_modules());
			model.put("loadingTime", xtLoadinfo.getXt_loadinfo_time());
			jsonArray.add(model);
		}
		return outItemsStr(jsonArray);
	}
}
