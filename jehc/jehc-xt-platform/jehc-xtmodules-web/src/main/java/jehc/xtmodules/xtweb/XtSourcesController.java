package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtSources;
import jehc.xtmodules.xtservice.XtSourcesService;

/**
* 平台静态资源 
* 2016-06-16 10:34:06  邓纯杰
*/
@Api(value = "平台静态资源", description = "平台静态资源")
@Controller
@RequestMapping("/xtSourcesController")
public class XtSourcesController extends BaseAction{
	@Autowired
	private XtSourcesService xtSourcesService;
	/**
	* 列表页面
	* @param xtSources
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtSources(XtSources xtSources,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-sources/xt-sources-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtSourcesListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtSourcesListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		if(null != request.getParameter("xt_sources_status") && !"".equals(request.getParameter("xt_sources_status"))){
			condition.put("xt_sources_status",request.getParameter("xt_sources_status"));
		}
		if(null != request.getParameter("xt_sources_type") && !"".equals(request.getParameter("xt_sources_type"))){
			condition.put("xt_sources_type",request.getParameter("xt_sources_type"));
		}
		if(null != request.getParameter("xt_sources_title") && !"".equals(request.getParameter("xt_sources_title"))){
			condition.put("xt_sources_title",request.getParameter("xt_sources_title"));
		}
		List<XtSources> xtSourcesList = xtSourcesService.getXtSourcesListByCondition(condition);
		String jehcsources_base_url = CommonUtils.getXtPathCache("jehcsources_base_url").get(0).getXt_path();
		for(int i = 0; i < xtSourcesList.size(); i++){
			xtSourcesList.get(i).setJehcsources_base_url(jehcsources_base_url);
		}
		PageInfo<XtSources> page = new PageInfo<XtSources>(xtSourcesList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_sources_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtSourcesById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtSourcesById(String xt_sources_id,HttpServletRequest request){
		XtSources xtSources = xtSourcesService.getXtSourcesById(xt_sources_id);
		return outDataStr(xtSources);
	}
	/**
	* 添加
	* @param xtSources
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtSources(XtSources xtSources,HttpServletRequest request){
		int i = 0;
		if(null != xtSources){
			xtSources.setXt_sources_id(UUID.toUUID());
			xtSources.setXt_sources_ctime(getSimpleDateFormat());
			xtSources.setXt_userinfo_id(getXtUid());
			i=xtSourcesService.addXtSources(xtSources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtSources
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtSources(XtSources xtSources,HttpServletRequest request){
		int i = 0;
		if(null != xtSources){
			xtSources.setXt_sources_mtime(getSimpleDateFormat());
			xtSources.setXt_userinfo_id(getXtUid());
			i=xtSourcesService.updateXtSources(xtSources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_sources_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtSources(String xt_sources_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_sources_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_sources_id",xt_sources_id.split(","));
			i=xtSourcesService.delXtSources(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_sources_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtSources(String xt_sources_id,HttpServletRequest request){
		int i = 0;
		XtSources xtSources = xtSourcesService.getXtSourcesById(xt_sources_id);
		if(null != xtSources){
			xtSources.setXt_userinfo_id(getXtUid());
			xtSources.setXt_sources_id(UUID.toUUID());
			i=xtSourcesService.addXtSources(xtSources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtSources",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtSources(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	
	/**
	* 视图列表
	* @param xtSources
	* @param request 
	* @return
	*/
	@ApiOperation(value="视图列表", notes="视图列表")
	@RequestMapping(value="/toXtSourcesDataView",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtSourcesDataView(XtSources xtSources,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-sources/xt-sources-dataview-list");
	}
}
