package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtModifyRecord;
import jehc.xtmodules.xtservice.XtModifyRecordService;

/**
* 修改记录日志 
* 2017-04-13 12:50:49  邓纯杰
*/
@Api(value = "修改记录日志", description = "修改记录日志")
@Controller
@RequestMapping("/xtModifyRecordController")
public class XtModifyRecordController extends BaseAction{
	@Autowired
	private XtModifyRecordService xtModifyRecordService;
	/**
	* 列表页面
	* @param xtModifyRecord
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtModifyRecord(XtModifyRecord xtModifyRecord,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-modify-record/xt-modify-record-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtModifyRecordListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtModifyRecordListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtModifyRecord> list = xtModifyRecordService.getXtModifyRecordListByCondition(condition);
		PageInfo<XtModifyRecord> page = new PageInfo<XtModifyRecord>(list);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_modify_record_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtModifyRecordById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtModifyRecordById(String xt_modify_record_id,HttpServletRequest request){
		XtModifyRecord xtModifyRecord = xtModifyRecordService.getXtModifyRecordById(xt_modify_record_id);
		return outDataStr(xtModifyRecord);
	}
	/**
	* 添加
	* @param xtModifyRecord
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtModifyRecord(XtModifyRecord xtModifyRecord,HttpServletRequest request){
		int i = 0;
		if(null != xtModifyRecord){
			xtModifyRecord.setXt_modify_record_id(UUID.toUUID());
			i=xtModifyRecordService.addXtModifyRecord(xtModifyRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtModifyRecord
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtModifyRecord(XtModifyRecord xtModifyRecord,HttpServletRequest request){
		int i = 0;
		if(null != xtModifyRecord){
			i=xtModifyRecordService.updateXtModifyRecord(xtModifyRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_modify_record_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtModifyRecord(String xt_modify_record_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_modify_record_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_modify_record_id",xt_modify_record_id.split(","));
			i=xtModifyRecordService.delXtModifyRecord(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_modify_record_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtModifyRecord(String xt_modify_record_id,HttpServletRequest request){
		int i = 0;
		XtModifyRecord xtModifyRecord = xtModifyRecordService.getXtModifyRecordById(xt_modify_record_id);
		if(null != xtModifyRecord){
			xtModifyRecord.setXt_modify_record_id(UUID.toUUID());
			i=xtModifyRecordService.addXtModifyRecord(xtModifyRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtModifyRecord",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtModifyRecord(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtModifyRecordDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtModifyRecordDetail(String xt_modify_record_id,HttpServletRequest request, Model model){
		XtModifyRecord xtModifyRecord = xtModifyRecordService.getXtModifyRecordById(xt_modify_record_id);
		model.addAttribute("xtModifyRecord", xtModifyRecord);
		return new ModelAndView("pc/xt-view/xt-modify-record/xt-modify-record-detail");
	}
}
