package jehc.xtmodules.xtweb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.base.BaseTreeGridEntity;
import jehc.xtmodules.xtcore.base.BaseZTreeEntity;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtmodel.XtFunctioninfo;
import jehc.xtmodules.xtmodel.XtMenuinfo;
import jehc.xtmodules.xtservice.XtFunctioninfoService;
import jehc.xtmodules.xtservice.XtMenuinfoService;

/**
* 功能
* 2015-06-01 20:41:56  邓纯杰
*/
@Api(value = "功能", description = "功能")
@Controller
@RequestMapping("/xtFunctioninfoController")
public class XtFunctioninfoController extends BaseAction{
	@Autowired
	private XtFunctioninfoService xtFunctioninfoService;
	
	@Autowired
	private XtMenuinfoService xtMenuinfoService;
	/**
	* 列表页面
	* @param xtFunctioninfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtFunctioninfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtFunctioninfo(XtFunctioninfo xtFunctioninfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-functioninfo/xt-functioninfo-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtFunctioninfoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtFunctioninfoListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtFunctioninfo> xtFunctioninfoList = xtFunctioninfoService.getXtFunctioninfoListByCondition(condition);
		PageInfo<XtFunctioninfo> page = new PageInfo<XtFunctioninfo>(xtFunctioninfoList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_functioninfo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtFunctioninfoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtFunctioninfoById(String xt_functioninfo_id,HttpServletRequest request){
		XtFunctioninfo xtFunctioninfo = xtFunctioninfoService.getXtFunctioninfoById(xt_functioninfo_id);
		return outDataStr(xtFunctioninfo);
	}
	/**
	* 添加
	* @param xtFunctioninfo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtFunctioninfo",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtFunctioninfo(XtFunctioninfo xtFunctioninfo,HttpServletRequest request){
		int i = 0;
		if(null != xtFunctioninfo){
			xtFunctioninfo.setXt_functioninfo_id(UUID.toUUID());
			i=xtFunctioninfoService.addXtFunctioninfo(xtFunctioninfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtFunctioninfo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtFunctioninfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtFunctioninfo(XtFunctioninfo xtFunctioninfo,HttpServletRequest request){
		int i = 0;
		if(null != xtFunctioninfo){
			i=xtFunctioninfoService.updateXtFunctioninfo(xtFunctioninfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_functioninfo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtFunctioninfo",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtFunctioninfo(String xt_functioninfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_functioninfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_functioninfo_id",xt_functioninfo_id);
			i=xtFunctioninfoService.delXtFunctioninfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 查询全部
	* @param request 
	*/
	@ApiOperation(value="查询全部", notes="查询全部")
	@ResponseBody
	@RequestMapping(value="/getXtFunctioninfoList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtFunctioninfoList(HttpServletRequest request){
		//1获取所有菜单
		Map<String, Object> condition = new HashMap<String, Object>();
		String expanded = request.getParameter("expanded");
		String singleClickExpand = request.getParameter("singleClickExpand");
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtFunctioninfo> xtFunctioninfoList = xtFunctioninfoService.getXtFunctioninfoList(condition);
		List<XtMenuinfo> xtMenuinfoList = xtMenuinfoService.getXtMenuinfoListAll(condition);
		for(int j = 0; j < xtMenuinfoList.size(); j++){
			XtMenuinfo xtMenuinfo = xtMenuinfoList.get(j);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtMenuinfo.getXt_menuinfo_id());
			BaseZTreeEntity.setPid(xtMenuinfo.getXt_menuinfo_parentId());
			BaseZTreeEntity.setText(xtMenuinfo.getXt_menuinfo_title());
			BaseZTreeEntity.setContent("");
			if("0".equals(xtMenuinfo.getXt_menuinfo_leaf())){
				BaseZTreeEntity.setLeaf(false);
			}else{
				BaseZTreeEntity.setLeaf(true);
				//当菜单为末级时判断是否存在功能
				if(hasLeaf(xtFunctioninfoList, xtMenuinfo.getXt_menuinfo_id())){
					BaseZTreeEntity.setLeaf(false);
				}else{
					BaseZTreeEntity.setLeaf(true);
				}
			}
//			BaseZTreeEntity.setIcon("../deng/images/icons/target.png");
			BaseZTreeEntity.setTempObject("菜单");
			if(xtMenuinfo.getXt_menuinfo_parentId().equals("0")){
				//展开第一级菜单
				BaseZTreeEntity.setExpanded(true);
			}
//			if(("true").equals(expanded)){
//				BaseZTreeEntity.setExpanded(true);
//			}else{
//				BaseZTreeEntity.setExpanded(false);
//			}
			if("true".equals(singleClickExpand)){
				BaseZTreeEntity.setSingleClickExpand(true);
			}else{
				BaseZTreeEntity.setSingleClickExpand(false);
			}
			list.add(BaseZTreeEntity);
		}
		for(int i = 0; i < xtFunctioninfoList.size(); i++){
			XtFunctioninfo xtFunctioninfo = xtFunctioninfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtFunctioninfo.getXt_functioninfo_id());
			BaseZTreeEntity.setPid(xtFunctioninfo.getXt_menuinfo_id());
			BaseZTreeEntity.setText(xtFunctioninfo.getXt_functioninfoTitle());
//			BaseZTreeEntity.setIcon("../deng/images/icons/target_point.png");
			BaseZTreeEntity.setTempObject("功能");
			BaseZTreeEntity.setContent(""+xtFunctioninfo.getXt_functioninfoTitle());
			BaseZTreeEntity.setIntegerappend(xtFunctioninfo.getXt_functioninfoIsAuthority()+","+xtFunctioninfo.getXt_functioninfoType());
			if(("true").equals(expanded)){
				BaseZTreeEntity.setExpanded(true);
			}else{
				BaseZTreeEntity.setExpanded(false);
			}
			if("true".equals(singleClickExpand)){
				BaseZTreeEntity.setSingleClickExpand(true);
			}else{
				BaseZTreeEntity.setSingleClickExpand(false);
			}
			BaseZTreeEntity.setLeaf(true);
			list.add(BaseZTreeEntity);
		}
		return outStr(BaseZTreeEntity.buildTree(list,false));
	}
	
	/**
	 * 判断菜单下面是否有功能
	 * @param xtFunctioninfoList
	 * @param xt_menuinfo_id
	 * @return
	 */
	public boolean hasLeaf(List<XtFunctioninfo> xtFunctioninfoList,String xt_menuinfo_id){
		boolean flag = true;
		for(int i = 0; i < xtFunctioninfoList.size(); i++){
			if(xtFunctioninfoList.get(i).getXt_menuinfo_id().equals(xt_menuinfo_id)){
				return true;
			}
		}
		flag = false;
		return flag;
	}
}
