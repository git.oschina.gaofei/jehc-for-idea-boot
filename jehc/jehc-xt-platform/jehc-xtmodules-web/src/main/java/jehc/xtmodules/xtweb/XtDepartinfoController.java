package jehc.xtmodules.xtweb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.base.BaseTreeGridEntity;
import jehc.xtmodules.xtcore.base.BaseZTreeEntity;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtDepartinfo;
import jehc.xtmodules.xtservice.XtDepartinfoService;

/**
* 部门信息
* 2015-05-13 15:46:38  邓纯杰
*/
@Api(value = "部门信息", description = "部门信息")
@Controller
@RequestMapping("/xtDepartinfoController")
public class XtDepartinfoController extends BaseAction{
	@Autowired
	private XtDepartinfoService xtDepartinfoService;
	/**
	* 列表页面
	* @param xtDepartinfo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtDepartinfo(XtDepartinfo xtDepartinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-departinfo/xt-departinfo-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtDepartinfoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDepartinfoListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListByCondition(condition);
		PageInfo<XtDepartinfo> page = new PageInfo<XtDepartinfo>(xtDepartinfoList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_departinfo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtDepartinfoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDepartinfoById(String xt_departinfo_id,HttpServletRequest request){
		XtDepartinfo xtDepartinfo = xtDepartinfoService.getXtDepartinfoById(xt_departinfo_id);
		return outDataStr(xtDepartinfo);
	}
	/**
	* 添加
	* @param xtDepartinfo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtDepartinfo(XtDepartinfo xtDepartinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtDepartinfo){
			xtDepartinfo.setXt_departinfo_id(UUID.toUUID());
			if(StringUtil.isEmpty(xtDepartinfo.getXt_departinfo_parentId())){
				xtDepartinfo.setXt_departinfo_parentId("0");
			}
			i=xtDepartinfoService.addXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtDepartinfo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtDepartinfo(XtDepartinfo xtDepartinfo,HttpServletRequest request){
		int i = 0;
		if(null != xtDepartinfo){
			if(StringUtil.isEmpty(xtDepartinfo.getXt_departinfo_parentId())){
				xtDepartinfo.setXt_departinfo_parentId("0");
			}
			i=xtDepartinfoService.updateXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_departinfo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtDepartinfo(String xt_departinfo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_departinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_id",xt_departinfo_id.split(","));
			i=xtDepartinfoService.delXtDepartinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	
	/**
	 * 查询部门树
	 * @param id
	 * @param request
	 */
	@ApiOperation(value="查询部门树", notes="查询部门树")
	@ResponseBody
	@RequestMapping(value="/getXtDepartinfoTree",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDepartinfoTree(String id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseTreeGridEntity.setPid(xtDepartinfo.getXt_departinfo_parentId());
			BaseTreeGridEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseTreeGridEntity.setExpanded(true);
			BaseTreeGridEntity.setSingleClickExpand(true);
			BaseTreeGridEntity.setIcon("../deng/images/icons/target.png");
			list.add(BaseTreeGridEntity);
		}
		return outStr(BaseTreeGridEntity.buildTree(list,false));
	}
	
	/**
	* 复制一行并生成记录
	* @param xt_departinfo_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtDepartinfo(String xt_departinfo_id,HttpServletRequest request){
		int i = 0;
		XtDepartinfo xtDepartinfo = xtDepartinfoService.getXtDepartinfoById(xt_departinfo_id);
		if(null != xtDepartinfo){
			xtDepartinfo.setXt_departinfo_id(UUID.toUUID());
			i=xtDepartinfoService.addXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtDepartinfo",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtDepartinfo(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param xt_departinfo_id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="根据各种情况查找集合不分页", notes="根据各种情况查找集合不分页")
	@ResponseBody
	@RequestMapping(value="/queryXtDepartinfoList",method={RequestMethod.POST,RequestMethod.GET})
	public String queryXtDepartinfoList(String xt_departinfo_id,HttpServletRequest request){
		List<XtDepartinfo> list = new ArrayList<XtDepartinfo>();
		if(null != xt_departinfo_id && !"".equals(xt_departinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_id", xt_departinfo_id.split(","));
			list = xtDepartinfoService.queryXtDepartinfoList(condition);
		}
		return  outItemsStr(list);
	}
	
	
	/**
	 * 读取部门树（Bootstrap---ztree）
	 * @param request
	 */
	@ApiOperation(value="读取部门树", notes="读取部门树")
	@ResponseBody
	@RequestMapping(value="/getXtDepartinfoBZTree",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDepartinfoBZTree(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseZTreeEntity.setPid(xtDepartinfo.getXt_departinfo_parentId());
			BaseZTreeEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			list.add(BaseZTreeEntity);
		}
		return outStr(BaseZTreeEntity.buildTree(list,false));
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtDepartinfoAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDepartinfoAdd(XtDepartinfo xtDepartinfo,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-departinfo/xt-departinfo-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtDepartinfoUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDepartinfoUpdate(String xt_departinfo_id,HttpServletRequest request, Model model){
		XtDepartinfo xtDepartinfo = xtDepartinfoService.getXtDepartinfoById(xt_departinfo_id);
		model.addAttribute("xtDepartinfo", xtDepartinfo);
		return new ModelAndView("pc/xt-view/xt-departinfo/xt-departinfo-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtDepartinfoDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDepartinfoDetail(String xt_departinfo_id,HttpServletRequest request, Model model){
		XtDepartinfo xtDepartinfo = xtDepartinfoService.getXtDepartinfoById(xt_departinfo_id);
		model.addAttribute("xtDepartinfo", xtDepartinfo);
		return new ModelAndView("pc/xt-view/xt-departinfo/xt-departinfo-detail");
	}
}
