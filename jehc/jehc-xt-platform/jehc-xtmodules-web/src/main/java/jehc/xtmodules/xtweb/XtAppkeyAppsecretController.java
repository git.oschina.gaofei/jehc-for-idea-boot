package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.allutils.MD5;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtAppkeyAppsecret;
import jehc.xtmodules.xtservice.XtAppkeyAppsecretService;

/**
* APPKEY
* 2016-08-28 14:37:16  邓纯杰
*/
@Api(value = "APPKEY", description = "APPKEY")
@Controller
@RequestMapping("/xtAppkeyAppsecretController")
public class XtAppkeyAppsecretController extends BaseAction{
	@Autowired
	private XtAppkeyAppsecretService xtAppkeyAppsecretService;
	/**
	* 列表页面
	* @param xtAppkeyAppsecret
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtAppkeyAppsecret(XtAppkeyAppsecret xtAppkeyAppsecret,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-appkey-appsecret/xt-appkey-appsecret-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtAppkeyAppsecretListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAppkeyAppsecretListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtAppkeyAppsecret> xtAppkeyAppsecretList = xtAppkeyAppsecretService.getXtAppkeyAppsecretListByCondition(condition);
		PageInfo<XtAppkeyAppsecret> page = new PageInfo<XtAppkeyAppsecret>(xtAppkeyAppsecretList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_appkey_appsecret_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtAppkeyAppsecretById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAppkeyAppsecretById(String xt_appkey_appsecret_id,HttpServletRequest request){
		XtAppkeyAppsecret xtAppkeyAppsecret = xtAppkeyAppsecretService.getXtAppkeyAppsecretById(xt_appkey_appsecret_id);
		return outDataStr(xtAppkeyAppsecret);
	}
	/**
	* 添加
	* @param xtAppkeyAppsecret
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtAppkeyAppsecret(XtAppkeyAppsecret xtAppkeyAppsecret,HttpServletRequest request){
		int i = 0;
		if(null != xtAppkeyAppsecret){
			xtAppkeyAppsecret.setXt_appkey_appsecret_id(UUID.toUUID());
			xtAppkeyAppsecret.setXt_ctime(getSimpleDateFormat());
			xtAppkeyAppsecret.setXt_appkey(UUID.toUUID());
			xtAppkeyAppsecret.setXt_appsecret(MD5.md5(UUID.toUUID()).toUpperCase());
			xtAppkeyAppsecret.setXt_userinfo_id(getXtUid());
			i=xtAppkeyAppsecretService.addXtAppkeyAppsecret(xtAppkeyAppsecret);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtAppkeyAppsecret
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtAppkeyAppsecret(XtAppkeyAppsecret xtAppkeyAppsecret,HttpServletRequest request){
		int i = 0;
		if(null != xtAppkeyAppsecret){
			xtAppkeyAppsecret.setXt_mtime(getSimpleDateFormat());
			xtAppkeyAppsecret.setXt_appsecret(MD5.md5(UUID.toUUID()).toUpperCase());
			xtAppkeyAppsecret.setXt_userinfo_id(getXtUid());
			i=xtAppkeyAppsecretService.updateXtAppkeyAppsecret(xtAppkeyAppsecret);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_appkey_appsecret_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtAppkeyAppsecret(String xt_appkey_appsecret_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_appkey_appsecret_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_appkey_appsecret_id",xt_appkey_appsecret_id.split(","));
			i=xtAppkeyAppsecretService.delXtAppkeyAppsecret(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_appkey_appsecret_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtAppkeyAppsecret(String xt_appkey_appsecret_id,HttpServletRequest request){
		int i = 0;
		XtAppkeyAppsecret xtAppkeyAppsecret = xtAppkeyAppsecretService.getXtAppkeyAppsecretById(xt_appkey_appsecret_id);
		if(null != xtAppkeyAppsecret){
			xtAppkeyAppsecret.setXt_appkey_appsecret_id(UUID.toUUID());
			xtAppkeyAppsecret.setXt_appkey(UUID.toUUID());
			xtAppkeyAppsecret.setXt_appsecret(MD5.md5(UUID.toUUID()).toUpperCase());
			i=xtAppkeyAppsecretService.addXtAppkeyAppsecret(xtAppkeyAppsecret);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtAppkeyAppsecret",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtAppkeyAppsecret(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtAppkeyAppsecretAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtAppkeyAppsecretAdd(XtAppkeyAppsecret xtAppkeyAppsecret,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-appkey-appsecret/xt-appkey-appsecret-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtAppkeyAppsecretUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtAppkeyAppsecretUpdate(String xt_appkey_appsecret_id,HttpServletRequest request, Model model){
		XtAppkeyAppsecret xtAppkeyAppsecret = xtAppkeyAppsecretService.getXtAppkeyAppsecretById(xt_appkey_appsecret_id);
		model.addAttribute("xtAppkeyAppsecret", xtAppkeyAppsecret);
		return new ModelAndView("pc/xt-view/xt-appkey-appsecret/xt-appkey-appsecret-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtAppkeyAppsecretDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtAppkeyAppsecretDetail(String xt_appkey_appsecret_id,HttpServletRequest request, Model model){
		XtAppkeyAppsecret xtAppkeyAppsecret = xtAppkeyAppsecretService.getXtAppkeyAppsecretById(xt_appkey_appsecret_id);
		model.addAttribute("xtAppkeyAppsecret", xtAppkeyAppsecret);
		return new ModelAndView("pc/xt-view/xt-appkey-appsecret/xt-appkey-appsecret-detail");
	}
}
