package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtmodel.XtAttachment;
import jehc.xtmodules.xtservice.XtAttachmentService;

/**
* DESC 附件管理
* CreateData 2015-05-24 08:36:53
* Author 邓纯杰
*/
@Api(value = "附件管理", description = "附件管理")
@Controller
@RequestMapping("/xtAttachmentController")
public class XtAttachmentController extends BaseAction{
	@Autowired
	private XtAttachmentService xtAttachmentService;
	/**
	* 列表页面
	* @param xtAttachment
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtAttachment",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtAttachment(XtAttachment xtAttachment,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-attachment/xt-attachment-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtAttachmentListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAttachmentListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtAttachment>XtAttachmentList = xtAttachmentService.getXtAttachmentListByCondition(condition);
		PageInfo<XtAttachment> page = new PageInfo<XtAttachment>(XtAttachmentList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_attachment_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtAttachmentById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtAttachmentById(String xt_attachment_id,HttpServletRequest request){
		XtAttachment xt_Attachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id);
		return outDataStr(xt_Attachment);
	}
	/**
	* 添加
	* @param xtAttachment
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtAttachment",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtAttachment(XtAttachment xtAttachment,HttpServletRequest request){
		int i = 0;
		if(null != xtAttachment){
			xtAttachment.setXt_attachment_id(UUID.toUUID());
			i=xtAttachmentService.addXtAttachment(xtAttachment);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtAttachment
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtAttachment",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtAttachment(XtAttachment xtAttachment,HttpServletRequest request){
		int i = 0;
		if(null != xtAttachment){
			i=xtAttachmentService.updateXtAttachment(xtAttachment);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_attachment_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtAttachment",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtAttachment(String xt_attachment_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_attachment_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_attachment_id",xt_attachment_id.split(","));
			i=xtAttachmentService.delXtAttachment(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtAttachmentDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtAttachmentDetail(String xt_attachment_id,HttpServletRequest request, Model model){
		XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id);
		model.addAttribute("xtAttachment", xtAttachment);
		return new ModelAndView("pc/xt-view/xt-attachment/xt-attachment-detail");
	}
}
