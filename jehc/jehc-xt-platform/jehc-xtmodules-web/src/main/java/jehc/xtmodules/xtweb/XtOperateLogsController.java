package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtOperateLogs;
import jehc.xtmodules.xtservice.XtOperateLogsService;

/**
* 操作日志
* 2015-09-30 16:48:48  邓纯杰
*/
@Api(value = "操作日志", description = "操作日志")
@Controller
@RequestMapping("/xtOperateLogsController")
public class XtOperateLogsController extends BaseAction{
	@Autowired
	private XtOperateLogsService xtOperateLogsService;
	/**
	* 列表页面
	* @param xtOperateLogs
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtOperateLogs(XtOperateLogs xtOperateLogs,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-operate-logs/xt-operate-logs-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtOperateLogsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtOperateLogsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtOperateLogs> xtOperateLogsList = xtOperateLogsService.getXtOperateLogsListByCondition(condition);
		PageInfo<XtOperateLogs> page = new PageInfo<XtOperateLogs>(xtOperateLogsList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_operate_log_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtOperateLogsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtOperateLogsById(String xt_operate_log_id,HttpServletRequest request){
		XtOperateLogs xtOperateLogs = xtOperateLogsService.getXtOperateLogsById(xt_operate_log_id);
		return outDataStr(xtOperateLogs);
	}
	/**
	* 添加
	* @param xtOperateLogs
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtOperateLogs(XtOperateLogs xtOperateLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtOperateLogs){
			xtOperateLogs.setXt_operate_log_id(UUID.toUUID());
			i=xtOperateLogsService.addXtOperateLogs(xtOperateLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtOperateLogs
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtOperateLogs(XtOperateLogs xtOperateLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtOperateLogs){
			i=xtOperateLogsService.updateXtOperateLogs(xtOperateLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_operate_log_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtOperateLogs(String xt_operate_log_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_operate_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_operate_log_id",xt_operate_log_id.split(","));
			i=xtOperateLogsService.delXtOperateLogs(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_operate_log_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtOperateLogs(String xt_operate_log_id,HttpServletRequest request){
		int i = 0;
		XtOperateLogs xtOperateLogs = xtOperateLogsService.getXtOperateLogsById(xt_operate_log_id);
		if(null != xtOperateLogs){
			xtOperateLogs.setXt_operate_log_id(UUID.toUUID());
			i=xtOperateLogsService.addXtOperateLogs(xtOperateLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtOperateLogs",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtOperateLogs(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtOperateLogsDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtOperateLogsDetail(String xt_operate_log_id,HttpServletRequest request, Model model){
		XtOperateLogs xtOperateLogs = xtOperateLogsService.getXtOperateLogsById(xt_operate_log_id);
		model.addAttribute("xtOperateLogs", xtOperateLogs);
		return new ModelAndView("pc/xt-view/xt-operate-logs/xt-operate-logs-detail");
	}
}
