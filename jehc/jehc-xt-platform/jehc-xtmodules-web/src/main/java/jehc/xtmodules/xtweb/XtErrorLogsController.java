package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtErrorLogs;
import jehc.xtmodules.xtservice.XtErrorLogsService;

/**
* 异常日志
* 2015-05-24 08:33:40  邓纯杰
*/
@Api(value = "异常日志", description = "异常日志")
@Controller
@RequestMapping("/xtErrorLogsController")
public class XtErrorLogsController extends BaseAction{
	@Autowired
	private XtErrorLogsService xtErrorLogsService;
	/**
	* 列表页面
	* @param xtErrorLogs
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtErrorLogs",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtErrorLogs(XtErrorLogs xtErrorLogs,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-error-logs/xt-error-logs-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtErrorLogsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtErrorLogsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtErrorLogs> xtErrorLogsList = xtErrorLogsService.getXtErrorLogsListByCondition(condition);
		PageInfo<XtErrorLogs> page = new PageInfo<XtErrorLogs>(xtErrorLogsList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_error_log_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtErrorLogsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtErrorLogsById(String xt_error_log_id,HttpServletRequest request){
		XtErrorLogs xtErrorLogs = xtErrorLogsService.getXtErrorLogsById(xt_error_log_id);
		return outDataStr(xtErrorLogs);
	}
	/**
	* 添加
	* @param xtErrorLogs
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtErrorLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtErrorLogs(XtErrorLogs xtErrorLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtErrorLogs){
			xtErrorLogs.setXt_error_log_id(UUID.toUUID());
			i=xtErrorLogsService.addXtErrorLogs(xtErrorLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtErrorLogs
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtErrorLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtErrorLogs(XtErrorLogs xtErrorLogs,HttpServletRequest request){
		int i = 0;
		if(null != xtErrorLogs){
			i=xtErrorLogsService.updateXtErrorLogs(xtErrorLogs);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_error_log_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtErrorLogs",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtErrorLogs(String xt_error_log_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_error_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_error_log_id",xt_error_log_id.split(","));
			i=xtErrorLogsService.delXtErrorLogs(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@RequestMapping(value="/exportXtErrorLogs",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtErrorLogs(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtErrorLogsDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtErrorLogsDetail(String xt_error_log_id,HttpServletRequest request, Model model){
		XtErrorLogs xtErrorLogs = xtErrorLogsService.getXtErrorLogsById(xt_error_log_id);
		model.addAttribute("xtErrorLogs", xtErrorLogs);
		return new ModelAndView("pc/xt-view/xt-error-logs/xt-error-logs-detail");
	}
}
