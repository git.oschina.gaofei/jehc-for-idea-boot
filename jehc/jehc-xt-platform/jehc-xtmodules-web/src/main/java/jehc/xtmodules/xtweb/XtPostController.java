package jehc.xtmodules.xtweb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.base.BaseTreeGridEntity;
import jehc.xtmodules.xtcore.base.BaseZTreeEntity;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtPost;
import jehc.xtmodules.xtservice.XtPostService;

/**
* 用户岗位
* 2015-05-13 16:48:11  邓纯杰
*/
@Api(value = "用户岗位", description = "用户岗位")
@Controller
@RequestMapping("/xtPostController")
public class XtPostController extends BaseAction{
	@Autowired
	private XtPostService xtPostService;
	/**
	* 列表页面
	* @param xtPost
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtPost(XtPost xtPost,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-post/xt-post-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtPostListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPostListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtPost> xtPostList = xtPostService.getXtPostListByCondition(condition);
		PageInfo<XtPost> page = new PageInfo<XtPost>(xtPostList);
		return outPageBootStr(page,request);
	}
	/**
	* 获取对象
	* @param xt_post_id 
	* @param request 
	*/
	@ApiOperation(value="获取对象", notes="获取对象")
	@ResponseBody
	@RequestMapping(value="/getXtPostById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPostById(String xt_post_id,HttpServletRequest request){
		XtPost xtPost = xtPostService.getXtPostById(xt_post_id);
		return outDataStr(xtPost);
	}
	/**
	* 添加
	* @param xtPost
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtPost(XtPost xtPost,HttpServletRequest request){
		int i = 0;
		if(null != xtPost){
			xtPost.setXt_post_id(UUID.toUUID());
			if(StringUtil.isEmpty(xtPost.getXt_post_parentId())){
				xtPost.setXt_post_parentId("0");
			}
			i=xtPostService.addXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtPost
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtPost(XtPost xtPost,HttpServletRequest request){
		int i = 0;
		if(null != xtPost){
			if(StringUtil.isEmpty(xtPost.getXt_post_parentId())){
				xtPost.setXt_post_parentId("0");
			}
			i=xtPostService.updateXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_post_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtPost(String xt_post_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_post_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_post_id",xt_post_id.split(","));
			i=xtPostService.delXtPost(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 查询岗位树
	 * @param id
	 * @param request
	 */
	@ApiOperation(value="查询岗位树", notes="查询岗位树")
	@ResponseBody
	@RequestMapping(value="/getXtPostTree",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPostTree(String id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtPost> xtPostList = xtPostService.getXtPostListAll(condition);
		for(int i = 0; i < xtPostList.size(); i++){
			XtPost xtPost = xtPostList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(xtPost.getXt_post_id());
			BaseTreeGridEntity.setPid(xtPost.getXt_post_parentId());
			BaseTreeGridEntity.setText(xtPost.getXt_post_name());
			BaseTreeGridEntity.setExpanded(true);
			BaseTreeGridEntity.setSingleClickExpand(true);
			BaseTreeGridEntity.setIcon("../deng/images/icons/target.png");
			list.add(BaseTreeGridEntity);
		}
		return outStr(BaseTreeGridEntity.buildTree(list,false));
	}
	
	/**
	 * 查询岗位树（bootstrap-ztree风格）
	 * @param xt_departinfo_id  部门编号
	 * @param request
	 */
	@ApiOperation(value="查询岗位树", notes="查询岗位树")
	@ResponseBody
	@RequestMapping(value="/getXtPostBZTree",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPostBZTree(String xt_departinfo_id,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_departinfo_id", xt_departinfo_id);
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtPost> xtPostList = xtPostService.getXtPostListByCondition(condition);
		for(int i = 0; i < xtPostList.size(); i++){
			XtPost xtPost = xtPostList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPid(xtPost.getXt_post_parentId());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			list.add(BaseZTreeEntity);
		}
		return outStr(BaseZTreeEntity.buildTree(list,false));
	}
	/**
	* 复制一行并生成记录
	* @param xt_post_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtPost(String xt_post_id,HttpServletRequest request){
		int i = 0;
		XtPost xtPost = xtPostService.getXtPostById(xt_post_id);
		if(null != xtPost){
			xtPost.setXt_post_id(UUID.toUUID());
			i=xtPostService.addXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtPost",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtPost(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 查询岗位
	 * @param xt_post_id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询岗位", notes="查询岗位")
	@ResponseBody
	@RequestMapping(value="/getXtPostList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPostList(String xt_post_id,HttpServletRequest request){
		List<XtPost> list = new ArrayList<XtPost>();
		if(!StringUtil.isEmpty(xt_post_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_post_id", xt_post_id.split(","));
			list = xtPostService.getXtPostList(condition);
		}
		return  outItemsStr(list);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtPostAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPostAdd(XtPost xtPost,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-post/xt-post-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtPostUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPostUpdate(String xt_post_id,HttpServletRequest request, Model model){
		XtPost xtPost = xtPostService.getXtPostById(xt_post_id);
		model.addAttribute("xtPost", xtPost);
		return new ModelAndView("pc/xt-view/xt-post/xt-post-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtPostDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPostDetail(String xt_post_id,HttpServletRequest request, Model model){
		XtPost xtPost = xtPostService.getXtPostById(xt_post_id);
		model.addAttribute("xtPost", xtPost);
		return new ModelAndView("pc/xt-view/xt-post/xt-post-detail");
	}
}
