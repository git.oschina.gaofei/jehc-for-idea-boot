package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtPath;
import jehc.xtmodules.xtservice.XtPathService;

/**
* 文件路径设置
* 2015-09-30 16:29:00  邓纯杰
*/
@Api(value = "文件路径设置", description = "文件路径设置")
@Controller
@RequestMapping("/xtPathController")
public class XtPathController extends BaseAction{
	@Autowired
	private XtPathService xtPathService;
	/**
	* 列表页面
	* @param xtPath
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtPath(XtPath xtPath,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-path/xt-path-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtPathListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPathListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtPath> xtPathList = xtPathService.getXtPathListByCondition(condition);
		PageInfo<XtPath> page = new PageInfo<XtPath>(xtPathList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_path_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtPathById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtPathById(String xt_path_id,HttpServletRequest request){
		XtPath xtPath = xtPathService.getXtPathById(xt_path_id);
		return outDataStr(xtPath);
	}
	/**
	* 添加
	* @param xtPath
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtPath(XtPath xtPath,HttpServletRequest request){
		int i = 0;
		if(null != xtPath){
			xtPath.setXt_path_id(UUID.toUUID());
			xtPath.setXt_time(CommonUtils.getDate());
			i=xtPathService.addXtPath(xtPath);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtPath
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtPath(XtPath xtPath,HttpServletRequest request){
		int i = 0;
		if(null != xtPath){
			i=xtPathService.updateXtPath(xtPath);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_path_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtPath(String xt_path_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_path_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_path_id",xt_path_id.split(","));
			i=xtPathService.delXtPath(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_path_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtPath(String xt_path_id,HttpServletRequest request){
		int i = 0;
		XtPath xtPath = xtPathService.getXtPathById(xt_path_id);
		xtPath.setXt_time(CommonUtils.getDate());
		if(null != xtPath){
			xtPath.setXt_path_id(UUID.toUUID());
			i=xtPathService.addXtPath(xtPath);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtPath",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtPath(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtPathAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPathAdd(XtPath xtPath,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-path/xt-path-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtPathUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPathUpdate(String xt_path_id,HttpServletRequest request, Model model){
		XtPath xtPath = xtPathService.getXtPathById(xt_path_id);
		model.addAttribute("xtPath", xtPath);
		return new ModelAndView("pc/xt-view/xt-path/xt-path-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtPathDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtPathDetail(String xt_path_id,HttpServletRequest request, Model model){
		XtPath xtPath = xtPathService.getXtPathById(xt_path_id);
		model.addAttribute("xtPath", xtPath);
		return new ModelAndView("pc/xt-view/xt-path/xt-path-detail");
	}
}
