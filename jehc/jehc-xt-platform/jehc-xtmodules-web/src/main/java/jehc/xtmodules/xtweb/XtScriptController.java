package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtScript;
import jehc.xtmodules.xtservice.XtScriptService;

/**
* 平台脚本 
* 2016-06-15 17:08:31  邓纯杰
*/
@Api(value = "平台脚本", description = "control")
@Controller
@RequestMapping("/xtScriptController")
public class XtScriptController extends BaseAction{
	@Autowired
	private XtScriptService xtScriptService;
	/**
	* 列表页面
	* @param xtScript
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtScript(XtScript xtScript,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-script/xt-script-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtScriptListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtScriptListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtScript> xtScriptList = xtScriptService.getXtScriptListByCondition(condition);
		PageInfo<XtScript> page = new PageInfo<XtScript>(xtScriptList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_script_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/getXtScriptById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtScriptById(String xt_script_id,HttpServletRequest request){
		XtScript xtScript = xtScriptService.getXtScriptById(xt_script_id);
		return outDataStr(xtScript);
	}
	/**
	* 添加
	* @param xtScript
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtScript(XtScript xtScript,HttpServletRequest request){
		int i = 0;
		if(null != xtScript){
			xtScript.setXt_script_id(UUID.toUUID());
			xtScript.setXt_script_text(request.getParameter("xt_script_text"));
			xtScript.setXt_script_ctime(getSimpleDateFormat());
			xtScript.setXt_userinfo_id(getXtUid());
			xtScript.setXt_script_content(request.getParameter("xt_script_content"));
			i=xtScriptService.addXtScript(xtScript);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtScript
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtScript(XtScript xtScript,HttpServletRequest request){
		int i = 0;
		if(null != xtScript){
			xtScript.setXt_script_mtime(getSimpleDateFormat());
			xtScript.setXt_userinfo_id(getXtUid());
			xtScript.setXt_script_content(request.getParameter("xt_script_content"));
			xtScript.setXt_script_text(request.getParameter("xt_script_text"));
			i=xtScriptService.updateXtScript(xtScript);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_script_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtScript(String xt_script_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_script_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_script_id",xt_script_id.split(","));
			i=xtScriptService.delXtScript(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_script_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtScript(String xt_script_id,HttpServletRequest request){
		int i = 0;
		XtScript xtScript = xtScriptService.getXtScriptById(xt_script_id);
		if(null != xtScript){
			xtScript.setXt_script_id(UUID.toUUID());
			xtScript.setXt_script_ctime(getSimpleDateFormat());
			i=xtScriptService.addXtScript(xtScript);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtScript",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtScript(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 查询集合
	* @param xtScript
	* @param request 
	*/
	@ApiOperation(value="查询集合", notes="查询集合")
	@ResponseBody
	@RequestMapping(value="/getXtScriptList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtScriptList(XtScript xtScript,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_script_type",1);
		condition.put("xt_script_status",0);
		List<XtScript> xt_ScriptList = xtScriptService.getXtScriptListByCondition(condition);
		return outComboDataStr(xt_ScriptList);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtScriptAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtScriptAdd(XtScript xtScript,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-script/xt-script-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtScriptUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtScriptUpdate(String xt_script_id,HttpServletRequest request, Model model){
		XtScript xtScript = xtScriptService.getXtScriptById(xt_script_id);
		model.addAttribute("xtScript", xtScript);
		return new ModelAndView("pc/xt-view/xt-script/xt-script-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtScriptDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtScriptDetail(String xt_script_id,HttpServletRequest request, Model model){
		XtScript xtScript = xtScriptService.getXtScriptById(xt_script_id);
		model.addAttribute("xtScript", xtScript);
		return new ModelAndView("pc/xt-view/xt-script/xt-script-detail");
	}
}
