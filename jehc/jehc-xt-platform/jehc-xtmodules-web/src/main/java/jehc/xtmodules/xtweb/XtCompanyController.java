package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtmodel.XtCompany;
import jehc.xtmodules.xtservice.XtCompanyService;

/**
* 平台公司信息表 
* 2015-05-12 22:59:42  邓纯杰
*/
@Api(value = "平台公司信息表", description = "平台公司信息表")
@Controller
@RequestMapping("/xtCompanyController")
public class XtCompanyController extends BaseAction{
	@Autowired
	private XtCompanyService xtCompanyService;

	/**
	 * 详情页面
	 * @param model
	 * @param request
	 * @return
	 */
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/loadXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtCompany(Model model,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtCompany> xtCompanyList = xtCompanyService.getXtCompanyListByCondition(condition);
		XtCompany xtCompany = new XtCompany();
		if(!xtCompanyList.isEmpty()){
			xtCompany = xtCompanyList.get(0);
		}
		model.addAttribute("xt_Company", xtCompany);
		return new ModelAndView("pc/xt-view/xt-company/xt-company-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtCompanyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtCompanyListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtCompany> xtCompanyList = xtCompanyService.getXtCompanyListByCondition(condition);
		PageInfo<XtCompany> page = new PageInfo<XtCompany>(xtCompanyList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_company_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtCompanyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtCompanyById(String xt_company_id,HttpServletRequest request){
		XtCompany xtCompany = xtCompanyService.getXtCompanyById(xt_company_id);
		return outDataStr(xtCompany);
	}
	/**
	* 查询单条记录
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtCompany(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtCompany> xtCompanyList = xtCompanyService.getXtCompanyListByCondition(condition);
		XtCompany xtCompany = new XtCompany();
		if(!xtCompanyList.isEmpty()){
			xtCompany = xtCompanyList.get(0);
		}
		return outDataStr(xtCompany);
	}
	
	/**
	* 添加或修改
	* @param xtCompany
	* @param request 
	*/
	@ApiOperation(value="添加或修改", notes="添加或修改")
	@ResponseBody
	@RequestMapping(value="/addOrUpdateXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public String addOrUpdateXtCompany(XtCompany xtCompany,HttpServletRequest request){
		int i = 0;
		if(null != xtCompany ){
			if(!StringUtil.isEmpty(xtCompany.getXt_company_id())){
				i = xtCompanyService.updateXtCompany(xtCompany);
			}else{
				xtCompany.setXt_company_id(UUID.toUUID());
				i=xtCompanyService.addXtCompany(xtCompany);
			}
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 添加
	* @param xtCompany
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtCompany(XtCompany xtCompany,HttpServletRequest request){
		int i = 0;
		if(null != xtCompany){
			xtCompany.setXt_company_id(UUID.toUUID());
			i=xtCompanyService.addXtCompany(xtCompany);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtCompany
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtCompany(XtCompany xtCompany,HttpServletRequest request){
		int i = 0;
		if(null != xtCompany){
			i=xtCompanyService.updateXtCompany(xtCompany);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_company_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtCompany",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtCompany(String xt_company_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_company_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_company_id",xt_company_id);
			i=xtCompanyService.delXtCompany(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
