package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtMonitor;
import jehc.xtmodules.xtservice.XtMonitorService;

/**
* 监控主表 
* 2016-03-04 12:49:59  邓纯杰
*/
@Api(value = "监控主表", description = "监控主表")
@Controller
@RequestMapping("/xtMonitorController")
@Scope("prototype")
public class XtMonitorController extends BaseAction{
	@Autowired
	private XtMonitorService xtMonitorService;
	/**
	* 列表页面
	* @param xtMonitor
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtMonitor(XtMonitor xtMonitor,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-monitor/xt-monitor-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtMonitor> xtMonitorList = xtMonitorService.getXtMonitorListByCondition(condition);
		PageInfo<XtMonitor> page = new PageInfo<XtMonitor>(xtMonitorList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_monitor_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorById(String xt_monitor_id,HttpServletRequest request){
		XtMonitor xtMonitor = xtMonitorService.getXtMonitorById(xt_monitor_id);
		return outDataStr(xtMonitor);
	}
	/**
	* 添加
	* @param xtMonitor
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtMonitor(XtMonitor xtMonitor,HttpServletRequest request){
		int i = 0;
		if(null != xtMonitor){
			xtMonitor.setXt_monitor_id(UUID.toUUID());
			i=xtMonitorService.addXtMonitor(xtMonitor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtMonitor
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtMonitor(XtMonitor xtMonitor,HttpServletRequest request){
		int i = 0;
		if(null != xtMonitor){
			i=xtMonitorService.updateXtMonitor(xtMonitor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_monitor_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtMonitor(String xt_monitor_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_monitor_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_monitor_id",xt_monitor_id.split(","));
			i=xtMonitorService.delXtMonitor(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_monitor_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtMonitor(String xt_monitor_id,HttpServletRequest request){
		int i = 0;
		XtMonitor xtMonitor = xtMonitorService.getXtMonitorById(xt_monitor_id);
		if(null != xtMonitor){
			xtMonitor.setXt_monitor_id(UUID.toUUID());
			i=xtMonitorService.addXtMonitor(xtMonitor);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtMonitor",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtMonitor(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	 * 当前JVM
	 * @param request
	 * @param response
	 */
	@ApiOperation(value="当前JVM", notes="当前JVM")
	@ResponseBody
	@RequestMapping(value="/getXtMonitorJvm",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtMonitorJvm(HttpServletRequest request,HttpServletResponse response){
		JSONArray jsonArray = new JSONArray();  
		Map<String, Object> model = new HashMap<String, Object>();
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(condition,request);
		List<XtMonitor> xtMonitorList = xtMonitorService.getXtMonitorListByCondition(condition);
		for(int i = 0; i < xtMonitorList.size(); i++){
			XtMonitor xt_monitor = xtMonitorList.get(i);
			model.put("xt_monitor_jvm_totalMem",(new Integer(xt_monitor.getXt_monitor_jvm_Mem())/(1024*1024)));
			model.put("xt_monitorTime", xt_monitor.getXt_monitorTime());
			jsonArray.add(model);
		}
		return outItemsStr(jsonArray);
	}
	/**
	 * 图表页面
	 * @return
	 */
	@ApiOperation(value="图表页面", notes="图表页面")
	@RequestMapping(value="/loadXtMonitorJvmChart",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtMonitorJvmChart(){
		return new ModelAndView("pc/xt-view/xt-monitor/xt-monitor-chart");
	}
}
