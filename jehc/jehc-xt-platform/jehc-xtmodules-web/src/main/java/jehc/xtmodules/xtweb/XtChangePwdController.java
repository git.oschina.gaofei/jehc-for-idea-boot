package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.annotation.AuthUneedLogin;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtChangePwd;
import jehc.xtmodules.xtservice.XtChangePwdService;

/**
* 密码找回中心 
* 2016-10-21 16:41:55  邓纯杰
*/
@Api(value = "密码找回中心", description = "密码找回中心")
@Controller
@RequestMapping("/xtChangePwdController")
public class XtChangePwdController extends BaseAction{
	@Autowired
	private XtChangePwdService xtChangePwdService;
	/**
	* 列表页面
	* @param xtChangePwd
	* @param request 
	* @return
	*/
    @ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtChangePwd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtChangePwd(XtChangePwd xtChangePwd,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-change-pwd/xt-change-pwd-list");
	}

	/**
	 * 审批详情页面
	 * @param lc_apply_model_biz_id
	 * @param request
	 * @param model
	 * @return
	 */
	@ApiOperation(value="审批详情页面", notes="审批详情页面")
	@RequestMapping(value="/loadXtChangePwdDetailApply",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtChangePwdDetailApply(String lc_apply_model_biz_id,HttpServletRequest request,Model model){
		model.addAttribute("lc_apply_model_biz_id", lc_apply_model_biz_id);
		return new ModelAndView("pc/xt-view/xt-change-pwd/xt-change-pwd-detail-apply");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtChangePwdListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtChangePwdListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtChangePwd> xtChangePwdList = xtChangePwdService.getXtChangePwdListByCondition(condition);
		PageInfo<XtChangePwd> page = new PageInfo<XtChangePwd>(xtChangePwdList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_change_pwd_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtChangePwdById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtChangePwdById(String xt_change_pwd_id,HttpServletRequest request){
		XtChangePwd xtChangePwd = xtChangePwdService.getXtChangePwdById(xt_change_pwd_id);
		return outDataStr(xtChangePwd);
	}
	/**
	* 添加
	* @param xtChangePwd
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@AuthUneedLogin
	@ResponseBody
	@RequestMapping(value="/addXtChangePwd",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtChangePwd(XtChangePwd xtChangePwd,HttpServletRequest request){
		int i = 0;
		if(null != xtChangePwd){
			xtChangePwd.setXt_change_pwd_id(UUID.toUUID());
			i=xtChangePwdService.addXtChangePwd(xtChangePwd);
		}
		if(i>0){
			return outAudStr(true,"您的提交申请已成功，我们会尽快联系人员处理，请您耐心等待!");
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtChangePwd
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtChangePwd",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtChangePwd(XtChangePwd xtChangePwd,HttpServletRequest request){
		int i = 0;
		if(null != xtChangePwd){
			i=xtChangePwdService.updateXtChangePwd(xtChangePwd);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_change_pwd_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtChangePwd",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtChangePwd(String xt_change_pwd_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_change_pwd_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_change_pwd_id",xt_change_pwd_id.split(","));
			i=xtChangePwdService.delXtChangePwd(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtChangePwd",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtChangePwd(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtChangePwdDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtChangePwdDetail(String xt_change_pwd_id,HttpServletRequest request, Model model){
		XtChangePwd xtChangePwd = xtChangePwdService.getXtChangePwdById(xt_change_pwd_id);
		model.addAttribute("xtChangePwd", xtChangePwd);
		return new ModelAndView("pc/xt-view/xt-change-pwd/xt-change-pwd-detail");
	}
}
