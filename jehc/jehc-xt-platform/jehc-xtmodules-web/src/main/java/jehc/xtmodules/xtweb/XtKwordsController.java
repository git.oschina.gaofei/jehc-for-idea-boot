package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtKwords;
import jehc.xtmodules.xtservice.XtKwordsService;

/**
* 关键词（敏感词） 
* 2016-10-08 15:03:41  邓纯杰
*/
@Api(value = "关键词", description = "关键词")
@Controller
@RequestMapping("/xtKwordsController")
public class XtKwordsController extends BaseAction{
	@Autowired
	private XtKwordsService xtKwordsService;
	/**
	* 列表页面
	* @param xtKwords
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtKwords",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtKwords(XtKwords xtKwords,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-kwords/xt-kwords-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtKwordsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtKwordsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtKwords> xtKwordsList = xtKwordsService.getXtKwordsListByCondition(condition);
		PageInfo<XtKwords> page = new PageInfo<XtKwords>(xtKwordsList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_kwords_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtKwordsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtKwordsById(String xt_kwords_id,HttpServletRequest request){
		XtKwords xtKwords = xtKwordsService.getXtKwordsById(xt_kwords_id);
		return outDataStr(xtKwords);
	}
	/**
	* 添加
	* @param xtKwords
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtKwords",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtKwords(XtKwords xtKwords,HttpServletRequest request){
		int i = 0;
		if(null != xtKwords){
			xtKwords.setXt_kwords_id(UUID.toUUID());
			xtKwords.setXt_kwords_ctime(getSimpleDateFormat());
			xtKwords.setXt_userinfo_id(getXtUid());
			i=xtKwordsService.addXtKwords(xtKwords);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtKwords
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtKwords",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtKwords(XtKwords xtKwords,HttpServletRequest request){
		int i = 0;
		if(null != xtKwords){
			xtKwords.setXt_kwords_mtime(getSimpleDateFormat());
			xtKwords.setXt_userinfo_id(getXtUid());
			i=xtKwordsService.updateXtKwords(xtKwords);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_kwords_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtKwords",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtKwords(String xt_kwords_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_kwords_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_kwords_id",xt_kwords_id.split(","));
			i=xtKwordsService.delXtKwords(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtKwords",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtKwords(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
