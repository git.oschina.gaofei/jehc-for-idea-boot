package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtDbtableStrategy;
import jehc.xtmodules.xtservice.XtDbtableStrategyService;

/**
* 分表策略
* 2017-02-14 16:23:48  邓纯杰
*/
@Api(value = "分表策略", description = "分表策略")
@Controller
@RequestMapping("/xtDbtableStrategyController")
public class XtDbtableStrategyController extends BaseAction{
	@Autowired
	private XtDbtableStrategyService xtDbtableStrategyService;
	/**
	* 列表页面
	* @param xtDbtableStrategy
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtDbtableStrategy(XtDbtableStrategy xtDbtableStrategy,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-dbtable-strategy/xt-dbtable-strategy-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtDbtableStrategyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDbtableStrategyListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtDbtableStrategy> xtDbtableStrategyList = xtDbtableStrategyService.getXtDbtableStrategyListByCondition(condition);
		PageInfo<XtDbtableStrategy> page = new PageInfo<XtDbtableStrategy>(xtDbtableStrategyList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_dbtable_strategy_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtDbtableStrategyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtDbtableStrategyById(String xt_dbtable_strategy_id,HttpServletRequest request){
		XtDbtableStrategy xtDbtableStrategy = xtDbtableStrategyService.getXtDbtableStrategyById(xt_dbtable_strategy_id);
		return outDataStr(xtDbtableStrategy);
	}
	/**
	* 添加
	* @param xtDbtableStrategy
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtDbtableStrategy(XtDbtableStrategy xtDbtableStrategy,HttpServletRequest request){
		int i = 0;
		if(null != xtDbtableStrategy){
			xtDbtableStrategy.setXt_dbtable_strategy_id(UUID.toUUID());
			xtDbtableStrategy.setCTime(getSimpleDateFormat());
			i=xtDbtableStrategyService.addXtDbtableStrategy(xtDbtableStrategy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtDbtableStrategy
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtDbtableStrategy(XtDbtableStrategy xtDbtableStrategy,HttpServletRequest request){
		int i = 0;
		if(null != xtDbtableStrategy){
			xtDbtableStrategy.setCTime(getSimpleDateFormat());
			i=xtDbtableStrategyService.updateXtDbtableStrategy(xtDbtableStrategy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_dbtable_strategy_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtDbtableStrategy(String xt_dbtable_strategy_id,HttpServletRequest request){
		int i = 0;
		if(null != xt_dbtable_strategy_id && !"".equals(xt_dbtable_strategy_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_dbtable_strategy_id",xt_dbtable_strategy_id.split(","));
			i=xtDbtableStrategyService.delXtDbtableStrategy(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_dbtable_strategy_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtDbtableStrategy(String xt_dbtable_strategy_id,HttpServletRequest request){
		int i = 0;
		XtDbtableStrategy xtDbtableStrategy = xtDbtableStrategyService.getXtDbtableStrategyById(xt_dbtable_strategy_id);
		if(null != xtDbtableStrategy){
			xtDbtableStrategy.setXt_dbtable_strategy_id(UUID.toUUID());
			i=xtDbtableStrategyService.addXtDbtableStrategy(xtDbtableStrategy);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtDbtableStrategy",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtDbtableStrategy(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtDbtableStrategyAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbtableStrategyAdd(XtDbtableStrategy xtDbtableStrategy,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-dbtable-strategy/xt-dbtable-strategy-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtDbtableStrategyUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbtableStrategyUpdate(String xt_dbtable_strategy_id,HttpServletRequest request, Model model){
		XtDbtableStrategy xtDbtableStrategy = xtDbtableStrategyService.getXtDbtableStrategyById(xt_dbtable_strategy_id);
		model.addAttribute("xtDbtableStrategy", xtDbtableStrategy);
		return new ModelAndView("pc/xt-view/xt-dbtable-strategy/xt-dbtable-strategy-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtDbtableStrategyDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtDbtableStrategyDetail(String xt_dbtable_strategy_id,HttpServletRequest request, Model model){
		XtDbtableStrategy xtDbtableStrategy = xtDbtableStrategyService.getXtDbtableStrategyById(xt_dbtable_strategy_id);
		model.addAttribute("xtDbtableStrategy", xtDbtableStrategy);
		return new ModelAndView("pc/xt-view/xt-dbtable-strategy/xt-dbtable-strategy-detail");
	}
}
