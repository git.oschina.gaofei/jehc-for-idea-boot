package jehc.xtmodules.xtweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtServiceCenter;
import jehc.xtmodules.xtmodel.XtServiceCenterParameter;
import jehc.xtmodules.xtservice.XtServiceCenterService;
import jehc.xtmodules.xtservice.XtServiceCenterParameterService;

/**
* 服务中心 
* 2017-03-27 12:32:04  邓纯杰
*/
@Api(value = "服务中心", description = "服务中心")
@Controller
@RequestMapping("/xtServiceCenterController")
public class XtServiceCenterController extends BaseAction{
	@Autowired
	private XtServiceCenterService xtServiceCenterService;
	@Autowired
	private XtServiceCenterParameterService xtServiceCenterParameterService;
	/**
	* 列表页面
	* @param xtServiceCenter
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtServiceCenter(XtServiceCenter xtServiceCenter,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-service-center/xt-service-center-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtServiceCenterListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtServiceCenterListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtServiceCenter> xtServiceCenterList = xtServiceCenterService.getXtServiceCenterListByCondition(condition);
		PageInfo<XtServiceCenter> page = new PageInfo<XtServiceCenter>(xtServiceCenterList);
		return outPageStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_service_center_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtServiceCenterById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtServiceCenterById(String xt_service_center_id,HttpServletRequest request){
		XtServiceCenter xtServiceCenter = xtServiceCenterService.getXtServiceCenterById(xt_service_center_id);
		return outDataStr(xtServiceCenter);
	}
	/**
	* 添加
	* @param xtServiceCenter
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtServiceCenter(XtServiceCenter xtServiceCenter,HttpServletRequest request){
		int i = 0;
		if(null != xtServiceCenter){
			xtServiceCenter.setXt_service_center_id(UUID.toUUID());
			i=xtServiceCenterService.addXtServiceCenter(xtServiceCenter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtServiceCenter
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtServiceCenter(XtServiceCenter xtServiceCenter,HttpServletRequest request){
		int i = 0;
		if(null != xtServiceCenter){
			i=xtServiceCenterService.updateXtServiceCenter(xtServiceCenter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_service_center_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtServiceCenter(String xt_service_center_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_service_center_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_service_center_id",xt_service_center_id.split(","));
			i=xtServiceCenterService.delXtServiceCenter(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_service_center_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtServiceCenter(String xt_service_center_id,HttpServletRequest request){
		int i = 0;
		XtServiceCenter xtServiceCenter = xtServiceCenterService.getXtServiceCenterById(xt_service_center_id);
		if(null != xtServiceCenter){
			xtServiceCenter.setXt_service_center_id(UUID.toUUID());
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_service_center_id", xt_service_center_id);
			List<XtServiceCenterParameter> xt_Service_Center_ParameterList = xtServiceCenterParameterService.getXtServiceCenterParameterListByCondition(condition);
			xtServiceCenter.setXt_Service_Center_Parameter(xt_Service_Center_ParameterList);
			i=xtServiceCenterService.addXtServiceCenter(xtServiceCenter);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtServiceCenter",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtServiceCenter(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
}
