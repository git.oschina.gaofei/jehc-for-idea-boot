package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtIpFrozen;
import jehc.xtmodules.xtservice.XtIpFrozenService;

/**
* 平台IP冻结账户 
* 2016-02-29 10:41:23  邓纯杰
*/
@Api(value = "平台IP冻结账户", description = "平台IP冻结账户")
@Controller
@RequestMapping("/xtIpFrozenController")
public class XtIpFrozenController extends BaseAction{
	@Autowired
	private XtIpFrozenService xtIpFrozenService;
	/**
	* 列表页面
	* @param xtIpFrozen
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtIpFrozen(XtIpFrozen xtIpFrozen,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-ip-frozen/xt-ip-frozen-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtIpFrozenListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtIpFrozenListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtIpFrozen> xtIpFrozenList = xtIpFrozenService.getXtIpFrozenListByCondition(condition);
		PageInfo<XtIpFrozen> page = new PageInfo<XtIpFrozen>(xtIpFrozenList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_ip_frozen_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtIpFrozenById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtIpFrozenById(String xt_ip_frozen_id,HttpServletRequest request){
		XtIpFrozen xtIpFrozen = xtIpFrozenService.getXtIpFrozenById(xt_ip_frozen_id);
		return outDataStr(xtIpFrozen);
	}
	/**
	* 添加
	* @param xtIpFrozen
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtIpFrozen(XtIpFrozen xtIpFrozen,HttpServletRequest request){
		int i = 0;
		if(null != xtIpFrozen){
			xtIpFrozen.setXt_ip_frozen_id(UUID.toUUID());
			xtIpFrozen.setXt_userinfo_id(CommonUtils.getXtUid());
			xtIpFrozen.setXt_ip_frozen_ctime(CommonUtils.getSimpleDateFormat());
			i=xtIpFrozenService.addXtIpFrozen(xtIpFrozen);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtIpFrozen
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtIpFrozen(XtIpFrozen xtIpFrozen,HttpServletRequest request){
		int i = 0;
		if(null != xtIpFrozen){
			xtIpFrozen.setXt_userinfo_id(CommonUtils.getXtUid());
			xtIpFrozen.setXt_ip_frozen_mtime(CommonUtils.getSimpleDateFormat());
			i=xtIpFrozenService.updateXtIpFrozen(xtIpFrozen);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_ip_frozen_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtIpFrozen(String xt_ip_frozen_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_ip_frozen_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_ip_frozen_id",xt_ip_frozen_id.split(","));
			i=xtIpFrozenService.delXtIpFrozen(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_ip_frozen_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtIpFrozen(String xt_ip_frozen_id,HttpServletRequest request){
		int i = 0;
		XtIpFrozen xtIpFrozen = xtIpFrozenService.getXtIpFrozenById(xt_ip_frozen_id);
		if(null != xtIpFrozen){
			xtIpFrozen.setXt_ip_frozen_id(UUID.toUUID());
			i=xtIpFrozenService.addXtIpFrozen(xtIpFrozen);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtIpFrozen",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtIpFrozen(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtIpFrozenAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtIpFrozenAdd(XtIpFrozen xtIpFrozen,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-ip-frozen/xt-ip-frozen-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtIpFrozenUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtIpFrozenUpdate(String xt_ip_frozen_id,HttpServletRequest request, Model model){
		XtIpFrozen xtIpFrozen = xtIpFrozenService.getXtIpFrozenById(xt_ip_frozen_id);
		model.addAttribute("xtIpFrozen", xtIpFrozen);
		return new ModelAndView("pc/xt-view/xt-ip-frozen/xt-ip-frozen-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtIpFrozenDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtIpFrozenDetail(String xt_ip_frozen_id,HttpServletRequest request, Model model){
		XtIpFrozen xtIpFrozen = xtIpFrozenService.getXtIpFrozenById(xt_ip_frozen_id);
		model.addAttribute("xtIpFrozen", xtIpFrozen);
		return new ModelAndView("pc/xt-view/xt-ip-frozen/xt-ip-frozen-detail");
	}
}
