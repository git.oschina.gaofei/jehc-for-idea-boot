package jehc.xtmodules.xtweb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtmodel.XtConstant;
import jehc.xtmodules.xtservice.XtConstantService;

/**
* 平台常量
* 2015-09-30 14:36:13  邓纯杰
*/
@Api(value = "平台常量", description = "平台常量")
@Controller
@RequestMapping("/xtConstantController")
public class XtConstantController extends BaseAction{
	@Autowired
	private XtConstantService xtConstantService;
	/**
	* 列表页面
	* @param xtConstant
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadXtConstant(XtConstant xtConstant,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-constant/xt-constant-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getXtConstantListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtConstantListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<XtConstant> xtConstantList = xtConstantService.getXtConstantListByCondition(condition);
		PageInfo<XtConstant> page = new PageInfo<XtConstant>(xtConstantList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param xt_constant_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getXtConstantById",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtConstantById(String xt_constant_id,HttpServletRequest request){
		XtConstant xtConstant = xtConstantService.getXtConstantById(xt_constant_id);
		return outDataStr(xtConstant);
	}
	/**
	* 添加
	* @param xtConstant
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public String addXtConstant(XtConstant xtConstant,HttpServletRequest request){
		int i = 0;
		if(null != xtConstant){
			xtConstant.setXt_constant_id(UUID.toUUID());
			i=xtConstantService.addXtConstant(xtConstant);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtConstant
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public String updateXtConstant(XtConstant xtConstant,HttpServletRequest request){
		int i = 0;
		if(null != xtConstant){
			i=xtConstantService.updateXtConstant(xtConstant);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_constant_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public String delXtConstant(String xt_constant_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_constant_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_constant_id",xt_constant_id.split(","));
			i=xtConstantService.delXtConstant(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param xt_constant_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public String copyXtConstant(String xt_constant_id,HttpServletRequest request){
		int i = 0;
		XtConstant xtConstant = xtConstantService.getXtConstantById(xt_constant_id);
		if(null != xtConstant){
			xtConstant.setXt_constant_id(UUID.toUUID());
			i=xtConstantService.addXtConstant(xtConstant);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportXtConstant",method={RequestMethod.POST,RequestMethod.GET})
	public void exportXtConstant(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	
	/**
	 * 根据类型查询常量集合
	 * @return
	 */
	@ApiOperation(value="根据类型查询常量集合", notes="根据类型查询常量集合")
	@ResponseBody
	@RequestMapping(value="/getXtConstantList",method={RequestMethod.POST,RequestMethod.GET})
	public String getXtConstantList(String xt_constantType){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_constantType", xt_constantType);
		List<XtConstant> xtConstantList = xtConstantService.getXtConstantListByCondition(condition);
		return outItemsStr(xtConstantList);
	}
	
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toXtConstantAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConstantAdd(XtConstant xtConstant,HttpServletRequest request){
		return new ModelAndView("pc/xt-view/xt-constant/xt-constant-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toXtConstantUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConstantUpdate(String xt_constant_id,HttpServletRequest request, Model model){
		XtConstant xtConstant = xtConstantService.getXtConstantById(xt_constant_id);
		model.addAttribute("xtConstant", xtConstant);
		return new ModelAndView("pc/xt-view/xt-constant/xt-constant-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toXtConstantDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toXtConstantDetail(String xt_constant_id,HttpServletRequest request, Model model){
		XtConstant xtConstant = xtConstantService.getXtConstantById(xt_constant_id);
		model.addAttribute("xtConstant", xtConstant);
		return new ModelAndView("pc/xt-view/xt-constant/xt-constant-detail");
	}
}
