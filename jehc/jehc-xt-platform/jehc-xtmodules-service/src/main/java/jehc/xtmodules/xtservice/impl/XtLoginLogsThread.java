package jehc.xtmodules.xtservice.impl;

import jehc.xtmodules.xtcore.httpclient.HttpUtil;
import jehc.xtmodules.xtcore.util.InitIpConfig;
import jehc.xtmodules.xtcore.util.SpringUtils;
import jehc.xtmodules.xtdao.XtLoginLogsDao;
import jehc.xtmodules.xtmodel.XtLoginLogs;
import jehc.xtmodules.xtservice.XtLoginLogsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class XtLoginLogsThread extends  Thread {
    private static Log logger = LogFactory.getLog(HttpUtil.class);

    XtLoginLogs xtLoginLogs;

    public XtLoginLogs getXtLoginLogs() {
        return xtLoginLogs;
    }

    public void setXtLoginLogs(XtLoginLogs xtLoginLogs) {
        this.xtLoginLogs = xtLoginLogs;
    }

    public XtLoginLogsThread(){

    }

    public XtLoginLogsThread(XtLoginLogs xtLoginLogs){
        this.xtLoginLogs = xtLoginLogs;
    }

    @Override
    public void run() {
        addLoginLogs();
    }

    private void addLoginLogs(){
        try {
            String ip = xtLoginLogs.getXt_login_logIP();
            InitIpConfig initIpConfig = SpringUtils.getBean(InitIpConfig.class);
            String api = (String)initIpConfig.getDataIpApi().get("ipApi");
            String res = HttpUtil.doGet(api,"ip="+ip,"gbk",false);
            xtLoginLogs.setXt_login_logContent(res+"---------->"+xtLoginLogs.getXt_login_logContent());
        }catch (Exception e){
            logger.error("记录ip出现异常："+e.getMessage());
        }
        SpringUtils.getBean(XtLoginLogsDao.class).addXtLoginLogs(xtLoginLogs);
    }
}
