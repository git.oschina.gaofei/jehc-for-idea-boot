package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtQuartzDao;
import jehc.xtmodules.xtmodel.XtQuartz;
import jehc.xtmodules.xtservice.XtQuartzService;

/**
* 任务调度配置信息
* 2015-10-29 16:50:03  邓纯杰
*/
@Service("xtQuartzService")
public class XtQuartzServiceImpl extends BaseService implements XtQuartzService{
	@Autowired
	private XtQuartzDao xtQuartzDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtQuartz> getXtQuartzListByCondition(Map<String,Object> condition){
		try{
			return xtQuartzDao.getXtQuartzListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public XtQuartz getXtQuartzById(String id){
		try{
			return xtQuartzDao.getXtQuartzById(id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtQuartz
	* @return
	*/
	public int addXtQuartz(XtQuartz xtQuartz){
		int i = 0;
		try {
			i = xtQuartzDao.addXtQuartz(xtQuartz);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtQuartz
	* @return
	*/
	public int updateXtQuartz(XtQuartz xtQuartz){
		int i = 0;
		try {
			i = xtQuartzDao.updateXtQuartz(xtQuartz);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtQuartz(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtQuartzDao.delXtQuartz(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 查找集合
	 * @param condition
	 * @return
	 */
	public List<XtQuartz> getXtQuartzListAllByCondition(Map<String,Object> condition){
		try{
			return xtQuartzDao.getXtQuartzListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
