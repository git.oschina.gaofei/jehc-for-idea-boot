package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtAreaRegionDao;
import jehc.xtmodules.xtmodel.XtAreaRegion;
import jehc.xtmodules.xtservice.XtAreaRegionService;

/**
* 行政区划
* 2017-05-04 14:54:34  邓纯杰
*/
@Service("xtAreaRegionService")
public class XtAreaRegionServiceImpl extends BaseService implements XtAreaRegionService{
	@Autowired
	private XtAreaRegionDao xtAreaRegionDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition){
		try{
			return xtAreaRegionDao.getXtAreaRegionListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	public XtAreaRegion getXtAreaRegionById(String ID){
		try{
			XtAreaRegion xt_Area_Region = xtAreaRegionDao.getXtAreaRegionById(ID);
			return xt_Area_Region;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	public int addXtAreaRegion(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.addXtAreaRegion(xtAreaRegion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegion(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateXtAreaRegion(xtAreaRegion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateXtAreaRegionBySelective(xtAreaRegion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtAreaRegion(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtAreaRegionDao.delXtAreaRegion(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	public int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.addBatchXtAreaRegion(xtAreaRegionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateBatchXtAreaRegion(xtAreaRegionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateBatchXtAreaRegionBySelective(xtAreaRegionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
