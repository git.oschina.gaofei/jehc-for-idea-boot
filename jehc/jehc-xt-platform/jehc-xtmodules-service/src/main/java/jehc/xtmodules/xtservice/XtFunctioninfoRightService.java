package jehc.xtmodules.xtservice;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtFunctioninfoRight;

/**
* 功能分配
* 2016-10-08 17:38:19  邓纯杰
*/
public interface XtFunctioninfoRightService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtFunctioninfoRight> getXtFunctioninfoRightListByCondition(Map<String,Object> condition);
	/**
	 * 初始化分页（for admin all function）
	 * @param condition
	 * @return
	 */
	public List<XtFunctioninfoRight> getXtFunctioninfoListForAdmin(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_functioninfo_right_id 
	* @return
	*/
	public XtFunctioninfoRight getXtFunctioninfoRightById(String xt_functioninfo_right_id);
	/**
	* 添加
	* @param xtFunctioninfoRight
	* @return
	*/
	public int addXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight);
	/**
	* 修改
	* @param xtFunctioninfoRight
	* @return
	*/
	public int updateXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtFunctioninfoRight(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtFunctioninfoRightList
	* @return
	*/
	public int addBatchXtFunctioninfoRight(List<XtFunctioninfoRight> xtFunctioninfoRightList);
	/**
	* 批量修改
	* @param xtFunctioninfoRightList
	* @return
	*/
	public int updateBatchXtFunctioninfoRight(List<XtFunctioninfoRight> xtFunctioninfoRightList);
}
