package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtVersionDao;
import jehc.xtmodules.xtmodel.XtVersion;
import jehc.xtmodules.xtservice.XtVersionService;

/**
* 平台版本 
* 2017-04-16 20:05:24  邓纯杰
*/
@Service("xtVersionService")
public class XtVersionServiceImpl extends BaseService implements XtVersionService{
	@Autowired
	private XtVersionDao xtVersionDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtVersion> getXtVersionListByCondition(Map<String,Object> condition){
		try{
			return xtVersionDao.getXtVersionListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_version_id 
	* @return
	*/
	public XtVersion getXtVersionById(String xt_version_id){
		try{
			XtVersion xt_Version = xtVersionDao.getXtVersionById(xt_version_id);
			return xt_Version;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtVersion
	* @return
	*/
	public int addXtVersion(XtVersion xtVersion){
		int i = 0;
		try {
			i = xtVersionDao.addXtVersion(xtVersion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtVersion
	* @return
	*/
	public int updateXtVersion(XtVersion xtVersion){
		int i = 0;
		try {
			i = xtVersionDao.updateXtVersion(xtVersion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param xtVersion
	* @return
	*/
	public int updateXtVersionBySelective(XtVersion xtVersion){
		int i = 0;
		try {
			i = xtVersionDao.updateXtVersionBySelective(xtVersion);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtVersion(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtVersionDao.delXtVersion(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtVersionList
	* @return
	*/
	public int addBatchXtVersion(List<XtVersion> xtVersionList){
		int i = 0;
		try {
			i = xtVersionDao.addBatchXtVersion(xtVersionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersion(List<XtVersion> xtVersionList){
		int i = 0;
		try {
			i = xtVersionDao.updateBatchXtVersion(xtVersionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersionBySelective(List<XtVersion> xtVersionList){
		int i = 0;
		try {
			i = xtVersionDao.updateBatchXtVersionBySelective(xtVersionList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
