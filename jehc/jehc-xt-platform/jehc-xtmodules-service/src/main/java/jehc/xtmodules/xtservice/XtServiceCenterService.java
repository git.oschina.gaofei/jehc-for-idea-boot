package jehc.xtmodules.xtservice;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtServiceCenter;

/**
* 服务中心 
* 2017-03-27 12:32:04  邓纯杰
*/
public interface XtServiceCenterService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtServiceCenter> getXtServiceCenterListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_service_center_id 
	* @return
	*/
	public XtServiceCenter getXtServiceCenterById(String xt_service_center_id);
	/**
	* 添加
	* @param xtServiceCenter
	* @return
	*/
	public int addXtServiceCenter(XtServiceCenter xtServiceCenter);
	/**
	* 修改
	* @param xtServiceCenter
	* @return
	*/
	public int updateXtServiceCenter(XtServiceCenter xtServiceCenter);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtServiceCenter(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtServiceCenterList
	* @return
	*/
	public int addBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList);
	/**
	* 批量修改
	* @param xtServiceCenterList
	* @return
	*/
	public int updateBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList);
}
