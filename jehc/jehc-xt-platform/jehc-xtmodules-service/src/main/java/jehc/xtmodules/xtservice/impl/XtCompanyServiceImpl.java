package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtCompanyDao;
import jehc.xtmodules.xtmodel.XtCompany;
import jehc.xtmodules.xtservice.XtCompanyService;

/**
* 平台公司信息
* 2015-05-12 22:59:42  邓纯杰
*/
@Service("xtCompanyService")
public class XtCompanyServiceImpl extends BaseService implements XtCompanyService{
	@Autowired
	private XtCompanyDao xtCompanyDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtCompany> getXtCompanyListByCondition(Map<String,Object> condition){
		try {
			return xtCompanyDao.getXtCompanyListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_company_id 
	* @return
	*/
	public XtCompany getXtCompanyById(String xt_company_id){
		try {
			return xtCompanyDao.getXtCompanyById(xt_company_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtCompany
	* @return
	*/
	public int addXtCompany(XtCompany xtCompany){
		int i = 0;
		try {
			i = xtCompanyDao.addXtCompany(xtCompany);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtCompany
	* @return
	*/
	public int updateXtCompany(XtCompany xtCompany){
		int i = 0;
		try {
			i = xtCompanyDao.updateXtCompany(xtCompany);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtCompany(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtCompanyDao.delXtCompany(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
