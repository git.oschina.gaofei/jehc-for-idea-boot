package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.allutils.StringUtil;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtServiceCenterDao;
import jehc.xtmodules.xtmodel.XtServiceCenter;
import jehc.xtmodules.xtmodel.XtServiceCenterParameter;
import jehc.xtmodules.xtservice.XtServiceCenterService;
import jehc.xtmodules.xtservice.XtServiceCenterParameterService;

import java.util.HashMap;
import java.util.ArrayList;

/**
* 服务中心 
* 2017-03-27 12:32:04  邓纯杰
*/
@Service("xtServiceCenterService")
public class XtServiceCenterServiceImpl extends BaseService implements XtServiceCenterService{
	@Autowired
	private XtServiceCenterDao xtServiceCenterDao;
	@Autowired
	private XtServiceCenterParameterService xtServiceCenterParameterService;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtServiceCenter> getXtServiceCenterListByCondition(Map<String,Object> condition){
		try{
			return xtServiceCenterDao.getXtServiceCenterListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_service_center_id 
	* @return
	*/
	public XtServiceCenter getXtServiceCenterById(String xt_service_center_id){
		try{
			XtServiceCenter xt_Service_Center = xtServiceCenterDao.getXtServiceCenterById(xt_service_center_id);
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_service_center_id", xt_service_center_id);
			List<XtServiceCenterParameter> xt_Service_Center_Parameter = xtServiceCenterParameterService.getXtServiceCenterParameterListByCondition(condition);
			xt_Service_Center.setXt_Service_Center_Parameter(xt_Service_Center_Parameter);
			return xt_Service_Center;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtServiceCenter
	* @return
	*/
	public int addXtServiceCenter(XtServiceCenter xtServiceCenter){
		int i = 0;
		try {
			i = xtServiceCenterDao.addXtServiceCenter(xtServiceCenter);
			List<XtServiceCenterParameter> xtServiceCenterParameterTempList = xtServiceCenter.getXt_Service_Center_Parameter();
			List<XtServiceCenterParameter> xtServiceCenterParameterList = new ArrayList<XtServiceCenterParameter>();
			for(int j = 0; j < xtServiceCenterParameterTempList.size(); j++){
				if(xtServiceCenter.getXt_Service_Center_Parameter_removed_flag().indexOf(","+j+",") == -1){
					xtServiceCenterParameterTempList.get(j).setXt_service_center_id(xtServiceCenter.getXt_service_center_id());
					xtServiceCenterParameterTempList.get(j).setXt_service_center_parameter_id(toUUID());
					xtServiceCenterParameterList.add(xtServiceCenterParameterTempList.get(j));
				}
			}
			if(!xtServiceCenterParameterList.isEmpty()&&xtServiceCenterParameterList.size()>0){
				for(XtServiceCenterParameter xtServiceCenterParameter:xtServiceCenterParameterList){
					xtServiceCenterParameterService.addXtServiceCenterParameter(xtServiceCenterParameter);
				}
			}
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtServiceCenter
	* @return
	*/
	public int updateXtServiceCenter(XtServiceCenter xtServiceCenter){
		int i = 0;
		try {
			i = xtServiceCenterDao.updateXtServiceCenter(xtServiceCenter);
			List<XtServiceCenterParameter> xtServiceCenterParameterList = xtServiceCenter.getXt_Service_Center_Parameter();
			List<XtServiceCenterParameter> xtServiceCenterParameterAddList = new ArrayList<XtServiceCenterParameter>();
			List<XtServiceCenterParameter> xtServiceCenterParameterUpdateList = new ArrayList<XtServiceCenterParameter>();
			for(int j = 0; j < xtServiceCenterParameterList.size(); j++){
				if(xtServiceCenter.getXt_Service_Center_Parameter_removed_flag().indexOf(","+j+",") == -1){
					xtServiceCenterParameterList.get(j).setXt_service_center_id(xtServiceCenter.getXt_service_center_id());
					if(StringUtil.isEmpty(xtServiceCenterParameterList.get(j).getXt_service_center_parameter_id())){
						xtServiceCenterParameterList.get(j).setXt_service_center_parameter_id(toUUID());
						xtServiceCenterParameterAddList.add(xtServiceCenterParameterList.get(j));
					}else{
						xtServiceCenterParameterUpdateList.add(xtServiceCenterParameterList.get(j));
					}
				}
			}
			if(!xtServiceCenterParameterAddList.isEmpty()&&xtServiceCenterParameterAddList.size()>0){
				xtServiceCenterParameterService.addBatchXtServiceCenterParameter(xtServiceCenterParameterAddList);
			}
			if(!xtServiceCenterParameterUpdateList.isEmpty()&&xtServiceCenterParameterUpdateList.size()>0){
				xtServiceCenterParameterService.updateBatchXtServiceCenterParameter(xtServiceCenterParameterUpdateList);
			}
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtServiceCenter(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtServiceCenterDao.delXtServiceCenter(condition);
			String[] xt_service_center_idList= (String[])condition.get("xt_service_center_id");
			for(String xt_service_center_id:xt_service_center_idList){
				xtServiceCenterParameterService.delXtServiceCenterParameterByForeignKey(xt_service_center_id);
			}
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtServiceCenterList
	* @return
	*/
	public int addBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList){
		int i = 0;
		try {
			i = xtServiceCenterDao.addBatchXtServiceCenter(xtServiceCenterList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtServiceCenterList
	* @return
	*/
	public int updateBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList){
		int i = 0;
		try {
			i = xtServiceCenterDao.updateBatchXtServiceCenter(xtServiceCenterList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
