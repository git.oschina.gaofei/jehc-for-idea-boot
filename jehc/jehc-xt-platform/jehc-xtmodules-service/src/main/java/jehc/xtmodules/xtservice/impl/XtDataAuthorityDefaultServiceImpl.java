package jehc.xtmodules.xtservice.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.xtmodules.xtservice.XtDataAuthorityDefaultService;
import jehc.xtmodules.xtdao.XtDataAuthorityDao;
import jehc.xtmodules.xtdao.XtDataAuthorityDefaultDao;
import jehc.xtmodules.xtmodel.XtDataAuthorityDefault;

/**
* 数权限初始化默认设置 
* 2017-06-20 14:38:52  邓纯杰
*/
@Service("xtDataAuthorityDefaultService")
public class XtDataAuthorityDefaultServiceImpl extends BaseService implements XtDataAuthorityDefaultService{
	@Autowired
	private XtDataAuthorityDefaultDao xtDataAuthorityDefaultDao;
	@Autowired
	private XtDataAuthorityDao xtDataAuthorityDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDataAuthorityDefault> getXtDataAuthorityDefaultListByCondition(Map<String,Object> condition){
		try{
			return xtDataAuthorityDefaultDao.getXtDataAuthorityDefaultListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_data_authority_default_id 
	* @return
	*/
	public XtDataAuthorityDefault getXtDataAuthorityDefaultById(String xt_data_authority_default_id){
		try{
			XtDataAuthorityDefault xtDataAuthorityDefault = xtDataAuthorityDefaultDao.getXtDataAuthorityDefaultById(xt_data_authority_default_id);
			return xtDataAuthorityDefault;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int addXtDataAuthorityDefault(XtDataAuthorityDefault xtDataAuthorityDefault){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.addXtDataAuthorityDefault(xtDataAuthorityDefault);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int updateXtDataAuthorityDefault(XtDataAuthorityDefault xtDataAuthorityDefault){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.updateXtDataAuthorityDefault(xtDataAuthorityDefault);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int updateXtDataAuthorityDefaultBySelective(XtDataAuthorityDefault xtDataAuthorityDefault){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.updateXtDataAuthorityDefaultBySelective(xtDataAuthorityDefault);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDataAuthorityDefault(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.delXtDataAuthorityDefault(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 根据情况删除
	 * @param condition
	 * @return
	 */
	public int delXtDataAuthorityDefaultAllByCondition(Map<String,Object> condition){
		int i = 0;
		try {
			xtDataAuthorityDao.delXtDataAuthorityByCondition(condition);
			xtDataAuthorityDefaultDao.delXtDataAuthorityDefaultAllByCondition(condition);
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int addBatchXtDataAuthorityDefault(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList,String xt_menuinfo_id){
		int i = 0;
		try {
			//1删除 原先
			Map<String,Object> condition = new HashMap<String, Object>();
			condition.put("xt_menuinfo_id", xt_menuinfo_id);
			//2删除 执行表
			condition.put("xt_data_authorityType", "4");
			xtDataAuthorityDao.delXtDataAuthorityByCondition(condition);
			xtDataAuthorityDefaultDao.delXtDataAuthorityDefaultAllByCondition(condition);
			//3添加 最新
			if(null != xtDataAuthorityDefaultList && xtDataAuthorityDefaultList.size()>0){
				for(XtDataAuthorityDefault xt_Data_Authority_Default :xtDataAuthorityDefaultList){
					xtDataAuthorityDefaultDao.addXtDataAuthorityDefault(xt_Data_Authority_Default);
				}
				//兼容oracle与mysql语法 废弃批量插入
//				xtDataAuthorityDefaultDao.addBatchXtDataAuthorityDefault(xt_Data_Authority_DefaultList);
			}
			//4统一推送
			addPushDataAuthority();
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int updateBatchXtDataAuthorityDefault(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.updateBatchXtDataAuthorityDefault(xtDataAuthorityDefaultList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int updateBatchXtDataAuthorityDefaultBySelective(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList){
		int i = 0;
		try {
			i = xtDataAuthorityDefaultDao.updateBatchXtDataAuthorityDefaultBySelective(xtDataAuthorityDefaultList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
