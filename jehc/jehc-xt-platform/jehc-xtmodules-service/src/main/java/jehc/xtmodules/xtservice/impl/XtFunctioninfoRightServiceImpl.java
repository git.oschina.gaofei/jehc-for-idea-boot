package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtFunctioninfoRightDao;
import jehc.xtmodules.xtmodel.XtFunctioninfoRight;
import jehc.xtmodules.xtservice.XtFunctioninfoRightService;

/**
* 功能分配
* 2016-10-08 17:38:19  邓纯杰
*/
@Service("xtFunctioninfoRightService")
public class XtFunctioninfoRightServiceImpl extends BaseService implements XtFunctioninfoRightService{
	@Autowired
	private XtFunctioninfoRightDao xtFunctioninfoRightDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtFunctioninfoRight> getXtFunctioninfoRightListByCondition(Map<String,Object> condition){
		try{
			return xtFunctioninfoRightDao.getXtFunctioninfoRightListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 初始化分页（for admin all function）
	 * @param condition
	 * @return
	 */
	public List<XtFunctioninfoRight> getXtFunctioninfoListForAdmin(Map<String,Object> condition){
		try{
			return xtFunctioninfoRightDao.getXtFunctioninfoListForAdmin(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_functioninfo_right_id 
	* @return
	*/
	public XtFunctioninfoRight getXtFunctioninfoRightById(String xt_functioninfo_right_id){
		try{
			return xtFunctioninfoRightDao.getXtFunctioninfoRightById(xt_functioninfo_right_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtFunctioninfoRight
	* @return
	*/
	public int addXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight){
		int i = 0;
		try {
			i = xtFunctioninfoRightDao.addXtFunctioninfoRight(xtFunctioninfoRight);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtFunctioninfoRight
	* @return
	*/
	public int updateXtFunctioninfoRight(XtFunctioninfoRight xtFunctioninfoRight){
		int i = 0;
		try {
			i = xtFunctioninfoRightDao.updateXtFunctioninfoRight(xtFunctioninfoRight);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtFunctioninfoRight(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtFunctioninfoRightDao.delXtFunctioninfoRight(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtFunctioninfoRightList
	* @return
	*/
	public int addBatchXtFunctioninfoRight(List<XtFunctioninfoRight> xtFunctioninfoRightList){
		int i = 0;
		try {
			i = xtFunctioninfoRightDao.addBatchXtFunctioninfoRight(xtFunctioninfoRightList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtFunctioninfoRightList
	* @return
	*/
	public int updateBatchXtFunctioninfoRight(List<XtFunctioninfoRight> xtFunctioninfoRightList){
		int i = 0;
		try {
			i = xtFunctioninfoRightDao.updateBatchXtFunctioninfoRight(xtFunctioninfoRightList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
