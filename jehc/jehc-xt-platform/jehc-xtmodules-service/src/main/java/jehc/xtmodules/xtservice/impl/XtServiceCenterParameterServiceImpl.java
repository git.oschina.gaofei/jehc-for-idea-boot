package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtServiceCenterParameterDao;
import jehc.xtmodules.xtmodel.XtServiceCenterParameter;
import jehc.xtmodules.xtservice.XtServiceCenterParameterService;

/**
* 服务中心参数 
* 2017-03-27 12:32:04  邓纯杰
*/
@Service("xtServiceCenterParameterService")
public class XtServiceCenterParameterServiceImpl extends BaseService implements XtServiceCenterParameterService{
	@Autowired
	private XtServiceCenterParameterDao xtServiceCenterParameterDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtServiceCenterParameter> getXtServiceCenterParameterListByCondition(Map<String,Object> condition){
		try{
			return xtServiceCenterParameterDao.getXtServiceCenterParameterListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_service_center_parameter_id 
	* @return
	*/
	public XtServiceCenterParameter getXtServiceCenterParameterById(String xt_service_center_parameter_id){
		try{
			XtServiceCenterParameter xtServiceCenterParameter = xtServiceCenterParameterDao.getXtServiceCenterParameterById(xt_service_center_parameter_id);
			return xtServiceCenterParameter;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtServiceCenterParameter
	* @return
	*/
	public int addXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.addXtServiceCenterParameter(xtServiceCenterParameter);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtServiceCenterParameter
	* @return
	*/
	public int updateXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.updateXtServiceCenterParameter(xtServiceCenterParameter);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 修改（根据动态条件）
	 * @param xt_Service_Center_Parameter
	 * @return
	 */
	public int updateXtServiceCenterParameterBySelective(XtServiceCenterParameter xt_Service_Center_Parameter){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.updateXtServiceCenterParameterBySelective(xt_Service_Center_Parameter);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i; 
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtServiceCenterParameter(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.delXtServiceCenterParameter(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtServiceCenterParameterList
	* @return
	*/
	public int addBatchXtServiceCenterParameter(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.addBatchXtServiceCenterParameter(xtServiceCenterParameterList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtServiceCenterParameterList
	* @return
	*/
	public int updateBatchXtServiceCenterParameter(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.updateBatchXtServiceCenterParameter(xtServiceCenterParameterList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 批量修改（根据动态条件）
	 * @param xtServiceCenterParameterList
	 * @return
	 */
	public int updateBatchXtServiceCenterParameterBySelective(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.updateBatchXtServiceCenterParameterBySelective(xtServiceCenterParameterList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 根据外键删除方法
	* @param xt_service_center_id
	* @return
	*/
	public int delXtServiceCenterParameterByForeignKey(String xt_service_center_id){
		int i = 0;
		try {
			i = xtServiceCenterParameterDao.delXtServiceCenterParameterByForeignKey(xt_service_center_id);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
