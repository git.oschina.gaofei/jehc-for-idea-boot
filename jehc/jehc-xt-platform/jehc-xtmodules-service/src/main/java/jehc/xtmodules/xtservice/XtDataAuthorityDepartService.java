package jehc.xtmodules.xtservice;
import java.util.List;
import java.util.Map;
import jehc.xtmodules.xtmodel.XtDataAuthorityDepart;

/**
* 数据权限按部门设置 
* 2017-06-20 14:36:19  邓纯杰
*/
public interface XtDataAuthorityDepartService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDataAuthorityDepart> getXtDataAuthorityDepartListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_data_authority_depart_id 
	* @return
	*/
	public XtDataAuthorityDepart getXtDataAuthorityDepartById(String xt_data_authority_depart_id);
	/**
	* 添加
	* @param xtDataAuthorityDepart
	* @return
	*/
	public int addXtDataAuthorityDepart(XtDataAuthorityDepart xtDataAuthorityDepart);
	/**
	* 修改
	* @param xtDataAuthorityDepart
	* @return
	*/
	public int updateXtDataAuthorityDepart(XtDataAuthorityDepart xtDataAuthorityDepart);
	/**
	* 修改（根据动态条件）
	* @param xtDataAuthorityDepart
	* @return
	*/
	public int updateXtDataAuthorityDepartBySelective(XtDataAuthorityDepart xtDataAuthorityDepart);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDataAuthorityDepart(Map<String,Object> condition);
	/**
	 * 根据条件删除
	 * @param condition
	 * @return
	 */
	public int delXtDataAuthorityDepartAllByCondition(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtDataAuthorityDepartList
	* @return
	*/
	public int addBatchXtDataAuthorityDepart(List<XtDataAuthorityDepart> xtDataAuthorityDepartList,String xt_departinfo_id,String id,String xt_menuinfo_id);
	/**
	* 批量修改
	* @param xtDataAuthorityDepartList
	* @return
	*/
	public int updateBatchXtDataAuthorityDepart(List<XtDataAuthorityDepart> xtDataAuthorityDepartList);
	/**
	* 批量修改（根据动态条件）
	* @param xtDataAuthorityDepartList
	* @return
	*/
	public int updateBatchXtDataAuthorityDepartBySelective(List<XtDataAuthorityDepart> xtDataAuthorityDepartList);
}
