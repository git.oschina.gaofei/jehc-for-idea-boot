package jehc.xtmodules.xtservice;
import java.util.List;
import java.util.Map;
import jehc.xtmodules.xtmodel.XtAreaRegion;

/**
* 行政区划
* 2017-05-04 14:54:34  邓纯杰
*/
public interface XtAreaRegionService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	public XtAreaRegion getXtAreaRegionById(String ID);
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	public int addXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtAreaRegion(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	public int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList);
}
