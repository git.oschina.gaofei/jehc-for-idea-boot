package jehc.xtmodules.xtservice;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtDbtableStrategy;

/**
* 分表策略
* 2017-02-14 16:23:48  邓纯杰
*/
public interface XtDbtableStrategyService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDbtableStrategy> getXtDbtableStrategyListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_dbtable_strategy_id 
	* @return
	*/
	public XtDbtableStrategy getXtDbtableStrategyById(String xt_dbtable_strategy_id);
	/**
	* 添加
	* @param xtDbtableStrategy
	* @return
	*/
	public int addXtDbtableStrategy(XtDbtableStrategy xtDbtableStrategy);
	/**
	* 修改
	* @param xtDbtableStrategy
	* @return
	*/
	public int updateXtDbtableStrategy(XtDbtableStrategy xtDbtableStrategy);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDbtableStrategy(Map<String,Object> condition);
}
