package jehc.xtmodules.xtservice.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtcore.util.db.DBMSMetaUtil;
import jehc.xtmodules.xtcore.util.db.DbInfo;
import jehc.xtmodules.xtcore.util.db.MapUtil;
import jehc.xtmodules.xtcore.util.generator.Gutil;
import jehc.xtmodules.xtdao.XtGeneratorDao;
import jehc.xtmodules.xtmodel.XtGenerator;
import jehc.xtmodules.xtmodel.XtGeneratorTableColumn;
import jehc.xtmodules.xtservice.XtGeneratorService;
/**
 * 代码生成器
 * @author邓纯杰
 *
 */
@Service("xtGeneratorService")
public class XtGeneratorServiceImpl extends BaseService implements XtGeneratorService {
	@Autowired
	private XtGeneratorDao xtGeneratorDao;
	/**
	 * 查询所有生成信息并分页
	 * @param condition
	 * @return
	 */
	public List<XtGenerator> getXtGeneratorListByCondition(Map<String, Object> condition){
		try {
			return xtGeneratorDao.getXtGeneratorListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 生成代码
	 * @param xtGenerator
	 */
	public int addXtGenerator(XtGenerator xtGenerator){
		Gutil gutil = new Gutil();
		int result = 0;
		try {
			String table_name = xtGenerator.getXt_generator_tbname();
			String comment = xtGenerator.getXt_generator_tbcomment();
			List<XtGeneratorTableColumn> xt_Generator_Table_ColumnList = new ArrayList<XtGeneratorTableColumn>();
			DbInfo dbInfo = DBMSMetaUtil.excuteDB(table_name);
			List<Map<String, Object>> columns = dbInfo.getColumns();
			List<Map<String, Object>> columnsPrimary = MapUtil.convertKeyList2LowerCase(dbInfo.getColumnsPrimary());
			String pk=null;
			for(int i = 0; i < columnsPrimary.size(); i++){
				Map<String, Object> map = columnsPrimary.get(i);
				pk = map.get("column_name").toString();
				break;
			}
			if(!columns.isEmpty() && columns.size()>0){
				for(int i = 0; i < columns.size(); i++){
					Map<String, Object> map = columns.get(i);
					XtGeneratorTableColumn xtGeneratorTableColumn = new XtGeneratorTableColumn();
					xtGeneratorTableColumn.setCOLUMN_NAME(map.get("COLUMN_NAME").toString());
					xtGeneratorTableColumn.setCOLUMN_COMMENT(map.get("REMARKS").toString());
					/**
					 xtGeneratorTableColumn.setDATA_TYPE(map.get("DATA_TYPE").toString());
					**/
					xtGeneratorTableColumn.setDATA_TYPE(map.get("TYPE_NAME").toString());
					xtGeneratorTableColumn.setIS_NULLABLE(map.get("IS_NULLABLE").toString());
					xtGeneratorTableColumn.setCHARACTER_MAXIMUM_LENGTH(map.get("COLUMN_SIZE").toString());
					if(null != pk && !"".equals(pk) &&pk.toLowerCase().equals(map.get("COLUMN_NAME").toString().toLowerCase())){
						xtGeneratorTableColumn.setCOLUMN_KEY("PRI");
					}
					xt_Generator_Table_ColumnList.add(xtGeneratorTableColumn);
				}
			}
			int i = gutil.createCode(xt_Generator_Table_ColumnList, xtGenerator);
			xtGenerator.setXt_userinfo_id(getXtUid());
			xtGenerator.setXt_generator_time(getSimpleDateFormat());
			if(i > 0){
				xtGenerator.setXt_generator_state("1");
				result = 1;
			}else{
				xtGenerator.setXt_generator_state("2");
				result = 0;
			}
			xtGenerator.setXt_generator_tbname(table_name);
			xtGenerator.setXt_generator_tbcomment(comment);
			result = xtGeneratorDao.addXtGenerator(xtGenerator);
		} catch (Exception e) {
			result = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return result;
	}
	
	/**
	 * 删除
	 * @param condition
	 */
	public int delXtGenerator(Map<String,Object> condition){
		int result = 0;
		try {
			result = xtGeneratorDao.delXtGenerator(condition);
		} catch (Exception e) {
			result = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return result;
	}
}
