package jehc.xtmodules.xtservice.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jehc.xtmodules.xtcore.base.BaseService;
import jehc.xtmodules.xtcore.util.ExceptionUtil;
import jehc.xtmodules.xtdao.XtURDao;
import jehc.xtmodules.xtmodel.XtUR;
import jehc.xtmodules.xtmodel.XtUserinfo;
import jehc.xtmodules.xtservice.XtURService;

/**
* 用户角色分配
* 2015-08-04 16:25:07  邓纯杰
*/
@Service("xtURService")
public class XtURServiceImpl extends BaseService implements XtURService{
	@Autowired
	private XtURDao xtURDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtUserinfo> getXtURListByCondition(Map<String,Object> condition){
		try{
			return xtURDao.getXtURListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_u_r_id 
	* @return
	*/
	public XtUR getXtURById(String xt_u_r_id){
		try{
			return xtURDao.getXtURById(xt_u_r_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtURList
	* @return
	*/
	public int addXtUR(List<XtUR> xtURList){
		int i = 0;
		try {
			for(int j = 0; j < xtURList.size(); j++){
				xtURDao.addXtUR(xtURList.get(j));
			}
			i = 1;
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtUR
	* @return
	*/
	public int updateXtUR(XtUR xtUR){
		int i = 0;
		try {
			i = xtURDao.updateXtUR(xtUR);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtUR(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtURDao.delXtUR(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 根据用户ID查找角色
	 * @param condition
	 * @return
	 */
	public List<XtUR> getXtURList(Map<String,Object> condition){
		try{
			return xtURDao.getXtURList(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 根据用户编号查找角色权限集合
	 * @param condition
	 * @return
	 */
	public List<XtUR> getXtRoleinfoListByUserinfoId(Map<String,Object> condition){
		try{
			return xtURDao.getXtRoleinfoListByUserinfoId(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
