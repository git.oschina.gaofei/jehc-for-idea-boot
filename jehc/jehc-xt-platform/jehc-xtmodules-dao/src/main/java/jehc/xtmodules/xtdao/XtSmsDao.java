package jehc.xtmodules.xtdao;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtSms;

/**
* 短信配置
* 2015-06-04 13:35:07  邓纯杰
*/
public interface XtSmsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtSms> getXtSmsListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_sms_id 
	* @return
	*/
	public XtSms getXtSmsById(String xt_sms_id);
	/**
	* 添加
	* @param xtSms
	* @return
	*/
	public int addXtSms(XtSms xtSms);
	/**
	* 修改
	* @param xtSms
	* @return
	*/
	public int updateXtSms(XtSms xtSms);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtSms(Map<String,Object> condition);
}
