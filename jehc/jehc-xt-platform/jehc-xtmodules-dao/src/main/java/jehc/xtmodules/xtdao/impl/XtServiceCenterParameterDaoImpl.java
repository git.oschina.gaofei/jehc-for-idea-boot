package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtServiceCenterParameterDao;
import jehc.xtmodules.xtmodel.XtServiceCenterParameter;

/**
* 服务中心参数 
* 2017-03-27 12:32:04  邓纯杰
*/
@Repository("xtServiceCenterParameterDao")
public class XtServiceCenterParameterDaoImpl  extends BaseDaoImpl implements XtServiceCenterParameterDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtServiceCenterParameter> getXtServiceCenterParameterListByCondition(Map<String,Object> condition){
		return (List<XtServiceCenterParameter>)this.getList("getXtServiceCenterParameterListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_service_center_parameter_id 
	* @return
	*/
	public XtServiceCenterParameter getXtServiceCenterParameterById(String xt_service_center_parameter_id){
		return (XtServiceCenterParameter)this.get("getXtServiceCenterParameterById", xt_service_center_parameter_id);
	}
	/**
	* 添加
	* @param xtServiceCenterParameter
	* @return
	*/
	public int addXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter){
		return this.add("addXtServiceCenterParameter", xtServiceCenterParameter);
	}
	/**
	* 修改
	* @param xtServiceCenterParameter
	* @return
	*/
	public int updateXtServiceCenterParameter(XtServiceCenterParameter xtServiceCenterParameter){
		return this.update("updateXtServiceCenterParameter", xtServiceCenterParameter);
	}
	/**
	 * 修改（根据动态条件）
	 * @param xtServiceCenterParameter
	 * @return
	 */
	public int updateXtServiceCenterParameterBySelective(XtServiceCenterParameter xtServiceCenterParameter){
		return this.update("updateXtServiceCenterParameterBySelective", xtServiceCenterParameter);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtServiceCenterParameter(Map<String,Object> condition){
		return this.del("delXtServiceCenterParameter", condition);
	}
	/**
	* 批量添加
	* @param xtServiceCenterParameterList
	* @return
	*/
	public int addBatchXtServiceCenterParameter(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		return this.add("addBatchXtServiceCenterParameter", xtServiceCenterParameterList);
	}
	/**
	* 批量修改
	* @param xtServiceCenterParameterList
	* @return
	*/
	public int updateBatchXtServiceCenterParameter(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		return this.update("updateBatchXtServiceCenterParameter", xtServiceCenterParameterList);
	}
	/**
	 * 批量修改（根据动态条件）
	 * @param xtServiceCenterParameterList
	 * @return
	 */
	public int updateBatchXtServiceCenterParameterBySelective(List<XtServiceCenterParameter> xtServiceCenterParameterList){
		return this.update("updateBatchXtServiceCenterParameterBySelective", xtServiceCenterParameterList);
	}
	/**
	* 根据外键删除方法
	* @param xt_service_center_id
	* @return
	*/
	public int delXtServiceCenterParameterByForeignKey(String xt_service_center_id){
		return this.del("delXtServiceCenterParameterByForeignKey", xt_service_center_id);
	}
}
