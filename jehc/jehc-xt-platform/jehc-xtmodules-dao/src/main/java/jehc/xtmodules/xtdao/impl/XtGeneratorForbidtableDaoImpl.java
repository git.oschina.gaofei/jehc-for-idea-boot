package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtGeneratorForbidtableDao;
import jehc.xtmodules.xtmodel.XtGeneratorForbidtable;

/**
* 禁止使用代码生成器生成的表信息 
* 2016-09-26 10:55:48  邓纯杰
*/
@Repository("xtGeneratorForbidtableDao")
public class XtGeneratorForbidtableDaoImpl  extends BaseDaoImpl implements XtGeneratorForbidtableDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtGeneratorForbidtable> getXtGeneratorForbidtableListByCondition(Map<String,Object> condition){
		return (List<XtGeneratorForbidtable>)this.getList("getXtGeneratorForbidtableListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_generator_forbidtable_id 
	* @return
	*/
	public XtGeneratorForbidtable getXtGeneratorForbidtableById(String xt_generator_forbidtable_id){
		return (XtGeneratorForbidtable)this.get("getXtGeneratorForbidtableById", xt_generator_forbidtable_id);
	}
	/**
	* 添加
	* @param xtGeneratorForbidtable
	* @return
	*/
	public int addXtGeneratorForbidtable(XtGeneratorForbidtable xtGeneratorForbidtable){
		return this.add("addXtGeneratorForbidtable", xtGeneratorForbidtable);
	}
	/**
	* 修改
	* @param xtGeneratorForbidtable
	* @return
	*/
	public int updateXtGeneratorForbidtable(XtGeneratorForbidtable xtGeneratorForbidtable){
		return this.update("updateXtGeneratorForbidtable", xtGeneratorForbidtable);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtGeneratorForbidtable(Map<String,Object> condition){
		return this.del("delXtGeneratorForbidtable", condition);
	}
	/**
	* 批量添加
	* @param xtGeneratorForbidtableList
	* @return
	*/
	public int addBatchXtGeneratorForbidtable(List<XtGeneratorForbidtable> xtGeneratorForbidtableList){
		return this.add("addBatchXtGeneratorForbidtable", xtGeneratorForbidtableList);
	}
	/**
	* 批量修改
	* @param xtGeneratorForbidtableList
	* @return
	*/
	public int updateBatchXtGeneratorForbidtable(List<XtGeneratorForbidtable> xtGeneratorForbidtableList){
		return this.update("updateBatchXtGeneratorForbidtable", xtGeneratorForbidtableList);
	}
}
