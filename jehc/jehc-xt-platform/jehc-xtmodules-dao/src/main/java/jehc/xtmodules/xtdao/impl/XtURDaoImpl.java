package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtURDao;
import jehc.xtmodules.xtmodel.XtUR;
import jehc.xtmodules.xtmodel.XtUserinfo;

/**
* 用户角色分配
* 2015-08-04 16:25:07  邓纯杰
*/
@Repository("xtURDao")
public class XtURDaoImpl  extends BaseDaoImpl implements XtURDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtUserinfo> getXtURListByCondition(Map<String,Object> condition){
		return (List<XtUserinfo>)this.getList("getXtURListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_u_r_id 
	* @return
	*/
	public XtUR getXtURById(String xt_u_r_id){
		return (XtUR)this.get("getXtURById", xt_u_r_id);
	}
	/**
	* 添加
	* @param xtUR
	* @return
	*/
	public int addXtUR(XtUR xtUR){
		return this.add("addXtUR", xtUR);
	}
	/**
	* 修改
	* @param xtUR
	* @return
	*/
	public int updateXtUR(XtUR xtUR){
		return this.update("updateXtUR", xtUR);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtUR(Map<String,Object> condition){
		return this.del("delXtUR", condition);
	}
	
	/**
	 * 根据用户ID查找角色
	 * @param condition
	 * @return
	 */
	public List<XtUR> getXtURList(Map<String,Object> condition){
		return (List<XtUR>)this.getList("getXtURList",condition);
	}
	
	/**
	 * 根据用户编号查找角色权限集合
	 * @param condition
	 * @return
	 */
	public List<XtUR> getXtRoleinfoListByUserinfoId(Map<String,Object> condition){
		return (List<XtUR>)this.getList("getXtRoleinfoListByUserinfoId",condition);
	}
}
