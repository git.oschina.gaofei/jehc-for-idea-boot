package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtCompanyDao;
import jehc.xtmodules.xtmodel.XtCompany;

/**
* 公司信息
* 2015-05-12 22:59:42  邓纯杰
*/
@Repository("xtCompanyDao")
public class XtCompanyDaoImpl  extends BaseDaoImpl implements XtCompanyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtCompany> getXtCompanyListByCondition(Map<String,Object> condition){
		return (List<XtCompany>)this.getList("getXtCompanyListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_company_id 
	* @return
	*/
	public XtCompany getXtCompanyById(String xt_company_id){
		return (XtCompany)this.get("getXtCompanyById", xt_company_id);
	}
	/**
	* 添加
	* @param xtCompany
	* @return
	*/
	public int addXtCompany(XtCompany xtCompany){
		return this.add("addXtCompany", xtCompany);
	}
	/**
	* 修改
	* @param xtCompany
	* @return
	*/
	public int updateXtCompany(XtCompany xtCompany){
		return this.update("updateXtCompany", xtCompany);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtCompany(Map<String,Object> condition){
		return this.del("delXtCompany", condition);
	}
}
