package jehc.xtmodules.xtdao;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtMR;

/**
 * 资源角色
 * 2015-08-04 16:27:46
 * Author 邓纯杰
 */
public interface XtMRDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtMR> getXtMRListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_m_r_id 
	* @return
	*/
	public XtMR getXtMRById(String xt_m_r_id);
	/**
	* 添加
	* @param xtMR
	* @return
	*/
	public int addXtMR(XtMR xtMR);
	/**
	* 修改
	* @param xtMR
	* @return
	*/
	public int updateXtMR(XtMR xtMR);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtMR(Map<String,Object> condition);
	
	/**
	 * 批量添加
	 * @param xtMRList
	 * @return
	 */
	public int addBatchXtMR(List<XtMR> xtMRList);
}
