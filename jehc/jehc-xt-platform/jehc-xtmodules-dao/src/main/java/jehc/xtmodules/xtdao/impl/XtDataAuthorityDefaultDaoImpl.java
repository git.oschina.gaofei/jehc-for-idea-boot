package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtDataAuthorityDefaultDao;
import jehc.xtmodules.xtmodel.XtDataAuthorityDefault;

/**
* 数权限初始化默认设置 
* 2017-06-20 14:38:52  邓纯杰
*/
@Repository("xtDataAuthorityDefaultDao")
public class XtDataAuthorityDefaultDaoImpl  extends BaseDaoImpl implements XtDataAuthorityDefaultDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDataAuthorityDefault> getXtDataAuthorityDefaultListByCondition(Map<String,Object> condition){
		return (List<XtDataAuthorityDefault>)this.getList("getXtDataAuthorityDefaultListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_data_authority_default_id 
	* @return
	*/
	public XtDataAuthorityDefault getXtDataAuthorityDefaultById(String xt_data_authority_default_id){
		return (XtDataAuthorityDefault)this.get("getXtDataAuthorityDefaultById", xt_data_authority_default_id);
	}
	/**
	* 添加
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int addXtDataAuthorityDefault(XtDataAuthorityDefault xtDataAuthorityDefault){
		return this.add("addXtDataAuthorityDefault", xtDataAuthorityDefault);
	}
	/**
	* 修改
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int updateXtDataAuthorityDefault(XtDataAuthorityDefault xtDataAuthorityDefault){
		return this.update("updateXtDataAuthorityDefault", xtDataAuthorityDefault);
	}
	/**
	* 修改（根据动态条件）
	* @param xtDataAuthorityDefault
	* @return
	*/
	public int updateXtDataAuthorityDefaultBySelective(XtDataAuthorityDefault xtDataAuthorityDefault){
		return this.update("updateXtDataAuthorityDefaultBySelective", xtDataAuthorityDefault);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDataAuthorityDefault(Map<String,Object> condition){
		return this.del("delXtDataAuthorityDefault", condition);
	}
	/**
	 * 根据情况删除
	 * @param condition
	 * @return
	 */
	public int delXtDataAuthorityDefaultAllByCondition(Map<String,Object> condition){
		return this.del("delXtDataAuthorityDefaultAllByCondition", condition);
	}
	/**
	* 批量添加
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int addBatchXtDataAuthorityDefault(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList){
		return this.add("addBatchXtDataAuthorityDefault", xtDataAuthorityDefaultList);
	}
	/**
	* 批量修改
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int updateBatchXtDataAuthorityDefault(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList){
		return this.update("updateBatchXtDataAuthorityDefault", xtDataAuthorityDefaultList);
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtDataAuthorityDefaultList
	* @return
	*/
	public int updateBatchXtDataAuthorityDefaultBySelective(List<XtDataAuthorityDefault> xtDataAuthorityDefaultList){
		return this.update("updateBatchXtDataAuthorityDefaultBySelective", xtDataAuthorityDefaultList);
	}
}
