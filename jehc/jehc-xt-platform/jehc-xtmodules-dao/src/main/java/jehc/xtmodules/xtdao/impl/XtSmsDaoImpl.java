package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtSmsDao;
import jehc.xtmodules.xtmodel.XtSms;

/**
* 短信配置
* 2015-06-04 13:35:07  邓纯杰
*/
@Repository("xtSmsDao")
public class XtSmsDaoImpl  extends BaseDaoImpl implements XtSmsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtSms> getXtSmsListByCondition(Map<String,Object> condition){
		return (List<XtSms>)this.getList("getXtSmsListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_sms_id 
	* @return
	*/
	public XtSms getXtSmsById(String xt_sms_id){
		return (XtSms)this.get("getXtSmsById", xt_sms_id);
	}
	/**
	* 添加
	* @param xtSms
	* @return
	*/
	public int addXtSms(XtSms xtSms){
		return this.add("addXtSms", xtSms);
	}
	/**
	* 修改
	* @param xtSms
	* @return
	*/
	public int updateXtSms(XtSms xtSms){
		return this.update("updateXtSms", xtSms);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtSms(Map<String,Object> condition){
		return this.del("delXtSms", condition);
	}
}
