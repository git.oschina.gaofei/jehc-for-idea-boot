package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtKwordsDao;
import jehc.xtmodules.xtmodel.XtKwords;

/**
* 关键词（敏感词） 
* 2016-10-08 15:03:41  邓纯杰
*/
@Repository("xtKwordsDao")
public class XtKwordsDaoImpl  extends BaseDaoImpl implements XtKwordsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtKwords> getXtKwordsListByCondition(Map<String,Object> condition){
		return (List<XtKwords>)this.getList("getXtKwordsListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_kwords_id 
	* @return
	*/
	public XtKwords getXtKwordsById(String xt_kwords_id){
		return (XtKwords)this.get("getXtKwordsById", xt_kwords_id);
	}
	/**
	* 添加
	* @param xtKwords
	* @return
	*/
	public int addXtKwords(XtKwords xtKwords){
		return this.add("addXtKwords", xtKwords);
	}
	/**
	* 修改
	* @param xtKwords
	* @return
	*/
	public int updateXtKwords(XtKwords xtKwords){
		return this.update("updateXtKwords", xtKwords);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtKwords(Map<String,Object> condition){
		return this.del("delXtKwords", condition);
	}
	/**
	* 批量添加
	* @param xtKworxtKwordsListds
	* @return
	*/
	public int addBatchXtKwords(List<XtKwords> xtKwordsList){
		return this.add("addBatchXtKwords", xtKwordsList);
	}
	/**
	* 批量修改
	* @param xtKwordsList
	* @return
	*/
	public int updateBatchXtKwords(List<XtKwords> xtKwordsList){
		return this.update("updateBatchXtKwords", xtKwordsList);
	}
}
