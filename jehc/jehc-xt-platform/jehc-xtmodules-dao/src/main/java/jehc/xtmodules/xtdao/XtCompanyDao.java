package jehc.xtmodules.xtdao;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtCompany;

/**
* 公司信息
* 2015-05-12 22:59:42  邓纯杰
*/
public interface XtCompanyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtCompany> getXtCompanyListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_company_id 
	* @return
	*/
	public XtCompany getXtCompanyById(String xt_company_id);
	/**
	* 添加
	* @param xtCompany
	* @return
	*/
	public int addXtCompany(XtCompany xtCompany);
	/**
	* 修改
	* @param xtCompany
	* @return
	*/
	public int updateXtCompany(XtCompany xtCompany);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtCompany(Map<String,Object> condition);
}
