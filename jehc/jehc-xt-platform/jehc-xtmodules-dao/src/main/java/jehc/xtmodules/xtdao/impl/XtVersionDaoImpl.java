package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtVersionDao;
import jehc.xtmodules.xtmodel.XtVersion;

/**
* 平台版本 
* 2017-04-16 20:05:24  邓纯杰
*/
@Repository("xtVersionDao")
public class XtVersionDaoImpl  extends BaseDaoImpl implements XtVersionDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtVersion> getXtVersionListByCondition(Map<String,Object> condition){
		return (List<XtVersion>)this.getList("getXtVersionListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_version_id 
	* @return
	*/
	public XtVersion getXtVersionById(String xt_version_id){
		return (XtVersion)this.get("getXtVersionById", xt_version_id);
	}
	/**
	* 添加
	* @param xtVersion
	* @return
	*/
	public int addXtVersion(XtVersion xtVersion){
		return this.add("addXtVersion", xtVersion);
	}
	/**
	* 修改
	* @param xtVersion
	* @return
	*/
	public int updateXtVersion(XtVersion xtVersion){
		return this.update("updateXtVersion", xtVersion);
	}
	/**
	* 修改（根据动态条件）
	* @param xtVersion
	* @return
	*/
	public int updateXtVersionBySelective(XtVersion xtVersion){
		return this.update("updateXtVersionBySelective", xtVersion);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtVersion(Map<String,Object> condition){
		return this.del("delXtVersion", condition);
	}
	/**
	* 批量添加
	* @param xtVersionList
	* @return
	*/
	public int addBatchXtVersion(List<XtVersion> xtVersionList){
		return this.add("addBatchXtVersion", xtVersionList);
	}
	/**
	* 批量修改
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersion(List<XtVersion> xtVersionList){
		return this.update("updateBatchXtVersion", xtVersionList);
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersionBySelective(List<XtVersion> xtVersionList){
		return this.update("updateBatchXtVersionBySelective", xtVersionList);
	}
}
