package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtDataAuthorityPostDao;
import jehc.xtmodules.xtmodel.XtDataAuthorityPost;

/**
* 数据权限按岗位设置 
* 2017-06-20 14:37:16  邓纯杰
*/
@Repository("xtDataAuthorityPostDao")
public class XtDataAuthorityPostDaoImpl  extends BaseDaoImpl implements XtDataAuthorityPostDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDataAuthorityPost> getXtDataAuthorityPostListByCondition(Map<String,Object> condition){
		return (List<XtDataAuthorityPost>)this.getList("getXtDataAuthorityPostListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_data_authority_post_id 
	* @return
	*/
	public XtDataAuthorityPost getXtDataAuthorityPostById(String xt_data_authority_post_id){
		return (XtDataAuthorityPost)this.get("getXtDataAuthorityPostById", xt_data_authority_post_id);
	}
	/**
	* 添加
	* @param xtDataAuthorityPost
	* @return
	*/
	public int addXtDataAuthorityPost(XtDataAuthorityPost xtDataAuthorityPost){
		return this.add("addXtDataAuthorityPost", xtDataAuthorityPost);
	}
	/**
	* 修改
	* @param xtDataAuthorityPost
	* @return
	*/
	public int updateXtDataAuthorityPost(XtDataAuthorityPost xtDataAuthorityPost){
		return this.update("updateXtDataAuthorityPost", xtDataAuthorityPost);
	}
	/**
	* 修改（根据动态条件）
	* @param xtDataAuthorityPost
	* @return
	*/
	public int updateXtDataAuthorityPostBySelective(XtDataAuthorityPost xtDataAuthorityPost){
		return this.update("updateXtDataAuthorityPostBySelective", xtDataAuthorityPost);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDataAuthorityPost(Map<String,Object> condition){
		return this.del("delXtDataAuthorityPost", condition);
	}
	/**
	 * 删除集合根据拥有者及菜单编号
	 * @param condition
	 * @return
	 */
	public int delXtDataAuthorityPostList(Map<String,Object> condition){
		return this.del("delXtDataAuthorityPostList", condition); 
	}
	/**
	* 批量添加
	* @param xtDataAuthorityPostList
	* @return
	*/
	public int addBatchXtDataAuthorityPost(List<XtDataAuthorityPost> xtDataAuthorityPostList){
		return this.add("addBatchXtDataAuthorityPost", xtDataAuthorityPostList);
	}
	/**
	* 批量修改
	* @param xtDataAuthorityPostList
	* @return
	*/
	public int updateBatchXtDataAuthorityPost(List<XtDataAuthorityPost> xtDataAuthorityPostList){
		return this.update("updateBatchXtDataAuthorityPost", xtDataAuthorityPostList);
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtDataAuthorityPostList
	* @return
	*/
	public int updateBatchXtDataAuthorityPostBySelective(List<XtDataAuthorityPost> xtDataAuthorityPostList){
		return this.update("updateBatchXtDataAuthorityPostBySelective", xtDataAuthorityPostList);
	}
}
