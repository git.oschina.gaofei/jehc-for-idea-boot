package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtMRDao;
import jehc.xtmodules.xtmodel.XtMR;

/**
* 资源角色
* 2015-08-04 16:27:46
* Author 邓纯杰
*/
@Repository("xtMRDao")
public class XtMRDaoImpl  extends BaseDaoImpl implements XtMRDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtMR> getXtMRListByCondition(Map<String,Object> condition){
		return (List<XtMR>)this.getList("getXtMRListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_m_r_id 
	* @return
	*/
	public XtMR getXtMRById(String xt_m_r_id){
		return (XtMR)this.get("getXtMRById", xt_m_r_id);
	}
	/**
	* 添加
	* @param xtMR
	* @return
	*/
	public int addXtMR(XtMR xtMR){
		return this.add("addXtMR", xtMR);
	}
	/**
	* 修改
	* @param xtMR
	* @return
	*/
	public int updateXtMR(XtMR xtMR){
		return this.update("updateXtMR", xtMR);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtMR(Map<String,Object> condition){
		return this.del("delXtMR", condition);
	}
	/**
	 * 批量添加
	 * @param xtMRList
	 * @return
	 */
	public int addBatchXtMR(List<XtMR> xtMRList){
		return this.add("addBatchXtMR", xtMRList);
	}
}
