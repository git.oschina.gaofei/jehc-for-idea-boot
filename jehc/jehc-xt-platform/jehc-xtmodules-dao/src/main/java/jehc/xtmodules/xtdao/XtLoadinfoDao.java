package jehc.xtmodules.xtdao;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtLoadinfo;

/**
* 页面加载信息 
* 2015-05-13 21:20:57  邓纯杰
*/
public interface XtLoadinfoDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtLoadinfo> getXtLoadinfoListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_loadinfo_id 
	* @return
	*/
	public XtLoadinfo getXtLoadinfoById(String xt_loadinfo_id);
	/**
	* 添加
	* @param xtLoadinfo
	* @return
	*/
	public int addXtLoadinfo(XtLoadinfo xtLoadinfo);
	/**
	* 修改
	* @param xtLoadinfo
	* @return
	*/
	public int updateXtLoadinfo(XtLoadinfo xtLoadinfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtLoadinfo(Map<String,Object> condition);
	
	/**
	 * 分组统计并平均值算法
	 * @return
	 */
	public List<XtLoadinfo> getXtLoadingGroupList();
}
