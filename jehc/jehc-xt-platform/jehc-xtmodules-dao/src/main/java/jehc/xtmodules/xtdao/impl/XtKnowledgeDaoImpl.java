package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtKnowledgeDao;
import jehc.xtmodules.xtmodel.XtKnowledge;

/**
* 平台知识内容 
* 2015-06-07 20:09:38  邓纯杰
*/
@Repository("xtKnowledgeDao")
public class XtKnowledgeDaoImpl  extends BaseDaoImpl implements XtKnowledgeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtKnowledge> getXtKnowledgeListByCondition(Map<String,Object> condition){
		return (List<XtKnowledge>)this.getList("getXtKnowledgeListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_knowledge_id 
	* @return
	*/
	public XtKnowledge getXtKnowledgeById(String xt_knowledge_id){
		return (XtKnowledge)this.get("getXtKnowledgeById", xt_knowledge_id);
	}
	/**
	* 添加
	* @param xtKnowledge
	* @return
	*/
	public int addXtKnowledge(XtKnowledge xtKnowledge){
		return this.add("addXtKnowledge", xtKnowledge);
	}
	/**
	* 修改
	* @param xtKnowledge
	* @return
	*/
	public int updateXtKnowledge(XtKnowledge xtKnowledge){
		return this.update("updateXtKnowledge", xtKnowledge);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtKnowledge(Map<String,Object> condition){
		return this.del("delXtKnowledge", condition);
	}
	/**
	 * 统计知识点数
	 * @param condition
	 * @return
	 */
	public int getXtKnowledgeCount(Map<String,Object> condition){
		return new Integer(this.get("getXtKnowledgeCount", condition).toString()); 
	}
}
