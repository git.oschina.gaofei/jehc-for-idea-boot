package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtServiceCenterDao;
import jehc.xtmodules.xtmodel.XtServiceCenter;

/**
* 服务中心 
* 2017-03-27 12:32:04  邓纯杰
*/
@Repository("xtServiceCenterDao")
public class XtServiceCenterDaoImpl  extends BaseDaoImpl implements XtServiceCenterDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtServiceCenter> getXtServiceCenterListByCondition(Map<String,Object> condition){
		return (List<XtServiceCenter>)this.getList("getXtServiceCenterListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_service_center_id 
	* @return
	*/
	public XtServiceCenter getXtServiceCenterById(String xt_service_center_id){
		return (XtServiceCenter)this.get("getXtServiceCenterById", xt_service_center_id);
	}
	/**
	* 添加
	* @param xtServiceCenter
	* @return
	*/
	public int addXtServiceCenter(XtServiceCenter xtServiceCenter){
		return this.add("addXtServiceCenter", xtServiceCenter);
	}
	/**
	* 修改
	* @param xtServiceCenter
	* @return
	*/
	public int updateXtServiceCenter(XtServiceCenter xtServiceCenter){
		return this.update("updateXtServiceCenter", xtServiceCenter);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtServiceCenter(Map<String,Object> condition){
		return this.del("delXtServiceCenter", condition);
	}
	/**
	* 批量添加
	* @param xtServiceCenterList
	* @return
	*/
	public int addBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList){
		return this.add("addBatchXtServiceCenter", xtServiceCenterList);
	}
	/**
	* 批量修改
	* @param xtServiceCenterList
	* @return
	*/
	public int updateBatchXtServiceCenter(List<XtServiceCenter> xtServiceCenterList){
		return this.update("updateBatchXtServiceCenter", xtServiceCenterList);
	}
}
