package jehc.xtmodules.xtdao;
import java.util.List;
import java.util.Map;

import jehc.xtmodules.xtmodel.XtVersion;

/**
* 平台版本 
* 2017-04-16 20:05:24  邓纯杰
*/
public interface XtVersionDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtVersion> getXtVersionListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_version_id 
	* @return
	*/
	public XtVersion getXtVersionById(String xt_version_id);
	/**
	* 添加
	* @param xtVersion
	* @return
	*/
	public int addXtVersion(XtVersion xtVersion);
	/**
	* 修改
	* @param xtVersion
	* @return
	*/
	public int updateXtVersion(XtVersion xtVersion);
	/**
	* 修改（根据动态条件）
	* @param xtVersion
	* @return
	*/
	public int updateXtVersionBySelective(XtVersion xtVersion);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtVersion(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtVersionList
	* @return
	*/
	public int addBatchXtVersion(List<XtVersion> xtVersionList);
	/**
	* 批量修改
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersion(List<XtVersion> xtVersionList);
	/**
	* 批量修改（根据动态条件）
	* @param xtVersionList
	* @return
	*/
	public int updateBatchXtVersionBySelective(List<XtVersion> xtVersionList);
}
