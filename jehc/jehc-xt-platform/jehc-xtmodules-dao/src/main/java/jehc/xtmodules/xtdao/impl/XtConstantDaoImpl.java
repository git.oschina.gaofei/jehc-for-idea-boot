package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtConstantDao;
import jehc.xtmodules.xtmodel.XtConstant;

/**
* 台平常量 
* 2015-05-24 08:47:31  邓纯杰
*/
@Repository("xtConstantDao")
public class XtConstantDaoImpl  extends BaseDaoImpl implements XtConstantDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtConstant> getXtConstantListByCondition(Map<String,Object> condition){
		return (List<XtConstant>)this.getList("getXtConstantListByCondition",condition);
	}
	/**
	* 查询对象
	* @param xt_constant_id 
	* @return
	*/
	public XtConstant getXtConstantById(String xt_constant_id){
		return (XtConstant)this.get("getXtConstantById", xt_constant_id);
	}
	/**
	* 添加
	* @param xtConstant
	* @return
	*/
	public int addXtConstant(XtConstant xtConstant){
		return this.add("addXtConstant", xtConstant);
	}
	/**
	* 修改
	* @param xtConstant
	* @return
	*/
	public int updateXtConstant(XtConstant xtConstant){
		return this.update("updateXtConstant", xtConstant);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtConstant(Map<String,Object> condition){
		return this.del("delXtConstant", condition);
	}
	
	/**
	 * 读取所有常量
	 * @param condition
	 * @return
	 */
	public List<XtConstant> getXtConstantListAllByCondition(Map<String,Object> condition){
		return (List<XtConstant>)this.getList("getXtConstantListAllByCondition",condition);
	}
}
