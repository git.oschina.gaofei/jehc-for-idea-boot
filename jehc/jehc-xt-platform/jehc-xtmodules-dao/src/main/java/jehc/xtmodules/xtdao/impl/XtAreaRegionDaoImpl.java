package jehc.xtmodules.xtdao.impl;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import jehc.xtmodules.xtcore.base.impl.BaseDaoImpl;
import jehc.xtmodules.xtdao.XtAreaRegionDao;
import jehc.xtmodules.xtmodel.XtAreaRegion;

/**
* 行政区划
* 2017-05-04 14:54:34  邓纯杰
*/
@Repository("xtAreaRegionDao")
public class XtAreaRegionDaoImpl  extends BaseDaoImpl implements XtAreaRegionDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition){
		return (List<XtAreaRegion>)this.getList("getXtAreaRegionListByCondition",condition);
	}
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	public XtAreaRegion getXtAreaRegionById(String ID){
		return (XtAreaRegion)this.get("getXtAreaRegionById", ID);
	}
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	public int addXtAreaRegion(XtAreaRegion xtAreaRegion){
		return this.add("addXtAreaRegion", xtAreaRegion);
	}
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegion(XtAreaRegion xtAreaRegion){
		return this.update("updateXtAreaRegion", xtAreaRegion);
	}
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion){
		return this.update("updateXtAreaRegionBySelective", xtAreaRegion);
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtAreaRegion(Map<String,Object> condition){
		return this.del("delXtAreaRegion", condition);
	}
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	public int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		return this.add("addBatchXtAreaRegion", xtAreaRegionList);
	}
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		return this.update("updateBatchXtAreaRegion", xtAreaRegionList);
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList){
		return this.update("updateBatchXtAreaRegionBySelective", xtAreaRegionList);
	}
}
