package jehc.oamodules.oaweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.oamodules.oamodel.OaMplanClassify;
import jehc.oamodules.oaservice.OaMplanClassifyService;

/**
* 个人计划分类 
* 2018-07-09 20:18:35  邓纯杰
*/
@Api(value = "个人计划分类", description = "个人计划分类")
@Controller
@RequestMapping("/oaMplanClassifyController")
public class OaMplanClassifyController extends BaseAction{
	@Autowired
	private OaMplanClassifyService oaMplanClassifyService;
	/**
	* 列表页面
	* @param oaMplanClassify
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadOaMplanClassify(OaMplanClassify oaMplanClassify,HttpServletRequest request){
		return new ModelAndView("pc/oa-view/oa-mplan-classify/oa-mplan-classify-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getOaMplanClassifyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getOaMplanClassifyListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<OaMplanClassify> oaMplanClassifyList = oaMplanClassifyService.getOaMplanClassifyListByCondition(condition);
		PageInfo<OaMplanClassify> page = new PageInfo<OaMplanClassify>(oaMplanClassifyList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param oa_mplan_classify_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getOaMplanClassifyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getOaMplanClassifyById(String oa_mplan_classify_id,HttpServletRequest request){
		OaMplanClassify oaMplanClassify = oaMplanClassifyService.getOaMplanClassifyById(oa_mplan_classify_id);
		return outDataStr(oaMplanClassify);
	}
	/**
	* 添加
	* @param oaMplanClassify
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String addOaMplanClassify(OaMplanClassify oaMplanClassify,HttpServletRequest request){
		int i = 0;
		if(null != oaMplanClassify){
			oaMplanClassify.setCtime(getDate());
			oaMplanClassify.setXt_userinfo_id(getXtUid());
			oaMplanClassify.setOa_mplan_classify_id(UUID.toUUID());
			i=oaMplanClassifyService.addOaMplanClassify(oaMplanClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oaMplanClassify
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String updateOaMplanClassify(OaMplanClassify oaMplanClassify,HttpServletRequest request){
		int i = 0;
		if(null != oaMplanClassify){
			oaMplanClassify.setMtime(getDate());
			i=oaMplanClassifyService.updateOaMplanClassify(oaMplanClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param oa_mplan_classify_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String delOaMplanClassify(String oa_mplan_classify_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(oa_mplan_classify_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("oa_mplan_classify_id",oa_mplan_classify_id.split(","));
			i=oaMplanClassifyService.delOaMplanClassify(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param oa_mplan_classify_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String copyOaMplanClassify(String oa_mplan_classify_id,HttpServletRequest request){
		int i = 0;
		OaMplanClassify oaMplanClassify = oaMplanClassifyService.getOaMplanClassifyById(oa_mplan_classify_id);
		if(null != oaMplanClassify){
			oaMplanClassify.setOa_mplan_classify_id(UUID.toUUID());
			i=oaMplanClassifyService.addOaMplanClassify(oaMplanClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportOaMplanClassify",method={RequestMethod.POST,RequestMethod.GET})
	public void exportOaMplanClassify(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toOaMplanClassifyAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaMplanClassifyAdd(OaMplanClassify oaMplanClassify,HttpServletRequest request){
		return new ModelAndView("pc/oa-view/oa-mplan-classify/oa-mplan-classify-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toOaMplanClassifyUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaMplanClassifyUpdate(String oa_mplan_classify_id,HttpServletRequest request, Model model){
		OaMplanClassify oaMplanClassify = oaMplanClassifyService.getOaMplanClassifyById(oa_mplan_classify_id);
		model.addAttribute("oaMplanClassify", oaMplanClassify);
		return new ModelAndView("pc/oa-view/oa-mplan-classify/oa-mplan-classify-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toOaMplanClassifyDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaMplanClassifyDetail(String oa_mplan_classify_id,HttpServletRequest request, Model model){
		OaMplanClassify oaMplanClassify = oaMplanClassifyService.getOaMplanClassifyById(oa_mplan_classify_id);
		model.addAttribute("oaMplanClassify", oaMplanClassify);
		return new ModelAndView("pc/oa-view/oa-mplan-classify/oa-mplan-classify-detail");
	}
}
