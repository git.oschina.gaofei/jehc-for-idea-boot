package jehc.oamodules.oaweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.oamodules.oamodel.OaNoteClassify;
import jehc.oamodules.oaservice.OaNoteClassifyService;

/**
* 记事本分类 
* 2018-07-04 21:06:37  邓纯杰
*/
@Api(value = "记事本分类", description = "记事本分类")
@Controller
@RequestMapping("/oaNoteClassifyController")
public class OaNoteClassifyController extends BaseAction{
	@Autowired
	private OaNoteClassifyService oaNoteClassifyService;
	/**
	* 列表页面
	* @param oaNoteClassify
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadOaNoteClassify(OaNoteClassify oaNoteClassify,HttpServletRequest request){
		return new ModelAndView("pc/oa-view/oa-note-classify/oa-note-classify-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getOaNoteClassifyListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getOaNoteClassifyListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<OaNoteClassify> oaNoteClassifyList = oaNoteClassifyService.getOaNoteClassifyListByCondition(condition);
		PageInfo<OaNoteClassify> page = new PageInfo<OaNoteClassify>(oaNoteClassifyList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param oa_note_classify_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getOaNoteClassifyById",method={RequestMethod.POST,RequestMethod.GET})
	public String getOaNoteClassifyById(String oa_note_classify_id,HttpServletRequest request){
		OaNoteClassify oaNoteClassify = oaNoteClassifyService.getOaNoteClassifyById(oa_note_classify_id);
		return outDataStr(oaNoteClassify);
	}
	/**
	* 添加
	* @param oaNoteClassify
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String addOaNoteClassify(OaNoteClassify oaNoteClassify,HttpServletRequest request){
		int i = 0;
		if(null != oaNoteClassify && !"".equals(oaNoteClassify)){
			oaNoteClassify.setOa_note_classify_id(UUID.toUUID());
			oaNoteClassify.setCtime(getDate());
			oaNoteClassify.setXt_userinfo_id(getXtUid());
			i=oaNoteClassifyService.addOaNoteClassify(oaNoteClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oaNoteClassify
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String updateOaNoteClassify(OaNoteClassify oaNoteClassify,HttpServletRequest request){
		int i = 0;
		if(null != oaNoteClassify){
			oaNoteClassify.setMtime(getDate());
			i=oaNoteClassifyService.updateOaNoteClassify(oaNoteClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param oa_note_classify_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String delOaNoteClassify(String oa_note_classify_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(oa_note_classify_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("oa_note_classify_id",oa_note_classify_id.split(","));
			i=oaNoteClassifyService.delOaNoteClassify(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param oa_note_classify_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public String copyOaNoteClassify(String oa_note_classify_id,HttpServletRequest request){
		int i = 0;
		OaNoteClassify oaNoteClassify = oaNoteClassifyService.getOaNoteClassifyById(oa_note_classify_id);
		if(null != oaNoteClassify){
			oaNoteClassify.setOa_note_classify_id(UUID.toUUID());
			i=oaNoteClassifyService.addOaNoteClassify(oaNoteClassify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportOaNoteClassify",method={RequestMethod.POST,RequestMethod.GET})
	public void exportOaNoteClassify(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@RequestMapping(value="/toOaNoteClassifyAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaNoteClassifyAdd(OaNoteClassify oaNoteClassify,HttpServletRequest request){
		return new ModelAndView("pc/oa-view/oa-note-classify/oa-note-classify-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toOaNoteClassifyUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaNoteClassifyUpdate(String oa_note_classify_id,HttpServletRequest request, Model model){
		OaNoteClassify oaNoteClassify = oaNoteClassifyService.getOaNoteClassifyById(oa_note_classify_id);
		model.addAttribute("oaNoteClassify", oaNoteClassify);
		return new ModelAndView("pc/oa-view/oa-note-classify/oa-note-classify-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toOaNoteClassifyDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOaNoteClassifyDetail(String oa_note_classify_id,HttpServletRequest request, Model model){
		OaNoteClassify oaNoteClassify = oaNoteClassifyService.getOaNoteClassifyById(oa_note_classify_id);
		model.addAttribute("oaNoteClassify", oaNoteClassify);
		return new ModelAndView("pc/oa-view/oa-note-classify/oa-note-classify-detail");
	}
}
