//基本流程信号设置
//信号属性表格
var signal_grid;
/**
 * 创建信号Grid
 */
function createSignalGrid(){
    signal_grid =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
				"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addSignalRow()\">新一行</button>"+
				"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='signal_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_signal_form\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='signal_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-3\">编号</div>"+
						"<div class=\"col-md-3\">名称</div>"+
						"<div class=\"col-md-3\">范围</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return signal_grid;
}

/**
 * 新一行
 * @param ID
 * @param name
 * @param type
 */
function addSignalRow(ID,name,scope){
    if(ID === undefined){
        ID = "";
    }
    if(name === undefined){
        name = "";
    }
    if(scope === undefined){
        scope = "";
    }

    var uuid = guid();
    validatorDestroy('node_signal_form');

   var scopeOption =  "<option value=''>请选择</option>" +
        "<option value='global'>global</option>" +
        "<option value='processInstance'>processInstance</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_Signal_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='ID"+uuid+"' name='ID' value='"+ID+"' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='name"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"name\" value='"+name+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//范围
			"<div class=\"col-md-3\">"+
				"<select class=\"form-control\" id='scope"+uuid+"' name=\"scope\">" +
        			scopeOption+
				"</select>"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delSignalRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#signal_grid").append(rows);
    $("#scope"+uuid).val(scope);
    reValidator('node_signal_form');
}

/**
 * 删除
 * @param rowID
 */
function delSignalRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_signal_form');
        $("#node_Signal_rows_"+rowID).remove();
        reValidator('node_signal_form');
    });
}

/**
 * 初始化表单及数据
 */
function initSignal_grid(){
    //表单配置
    $('#node_signal_form').bootstrapValidator({
        message:'此值不是有效的'
    });
    initSignal_data();
}

/**
 * 初始化列数据
 */
function initSignal_data(){
    var signal_node_value = $("#signal_node_value").val();
    if(null != signal_node_value && "" != signal_node_value){
        signal_node_value = eval('(' + signal_node_value + ')');
        var IDList = $.makeArray(signal_node_value["ID"]);
        var nameList = $.makeArray(signal_node_value["name"]);
        var scopeList = $.makeArray(signal_node_value["scope"]);
        for(var i = 0; i < nameList.length; i++){
            var name = nameList[i];
            var ID = IDList[i];
            var scope = scopeList[i];
            addSignalRow(ID,name,scope);
        }
    }
}

/**
 * 点击确定按钮设置mxgraph中cell属性
 * @returns {boolean}
 */
function signal_setvalue(){
    var bootform = $('#node_signal_form');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#signal_node_value").val(JSON.stringify($("#node_signal_form").serializeObject()));
    return true;
}