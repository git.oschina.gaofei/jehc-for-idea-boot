//基本流程消息设置
//消息属性表格
var message_grid;
/**
 * 创建消息属性 Grid
 * @param cell
 * @returns {string|*}
 */
function createMessageGrid(){
    message_grid =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
				"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addMessageRow()\">新一行</button>"+
				"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='message_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_message_form\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='message_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
					"<div class=\"col-md-4\">编号</div>"+
					"<div class=\"col-md-4\">名称</div>"+
					"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return message_grid;
}

/**
 * 新一行
 * @param ID
 * @param name
 */
function addMessageRow(ID,name){
    if(ID === undefined){
        ID = "";
    }
    if(name === undefined){
        name = "";
    }
    var uuid = guid();
    validatorDestroy('node_message_form');
    var rows =
        "<div class=\"form-group m-form__group row\" id='node_message_rows_"+uuid+"'>"+
        //编号
        "<div class=\"col-md-4\">"+
        "<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='ID"+uuid+"' name='ID' value='"+ID+"' placeholder='请输入编号'>"+
        "</div>"+


        //名称
        "<div class=\"col-md-4\">"+
        	"<input class=\"form-control\" type=\"text\" id='name"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"name\" value='"+name+"' placeholder=\"请输入名称\">"+
        "</div>"+

        //操作
        "<div class=\"col-md-1\">"+
        	"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delMessageRow(\""+uuid+"\")'>删除</button>"+
        "</div>"+
        "</div>";
    $("#message_grid").append(rows);
    reValidator('node_message_form');
}

/**
 * 删除
 * @param rowID
 */
function delMessageRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_message_form');
        $("#node_message_rows_"+rowID).remove();
        reValidator('node_message_form');
    });
}

/**
 * 初始化表单及数据
 */
function initmessage_grid(){
    //表单配置
    $('#node_message_form').bootstrapValidator({
        message:'此值不是有效的'
    });
    initmessage_data();
}

/**
 * 初始化列数据
 */
function initmessage_data(){
    var message_node_value = $("#message_node_value").val();
    if(null != message_node_value && "" != message_node_value){
        message_node_value = eval('(' + message_node_value + ')');
        var IDList = $.makeArray(message_node_value["ID"]);
        var nameList = $.makeArray(message_node_value["name"]);
        for(var i = 0; i < nameList.length; i++){
            var name = nameList[i];
            var ID = IDList[i];
            addMessageRow(ID,name);
        }
    }
}


/**
 * 点击确定按钮设置mxgraph中cell属性
 * @param cell
 */
function message_setvalue(){
    var bootform = $('#node_message_form');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#message_node_value").val(JSON.stringify($("#node_message_form").serializeObject()));
    return true;
}