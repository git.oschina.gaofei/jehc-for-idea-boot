//创建节点模板 暂时未使用
function insertEdgeTemplate(panel, graph, name, icon, style, width, height, value, parentNode){
		var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), style)];
		cells[0].geometry.setTerminalPoint(new mxPoint(0, height), true);
		cells[0].geometry.setTerminalPoint(new mxPoint(width, 0), false);
		cells[0].edge = true;
		var funct = function(graph, evt, target){
			cells = graph.getImportableCells(cells);
			if(cells.length > 0){
				var validDropTarget = (target != null) ? graph.isValidDropTarget(target, cells, evt) : false;
				var select = null;
				if(target != null && !validDropTarget){
					target = null;
				}
				var pt = graph.getPointForEvent(evt);
				var scale = graph.view.scale;
				pt.x -= graph.snap(width / 2);
				pt.y -= graph.snap(height / 2);
				select = graph.importCells(cells, pt.x, pt.y, target);
				GraphEditor.edgeTemplate = select[0];
				graph.scrollCellToVisible(select[0]);
				graph.setSelectionCells(select);
			}
		};
		var node = panel.addTemplate(name, icon, parentNode, cells);
		var installDrag = function(expandedNode){
			if (node.ui.elNode != null){
				var dragPreview = document.createElement('div');
				dragPreview.style.border = 'dashed black 1px';
				dragPreview.style.width = width+'px';
				dragPreview.style.height = height+'px';
				mxUtils.makeDraggable(node.ui.elNode, graph, funct, dragPreview, -width / 2, -height / 2,graph.autoscroll, true);
			}
		};
		if(!node.parentNode.isExpanded()){
			panel.on('expandnode', installDrag);
		}else{
			installDrag(node.parentNode);
		}
		return node;
};
// 添加元素右上角的删除图标  
function addOverlays(graph, cell, addDeleteIcon){  
    var overlay = new mxCellOverlay(new mxImage('images/add.png', 24, 24), 'Add child');  
    overlay.cursor = 'hand';  
    overlay.align = mxConstants.ALIGN_CENTER;  
    overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
        addChild(graph, cell);  
    }));  
    graph.addCellOverlay(cell, overlay);  
    if (addDeleteIcon){  
        overlay = new mxCellOverlay(new mxImage('images/close.png', 30, 30), 'Delete');  
        overlay.cursor = 'hand';  
        overlay.offset = new mxPoint(-4, 8);  
        overlay.align = mxConstants.ALIGN_RIGHT;  
        overlay.verticalAlign = mxConstants.ALIGN_TOP;  
        overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
            deleteSubtree(graph, cell);  
        }));  
        graph.addCellOverlay(cell, overlay);  
    }  
}; 
// 添加子元素  
function addChild(graph, cell){  
    var model = graph.getModel();  
    var parent = graph.getDefaultParent();  
    var vertex;  
    model.beginUpdate();  
    try {  
        vertex = graph.insertVertex(parent, null, 'Double click to set name');  
        var geometry = model.getGeometry(vertex);  
        var size = graph.getPreferredSizeForCell(vertex);  
        geometry.width = size.width;  
        geometry.height = size.height;  
        var edge = graph.insertEdge(parent, null, '', cell, vertex);  
        edge.geometry.x = 1;  
        edge.geometry.y = 0;  
        edge.geometry.offset = new mxPoint(0, -20);  
    }finally{  
        model.endUpdate();  
    }  
    return vertex;  
};

////导入流程
function imp(graph,history){
    graph_refresh = graph;
    var processSelectModalCount = 0 ;
    $('#processSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#processSelectModal').on("shown.bs.modal",function(){
        if(++processSelectModalCount == 1){
            $('#searchFormprocess')[0].reset();
            var opt = {
                searchformId:'searchForprocess'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,rest_url_prefix_lc+'/lcProcess/blist',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"lc_process_id",
                        width:"50px"
                    },
                    {
                        data:'xt_userinfo_realName'
                    },
                    {
                        data:'lc_process_title'
                    },
                    {
                        data:'lc_process_status',
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "待发布";
                            }
                            if(data == 1){
                                return "发布中";
                            }
                            if(data == 2){
                                return "已关闭";
                            }
                        }
                    },
                    {
                        data:"lc_process_id",
                        render:function(data, type, row, meta) {
                            var lc_process_flag = row.lc_process_flag;
                            var lc_process_title = row.lc_process_title;
                            var xt_attachment = row.xt_attachment;
                            var btn = '<button class="btn btn-default" onclick=doImpl("'+data+'")><i class="glyphicon glyphicon-eye-open"></i>导入流程</button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#processDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('processDatatables');
        }
    })
}
function doImpl(id){
    msgTishCallFnBoot("确定导入该流程数据？",function(){
        ajaxBRequestCallFn(rest_url_prefix_lc+'/lcProcess/get/'+id,null,function(result){
            if(typeof(result.success) != "undefined" && result.success == true){
                var lc_process_title = result.data.lc_process_title;
                var lc_process_mxgraph_style = result.data.lc_process_mxgraph_style;
                var lc_process_mxgraphxml = result.data.lc_process_mxgraphxml;
                var processId = result.data.lc_process_uid;
                var candidateStarterUsers = result.data.candidateStarterUsers;
                var candidateStarterGroups = result.data.candidateStarterGroups;
                var lc_process_remark = result.data.lc_process_remark;
                var lc_process_id = result.data.lc_process_id;
                var xt_constant_id = result.data.xt_constant_id;
                //////////////////////////////////开始导入XML文件///////////////////////////
                var graph = graph_refresh;
                graph.getModel().beginUpdate();
                try{
                    /*
                     * 直接读取流程图的xml, 并展示流程图
                    */
                    if(lc_process_mxgraphxml != null && lc_process_mxgraphxml.length > 0){
                        var doc = mxUtils.parseXml(lc_process_mxgraphxml);
                        var dec = new mxCodec(doc);
                        dec.decode(doc.documentElement, graph.getModel());
                    }
                    validatePOOL(graph);
                }catch (e){
                    console.log("---导入流程出现异常---",e)
                }finally{
                    graph_refresh.getModel().endUpdate();
                    linetostyle(lc_process_mxgraph_style,graph);
                    graph_refresh.refresh();
                    $('#processId').val(processId);
                    $('#processName').val(lc_process_title);
                    $('#mxgraphxml').val(lc_process_mxgraphxml);
                    $('#lc_process_mxgraph_style').val(lc_process_mxgraph_style);
                    $('#lc_process_id').val(lc_process_id);
                    $('#candidateStarterUsers').val(candidateStarterUsers);
                    $('#remark').val(lc_process_remark);
                    $('#xt_constant_id').val(xt_constant_id);
                    $('#processSelectModal').modal('hide');
                }
                //////////////////////////////////结束导入XML文件///////////////////////////
            }
        },null,"GET");
    })
}

//连线样式设置虚线
function connectEdge(editor){
	if (editor.defaultEdge != null){
		editor.defaultEdge.style = 'straightEdge';
	}
}

function importP(graph,history){
    //initScroll()
	console.log("importP---");
    imp(graph,history)
}

function saveP(graph){
    //initScroll()
    //获取mxgraph拓扑图数据
    //var enc = new mxCodec(mxUtils.createXmlDocument());
    //var node1 = enc.encode(graph.getModel());
    //var mxgraphxml = mxUtils.getXml(node1);
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var mxgraphxml = mxUtils.getPrettyXml(node);
    mxgraphxml = mxgraphxml.replace(/\"/g,"'");
    //mxgraphxml = encodeURIComponent(mxgraphxml);

    var xmlDoc = mxUtils.createXmlDocument();
    var root = xmlDoc.createElement('output');
    xmlDoc.appendChild(root);
    var xmlCanvas = new mxXmlCanvas2D(root);
    var imgExport = new mxImageExport();
    imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    var bounds = graph.getGraphBounds();
    var w = Math.round(bounds.x + bounds.width + 4);
    var h = Math.round(bounds.y + bounds.height + 4);
    var imgxml = mxUtils.getXml(root);
    //imgxml = "<output>"+imgxml+"</output>";
    //imgxml = encodeURIComponent(imgxml);

    saveDesign(mxgraphxml,w,h,imgxml);
}

function exportP(){
    tlocation(base_html_redirect+'/pc/design/power-design/grapheditor/preview.html');
}

function closeJehcLcWin(){
    $('#jehcLcModal').modal('hide');
}

function initScroll(){
    // $("#sidebarContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#diagramContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false}); // First scrollable DIV
}

function nodeScroll(){
    // $("#mportletId").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#mportletId1").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#TabCol").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
}



function linetostyle(flag,graph){
    // var line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/default-style.xml';
    // if(flag == 0){
    //     //如果为1直线
    //     line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/default-style.xml';
    // }else if(flag == 1){
    //     //如果为1曲线
    //     line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/bight-style.xml';
    // }
    // var history = new mxUndoManager();
    // //载入默认样式
    // var node = mxUtils.load(line_style).getDocumentElement();
    // var dec = new mxCodec(node.ownerDocument);
    // dec.decode(node, graph.getStylesheet());
    // var edgeStyle = graph.getStylesheet().getDefaultEdgeStyle();
    // //edgeStyle[mxConstants.STYLE_EDGE] = mxEdgeStyle.TopToBottom;
    // edgeStyle['gradientColor'] = '#c0c0c0';
    // edgeStyle['strokeColor'] = '#c0c0c0'; //更改连线默认样式此处为颜色
    // edgeStyle['dashed'] = '1'; //虚线
    // edgeStyle['strokeWidth'] = 0.1;
    // edgeStyle['fontSize'] = '8';
    // edgeStyle['fontColor'] = '#000';
    // edgeStyle['arrowWidth'] = 0.1;
    // graph.alternateEdgeStyle = 'elbow=vertical';
    // graph.refresh();
    // Ext.getCmp('lc_process_mxgraph_style').setValue(flag);
}
//载入XML流程图
function loadXml(graph,xml){
    graph.getModel().beginUpdate();
    try{
        if(xml != null && xml.length > 0){
            var doc = mxUtils.parseXml(xml);
            var dec = new mxCodec(doc);
            dec.decode(doc.documentElement, graph.getModel());
        }
    }finally{
        graph.getModel().endUpdate();
        graph.refresh();
    }
    setTimeout(function(){
        resetZxLine(graph);
    },100);

}
$(document).ready(function() {
    $('#BaseForm').bootstrapValidator({
        message: '此值不是有效的'
    });
});
//保存流程
function saveDesign(mxgraphxml,w,h,imgxml){
    msgTishCallFnBoot("确定保存该设计？",function(){
        $('#mxgraphxml').val(mxgraphxml);
        $('#imgxml').val(imgxml);
        $('#w').val(w);
        $('#h').val(h);
        $("#stationId").val("1");
        submitBFormCallFn('BaseForm','/save',function(result){
            try {
                var stationId = result.stationId;
                if(stationId != null){
                    window.parent.toastrBoot(3,"保存成功");
                    $('#stationId').val(result.stationId);
                    openNewWindow("/preview.html");
                }else{
                    window.parent.toastrBoot(4,"保存失败");
                }
            } catch (e) {

            }
        });
    })
}


//将字符串转换XML
function printString(xmlobj){
    var xmlDom;
    //IE
    if(document.all){
        xmlDom=new ActiveXObject("Microsoft.XMLDOM");
        xmlDom.loadXML(xmlobj);
    }
    //非IE
    else {
        xmlDom = new DOMParser().parseFromString(xmlobj, "text/xml");
    }
    return xmlDom;
}

function resetZxLine(graph,reFlow){
    var allLines =graph.getModel().cells;
    for(var i in allLines){
        var cell = allLines[i];
        if(cell.nodeType == "zxLine"){
            // Adds animation to edge shape and makes "pipe" visible
            graph.getModel().beginUpdate();
            try
            {
                var state = graph.view.getState(cell);
                if(null != state){
                    state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
                    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
                    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
                    state.shape.node.getElementsByTagName('path')[0].removeAttribute('class');
                    if(undefined != reFlow && reFlow == true){
                        state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'reflow');
                    }else{
                        state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');
                    }

                }
            }
            finally
            {
                // Updates the display
                graph.getModel().endUpdate();
            }
        }
    }
}


function aminLine(graph,parent,x,y){
    var vertexStyle = 'shape=cylinder;strokeWidth=2;fillColor=#ffffff;strokeColor=black;' +
        'gradientColor=#a0a0a0;fontColor=black;fontStyle=1;spacingTop=14;';
    graph.getModel().beginUpdate();
    try
    {
        var v1 = graph.insertVertex(parent, null, 'Pump', x, y, 48, 48,vertexStyle);
        var v2 = graph.insertVertex(parent, null, 'Tank', x+160, y, 48, 48,vertexStyle);
        var edge = graph.insertEdge(parent, null, '', v1, v2,'strokeWidth=3;endArrow=block;endSize=2;endFill=1;strokeColor=black;rounded=1;');
        edge.geometry.points = [new mxPoint(x, y)];
        graph.orderCells(true, [edge]);
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }

    // Adds animation to edge shape and makes "pipe" visible
    var state = graph.view.getState(edge);
    // state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
    state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');

    graph.getModel().beginUpdate();
    try
    {
        graph.removeCells([v1], false);
        graph.removeCells([v2], false);
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }

}


function addAnimationEdge(graph,edge){
    // Adds animation to edge shape and makes "pipe" visible
    graph.getModel().beginUpdate();
    try
    {
        var state = graph.view.getState(edge);
        state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
        state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
        state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
        state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }
}

function initData(graph){
    var params = {graph_site_id:$('#graph_site_id').val()};
    ajaxBRequestCallFn(basePath+'/graphSiteController/getGraphSiteById',params,function(result){
        result = eval("(" + result + ")");
        loadXml(graph,result.data.graph_site_mxgraph);
    });
}