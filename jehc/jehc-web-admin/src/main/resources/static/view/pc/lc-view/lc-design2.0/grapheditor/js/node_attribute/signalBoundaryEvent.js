/**
 * 信号边界
 */
var signalBoundaryEventForm;
function signalBoundaryEventWin_(cell,graph_refresh){
    signalBoundaryEventPanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function signalBoundaryEventPanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,2);
    event_grid = creatEventGrid(cell);
    signalBoundaryEventForm = createSignalBoundaryEvent(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1'id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href='javascript:setSignalBoundaryEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+signalBoundaryEventForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    //基本属性
    initSignalThrowingEventData(cell);
    nodeScroll();
}


/**
 *
 * @returns {string|*}
 */
function createSignalBoundaryEvent(cell){
	var signalBoundaryEventOption =
        "<option value='false'>false</option>" +
        "<option value='true'>true</option>";
    signalBoundaryEventForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//结束活动
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >结束活动</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<select class=\"form-control\" id='cancelActivity' name=\"cancelActivity\">" +
        						signalBoundaryEventOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//信号依附
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >信号依附</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" id=\"signalRef\" name=\"signalRef\" placeholder=\"请输入信号依附\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return signalBoundaryEventForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initSignalBoundaryEventData(cell){
    var cancelActivity = cell.cancelActivity;
    var signalRef = cell.signalRef;
    $('#cancelActivity').val(cancelActivity);
    $("#signalRef").val(signalRef);
}

/**
 * 设置内容
 */
function setSignalBoundaryEventValue(){
    var cancelActivity = $('#cancelActivity').val();
    var signalRef = $('#signalRef').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        if(null != cancelActivity && "" != cancelActivity){
            JehcClickCell.cancelActivity = cancelActivity;
        }
        //3基本配置
        if(null != signalRef && "" != signalRef){
            JehcClickCell.signalRef = signalRef;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}