//开始节点[start]到目前为止只能有一个开始节点
/**
 基本配置:名称 表单 备注
 事件配置:事件类型(开始,结束) 类名
**/
var startNodeAttributeForm;

/**
 * 开始节点面板
 * @param cell
 * @param graph_refresh
 */
function startNodeAttributeWin(cell,graph_refresh){
    startNodeAttributePanel(cell,graph_refresh);
}

/**
 * 开始节点属性
 * @param cell
 * @returns {string|*}
 */
function createStartNodeAttributeForm(cell){
    startNodeAttributeForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//发起人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >发&nbsp;&nbsp;起&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请选择发起人\" id=\"initiator\" name=\"initiator\" placeholder=\"请选择发起人\">"+
						"</div>"+
					"</div>"+

					//表单键
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >表&nbsp;&nbsp;单&nbsp;键</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入表单键\" id=\"formKey\" name=\"formKey\" placeholder=\"请输入表单键\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
		"</div>";
	return startNodeAttributeForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initStartData(cell){
    var initiator = cell.initiator;
    var formKey = cell.formKey;
    $("#initiator").val(initiator);
    $("#formKey").val(formKey);
}

function startNodeAttributePanel(cell,graph_refresh){
    startNodeAttributeForm = createStartNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell,1);
    form_grid = creatFormGrid(cell);
    event_grid = creatEventGrid(cell);
    //Tab Index
    var Tab =
        	"<div class='col-md-1' id='TabCol'>"+
				"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
					"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

					"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

					"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">表单配置</a>"+

					"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

        			"<a href='javascript:setStartValue()' class='svBtn'>保存配置</a>"+
				"</div>"+
			"</div>"+

			"<div class='col-md-11'>"+
				"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+startNodeAttributeForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+form_grid+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

	//基本配置
    initStartData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //表单配置信息
    initform_grid(cell);
    //共用taskGrid属性事件
    initevent_grid(cell,1);

    nodeScroll();
}

/**
 * 设置内容
 */
function setStartValue(){
    var initiator = $("#initiator").val();
    var formKey = $("#formKey").val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        if(null != initiator && "" != initiator){
            JehcClickCell.initiator = initiator;
        }
        if(null != formKey && "" != formKey){
            JehcClickCell.formKey = formKey;
        }
        //4配置表单事件
        if(event_form_setvalue(JehcClickCell) == false){
            return;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}