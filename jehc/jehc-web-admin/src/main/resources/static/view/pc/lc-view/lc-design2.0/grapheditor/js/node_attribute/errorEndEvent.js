var errorEndEventNodeAttributeForm;

/**
 * 错误结束事件
 * @param cell
 * @param graph_refresh
 * @private
 */
function errorEndEventWin_(cell,graph_refresh){
    errorEndEventPanel(cell,graph_refresh);
}

/**
 * @param cell
 * @returns {string|*}
 */
function createErrorEndEventNodeAttributeForm(cell){
    errorEndEventNodeAttributeForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//错误编码
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >错误依附</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入错误依附\" id=\"errorRef\" name=\"errorRef\" placeholder=\"请输入错误依附\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return errorEndEventNodeAttributeForm;
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function errorEndEventPanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,1);
    event_grid = creatEventGrid(cell);
    errorEndEventNodeAttributeForm = createErrorEndEventNodeAttributeForm(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href='javascript:setErrorEndEventValue()' class='svBtn'>保存配置</a>"+
				"</div>"+
		"</div>"+

		"<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+errorEndEventNodeAttributeForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    //基本数据
    initErrorEndEventData(cell);

    nodeScroll();
}

/**
 *
 */
function setErrorEndEventValue(){
    var errorRef = $('#errorRef').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        if(null != errorRef && "" != errorRef){
            JehcClickCell.errorRef = errorRef;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

/**
 *
 * @param cell
 * @constructor
 */
function initErrorEndEventData(cell){
    var errorRef = cell.errorRef;
    $("#errorRef").val(errorRef);
}