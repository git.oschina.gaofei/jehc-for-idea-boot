var form_grid;
/**
 * 表单配置
 * @param cell
 * @returns {string|*}
 */
function creatFormGrid(cell){
    form_grid=
	"<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
		"<div class=\"m-portlet__head\">"+
			"<div class=\"m-portlet__head-caption\">" +
				"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventFormRow()\">新一行</button>"+
				"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='event_form_setvalue()'>"+
			"</div>"+
		"</div>"+

		"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventForm\" method=\"post\">"+
			"<div class=\"m-portlet__body\" id='event_form_grid'>"+
				//Title
				"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
					"<div class=\"col-md-1\">表单编号</div>"+
					"<div class=\"col-md-1\">表单名称</div>"+
					"<div class=\"col-md-1\">表单类型</div>"+
					"<div class=\"col-md-1\">表达式</div>"+
					"<div class=\"col-md-1\">变量</div>"+
					"<div class=\"col-md-1\">默认</div>"+
					"<div class=\"col-md-1\">格式日期</div>"+
					"<div class=\"col-md-1\">可读</div>"+
					"<div class=\"col-md-1\">可写</div>"+
					"<div class=\"col-md-1\">是否校验</div>"+
					"<div class=\"col-md-1\">配置字段</div>"+
					"<div class=\"col-md-1\">操作</div>"+
				"</div>"+

			"</div>"+
		"</form>"+
	"</div>";
	return form_grid;
}

/**
 * 新一行 “表单配置”
 * @param formID          表单编号
 * @param formName        表单名称
 * @param formType        表单类型
 * @param formExpression  表达式
 * @param formVariable    变量
 * @param formDefault     默认
 * @param formDatePattern 格式日期
 * @param formReadable    可读
 * @param formWriteable   可写
 * @param formRequired    是否校验
 * @param formFormValues  配置字段
 */
function addEventFormRow(formID,formName,formType,formExpression,formVariable,formDefault,formDatePattern,formReadable,formRequired,formWriteable,formFormValues){
	if(formID === undefined){
        formID = "";
	}
	if(formName === undefined){
        formName = "";
	}
    if(formType === undefined){
        formType = "";
    }
    if(formExpression === undefined){
        formExpression = "";
    }
    if(formVariable === undefined){
        formVariable = "";
    }
    if(formDefault === undefined){
        formDefault = "";
    }
    if(formDatePattern === undefined){
        formDatePattern = "";
    }
    if(formReadable === undefined){
        formReadable = "";
    }
    if(formWriteable === undefined){
        formWriteable = "";
    }
    if(formRequired === undefined){
        formRequired = "";
    }
    if(formFormValues === undefined){
        formFormValues = "";
    }
	var uuid = guid();
    validatorDestroy('eventForm');
	var rows =
    "<div class=\"form-group m-form__group row\" id='event_form_rows_"+uuid+"'>"+
		//表单编号
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入表单编号' id='formID"+uuid+"' name='formID' value='"+formID+"' placeholder='请输入表单编号'>"+
		"</div>"+

		//表单名称
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入表单名称\" id='formName"+uuid+"' name='formName' value='"+formName+"' placeholder='请输入表单名称'>"+
		"</div>"+

		//表单类型
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formType"+uuid+"' name=\"formType\">" +
				"<option value=''>请选择</option>" +
				"<option value='string'>string</option>" +
				"<option value='long'>long</option>" +
				"<option value='enum'>enum</option>" +
				"<option value='date'>date</option>" +
				"<option value='boolean'>boolean</option>" +
			"</select>"+
		"</div>"+

		//表达式
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formExpression"+uuid+"' name=\"formExpression\" value='"+formExpression+"' placeholder=\"请输入表达式\">"+
		"</div>"+

		//变量
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formVariable"+uuid+"' name=\"formVariable\" value='"+formVariable+"' placeholder=\"请输入变量\">"+
		"</div>"+

		//默认
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"id='formDefault"+uuid+"' name=\"formDefault\" value='"+formDefault+"' placeholder=\"请输入\">"+
		"</div>"+

		//格式日期
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formDatePattern"+uuid+"' name=\"formDatePattern\" value='"+formDatePattern+"' placeholder=\"请输入\">"+
		"</div>"+

		//可读
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formReadable"+uuid+"' name=\"formReadable\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//可写
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formWriteable"+uuid+"' name=\"formWriteable\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//是否校验
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formRequired"+uuid+"' name=\"formRequired\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//配置字段
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" readonly='readonly' onclick='showFormValuesWin(this,"+null+",\""+uuid+"\")' id='formFormValues"+uuid+"' name=\"formFormValues\" value='"+formFormValues+"' placeholder=\"请输入输入\">"+
		"</div>"+

        //操作
        "<div class=\"col-md-1\">"+
			"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventFormRow(\""+uuid+"\")'>删除</button>"+
        "</div>"+
    "</div>";
    $("#event_form_grid").append(rows);
    $("#formType"+uuid).val(formType);
    $("#formReadable"+uuid).val(formReadable);
    $("#formWriteable"+uuid).val(formWriteable);
    $("#formRequired"+uuid).val(formRequired);
    reValidator('eventForm');
}

/**
 * 删除表单配置
 * @param rowID
 */
function delEventFormRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('eventForm');
        $("#event_form_rows_"+rowID).remove();
        reValidator('eventForm');
	});
}

/**
 * 保存表单配置
 * @param cell
 */
function event_form_setvalue(){
    var bootform = $('#eventForm');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var form_node_value = JSON.stringify($("#eventForm").serializeObject());
    if(null != form_node_value && "" != form_node_value){
        JehcClickCell.form_node_value = form_node_value;
	}else{
        JehcClickCell.form_node_value = "";
	}
	return true;
}

/**
 * 初始化init form grid
 * @param cell
 */
function initform_grid(cell){
    //表单配置
    $('#eventForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    initform_data(cell)
}
/**
 * 抽取出共同方法即渲染编辑表格方法(注意:flag为1表示只有开始和结束 2表示开始，结束，分配任务)
 * @param cell
 */
function initform_data(cell){
	var form_node_value = cell.form_node_value;
    if(null != form_node_value && "" != form_node_value){
        form_node_value = eval('(' + form_node_value + ')');
        var formIDList = $.makeArray(form_node_value["formID"]);
        var formNameList = $.makeArray(form_node_value["formName"]);
        var formTypeList = $.makeArray(form_node_value["formType"]);
        var formExpressionList = $.makeArray(form_node_value["formExpression"]);
        var formVariableList = $.makeArray(form_node_value["formVariable"]);
        var formDefaultList = $.makeArray(form_node_value["formDefault"]);
        var formDatePatternList = $.makeArray(form_node_value["formDatePattern"]);
        var formReadableList = $.makeArray(form_node_value["formReadable"]);
        var formWriteableList = $.makeArray(form_node_value["formWriteable"]);
        var formRequiredList = $.makeArray(form_node_value["formRequired"]);
        var formFormValuesList = $.makeArray(form_node_value["formFormValues"]);
        for(var i = 0; i < formIDList.length; i++){
            var formID = formIDList[i];
            var formName = formNameList[i];
            var formType = formTypeList[i];
            var formExpression = formExpressionList[i];
            var formVariable = formVariableList[i];
            var formDefault = formDefaultList[i];
            var formDatePattern = formDatePatternList[i];
            var formReadable = formReadableList[i];
            var formWriteable = formWriteableList[i];
            var formRequired = formRequiredList[i];
            var formFormValues = formFormValuesList[i];
            addEventFormRow(formID,formName,formType,formExpression,formVariable,formDefault,formDatePattern,formReadable,formRequired,formWriteable,formFormValues);
        }
    }
}

var form_values_grid;
/**
 * 设置表单值
 * @param thiz
 * @param cell
 * @param id
 */
function showFormValuesWin(thiz,cell,id){
	$('#jehcLcModalLabel').empty();
    $('#jehcLcForm').empty();
    $('#jehcLcModalLabel').append("配置表单字段属性");
    form_values_grid = creatFormValuesGrid(cell,id);
    $("#jehcLcForm").append(form_values_grid);
    $('#eventFormValues').bootstrapValidator({
        message:'此值不是有效的'
    });
    initEventFormValues(id);//初始化数据
    $('#jehcLcModal').modal({backdrop: 'static', keyboard: false});
}

/**
 * 创建配置字段表单
 * @param cell
 * @param id
 * @returns {string|*}
 */
function creatFormValuesGrid(cell,id){
    form_values_grid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventFormValuesRow()\">新一行</button>"+
					"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick='saveEventFormValues(\""+id+"\")'>保存</button>"+
        			"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"closeJehcLcWin()\">关闭</button>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventFormValues\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='event_form_values_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-3\">编号</div>"+
						"<div class=\"col-md-5\">名称</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return form_values_grid;
}

/**
 * 初始化表单配置字段值
 * @param id
 */
function initEventFormValues(id){
    var formFormValues = $("#formFormValues"+id).val();
    if(null != formFormValues && "" != formFormValues){
        formFormValues = eval('(' + formFormValues + ')');
        var fidList = $.makeArray(formFormValues["fid"]);
        var fnameList = $.makeArray(formFormValues["fname"]);
		for(var i = 0; i < fidList.length; i++){
			var fid = fidList[i];
			var fname = fnameList[i];
            addEventFormValuesRow(fid,fname)
        }
    }
}

/**
 * 动态添加 新一行 ”表单字段“
 * @param fid
 * @param fname
 */
function addEventFormValuesRow(fid,fname){
    validatorDestroy('eventFormValues');
    var uuid = guid();
    if(fid === undefined){
        fid = "";
	}
	if(fname === undefined){
    	fname = "";
	}
    var rows =
        "<div class=\"form-group m-form__group row\" id='event_form_values_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='fid"+uuid+"' value='"+fid+"' name='fid' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-5\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入名称\" id='fname"+uuid+"' value='"+fname+"' name='fname' placeholder='请输入名称'>"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventFormValuesRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#event_form_values_grid").append(rows);
    reValidator('eventFormValues');
}

/**
 * 保存配置字段
 * @param id
 */
function saveEventFormValues(id){
	var bootform = $('#eventFormValues');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#formFormValues"+id).val(JSON.stringify($("#eventFormValues").serializeObject()))
    closeJehcLcWin();
}

/**
 * 删除配置字段
 * @param id
 */
function delEventFormValuesRow(id){
    validatorDestroy('eventFormValues');
	$("#event_form_values_rows_"+id).remove();
    reValidator('eventFormValues');
}