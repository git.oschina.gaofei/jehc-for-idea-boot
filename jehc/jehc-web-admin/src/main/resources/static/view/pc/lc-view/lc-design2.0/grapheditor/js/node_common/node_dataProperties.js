//////////////数据属性事件子流程 子流程 事物流程 流程信息等地方使用///////////
//数据属性表格
var dataProperties_grid;
/**
 * 创建数据属性数据源Grid
 */
function createDataPropertiesGrid(){
    dataProperties_grid =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
				"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addDataPropertiesRow()\">新一行</button>"+
				"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='dataProperties_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_dataProperties_form\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='dataProperties_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-3\">编号</div>"+
						"<div class=\"col-md-3\">名称</div>"+
                        "<div class=\"col-md-2\">类型</div>"+
						"<div class=\"col-md-3\">值</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return dataProperties_grid;
}

/**
 * 新一行
 * @param ID
 * @param name
 * @param type
 * @param value
 */
function addDataPropertiesRow(ID,name,type,value){
    if(ID === undefined){
        ID = "";
    }
    if(name === undefined){
        name = "";
    }
    if(type === undefined){
        type = "";
    }
    if(value === undefined){
        value = "";
    }

    var uuid = guid();
    validatorDestroy('node_dataProperties_form');
    data:[["string",""],["","boolean"],["","datetime"],["","double"],["","int"],["","long"]]
    var typeOption =  "<option value=''>请选择</option>" +
        "<option value='string'>string</option>" +
        "<option value='boolean'>boolean</option>"+
        "<option value='datetime'>datetime</option>"+
        "<option value='double'>double</option>"+
        "<option value='int'>int</option>"+
        "<option value='long'>long</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_dataProperties_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='ID"+uuid+"' name='ID' value='"+ID+"' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='name"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"name\" value='"+name+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//类型
			"<div class=\"col-md-2\">"+
				"<select class=\"form-control\" id='type"+uuid+"' name=\"type\">" +
					typeOption+
				"</select>"+
			"</div>"+

            //值
            "<div class=\"col-md-3\">"+
                "<input class=\"form-control\" type=\"text\" id='value"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"value\" value='"+value+"' placeholder=\"请输入名称\">"+
            "</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delDataPropertiesRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#dataProperties_grid").append(rows);
    $("#type"+uuid).val(type);
    reValidator('node_dataProperties_form');
}

/**
 * 删除
 * @param rowID
 */
function delDataPropertiesRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_dataProperties_form');
        $("#node_dataProperties_rows_"+rowID).remove();
        reValidator('node_dataProperties_form');
    });
}

/**
 * 初始化表单及数据
 */
function initDataProperties_grid(cell){
    //表单配置
    $('#node_dataProperties_form').bootstrapValidator({
        message:'此值不是有效的'
    });
    initDataProperties_data(cell);
}

/**
 * 初始化列数据
 */
function initDataProperties_data(cell){
    var dataProperties_node_value = cell.dataProperties_node_value;
    if(null != dataProperties_node_value && "" != dataProperties_node_value){
        dataProperties_node_value = eval('(' + dataProperties_node_value + ')');
        var IDList = $.makeArray(dataProperties_node_value["ID"]);
        var nameList = $.makeArray(dataProperties_node_value["name"]);
        var typeList = $.makeArray(dataProperties_node_value["type"]);
        var valueList = $.makeArray(dataProperties_node_value["value"]);
        for(var i = 0; i < nameList.length; i++){
            var name = nameList[i];
            var ID = IDList[i];
            var type = typeList[i];
            var value = valueList[i];
            addDataPropertiesRow(ID,name,type,value);
        }
    }
}


//点击确定按钮设置mxgraph中cell属性
var dataProperties_node_value;
function dataProperties_setvalue(cell){
    var bootform = $('#node_dataProperties_form');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    dataProperties_node_value = JSON.stringify($("#node_dataProperties_form").serializeObject());
    if(null != dataProperties_node_value && "" != dataProperties_node_value){
        JehcClickCell.dataProperties_node_value = dataProperties_node_value;
    }else{
        JehcClickCell.dataProperties_node_value = "";
    }
}