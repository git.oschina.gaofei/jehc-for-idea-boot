//节点的共同事件配置属性
//事件表格
var event_grid;
/**
 * 事件配置
 * @param cell
 * @returns {string|*}
 */
function creatEventGrid(cell){
    event_grid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventRow()\">新一行</button>"+
					"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='event_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_event\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='node_event_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-2\">执行的类或表达式</div>"+
						"<div class=\"col-md-1\">类型</div>"+
						"<div class=\"col-md-1\">事件</div>"+
						"<div class=\"col-md-2\">配置字段</div>"+
						"<div class=\"col-md-2\">脚本</div>"+
						"<div class=\"col-md-1\">运行方式</div>"+
						"<div class=\"col-md-2\">脚本解析器</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return event_grid;
}

function addEventRow(flag,javaclass_express,event_type,event,fields,script,runAs,scriptProcessor){
    if(javaclass_express === undefined){
        javaclass_express = "";
    }
    if(event_type === undefined){
        event_type = "";
    }
    if(event === undefined){
        event = "";
    }
    if(fields === undefined){
        fields = "";
    }
    if(script === undefined){
        script = "";
    }
    if(runAs === undefined){
        runAs = "";
    }
    if(scriptProcessor === undefined){
        scriptProcessor = "";
    }
    var uuid = guid();
    validatorDestroy('node_event');
    var event_typeOption;
    var event_option;
    if(flag == 1){
        event_option =  "<option value=''>请选择</option>" +
						"<option value='start'>start</option>" +
						"<option value='end'>end</option>";
    }else if(flag == 3){
        event_option =  "<option value=''>请选择</option>" +"<option value='take'>take</option>";
    }else{
        event_option = "<option value=''>请选择</option>" +
			"<option value='create'>create</option>" +
            "<option value='assignment'>assignment</option>"+
			"<option value='complete'>complete</option>"+
            "<option value='all'>all</option>";
    }
    var event_typeOption = "<option value=''>请选择</option>" +
        "<option value='javaclass'>javaclass</option>" +
        "<option value='express'>express</option>"+
        "<option value='delegateExpression'>delegateExpression</option>"+
        "<option value='ScriptExecutionListener'>ScriptExecutionListener</option>"+
        "<option value='ScriptTaskListener'>ScriptTaskListener</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_event_rows_"+uuid+"'>"+
			//执行的类或表达式
			"<div class=\"col-md-2\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='javaclass_express"+uuid+"' name='javaclass_express' value='"+javaclass_express+"' placeholder='请输入'>"+
			"</div>"+

			//类型
			"<div class=\"col-md-1\">"+
				"<select class=\"form-control\" id='event_type"+uuid+"' name=\"event_type\">" +
        			event_typeOption+
				"</select>"+
			"</div>"+

			//事件
			"<div class=\"col-md-1\">"+
				"<select class=\"form-control\" id='event"+uuid+"' name=\"event\">" +
        			event_option+
				"</select>"+
			"</div>"+

			//配置字段
			"<div class=\"col-md-2\">"+
				"<input class=\"form-control\" type=\"text\" readonly='readonly' onclick='showFieldWin(this,"+null+",\""+uuid+"\")' id='fields"+uuid+"' name=\"fields\" value='"+fields+"' placeholder=\"请配置字段\">"+
			"</div>"+

			//脚本
			"<div class=\"col-md-2\">"+
				"<input class=\"form-control\" type=\"text\" id='script"+uuid+"' name=\"script\" value='"+script+"' placeholder=\"请输入脚本\">"+
			"</div>"+

			//运行方式
			"<div class=\"col-md-1\">"+
				"<input class=\"form-control\" type=\"text\" id='runAs"+uuid+"' name=\"runAs\" value='"+runAs+"' placeholder=\"请输入运行方式\">"+
			"</div>"+

			//脚本解析器
			"<div class=\"col-md-2\">"+
				"<input class=\"form-control\" type=\"text\"id='scriptProcessor"+uuid+"' name=\"scriptProcessor\" value='"+scriptProcessor+"' placeholder=\"请输入脚本解析器\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
			"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#node_event_grid").append(rows);
    $("#event_type"+uuid).val(event_type);
    $("#event"+uuid).val(event);
    reValidator('node_event');
}

/**
 * 删除
 * @param rowID
 */
function delEventRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_event');
        $("#node_event_rows_"+rowID).remove();
        reValidator('node_event');
    });
}

/**
 * 初始化init event grid
 * @param cell
 * @param flag
 */
function initevent_grid(cell,flag){
    //表单配置
    $('#node_event').bootstrapValidator({
        message:'此值不是有效的'
    });
    initevent_data(cell,flag)
}

/**
 *
 * @param cell
 * @param flag
 */
function initevent_data(cell,flag){
    var event_node_value = cell.event_node_value;
    if(null != event_node_value && "" != event_node_value){
        event_node_value = eval('(' + event_node_value + ')');
        var javaclass_expressList = $.makeArray(event_node_value["javaclass_express"]);
        var event_typeList = $.makeArray(event_node_value["event_type"]);
        var eventList = $.makeArray(event_node_value["event"]);
        var fieldsList = $.makeArray(event_node_value["fields"]);
        var scriptList = $.makeArray(event_node_value["script"]);
        var runAsList = $.makeArray(event_node_value["runAs"]);
        var scriptProcessorList = $.makeArray(event_node_value["scriptProcessor"]);
        for(var i = 0; i < javaclass_expressList.length; i++){
            var javaclass_express = javaclass_expressList[i];
            var event_type = event_typeList[i];
            var event = eventList[i];
            var fields = fieldsList[i];
            var script = scriptList[i];
            var runAs = runAsList[i];
            var scriptProcessor = scriptProcessorList[i];
            addEventRow(flag,javaclass_express,event_type,event,fields,script,runAs,scriptProcessor)
        }
    }
}

/**
 * 配置事件字段
 * @param thiz
 * @param cell
 * @param id
 */
var fieldGrid;
function showFieldWin(thiz,cell,id){
    $('#jehcLcModalLabel').empty();
    $('#jehcLcForm').empty();
    $('#jehcLcModalLabel').append("配置事件字段属性");
    fieldGrid = creatFieldGrid(cell,id);
    $("#jehcLcForm").append(fieldGrid);
    $('#eventField').bootstrapValidator({
        message:'此值不是有效的'
    });
    initEventField(id);//初始化数据
    $('#jehcLcModal').modal({backdrop: 'static', keyboard: false});
}

/**
 * 创建事件字段
 * @param cell
 * @param id
 */
function creatFieldGrid(cell,id){
    fieldGrid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventFieldRow()\">新一行</button>"+
					"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick='saveEventField(\""+id+"\")'>保存</button>"+
					"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"closeJehcLcWin()\">关闭窗体</button>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventField\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='event_fields_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
					"<div class=\"col-md-3\">字段名称</div>"+
					"<div class=\"col-md-2\">字段类型</div>"+
        			"<div class=\"col-md-4\">字段值</div>"+
					"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return fieldGrid;
}

/**
 * 初始化事件字段数据
 * @param id
 */
function initEventField(id){
    var fields = $("#fields"+id).val();
    if(null != fields && "" != fields){
        fields = eval('(' + fields + ')');
        var fieldNameList = $.makeArray(fields["fieldName"]);
        var fieldtypeList = $.makeArray(fields["fieldtype"]);
        var fieldValueList = $.makeArray(fields["fieldValue"]);
        for(var i = 0; i < fieldNameList.length; i++){
            var fieldName = fieldNameList[i];
            var fieldtype = fieldtypeList[i];
            var fieldValue = fieldValueList[i];
            addEventFieldRow(fieldName,fieldtype,fieldValue);
        }
    }
}


/**
 * 动态添加 新一行 ”事件字段“
 * @param fieldName
 * @param fieldtype
 * @param fieldValue
 */
function addEventFieldRow(fieldName,fieldtype,fieldValue){
    validatorDestroy('eventField');
    var uuid = guid();
    if(fieldName === undefined){
        fieldName = "";
    }
    if(fieldtype === undefined){
        fieldtype = "";
    }
    if(fieldValue === undefined){
        fieldValue = "";
    }
    var fieldtypeOption =
		"<option value=''>请选择</option>" +
        "<option value='string'>string</option>"+
        "<option value='expression'>expression</option>";
    var rows =
        "<div class=\"form-group m-form__group row\" id='event_fields_rows_"+uuid+"'>"+
        //名称
        "<div class=\"col-md-3\">"+
        	"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入字段名称' id='fieldName"+uuid+"' value='"+fieldName+"' name='fieldName' placeholder='请输入字段名称'>"+
        "</div>"+

        //类型
        "<div class=\"col-md-2\">"+
			"<select class=\"form-control\" id='fieldtype"+uuid+"' name=\"fieldtype\">" +
        		fieldtypeOption+
			"</select>"+
        "</div>"+

        //值
        "<div class=\"col-md-4\">"+
        	"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入值\" id='fieldValue"+uuid+"' value='"+fieldValue+"' name='fieldValue' placeholder='请输入值'>"+
        "</div>"+

        //操作
        "<div class=\"col-md-1\">"+
        	"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventFieldRow(\""+uuid+"\")'>删除</button>"+
        "</div>"+
        "</div>";
    $("#event_fields_grid").append(rows);
    $("#fieldtype"+uuid).val(fieldtype);
    reValidator('eventField');
}


/**
 * 保存配置字段
 * @param id
 */
function saveEventField(id){
    var bootform = $('#eventField');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#fields"+id).val(JSON.stringify($("#eventField").serializeObject()))
    closeJehcLcWin();
}

/**
 * 删除事件字段
 * @param id
 */
function delEventFieldRow(id){
    validatorDestroy('eventField');
    $("#event_fields_rows_"+id).remove();
    reValidator('eventField');
}

/**
 *
 * @param cell
 * @param flag
 */
function event_task_grid(cell,flag){

}


/**
 * 点击确定按钮设置mxgraph中cell属性
 * @param cell
 * @returns {boolean}
 */
function event_setvalue(cell){
    var bootform = $('#node_event');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var event_node_value = JSON.stringify($("#node_event").serializeObject());
    if(null != event_node_value && "" != event_node_value){
        JehcClickCell.event_node_value = event_node_value;
    }else{
        JehcClickCell.event_node_value = "";
    }
    return true;
}