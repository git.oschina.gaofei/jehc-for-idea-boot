var setting = {
    view: {
        showLine: false,
        showIcon: false,
        selectedMulti: false,
        dblClickExpand: false,
        addDiyDom: addDiyDom
    },
    data:{
        //必须使用data
        simpleData:{
            enable:true,
            idKey:"id",//id编号命名 默认
            pIdKey:"pId",//父id编号命名 默认
            rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值
        }
    },
    edit:{
        enable:false
    },
    callback:{
        beforeClick: beforeClick,
        onClick:onClick,//单击事件
        onAsyncSuccess:onAsyncSuccess//加载数据完成事件
    }
};

function beforeClick(treeId, treeNode) {
    if (treeNode.level == 0 ) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        zTree.expandNode(treeNode);
        return false;
    }
    return true;
}

function addDiyDom(treeId, treeNode) {
    var spaceWidth = 5;
    var switchObj = $("#" + treeNode.tId + "_switch"),
        icoObj = $("#" + treeNode.tId + "_ico");
    switchObj.remove();
    icoObj.before(switchObj);

    if (treeNode.level > 1) {
        var spaceStr = "<span style='display: inline-block;width:" + (spaceWidth * treeNode.level)+ "px'></span>";
        switchObj.before(spaceStr);
    }
}

$(document).ready(function(){
    InitztData();
});
//初始数据
function InitztData() {
    var zTreeNodes;
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    ajaxBRequestCallFn(basePath+'/xtAreaRegionController/getXtAreaRegionListByCondition',null,function(result){
        zTreeNodes = eval("(" + result + ")");
        treeObj = $.fn.zTree.init($("#tree"), setting,zTreeNodes);
        // treeObj.expandAll(true);
        var length = $('#keyword').val().length;
        if(length > 0){
            var zTree = $.fn.zTree.getZTreeObj("tree");
            var nodeList = zTree.getNodesByParamFuzzy("name", $('#keyword').val());
            //将找到的nodelist节点更新至Ztree内
            treeObj = $.fn.zTree.init($("#tree"), setting, nodeList);
            treeObj.expandAll(true);
        }
        $("#zTreeHeight").height(tableHeight()*0.4);

        var t = $("#tree");
        t.hover(function () {
            if (!t.hasClass("showIcon")) {
                t.addClass("showIcon");
            }
        }, function() {
            t.removeClass("showIcon");
        });
        closeWating(null,dialogWating);
    });
}

//刷新
function refreshAll(){
    InitztData();
}

//加载数据完成事件
function onAsyncSuccess(event, treeId, treeNode, msg){
    // var length = $('#keyword').val().length;
    // if(length > 0){
    // 	var zTree = $.fn.zTree.getZTreeObj(treeId);
    //     var nodeList = zTree.getNodesByParamFuzzy("name", $('#keyword').val());
    //     //将找到的nodelist节点更新至Ztree内
    //     $.fn.zTree.init($("#"+treeId), setting, nodeList);
    // }
    // closeWating(null,dialogWating);
}

//单击事件
function onClick(event, treeId, treeNode, msg){
}

/**
 * 搜索树，显示并展示
 * @param treeId
 * @param text文本框的id
 */
function filter(){
    InitztData();
}

//删除
function delXtAreaRegion(){
	var zTree = $.fn.zTree.getZTreeObj("tree"),
	nodes = zTree.getSelectedNodes();
	if (nodes.length != 1) {
		toastrBoot(4,"必须选择一条记录进行删除");
		return;
	}
	var params = {ID:nodes[0].id};
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		ajaxBRequestCallFn(basePath+'/xtAreaRegionController/delXtAreaRegion',params,function(result){
			try {
	    		result = eval("(" + result + ")");  
	    		if(typeof(result.success) != "undefined"){
	    			if(result.success){
	            		window.parent.toastrBoot(3,result.msg);
	            		refreshAll();
	        		}else{
	        			window.parent.toastrBoot(4,result.msg);
	        		}
	    		}
			} catch (e) {
				
			}
		});
	})
}