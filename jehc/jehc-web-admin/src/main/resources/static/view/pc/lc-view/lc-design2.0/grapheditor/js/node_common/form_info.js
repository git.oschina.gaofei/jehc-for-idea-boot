function appendFormInfo(){
    message_grid = createMessageGrid();
    signal_grid = createSignalGrid();
    datamainProperties_grid = createDatamainPropertiesGrid();
    event_main_grid = createEventMainGrid();
    var baseContent =
        "<div class=\"m-portlet\" id='mportletId' style='height:350px;overflow: auto;'>"+
            "<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
                "<div class=\"m-portlet__body\">"+
                    //流程名称
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >流程名称</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<input class=\"form-control\" type=\"text\" maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入流程名称\" id=\"processNameTemp\" name=\"processNameTemp\" placeholder=\"请输入流程名称\">"+
                        "</div>"+
                    "</div>"+

                    //命名空间
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >命名空间</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<input class=\"form-control\" type=\"text\" maxlength=\"50\" value=\"http://www.activiti.org/test\" data-bv-notempty data-bv-notempty-message=\"请输入命名空间\" id=\"mainNameSpaceTemp\" name=\"mainNameSpaceTemp\" placeholder=\"请输入命名空间\">"+
                        "</div>"+
                    "</div>"+

                    //发起人
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >发&nbsp;&nbsp;起&nbsp;&nbsp;人</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<select class=\"form-control\" id=\"candidateStarterUsersTemp\" name=\"candidateStarterUsersTemp\"><option value=''>请选择</option></select>"+
                        "</div>"+
                    "</div>"+

                    //发起人组
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >发起人组</label>"+
                        "</div>"+
                        "<div class=\"col-md-6\">"+
                            "<select class=\"form-control\" id=\"candidateStarterGroupsTemp\" name=\"candidateStarterGroupsTemp\"><option value=''>请选择</option></select>"+
                        "</div>"+
                    "</div>"+

                    //备注
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注</label>"+
                        "</div>"+
                        "<div class=\"col-md-6\">"+
                            "<textarea class=\"form-control\" maxlength=\"500\" id=\"remarkTemp\" name=\"remarkTemp\" placeholder=\"请输入备注\"></textarea>"+
                        "</div>"+
                    "</div>"+


                    //所属模块
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"control-label\" >模&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;块</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<select class=\"form-control\" id=\"xt_constant_idTemp\" name=\"xt_constant_idTemp\"><option value=''>请选择</option></select>"+
                        "</div>"+
                    "</div>"+


                "</div>"+
            "</form>"+
        "</div>";

    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
            "<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
                "<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">基础信息</a>"+

                "<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

                "<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">数据属性</a>"+

                "<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">信号定义</a>"+

                "<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">消息配置</a>"+

                "<a href='javascript:setBaseFormInfo()' class='svBtn'>保存配置</a>"+

            "</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
            "<div class=\"tab-content tab-content-default\">"+
                "<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+baseContent+"</div>"+
                "<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+event_main_grid+"</div>"+
                "<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+datamainProperties_grid+"</div>"+
                "<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+signal_grid+"</div>"+
                "<div class=\"tab-pane fade\" id=\"v-pills-messages5\">"+message_grid+"</div>"+
            "</div>"+
        "</div>";

    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"

    $("#geSetContainer").empty();
	$("#geSetContainer").append(formInfo);

    initBaseFormData();

    nodeScroll();
}

/**
 * 设置Form基本信息
 */
function setBaseFormInfo(){
    var processNameTemp = $("#processNameTemp").val();

    var processIdTemp = $("#processIdTemp").val();

    var mainNameSpaceTemp = $("#mainNameSpaceTemp").val();

    var candidateStarterUsersTemp = $("#candidateStarterUsersTemp").val();

    var candidateStarterGroupsTemp = $("#candidateStarterGroupsTemp").val();

    var remarkTemp = $("#remarkTemp").val();

    var xt_constant_idTemp = $("#xt_constant_idTemp").val();

    $("#processName").val(processNameTemp);

    $("#processId").val(processIdTemp);

    $("#mainNameSpace").val(mainNameSpaceTemp);

    $("#candidateStarterUsers").val(candidateStarterUsersTemp);

    $("#candidateStarterGroups").val(candidateStarterGroupsTemp);

    $("#remark").val(remarkTemp);

    $("#xt_constant_id").val(xt_constant_idTemp);


    //验证事件
    if(event_main_setvalue() == false){
        return;
    }

    //验证数据属性
    if(datamainProperties_setvalue() == false){
        return;
    }

    //验证信号
    if(signal_setvalue() == false){
        return;
    }

    //验证消息
    if(message_setvalue() == false){
        return;
    }
}

/**
 * 初始化基础Form数据
 */
function initBaseFormData(){
    var processName = $("#processName").val();

    var processId = $("#processId").val();

    var mainNameSpace = $("#mainNameSpace").val();

    var candidateStarterUsers = $("#candidateStarterUsers").val();

    var candidateStarterGroups = $("#candidateStarterGroups").val();

    var remark = $("#remark").val();

    var xt_constant_id = $("#xt_constant_id").val();

    $("#processNameTemp").val(processName);

    $("#processIdTemp").val(processId);

    if(null == mainNameSpace || "" == mainNameSpace){
        $("#mainNameSpaceTemp").val("http://www.activiti.org/test");
    }else{
        $("#mainNameSpaceTemp").val(mainNameSpace);
    }


    $("#candidateStarterUsersTemp").val(candidateStarterUsers);

    $("#candidateStarterGroupsTemp").val(candidateStarterGroups);

    $("#remarkTemp").val(remark);

    $("#xt_constant_idTemp").val(xt_constant_id);

    //初始化事件
    initeventmain_grid();

    //初始化数据属性
    initDatamainProperties_grid();

    //初始化消息
    initmessage_grid();

    //初始化信号
    initSignal_grid();
}