//业务规则
var businessRuleTaskNodeAttributeForm;
/**
 *
 * @param cell
 * @param graph_refresh
 */
function showBusinessRuleTaskNodeAttributeWin(cell,graph_refresh){
    businessRuleTaskNodeAttributePanel(cell,graph_refresh);
}

/**
 * 基本节点属性
 * @param cell
 * @returns {string|*}
 */
function createBusinessRuleTaskNodeAttributeForm(cell){
    businessRuleTaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//是否使用规则
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >是否使用规则</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<select class=\"form-control\" id='excluded' name=\"excluded\">" +
								"<option value=''>请选择</option>" +
								"<option value='1'>是</option>" +
							"</select>"+
						"</div>"+
					"</div>"+

					//规则名称
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >规则名称</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入规则名称\" id=\"ruleName\" name=\"ruleName\" placeholder=\"请输入规则名称\">"+
						"</div>"+
					"</div>"+


					//输入变量
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >输入变量</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入输入变量\" id=\"ruleVariablesInput\" name=\"ruleVariablesInput\" placeholder=\"请输入输入变量\">"+
						"</div>"+
					"</div>"+

					//输出变量
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >输出变量</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入输出变量\" id=\"resultVariables\" name=\"resultVariables\" placeholder=\"请输入输出变量\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return businessRuleTaskNodeAttributeForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initBusinessRuleTaskData(cell){
    /**取值**/
    var excluded = cell.excluded;
    var ruleName = cell.ruleName;
    var ruleVariablesInput = cell.ruleVariablesInput;
    var resultVariables = cell.resultVariables;
    /**赋值**/
    $('#excluded').val(excluded);
    $('#ruleName').val(ruleName);
    $('#ruleVariablesInput').val(ruleVariablesInput);
    $('#resultVariables').val(resultVariables);
}


/**
 * 创建表单
 * @param cell
 * @param graph_refresh
 */
function businessRuleTaskNodeAttributePanel(cell,graph_refresh) {
    businessRuleTaskNodeAttributeForm = createBusinessRuleTaskNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell, 1);
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell);
    event_grid = creatEventGrid(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>" +
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>" +

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>" +

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">事件配置</a>" +

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">会签配置</a>" +

				"<a href='javascript:setBusinessRuleTaskValue()' class='svBtn'>保存配置</a>" +
			"</div>" +
        "</div>" +

        "<div class='col-md-11'>" +
			"<div class=\"tab-content tab-content-default\">" +
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">" + businessRuleTaskNodeAttributeForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + event_grid + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + multiInstanceLoopCharacteristicForm + "</div>" +
			"</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

    //基本配置
    initBusinessRuleTaskData(cell);

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);

    //共用taskGrid属性事件
    initevent_grid(cell, 1);

    //初始化会签数据
    initMultiInstanceData(cell);

    nodeScroll();
}


/**
 * 设置内容
 */
function setBusinessRuleTaskValue(){
    var excluded = $('#excluded').val();
    var ruleName = $('#ruleName').val();
    var ruleVariablesInput = $('#ruleVariablesInput').val();
    var resultVariables = $('#resultVariables').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        if(null != excluded && '' != excluded){
            JehcClickCell.excluded = excluded;
        }
        if(null != ruleName && '' != ruleName){
            JehcClickCell.ruleName = ruleName;
        }
        if(null != ruleVariablesInput && '' != ruleVariablesInput){
            JehcClickCell.ruleVariablesInput = ruleVariablesInput;
        }
        if(null != resultVariables && '' != resultVariables){
            JehcClickCell.resultVariables = resultVariables;
        }
        //4配置会签
        multi_instance_setvalue(JehcClickCell);
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}