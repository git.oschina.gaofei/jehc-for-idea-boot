/**
  *配置会签
**/
var multiInstanceLoopCharacteristicForm;

/**
 * 创建会签表单
 * @param cell
 */
function createMultiInstance(cell){
    var isSequentialOption =
        "<option value='0'>不启动多实例</option>"+
        "<option value='true'>顺序</option>"+
        "<option value='false'>并行</option>";

    multiInstanceLoopCharacteristicForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//状态
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id='isSequential' name=\"isSequential\">" +
        						isSequentialOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//循环数量
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >循环数量</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"loopCardinality\" name=\"loopCardinality\" placeholder=\"请循环数量\">"+
						"</div>"+
					"</div>"+

					//循环集合
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >循环集合</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"collection\" name=\"collection\" placeholder=\"请输入循环集合\">"+
						"</div>"+
					"</div>"+

					//元素名称
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >元素名称</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"elementVariable\" name=\"elementVariable\" placeholder=\"请输入元素名称\">"+
						"</div>"+
					"</div>"+

					//结束条件
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >结束条件</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"completionCondition\" name=\"completionCondition\" placeholder=\"请输入结束条件\">"+
						"</div>"+
					"</div>"+

       			 "</div>"+
			"</form>"+
        "</div>";
    return multiInstanceLoopCharacteristicForm;
}

/**
 * 初始化数据
 * @param cell
 */
function initMultiInstanceData(cell){
    /**取值**/
	var isSequential = cell.isSequential;
 	var loopCardinality = cell.loopCardinality;
 	var collection = cell.collection;
 	var elementVariable = cell.elementVariable;
 	var completionCondition = cell.completionCondition;
 	/**赋值**/
    $('#isSequential').val(isSequential);
    $('#loopCardinality').val(loopCardinality);
    $('#collection').val(collection);
    $('#elementVariable').val(elementVariable);
    $('#completionCondition').val(completionCondition);
}

/**
 * 赋值
 * @param cell
 */
function multi_instance_setvalue(cell){
	var isSequential = $('#isSequential').val();
	var loopCardinality = $('#loopCardinality').val();
	var collection = $('#collection').val();
	var elementVariable = $('#elementVariable').val();
	var completionCondition = $('#completionCondition').val();
 	if(null != isSequential && '' != isSequential){
    	cell.isSequential = isSequential;
    }
    if(null != loopCardinality && '' != loopCardinality){
    	cell.loopCardinality = loopCardinality;
    }
    if(null != collection && '' != collection){
    	cell.collection = collection;
    }
    if(null != elementVariable && '' != elementVariable){
    	cell.elementVariable = elementVariable;
    }
    if(null != completionCondition && '' != completionCondition){
    	cell.completionCondition = completionCondition;
    }
}