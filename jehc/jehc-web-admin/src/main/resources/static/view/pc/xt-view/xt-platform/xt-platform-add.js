//返回r
function goback(){
	tlocation(basePath+"/xtPlatformController/loadXtPlatform");
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addXtPlatform(){
	submitBForm('defaultForm',basePath+'/xtPlatformController/addXtPlatform',basePath+'/xtPlatformController/loadXtPlatform');
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

function addXtPlatformFeedbackItems(){
	validatorDestroy('defaultForm');
	var numbers = $('#xtPlatformFeedbackFormNumber').val();
	numbers = parseInt(numbers)+1;
	$('#xtPlatformFeedbackFormNumber').val(numbers);
	//点击添加新一行
	var removeBtn = '<a class="btn btn-secondary m-btn m-btn--custom m-btn--icon" href="javascript:delXtPlatformFeedbackItems(this,'+numbers+')" >删除该条信息</a>';
	var form = 
		'<div id="form_xtPlatformFeedback_'+numbers+'">'+
			'<fieldset>'+
				'<legend><h5>序号'+numbers+'</h5></legend>'+
					'<div class="form-group m-form__group row"><div class="col-md-1">'+removeBtn+'</div></div>'+
			'<div class="form-group m-form__group row">'+
				'<label class="col-md-1 control-label">评论内容</label>'+
				'<div class="col-md-3">'+
					'<textarea class="form-control" maxlength="500"  id="xtPlatformFeedback_'+numbers+'_xt_platform_feedback_remark" name="xtPlatformFeedback['+numbers+'].xt_platform_feedback_remark"  placeholder="请输入评论内容"></textarea>'+
				'</div>'+
			'</div>'+
				'</fieldset>'+
		'</div>'
	$(".form_xtPlatformFeedback").append(form);

	datetimeInit();
	reValidator('defaultForm');
}
function delXtPlatformFeedbackItems(thiz,numbers){
	validatorDestroy('defaultForm');
	$("#form_xtPlatformFeedback_"+numbers).remove();
	var xtPlatformFeedback_removed_flag = $('#xtPlatformFeedback_removed_flag').val()
	if(null == xtPlatformFeedback_removed_flag || '' == xtPlatformFeedback_removed_flag){
		$('#xtPlatformFeedback_removed_flag').val(','+numbers+',');
	}else{
		$('#xtPlatformFeedback_removed_flag').val(xtPlatformFeedback_removed_flag+numbers+',')
	}
	reValidator('defaultForm');
}
