var poolForm;

/**
 * 泳道池
 * @param cell
 * @param graph_refresh
 * @private
 */
function poolWin_(cell,graph_refresh){
    poolPanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @returns {string|*}
 */
function createPoolNodeAttributeForm(cell){
    poolForm =
        "<div class=\"m-portlet\" id='mportletId'  style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//流程编号
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >流程编号</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入流程编号\" id=\"processId_\" name=\"processId_\" placeholder=\"请输入流程编号\">"+
						"</div>"+
					"</div>"+

					//流程名称
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >流程名称</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入流程名称\" id=\"processName_\" name=\"processName_\" placeholder=\"请输入流程名称\">"+
						"</div>"+
					"</div>"+

					//命名空间
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >命名空间</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入命名空间\" value='http://www.activiti.org/test' id=\"poolnameSpace\" name=\"poolnameSpace\" placeholder=\"请输入命名空间\">"+
						"</div>"+
					"</div>"+

					//发起人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >发&nbsp;起&nbsp;&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请选择发起人\" id=\"candidateStarterUsers_\" name=\"candidateStarterUsers_\" placeholder=\"请选择发起人\">"+
						"</div>"+
					"</div>"+

					//发起人组
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >发起人组</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请选择发起人组\" id=\"candidateStarterGroups_\" name=\"candidateStarterGroups_\" placeholder=\"请选择发起人组\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return poolForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initPoolData(cell){
    var processId_ = cell.processId_;
    var processName_ = cell.processName_;
    var poolnameSpace = cell.poolnameSpace;
    var candidateStarterUsers_ = cell.candidateStarterUsers_;
    var candidateStarterGroups_ = cell.candidateStarterGroups_;
    $('#processId_').val(processId_);
    $('#processName_').val(processName_);
    if(null != poolnameSpace && '' != poolnameSpace){
        $('#poolnameSpace').val(poolnameSpace);
    }else{
        $('#poolnameSpace').val('http://www.activiti.org/test');
    }
    $('#candidateStarterUsers_').val(candidateStarterUsers_);
    $('#candidateStarterGroups_').val(candidateStarterGroups_);
    // initACC(candidateStarterUsers_,null,candidateStarterGroups_,3);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function poolPanel(cell,graph_refresh){
    poolForm = createPoolNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell,2);
    event_grid = creatEventGrid(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">流程配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href='javascript:setPoolValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+poolForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

    //基本配置
    initPoolData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    nodeScroll();
}


/**
 * 设置内容
 */
function setPoolValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        var processId_ = $('#processId_').val();
        var processName_ = $('#processName_').val();
        var poolnameSpace = $('#poolnameSpace').val();
        var candidateStarterUsers_ = $('#candidateStarterUsers_').val();
        var candidateStarterGroups_ = $('#candidateStarterGroups_').val();
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //4配置流程
        if(null != processId_ && "" != processId_){
            JehcClickCell.processId_ = processId_;
        }
        if(null != processName_ && '' != processName_){
            JehcClickCell.processName_ = processName_;
        }
        if(null != poolnameSpace && '' != poolnameSpace){
            JehcClickCell.poolnameSpace = poolnameSpace;
        }
        if(null != candidateStarterUsers_ && '' != candidateStarterUsers_){
            JehcClickCell.candidateStarterUsers_ = candidateStarterUsers_;
        }
        if(null != candidateStarterGroups_ && '' != candidateStarterGroups_){
            JehcClickCell.candidateStarterGroups_ = candidateStarterGroups_;
        }
        graph.startEditing();
        if(clickPOOLSureBtn(graph_refresh,JehcClickCell) == true){
            //赋值给流程基本属性
            var processId_ = JehcClickCell.processId_;
            var processName_ = JehcClickCell.processName_;
            var poolnameSpace = JehcClickCell.poolnameSpace;
            var candidateStarterUsers_ = JehcClickCell.candidateStarterUsers_;
            var candidateStarterGroups_ = JehcClickCell.candidateStarterGroups_;
            $('#processId').val(processId_);
            $('#processName').val(processName_);
            $('#mainNameSpace').val(poolnameSpace);
            $('#candidateStarterUsers').val(candidateStarterUsers_);
            $('#candidateStarterGroups').val(candidateStarterGroups_);
        }
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}
