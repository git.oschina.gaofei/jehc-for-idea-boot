function appendFormInfo(cell,graph_refresh){
    var cExpansion = "<div id='SSZK' style='position:absolute;width:50px;right: 0px;height: 20px;line-height: 20px;'><a href='javascript:doE()' class='flaticon2-up' style='text-decoration:none;color:#6c7293' id='sszkHref'>收缩</a></div>";
    if(null == cell || undefined === cell){
        window.setTimeout(function()
        {
            setContainerHeight = 0;
            ThizViewPort.editorUi.refresh();
        }, 100);

        var pickColors = $('#pickColors');
        if(null != pickColors && undefined != pickColors){
            // console.log("pickColors",pickColors);
            // $('#pickColors').colorpicker().off('changeColor');
            // $('#pickColors').hidePicker();
            // $('#pickColors').colorpicker('destroy');
            // $('#pickColors').colorpicker('update');
            $('.colorpicker').hide()
        }
        $("#geSetContainer").empty();
        // $("#geSetContainer").append(cExpansion);//收缩/展开
        // initSZ();

        return;
    }

    window.setTimeout(function()
    {
        setContainerHeight = 220;
        ThizViewPort.editorUi.refresh();
    }, 100);

    var baseContent =
        "<div class=\"m-portlet\" id='mportletId' style='height:160px;overflow: auto;width: 100%'>"+
            "<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
                "<div class=\"m-portlet__body\">"+
                    //键
                    "<div class=\"form-group m-form__group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >键</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" id='pixelKey' placeholder='暂无'  readonly>"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >图元类别</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" id='pixelType' placeholder='暂无'  readonly>"+
                        "</div>"+
                        "<div class=\"col-md-1\" id='stockFillLabel'>"+
                            "<label class=\"col-form-label\" >线条颜色</label>"+
                        "</div>"+
                        "<div class=\"col-md-1\" id='stockFillDiv'>"+
                            "<div id=\"pickColors\" class=\"input-group colorpicker-component\" title=\"颜色选择器\">" +
                                "<input type=\"text\" id='stockFill' class=\"form-control input-lg\" onchange='stockFillVal()' readonly/>" +
                                "<span class=\"input-group-addon\"><i></i></span>" +
                            "</div>"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<div class='m-radio-inline'><label class='m-checkbox m-checkbox--state-success'><input onclick=\"enableLink(this)\" id='EnableLink' type='checkbox'> 链接？<span></span></label></div>"+
                        "</div>"+
                    "</div>"+
                    //提示信息
                    "<div class=\"form-group m-form__group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >提示信息</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" placeholder='请输入' id=\"pixelMsg\">"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" id='pixelLabel' placeholder='暂无' onchange='setLabel(this)' type=\"text\" >"+
                        "</div>"+
                        "<div class=\"col-md-1\" id='fillFillLabel'>"+
                            "<label class=\"col-form-label\" >填充</label>"+
                        "</div>"+
                        "<div class=\"col-md-1\" id='fillFillDiv'>"+
                            "<div id=\"fillColors\" class=\"input-group colorpicker-component\" title=\"颜色选择器\">" +
                                "<input type=\"text\" id='fillFill' class=\"form-control input-lg\" readonly/>" +
                                "<span class=\"input-group-addon\"><i></i></span>" +
                            "</div>"+
                        "</div>"+
                    "</div>"+
                    //绑定名称
                    "<div class=\"form-group m-form__group row\" id='pixelDiv'>"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >绑定名称</label>"+
                        "</div>"+
                        "<div class=\"col-md-5\">"+
                            "<input class=\"form-control\" type=\"hidden\" id=\"pixelId\">"+
                            "<div class='form-group input-group'>" +
                                "<input class=\"form-control\" type=\"text\" maxlength=\"32\" readonly=\"readonly\" id =\"pixelName\"placeholder=\"请选择\">\n" +
                                "<span class=\"input-group-btn\">" +
                                    "<button class=\"btn btn-default\" type=\"button\" onclick=\"pixelSelect()\">选择</button>" +
                                "</span>" +
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>"+
            "</form>"+
        "</div>";
    var formInfo = "<div class='card-body' id='cardBody'><div class='row' style='height:200px;overflow: auto;width: 100%'>"+ baseContent+ "</div></div>"
    $("#geSetContainer").empty();
    // $("#geSetContainer").append(cExpansion);//收缩/展开
	$("#geSetContainer").append(formInfo);
    // initSZ();


    if(cell.nodeType == "Text" || undefined === cell.nodeType || cell.nodeType == "oilConsumption"){
        $("#fillFillLabel").hide();
        $("#fillFillDiv").hide();
        $("#stockFillLabel").hide();
        $("#stockFillDiv").hide();
        if(cell.nodeType == "oilConsumption"){
            $("#pixelLabel").attr("readonly","readonly");
        }
    }else{
        $("#pixelLabel").attr("readonly","readonly");
    }

    if(cell.edge ){
        $("#pixelDiv").hide();
        $("#fillFillLabel").hide();
        $("#fillFillDiv").hide();
        $("#stockFillLabel").hide();
        $("#stockFillDiv").hide();
    }
    if(cell.nodeType == 'onoffclose2' ||
        cell.nodeType == 'onoffopen2' ||
        cell.nodeType == 'onOffClose' ||
        cell.nodeType == 'onOffOpen' ||
        cell.nodeType == "point"){
        $("#pixelDiv").hide();
        // $("#fillFillLabel").hide();
        // $("#fillFillDiv").hide();
        $("#stockFillLabel").hide();
        $("#stockFillDiv").hide();
    }
    initBaseFormData();

    if(null != cell && undefined !== cell){
        var stockFill = cell.stockFill;//线条颜色
        if(null != stockFill && undefined !== stockFill){
            $('#stockFill').val(stockFill);
        }
        var fills = cell.fills;
        if(null != fills && undefined !== fills){
            $('#fillFill').val(fills);
        }
    }

    nodeScroll();
    // 基本实例化:
    $('#pickColors').colorpicker({
        // allowEmpty:true,//允许为空,显示清楚颜色按钮
        // color: "#DD0F20",//初始化颜色
        // showInput: true,//显示输入
        // containerClassName: "full-spectrum",
        // showInitial: true,//显示初始颜色,提供现在选择的颜色和初始颜色对比
        // showPalette: true,//显示选择器面板
        // showSelectionPalette: true,//记住选择过的颜色
        // showAlpha: true,//显示透明度选择
        // maxPaletteSize: 7,//记住选择过的颜色的最大数量
        // preferredFormat: "hex"//输入框颜色格式,(hex十六进制,hex3十六进制可以的话只显示3位,hsl,rgb三原//
        }).on("changeColor",function(ev){
            cell.stockFill = $('#stockFill').val();
            cell.fills = $('#fillFill').val();
            ChStockFill(cell,graph_refresh)
        });

    $('#fillColors').colorpicker({
        // allowEmpty:true,//允许为空,显示清楚颜色按钮
        // color: "#DD0F20",//初始化颜色
        // showInput: true,//显示输入
        // containerClassName: "full-spectrum",
        // showInitial: true,//显示初始颜色,提供现在选择的颜色和初始颜色对比
        // showPalette: true,//显示选择器面板
        // showSelectionPalette: true,//记住选择过的颜色
        // showAlpha: true,//显示透明度选择
        // maxPaletteSize: 7,//记住选择过的颜色的最大数量
        // preferredFormat: "hex"//输入框颜色格式,(hex十六进制,hex3十六进制可以的话只显示3位,hsl,rgb三原//
    }).on("changeColor",function(ev){
        cell.stockFill = $('#stockFill').val();
        cell.fills = $('#fillFill').val();
        ChFills(cell,graph_refresh)
    });

}

function initFormInfo(cell,graph_refresh){
    appendFormInfo(cell,graph_refresh);
}

function enableLink(thiz){
    if ( thiz.checked == true){
        JehcClickCell.EnableLink =true;
    }else{
        JehcClickCell.EnableLink = false;
    }
    ChFills( JehcClickCell,graph_refresh)
}

/**
 * 设置Form基本信息
 */
function setBaseFormInfo(){
    var pixelKey = $("#pixelKey").val();
    var pixelType = $("#pixelType").val();
    var pixelMsg = $("#pixelMsg").val();
    var pixelLabel = $("#pixelLabel").val();
    var pixelId = $("#pixelId").val();
    var pixelName = $("#pixelName").val();

    $("#pixelKey").val(pixelKey);
    $("#pixelType").val(pixelType);
    $("#pixelMsg").val(pixelMsg);
    $("#pixelLabel").val(pixelLabel);
    $("#pixelId").val(pixelId);
    $("#pixelName").val(pixelName);

    try
    {
        var graph = new mxGraph();
        graph.getModel().beginUpdate();
        if(null != pixelKey && "" != pixelKey){
            JehcClickCell.pixelKey = pixelKey;
        }
        if(null != pixelType && "" != pixelType){
            JehcClickCell.pixelType = pixelType;
        }
        if(null != pixelMsg && "" != pixelMsg){
            JehcClickCell.pixelMsg = pixelMsg;
        }
        if(null != pixelLabel && "" != pixelLabel){
            JehcClickCell.pixelLabel = pixelLabel;
        }
        if(null != pixelId && '' != pixelId){
            JehcClickCell.pixelId = pixelId;
        }
        if(null != pixelName && '' != pixelName){
            JehcClickCell.pixelName = pixelName;
        }
        if($('#EnableLink').is(':checked')) {
            JehcClickCell.EnableLink = true;
        }else{
            JehcClickCell.EnableLink = false;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

/**
 * 初始化基础Form数据
 */
function initBaseFormData(){
    // console.log("CELL",JehcClickCell);
    if(null != JehcClickCell && undefined != JehcClickCell){
        var pixelKey = JehcClickCell.pixelKey;
        var pixelType = JehcClickCell.pixelType;
        var pixelMsg = JehcClickCell.pixelMsg;
        var pixelLabel = JehcClickCell.value;
        var pixelId = JehcClickCell.pixelId;;
        var pixelName = JehcClickCell.pixelName;
        $("#pixelKey").val(pixelKey);
        $("#pixelType").val(pixelType);
        $("#pixelMsg").val(pixelMsg);
        $("#pixelLabel").val(pixelLabel);
        $("#pixelId").val(pixelId);
        $("#pixelName").val(pixelName);
        var EnableLink = JehcClickCell.EnableLink;
        if(EnableLink == true) {
            $("#EnableLink").attr("checked","checked");
        }
    }
}

//设备选择器
function pixelSelect(){
    var pixelSelectModalCount = 0 ;
    // $('#lcDesignPanelBody').height(reGetBodyHeight()*0.9);
    // $('#lcDesignModalLabel').html("在线设计---<font color=red>"+lc_process_title+"</font>");
    $('#pixelSelectModal').modal({backdrop:'static',keyboard:false});
    // $("#lcDesignIframe",document.body).attr("src",base_html_redirect+'/pc/lc-view/lc-design/lc-design.html?lc_process_id='+lc_process_id)
    $('#pixelSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // // 是弹出框居中。。。
        // var $modal_dialog = $("#lcDesignModalDialog");
        // $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
    	
    	$("#substation").html("");;
    	 var str = "<option value=''>请选择</option>";
    	    $.ajax({
    	        type:"GET",
    	        url:baseUrl+'substations',
    	        dataType:"JSON",
    	        async:false,
    	        xhrFields:{withCredentials:true},
    	        success:function(data){
    	        	console.log(data);
    	            //从服务器获取数据进行绑定
    	            $.each(data, function(i, item){
    	            	 str += "<option value='" + item.id + "'>" + item.name + "</option>";
    	            })
    	            //将数据添加到省份这个下拉框里面
    	           $("#substation").append(str);
    	        },
    	        error:function(){}
    	    });
    	    
    	    

        if(++pixelSelectModalCount == 1){
            $('#searchPixelForm')[0].reset();
            var opt = {
                searchformId:'searchPixelForm'
            };
            var options = DataTablesList.listOptions({
                ajax:function (data, callback, settings){datatablesListGetMethodCallBack(data, callback, settings,baseUrl+'devices',opt);},//渲染数据
                //在第一位置追加序列号
//                fnRowCallback:function(nRow, aData, iDisplayIndex){
//                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
//                    return nRow;
//                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
//                    {
//                        sClass:"text-center",
//                        width:"20px",
//                        data:"id",
//                        render:function (data, type, full, meta) {
//                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildDeletedUserinfo" value="' + data + '" /><span></span></label>';
//                        },
//                        bSortable:false
//                    },
//                    {
//                        data:"id",
//                        width:"20px"
//                    },
					{
                        data:"id",
                        width:"80px",
        				render:function(data, type, row, meta) {
        					var id = row.id;
          					var name = row.name;
        					var btn = '<button class="btn btn-default" onclick=doSelect("'+id+'","'+name+'")>确定</button>';
        					return btn;
        				}
                    },
                    {
                        data:"id",
                        width:"20px"
                    },
                    {
                        data:'type'
                    },
                    {
                        data:'name'
                    },
                    {
                        data:'attributes'
                    }
                ]
            });
            grid=$('#pixelDataTables').dataTable(options);
            //实现单击行选中
            clickrowselected('pixelDataTables');
        }
    });
}

function doSelect(id,name,cell){
//	msgTishCallFnBoot("确定选择该数据？",function(){
		$('#pixelId').val(id);
		$('#pixelName').val(name);
		if(null != JehcClickCell && undefined != JehcClickCell){
			JehcClickCell.pixelId = id;;
	        JehcClickCell.pixelName = name;
	    }
		$('#pixelSelectModal').modal('hide');
//	})
}

function doE(){
    var isSZ = $("#isSZ").val();
    if(isSZ == 0){
        //执行收缩
        // $("#geSetContainer").css("height","20px");
        // $(".geDiagramContainer").css("bottom","20px");
        setContainerHeight = 20;
        $("#isSZ").val("1");
        $("#sszkHref").text('展开');
        // $("#sszkHref").attr("class","fa fa-angle-down");
        $("#sszkHref").attr("class","fa fa-angle-double-down");
        $("#cardBody").hide();
    }else{
        //执行展开
        // $("#geSetContainer").css("height","220px");
        // $(".geDiagramContainer").css("bottom","220px");
        setContainerHeight = 220;
        $("#isSZ").val("0");
        $("#sszkHref").text('合并');
        // $("#sszkHref").attr("class","fa fa-angle-up");
        $("#sszkHref").attr("class","fa fa-angle-double-up");
        $("#cardBody").show();
    }
    ThizViewPort.editorUi.refresh();
}

function initSZ(){
    var isSZ = $("#isSZ").val();
    if(isSZ == 0){
        $("#sszkHref").text('合并');
        // $("#sszkHref").attr("class","fa fa-angle-up");
        $("#sszkHref").attr("class","fa fa-angle-double-up");
        $("#cardBody").show();
    }else{
        $("#sszkHref").text('展开');
        // $("#sszkHref").attr("class","fa fa-angle-down");
        $("#sszkHref").attr("class","fa fa-angle-double-down");
        $("#cardBody").hide();
    }
}


function setLabel(thiz){
    try
    {
        var graph = new mxGraph();
        graph.getModel().beginUpdate();
        JehcClickCell.value = thiz.value;
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
        resetZxLine(graph_refresh)
    }
}