//基本流程中事件配置
//事件表格
var event_main_grid;

/**
 * 创建基础Form grid Event
 */
function createEventMainGrid(){
    event_main_grid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventMainRow()\">新一行</button>"+
					"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='event_main_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_main_event\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='node_event_main_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-2\">执行的类或表达式</div>"+
						"<div class=\"col-md-1\">类型</div>"+
						"<div class=\"col-md-1\">事件</div>"+
						"<div class=\"col-md-6\">配置字段</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return event_main_grid;
}

/**
 * 新一行
 * @param flag
 * @param javaclass_express
 * @param event_type
 * @param event
 * @param fields
 */
function addEventMainRow(flag,javaclass_express,event_type,event,fields){
    if(javaclass_express === undefined){
        javaclass_express = "";
    }
    if(event_type === undefined){
        event_type = "";
    }
    if(event === undefined){
        event = "";
    }
    if(fields === undefined){
        fields = "";
    }
    var uuid = guid();
    validatorDestroy('node_main_event');
    var event_typeOption;
    var event_option;
    if(flag == 1){
        event_option =  "<option value=''>请选择</option>" +
            "<option value='start'>start</option>" +
            "<option value='end'>end</option>";
    }else if(flag == 3){
        event_option =  "<option value=''>请选择</option>" +"<option value='take'>take</option>";
    }else{
        event_option = "<option value=''>请选择</option>" +
            "<option value='create'>create</option>" +
            "<option value='assignment'>assignment</option>"+
            "<option value='complete'>complete</option>"+
            "<option value='all'>all</option>";
    }
    var event_typeOption = "<option value=''>请选择</option>" +
        "<option value='javaclass'>javaclass</option>" +
        "<option value='express'>express</option>"+
        "<option value='delegateExpression'>delegateExpression</option>"+
        "<option value='ScriptExecutionListener'>ScriptExecutionListener</option>"+
        "<option value='ScriptTaskListener'>ScriptTaskListener</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_event_main_rows_"+uuid+"'>"+
			//执行的类或表达式
			"<div class=\"col-md-2\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='javaclass_express"+uuid+"' name='javaclass_express' value='"+javaclass_express+"' placeholder='请输入'>"+
			"</div>"+

			//类型
			"<div class=\"col-md-1\">"+
				"<select class=\"form-control\" id='event_type"+uuid+"' name=\"event_type\">" +
				event_typeOption+
				"</select>"+
			"</div>"+

			//事件
			"<div class=\"col-md-1\">"+
				"<select class=\"form-control\" id='event"+uuid+"' name=\"event\">" +
				event_option+
				"</select>"+
			"</div>"+

			//配置字段
			"<div class=\"col-md-6\">"+
				"<input class=\"form-control\" type=\"text\" readonly='readonly' onclick='showFieldMainWin(this,\""+uuid+"\")' id='fields"+uuid+"' name=\"fields\" value='"+fields+"' placeholder=\"请配置字段\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventMainRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#node_event_main_grid").append(rows);
    $("#event_type"+uuid).val(event_type);
    $("#event"+uuid).val(event);
    reValidator('node_main_event');
}

/**
 * 删除
 * @param rowID
 */
function delEventMainRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_main_event');
        $("#node_event_main_rows_"+rowID).remove();
        reValidator('node_main_event');
    });
}

/**
 * 初始化init event grid
 * @param cell
 * @param flag
 */
function initeventmain_grid(flag){
    //表单配置
    $('#node_main_event').bootstrapValidator({
        message:'此值不是有效的'
    });
    initeventmain_data(flag)
}


/**
 *
 * @param cell
 * @param flag
 */
function initeventmain_data(flag){
    var event_node_value = $("#eventMainNode").val();
    if(null != event_node_value && "" != event_node_value){
        event_node_value = eval('(' + event_node_value + ')');
        var javaclass_expressList = $.makeArray(event_node_value["javaclass_express"]);
        var event_typeList = $.makeArray(event_node_value["event_type"]);
        var eventList = $.makeArray(event_node_value["event"]);
        var fieldsList = $.makeArray(event_node_value["fields"]);
        for(var i = 0; i < javaclass_expressList.length; i++){
            var javaclass_express = javaclass_expressList[i];
            var event_type = event_typeList[i];
            var event = eventList[i];
            var fields = fieldsList[i];
            addEventMainRow(flag,javaclass_express,event_type,event,fields)
        }
    }
}


/**
 * 配置事件字段
 * @param thiz
 * @param id
 */
var fieldMainGrid;
function showFieldMainWin(thiz,id){
    $('#jehcLcModalLabel').empty();
    $('#jehcLcForm').empty();
    $('#jehcLcModalLabel').append("配置事件字段属性");
    fieldMainGrid = creatFieldMainGrid(id);
    $("#jehcLcForm").append(fieldMainGrid);
    $('#eventMainField').bootstrapValidator({
        message:'此值不是有效的'
    });
    initEventMainField(id);//初始化数据
    $('#jehcLcModal').modal({backdrop: 'static', keyboard: false});
}


/**
 * 创建事件字段
 * @param cell
 */
function creatFieldMainGrid(id){
    fieldMainGrid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addEventMainFieldRow()\">新一行</button>"+
					"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick='saveEventMainField(\""+id+"\")'>保存</button>"+
					"&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"closeJehcLcWin()\">关闭窗体</button>"+
				"</div>"+
			"</div>"+

		"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventMainField\" method=\"post\">"+
			"<div class=\"m-portlet__body\" id='event_main_fields_grid'>"+
				//Title
				"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
					"<div class=\"col-md-3\">字段名称</div>"+
					"<div class=\"col-md-2\">字段类型</div>"+
					"<div class=\"col-md-4\">字段值</div>"+
					"<div class=\"col-md-1\">操作</div>"+
				"</div>"+
			"</div>"+
        "</form>"+
        "</div>";
    return fieldMainGrid;
}


/**
 * 初始化事件字段数据
 * @param id
 */
function initEventMainField(id){
    var fields = $("#fields"+id).val();
    if(null != fields && "" != fields){
        fields = eval('(' + fields + ')');
        var fieldNameList = $.makeArray(fields["fieldName"]);
        var fieldtypeList = $.makeArray(fields["fieldtype"]);
        var fieldValueList = $.makeArray(fields["fieldValue"]);
        for(var i = 0; i < fieldNameList.length; i++){
            var fieldName = fieldNameList[i];
            var fieldtype = fieldtypeList[i];
            var fieldValue = fieldValueList[i];
            addEventMainFieldRow(fieldName,fieldtype,fieldValue);
        }
    }
}


/**
 * 动态添加 新一行 ”事件字段“
 * @param fieldName
 * @param fieldtype
 * @param fieldValue
 */
function addEventMainFieldRow(fieldName,fieldtype,fieldValue){
    validatorDestroy('eventMainField');
    var uuid = guid();
    if(fieldName === undefined){
        fieldName = "";
    }
    if(fieldtype === undefined){
        fieldtype = "";
    }
    if(fieldValue === undefined){
        fieldValue = "";
    }
    var fieldtypeOption =
        "<option value=''>请选择</option>" +
        "<option value='string'>string</option>"+
        "<option value='expression'>expression</option>";
    var rows =
        "<div class=\"form-group m-form__group row\" id='event_main_fields_rows_"+uuid+"'>"+
			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入字段名称' id='fieldName"+uuid+"' value='"+fieldName+"' name='fieldName' placeholder='请输入字段名称'>"+
			"</div>"+

			//类型
			"<div class=\"col-md-2\">"+
				"<select class=\"form-control\" id='fieldtype"+uuid+"' name=\"fieldtype\">" +
					fieldtypeOption+
				"</select>"+
			"</div>"+

			//值
			"<div class=\"col-md-4\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入值\" id='fieldValue"+uuid+"' value='"+fieldValue+"' name='fieldValue' placeholder='请输入值'>"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delEventMainFieldRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#event_main_fields_grid").append(rows);
    $("#fieldtype"+uuid).val(fieldtype);
    reValidator('eventMainField');
}


/**
 * 保存配置字段
 * @param id
 */
function saveEventMainField(id){
    var bootform = $('#eventMainField');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#fields"+id).val(JSON.stringify($("#eventMainField").serializeObject()))
    closeJehcLcWin();
}

/**
 * 删除事件字段
 * @param id
 */
function delEventMainFieldRow(id){
    validatorDestroy('eventMainField');
    $("#event_main_fields_rows_"+id).remove();
    reValidator('eventMainField');
}


/**
 * 点击确定按钮设置mxgraph中cell属性
 * @param cell
 * @returns {boolean}
 */
function event_main_setvalue(cell){
    var bootform = $('#node_main_event');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var event_node_value = JSON.stringify($("#node_main_event").serializeObject());
    if(null != event_node_value && "" != event_node_value){
        $("#eventMainNode").val(event_node_value);
    }else{
        $("#eventMainNode").val("");
    }
    return true;
}