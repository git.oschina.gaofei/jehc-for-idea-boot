//基本配置
var TaskNodeAttributeForm;
//配置表单
var TaskNodeAttributeFormSenior;

/**
 * 任务节点[task]
 * @param cell
 * @param graph_refresh
 */
function userTaskNodeAttributeWin(cell,graph_refresh){
    taskNodeAttributePanel(cell,graph_refresh);
}

/**
 * 创建用户任务  “审批人” 属性
 * @param cell
 * @returns {string|*}
 */

function createUserTaskNodeAttributeForm(cell){
    TaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"userNodeForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//处理人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >处&nbsp;&nbsp;理&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"assignee\" name=\"assignee\" placeholder=\"请选择处理人\">"+
						"</div>"+
					"</div>"+

					//候选人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >候&nbsp;选&nbsp;&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"candidateUsers\" name=\"candidateUsers\" placeholder=\"请选择候选人\">"+
						"</div>"+
					"</div>"+

					//候选群组
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >候选群组</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"candidateGroups\" name=\"candidateGroups\" placeholder=\"请选择候选群组\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return TaskNodeAttributeForm;
}

/**
 * 基本配置
 * @param cell
 */
function createTaskNodeAttributeFormSenior(cell){
    var isUseExpressionOption =
        "<option value='2'>否</option>"+
        "<option value='1'>是</option>";
    TaskNodeAttributeFormSenior =
        "<div class=\"m-portlet\" id='mportletId1'  style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//表单键
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
						"<label class=\"control-label\" >表&nbsp;单&nbsp;&nbsp;键</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
						"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"formKey\" name=\"formKey\" placeholder=\"请输入表单键\">"+
						"</div>"+
					"</div>"+

					//截止日期
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >截止日期</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"dueDate\" name=\"dueDate\" placeholder=\"请选择截止日期\">"+
						"</div>"+
					"</div>"+

					//使用表达式
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >是否使用表达式</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id='isUseExpression' name=\"isUseExpression\">" +
        						isUseExpressionOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//表达式
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >表&nbsp;达&nbsp;&nbsp;式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"Expression\" name=\"Expression\" placeholder=\"请输入\">"+
						"</div>"+
					"</div>"+

					//分类
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;类</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"category\" name=\"category\" placeholder=\"请输入\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return TaskNodeAttributeFormSenior;
}

/**
 * 人工任务
 * @param cell
 */
function initUserTaskData(cell){
	//初始化配置用户数据
    var assignee = cell.assignee;
    var candidateUsers = cell.candidateUsers;
    var candidateGroups = cell.candidateGroups;
    $('#assignee').val(assignee);
    $('#candidateUsers').val(candidateUsers);
    $('#candidateGroups').val(candidateGroups);
    // initACC(assignee,candidateUsers,candidateGroups,1);

	//初始化基本配置
    var formKey = cell.formKey;
    var dueDate = cell.dueDate;
    var priority = cell.priority;
    var Expression = cell.Expression;
    var isUseExpression = cell.isUseExpression;
    var category = cell.category;
    $('#formKey').val(formKey);
    $('#dueDate').val(dueDate);
    $('#priority').val(priority);
    $('#Expression').val(Expression);
    if(isUseExpression == 1){
        $('#isUseExpression').val(1);
    }else{
        $('#isUseExpression').val(2);
    }
    $('#category').val(category);
}

/**
 * 初始化面板属性
 * @param cell
 * @param graph_refresh
 */
function taskNodeAttributePanel(cell,graph_refresh){
    TaskNodeAttributeForm = createUserTaskNodeAttributeForm(cell);
    TaskNodeAttributeFormSenior = createTaskNodeAttributeFormSenior(cell);
    nodeNormalForm = createNodeNormalForm(cell,1);
    form_grid = creatFormGrid(cell);
    event_grid = creatEventGrid(cell);
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">表单配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">用户配置</a>"+

				"<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href=\"#v-pills-messages6\" data-toggle=\"pill\" class=\"\">会签配置</a>"+

				"<a href='javascript:setUserTaskValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
		"</div>"+

		"<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
			"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+TaskNodeAttributeFormSenior+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+form_grid+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+TaskNodeAttributeForm+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages5\">"+event_grid+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages6\">"+multiInstanceLoopCharacteristicForm+"</div>"+
		"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

    //基本配置,用户配置
    initUserTaskData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //表单配置信息
    initform_grid(cell);
    //共用taskGrid属性事件
    initevent_grid(cell,2);
	//初始化会签数据
    initMultiInstanceData(cell);
    nodeScroll();
}

/**
 * 设置内容
 */
function setUserTaskValue(){
    /**
     *assignee处理人
     *candidateUsers候选人ID
     *candidateGroups候选群组ID
     **/
    var assignee = $('#assignee').val();
    var candidateUsers = $('#candidateUsers').val();
    var candidateGroups = $('#candidateGroups').val();
    /**
     *表单配置
     **/
    var formKey = $('#formKey').val();
    var dueDate = $('#dueDate').val();
    var priority = $('#priority').val();
    var Expression = $('#Expression').val();
    var isUseExpression = $('#isUseExpression').val();
    var category = $('#category').val();
    try
    {
		var graph = new mxGraph();
		graph.getModel().beginUpdate();

		//1.通用基本配置并具有赋值功能
		if(node_normal_setvalue(JehcClickCell,1)== false){
			return;
		}

		//2.事件配置
		if(event_setvalue(JehcClickCell)== false){
			return;
		}

		//3.配置用户
		if(null != assignee && "" != assignee){
			JehcClickCell.assignee = assignee;
		}
		if(null != candidateUsers && "" != candidateUsers){
			JehcClickCell.candidateUsers = candidateUsers;
		}
		if(null != candidateGroups && "" != candidateGroups){
			JehcClickCell.candidateGroups = candidateGroups;
		}

		//4.配置表单
		if(null != formKey && "" != formKey){
			JehcClickCell.formKey = formKey;
		}
		if(null != dueDate && '' != dueDate){
			JehcClickCell.dueDate = dueDate;
		}
		if(null != priority && '' != priority){
			JehcClickCell.priority = priority;
		}
		if(null != Expression && '' != Expression){
			JehcClickCell.Expression = Expression;
		}
		if(null != isUseExpression && '' != isUseExpression){
			JehcClickCell.isUseExpression = isUseExpression;
		}else{
			JehcClickCell.isUseExpression = "2"
		}
		if(null != category && '' != category){
			JehcClickCell.category = category;
		}

		//5.配置会签
		multi_instance_setvalue(JehcClickCell);

		//6.配置表单事件
		if(event_form_setvalue(JehcClickCell) == false){
			return;
		}
		graph.startEditing();
	}
	finally
	{
		graph.getModel().endUpdate();
		graph_refresh.refresh();
	}
}


	