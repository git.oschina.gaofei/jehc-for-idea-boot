/**
 * 定时启动事件
 */
var timerStartEventForm;
function timerStartEventWin_(cell,graph_refresh){
    timerStartEventNodeAttributePanel(cell,graph_refresh);
}

/**
 * @param cell
 * @returns {string|*}
 */
function createTimerStartEventAttributeForm(cell){
    timerStartEventForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+

					//持续时间
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >持续时间</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入持续时间\" id=\"timeDuration\" name=\"timeDuration\" placeholder=\"请输入时间段\">"+
						"</div>"+
					"</div>"+

					//时间日期
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >时间日期</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" id=\"timeDuration\" name=\"timeDuration\" placeholder=\"请输入时间日期\">"+
						"</div>"+
					"</div>"+

					//时间转换
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >时间转换</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" id=\"timeCycle\" name=\"timeCycle\" placeholder=\"时间转换\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return timerStartEventForm;
}

/**
 *
 */
function initTimerBoundaryEventData(cell){
    var timeDuration = cell.timeDuration;
    var timeDate = cell.timeDate;
    var timeCycle = cell.timeCycle;
    $('#timeDuration').val(timeDuration);
    $('#timeDate').val(timeDate);
    $('#timeCycle').val(timeCycle);
}


/**
 *
 * @param cell
 * @param graph_refresh
 */
function timerStartEventNodeAttributePanel(cell,graph_refresh){
    timerStartEventForm = createTimerStartEventAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell,2);
    event_grid = creatEventGrid(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href='javascript:setTimerStartEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+timerStartEventForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);

    //基本配置
    initTimerStartEventData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    nodeScroll();
}

/**
 *
 */
function initTimerStartEventData(cell){
    var timeDuration = cell.timeDuration;
    var timeDate = cell.timeDate;
    var timeCycle = cell.timeCycle;
    $('#timeDuration').val(timeDuration);
    $('#timeDate').val(timeDate);
    $('#timeCycle').val(timeCycle);
}

/**
 * 设置内容
 */
function setTimerStartEventValue(){
    //var attachedToRef = $('#attachedToRef').val();
    var timeDuration = $('#timeDuration').val();
    var timeDate = $('#timeDate').val();
    var timeCycle = $('#timeCycle').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        /**
         if(null != attachedToRef && "" != attachedToRef){
			cell.attachedToRef = attachedToRef;
		}
         **/
        if(null != timeDuration && "" != timeDuration){
            cell.timeDuration = timeDuration;
        }
        if(null != timeDate && "" != timeDate){
            cell.timeDate = timeDate;
        }
        if(null != timeCycle && "" != timeCycle){
            cell.timeCycle = timeCycle;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}