function dragCell(graph,cells,x,y,target,event){
	graph.getModel().beginUpdate();
	try{
		var parent = graph.getDefaultParent();  
//		var pt = graph.getFreeInsertPoint();
//		var x = pt.x;
//		var y = pt.y;
//		initCellsSytle(graph);
   		cells = graph.getImportableCells(cells);
   		//graph.getDefaultParent().node_type = node_type;/////////////dengcj注释暂时不用设置
        var model = graph.getModel();  
        var node_type = cells[0].node_type;
        var mcell;
        var label = cells[0].value;
        var image = cells[0].image;
        //使用时候image4gray为上述定义
        //console.log(node_type);
        if(node_type == 'select'){
        	//重新选择
        	graph.panningHandler.useLeftButtonForPanning = false;
			graph.setConnectable(false);
			toastrBoot(4,"不支持该操作!");
        	return;
        }else if(node_type == 'zxline'){
        	//直线连接
        	graph.panningHandler.useLeftButtonForPanning = false;
			graph.setConnectable(true);
        	connectEdge('strokeWidth=3;labelBackgroundColor=white;fontStyle=1');
        	toastrBoot(4,"不支持该操作!");
       		return;
        }else if(node_type == 'zjline'){
        	//直角连接
        	graph.panningHandler.useLeftButtonForPanning = false;
			graph.setConnectable(true);
        	connectEdge('edgeStyle=orthogonalEdgeStyle;');
        	toastrBoot(4,"不支持该操作!");
        	return;
        }else if(node_type == 'qxline'){
        	//曲线连接
        	graph.panningHandler.useLeftButtonForPanning = false;
			graph.setConnectable(true);
        	connectEdge('edgeStyle=elbowEdgeStyle;elbow=horizontal;orthogonal=0;entryPerimeter=1;');
        	toastrBoot(4,"不支持该操作!");
        	return;
        }else if(node_type == 'xxline'){
        	//虚线连接
        	graph.panningHandler.useLeftButtonForPanning = false;
			graph.setConnectable(true);
        	connectEdge('crossover');
        	toastrBoot(4,"不支持该操作!");
        	return;
        }else if(node_type == 'startEvent' || node_type == 'timerStartEvent' || node_type == 'messageStartEvent' || node_type == 'errorStartEvent' || node_type == 'signalStartEvent'){
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道池中
        		if(node_type == 'startEvent'){
        			//排除开始节点不能放任事件子流程中
        			if(target.node_type == 'eventSubProcess'){
        				toastrBoot(4,"开始事件不能放入事件子流程节点中、请重新选择!");
	            		return;
        			}else{
        				var xyArray = resultXy(x,y,graph,target);
        				mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'start_image4gray;rounded=true;strokeColor=none;fillColor=yellow;size=12'); 
        			}
        		}
        		if(node_type == 'timerStartEvent'){
        			//排除定时启动事件不能放任事件子流程中
        			if(typeof(target) != "undefined" && target.node_type == 'eventSubProcess'){
        				toastrBoot(4,"定时启动事件不能放入事件子流程节点中、请重新选择!");
	            		return;
        			}else{
        				var xyArray = resultXy(x,y,graph,target);
        				mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'timerStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        			}
        		}
        		if(node_type == 'messageStartEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'messageStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		}
        		if(node_type == 'errorStartEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			if(typeof(target) != "undefined" && null != target && (target.node_type == 'eventSubProcess')){
	            		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'errorStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
	            	}else{
	            		toastrBoot(4,"错误启动事件只能放入事件子流程节点中、请重新选择!");
	            		return;
	            	}
        		}
        		if(node_type == 'signalStartEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'signalStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		}
        	}else{
        		if(node_type == 'startEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'start_image4gray;rounded=true;strokeColor=none;fillColor=yellow;'); 
        		}
        		if(node_type == 'timerStartEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'timerStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		}
        		if(node_type == 'messageStartEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'messageStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		}
        		if(node_type == 'errorStartEvent'){
        			if(typeof(target) != "undefined" && null != target && (target.node_type == 'eventSubProcess')){
	            		mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'errorStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
	            	}else{
	            		toastrBoot(4,"错误启动事件只能放入事件子流程节点中、请重新选择!");
	            		return;
	            	}
        		}
        		if(node_type == 'signalStartEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'signalStartEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		}
        	}
        }else if(node_type == 'endEvent' || node_type == 'errorEndEvent' || node_type == 'terminateEndEvent' || node_type == 'cancelEndEvent'){
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		if(node_type == 'endEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'end_image4gray;rounded=true;strokeColor=none;fillColor=yellow;');
        		}
        		if(node_type == 'errorEndEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'errorEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        		if(node_type == 'terminateEndEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'terminateEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        		if(node_type == 'cancelEndEvent'){
        			var xyArray = resultXy(x,y,graph,target);
        			mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'cancelEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        	}else{
        		if(node_type == 'endEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'end_image4gray;rounded=true;strokeColor=none;fillColor=yellow;');
        		}
        		if(node_type == 'errorEndEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'errorEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        		if(node_type == 'terminateEndEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'terminateEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        		if(node_type == 'cancelEndEvent'){
        			mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'cancelEndEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        		}
        	} 
        }else if(node_type == 'timerBoundaryEvent'){
        	console.log("timerBoundaryEvent drag to Task ---target----",target);
        	//时间边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'userTask' || target.node_type == 'manualTask' 
        		|| target.node_type == 'scriptTask' 
        		|| target.node_type == 'serviceTask' || target.node_type == 'mailTask' 
        		|| target.node_type == 'receiveTask' || target.node_type == 'businessRuleTask' 
        		|| target.node_type == 'callActivity' || target.node_type == 'eventSubProcess' 
        		|| target.node_type == 'transactionProcess' || target.node_type == 'subProcess')){
        		/**左边**/
        		//mcell = graph.insertVertex(target, image, label, 0, 0, 24, 24,'timerBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;labelPosition=left;align=right'); 
        		/**右边**/
        		//mcell = graph.insertVertex(target, image, label, 0, 0, 24, 24,'timerBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top'); 
        		/**下边**/
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;timerBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        		/**上边**/
        		//mcell = graph.insertVertex(target, image, label, 0, 0, 24, 24,'timerBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=top;verticalAlign=bottom'); 
        	}else{
        		toastrBoot(4,"时间边界事件只能放入任务节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'errorBoundaryEvent'){
        	//错误边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'scriptTask' || target.node_type == 'serviceTask' 
        		|| target.node_type == 'mailTask' || target.node_type == 'callActivity'
        		|| target.node_type == 'eventSubProcess' 
        		|| target.node_type == 'transactionProcess' || target.node_type == 'subProcess')){
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;errorBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        		
        	}else{
        		toastrBoot(4,"错误边界事件只能放入任务节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'messageBoundaryEvent'){
        	//消息边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'userTask' || target.node_type == 'manualTask' 
        		|| target.node_type == 'scriptTask' || target.node_type == 'serviceTask' 
        		|| target.node_type == 'mailTask' || target.node_type == 'receiveTask' 
        		|| target.node_type == 'businessRuleTask' || target.node_type == 'callActivity'
        		|| target.node_type == 'eventSubProcess' 
        		|| target.node_type == 'transactionProcess' || target.node_type == 'subProcess')){
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;messageBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        	}else{
        		toastrBoot(4,"消息边界事件只能放入任务节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'cancelBoundaryEvent'){
        	//取消边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'transactionProcess')){
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;cancelBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        	}else{
        		toastrBoot(4,"取消边界事件只能放入事物流程节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'compensationBoundaryEvent'){
        	//补偿边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'userTask' 
        		|| target.node_type == 'manualTask' || target.node_type == 'scriptTask' || target.node_type == 'serviceTask' 
        		|| target.node_type == 'mailTask' || target.node_type == 'receiveTask' || target.node_type == 'businessRuleTask' 
        		|| target.node_type == 'callActivity')){
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;compensationBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        	}else{
        		toastrBoot(4,"补偿边界事件只能放入任务节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'signalBoundaryEvent'){
        	//信号边界
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'userTask' 
        		|| target.node_type == 'manualTask' || target.node_type == 'scriptTask' || target.node_type == 'serviceTask' 
        		|| target.node_type == 'mailTask' || target.node_type == 'receiveTask' || target.node_type == 'businessRuleTask' 
        		|| target.node_type == 'callActivity' || target.node_type == 'eventSubProcess' || target.node_type == 'subProcess' 
        		|| target.not_type == 'transactionProcess')){
        		mcell = graph.insertVertex(target, null, label, 0, 0.5, 24, 24,'gradientColor=#ffefbb;signalBoundaryEvent_image4gray;rounded=true;strokeColor=none;fillColor=ffefbb;verticalLabelPosition=bottom;verticalAlign=top'); 
        		mcell.geometry.offset = new mxPoint(-10, -10);
				mcell.geometry.relative = true;
        	}else{
        		toastrBoot(4,"信号边界事件只能放入任务，事件子流程，事物流程或子流程节点中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'eventSubProcess' || node_type == 'transactionProcess' || node_type == 'subProcess'){
        	if(typeof(target) != "undefined" && null != target && target.node_type == 'lane'){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 420, 180,'other_style;gradientColor=#fff;fillColor=#fff;');  
        	}else if(typeof(target) != "undefined" && null != target 
        		&&(target.node_type == 'eventSubProcess' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess')){
        		//如果在节点在事物子流程 事件子流程 子流程等节点中则也可以
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 220, 120,'other_style;gradientColor=#fff;fillColor=#fff;');  
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 540, 180,'other_style;gradientColor=#fff;fillColor=#fff;'); 
        	}
        	mcell.setConnectable(true); 
        }else if(node_type == 'pool'){
	        mcell = graph.insertVertex(parent, null, label, x, y, 540, 180,'gradientColor=#fff;fillColor=#fff;pool_style;');  
	        mcell.setConnectable(false);  
	        var nums = new Date().getTime();
       		mcell.processId_ = 'process'+nums;
       		mcell.processName_ = '流程名称';
        }else if(node_type == 'lane'){
        	console.log("drag to pool target-------",target);
        	if(typeof(target) != "undefined" && null != target && target.node_type == 'pool'){
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 540, 180,'gradientColor=#fff;fillColor=#fff;pool_style');  
	        	mcell.setConnectable(false);
        	}else{
        		toastrBoot(4,"泳道必须放入泳道池中、请重新选择!");
        		return;
        	}
        }else if(node_type == 'timerCatchingEvent' || node_type == 'signalCatchingEvent' || node_type == 'messageCatchingEvent' || node_type == 'signalThrowingEvent' || node_type == 'compensationThrowingEvent' || node_type == 'noneThrowingEvent'){
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道池中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageAlign=center;verticalAlign=bottom;fontSize=8;fontColor=#000'); 
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageAlign=center;verticalAlign=bottom;fontSize=8;fontColor=#000'); 
        	}
        }else if(node_type == 'userTask' || node_type == 'manualTask' || node_type == 'scriptTask' || node_type == 'serviceTask' || node_type == 'mailTask' || node_type == 'receiveTask' || node_type == 'businessRuleTask' || node_type == 'callActivity'){
        	console.log("Drag Task ---target------------------------",target);
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道池中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageWidth=12;imageHeight=12;imageAlign=center;verticalAlign=bottom;fontSize=8;fontColor=#000'); 
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageWidth=12;imageHeight=12;imageAlign=center;verticalAlign=bottom;fontSize=8;fontColor=#000;'); 
        	}
        }else if(node_type == 'exclusiveGateway'){
        	//并行网关
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'exclusiveGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'exclusiveGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	} 
        }else if(node_type == 'parallelGateway'){
        	//排他网关
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'parallelGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'parallelGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	} 
        }else if(node_type == 'inclusiveGateway'){
        	//包括网关
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'inclusiveGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'inclusiveGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	} 
        }else if(node_type == 'eventGateway'){
        	//事件网关
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 32, 32,'eventGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 32, 32,'eventGateway_image4gray;rounded=true;strokeColor=none;fillColor=yellow;verticalLabelPosition=bottom;verticalAlign=top');
        	} 
        }else{
        	if(typeof(target) != "undefined" && null != target && (target.node_type == 'lane' || target.node_type == 'transactionProcess' || target.node_type == 'subProcess' || target.node_type == 'eventSubProcess')){
        		//在泳道中
        		var xyArray = resultXy(x,y,graph,target);
        		mcell = graph.insertVertex(target, null, label, xyArray[0], xyArray[1], 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageAlign=center;verticalAlign=bottom;fontSize=8'); 
        	}else{
        		mcell = graph.insertVertex(parent, null, label, x, y, 108, 50,'gradientColor=#ffefbb;rounded=true;strokeColor=none;fillColor=#ffefbb;image='+image+';imageAlign=center;verticalAlign=bottom;fontSize=8'); 
        	}
        	////////////////////////////////以下为添加边界图片///////////////////////
        	/**
        	// 复写按钮图片  
            var overlay = new mxCellOverlay(  
                new mxImage('../view/pc/lc-view/lc-design/archive/grapheditor/images/activities/48/boundaryEventTime.png', 32, 32), '边界事件');
                // 在图形中覆盖  
                graph.addCellOverlay(mcell, overlay);  
                // 单击显示提示 
                overlay.addListener(mxEvent.CLICK, function(sender, evt2){  
                    mxUtils.alert('单击');  
                });
             overlay.cursor = 'hand';  
				 overlay.align = mxConstants.ALIGN_CENTER;  
				 console.info(overlay);
             // 复写按钮图片  
             var overlay1 = new mxCellOverlay(  
                new mxImage('../view/pc/lc-view/lc-design/archive/grapheditor/images/activities/48/exclusiveGatewayError.png', 32, 32), '错误');
                // 在图形中覆盖  
                graph.addCellOverlay(mcell, overlay1);  
                // 单击显示提示 
                overlay1.addListener(mxEvent.CLICK, function(sender, evt2){  
                    mxUtils.alert('单击');  
                });
            	///////////////////////////////////以上为添加边界事件图片/////////////////////////////
            	**/
                //双击显示提示
        		//graph.insertVertex(mcell, null, '边界事件', x, y, 32, 32,'start_image4gray;rounded=true;strokeColor=none;fillColor=yellow;'); 
        }
        if(null != mcell){
        	var nums = new Date().getTime();
       		mcell.node_type = node_type;
       		mcell.nodeID = node_type+nums;
       		mcell.value = label;
        }
//        validateMoveCell(graph, history,mcell);
//        graph.stopEditing(false);
//        //验证泳道是否存在 如果存在则基本信息为第一个泳道中流程信息  
//        validatePOOL(graph);
	}finally{
		graph.getModel().endUpdate();
	}
}