var onOffClose;
function initCellsSytle(graph){
    // //style的使用，插入背景图
    //
    // //1-0.开关-0（开）
    // var pointStyle = new Object();
    // pointStyle[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // pointStyle[mxConstants.STYLE_FONTSIZE] = '8';
    // pointStyle[mxConstants.STYLE_FONTCOLOR] = '#000';
    // pointStyle[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // pointStyle[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/point.png';
    // graph.getStylesheet().putCellStyle('pointStyle_image4gray', pointStyle);
    //
    // //1-1.开关-1（开）
    // var onOffOpen = new Object();
    // onOffOpen[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // onOffOpen[mxConstants.STYLE_FONTSIZE] = '8';
    // onOffOpen[mxConstants.STYLE_FONTCOLOR] = '#000';
    // onOffOpen[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // onOffOpen[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/on-off-open.png';
    // graph.getStylesheet().putCellStyle('onOffOpen_image4gray', onOffOpen);
    //
    // //1-2.开关-1（关）
    // onOffClose = new Object();
    // onOffClose[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // onOffClose[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // onOffClose[mxConstants.STYLE_FONTSIZE] = '8';
    // onOffClose[mxConstants.STYLE_FONTCOLOR] = '#000';
    // onOffClose[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/on-off-close.png';
    // graph.getStylesheet().putCellStyle('onOffClose_image4gray', onOffClose);
    //
    // //1-3.指示灯
    // var pilotLight = new Object();
    // pilotLight[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // pilotLight[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // pilotLight[mxConstants.STYLE_FONTSIZE] = '8';
    // pilotLight[mxConstants.STYLE_FONTCOLOR] = '#000';
    // pilotLight[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/pilot-light.png';
    // graph.getStylesheet().putCellStyle('pilotLight_image4gray', pilotLight);
    //
    // //1-4.接地符
    // var groundingCharacter = new Object();
    // groundingCharacter[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // groundingCharacter[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // groundingCharacter[mxConstants.STYLE_FONTSIZE] = '8';
    // groundingCharacter[mxConstants.STYLE_FONTCOLOR] = '#000';
    // groundingCharacter[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/grounding-character.png';
    // graph.getStylesheet().putCellStyle('groundingCharacter_image4gray', groundingCharacter);
    //
    // //1-5.熔断器
    // var hystrix = new Object();
    // hystrix[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // hystrix[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // hystrix[mxConstants.STYLE_FONTSIZE] = '8';
    // hystrix[mxConstants.STYLE_FONTCOLOR] = '#000';
    // hystrix[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/hystrix.png';
    // graph.getStylesheet().putCellStyle('hystrix_image4gray', hystrix);
    //
    // //1-6.刀闸（2）-合
    // var disconnectorClose = new Object();
    // disconnectorClose[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // disconnectorClose[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // disconnectorClose[mxConstants.STYLE_FONTSIZE] = '8';
    // disconnectorClose[mxConstants.STYLE_FONTCOLOR] = '#000';
    // disconnectorClose[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/disconnector-close.png';
    // graph.getStylesheet().putCellStyle('disconnectorClose_image4gray', disconnectorClose);
    //
    //
    // //1-7.刀闸（2）-分
    // var disconnectorOpen = new Object();
    // disconnectorOpen[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // disconnectorOpen[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // disconnectorOpen[mxConstants.STYLE_FONTSIZE] = '8';
    // disconnectorOpen[mxConstants.STYLE_FONTCOLOR] = '#000';
    // disconnectorOpen[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/disconnector-open.png';
    // graph.getStylesheet().putCellStyle('disconnectorOpen_image4gray', disconnectorOpen);
    //
    // //1-8.刀闸（4）-合
    // var disconnector4close = new Object();
    // disconnector4close[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // disconnector4close[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // disconnector4close[mxConstants.STYLE_FONTSIZE] = '8';
    // disconnector4close[mxConstants.STYLE_FONTCOLOR] = '#000';
    // disconnector4close[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/disconnector-4-close.png';
    // graph.getStylesheet().putCellStyle('disconnector4close_image4gray', disconnector4close);
    //
    //
    //
    // //1-9.刀闸（4）-分
    // var disconnector4open = new Object();
    // disconnector4open[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // disconnector4open[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // disconnector4open[mxConstants.STYLE_FONTSIZE] = '8';
    // disconnector4open[mxConstants.STYLE_FONTCOLOR] = '#000';
    // disconnector4open[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/disconnector-4-open.png';
    // graph.getStylesheet().putCellStyle('disconnector4open_image4gray', disconnector4open);
    //
    //
    // //1-10.电动机
    // var electricmotor = new Object();
    // electricmotor[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // electricmotor[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // electricmotor[mxConstants.STYLE_FONTSIZE] = '8';
    // electricmotor[mxConstants.STYLE_FONTCOLOR] = '#000';
    // electricmotor[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/electric-motor.png';
    // graph.getStylesheet().putCellStyle('electricmotor_image4gray', electricmotor);
    //
    //
    // //1-11.发电机
    // var dynamo = new Object();
    // dynamo[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // dynamo[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // dynamo[mxConstants.STYLE_FONTSIZE] = '8';
    // dynamo[mxConstants.STYLE_FONTCOLOR] = '#000';
    // dynamo[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/dynamo.png';
    // graph.getStylesheet().putCellStyle('dynamo_image4gray', dynamo);
    //
    //
    // //1-12.电感
    // var inductance = new Object();
    // inductance[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // inductance[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // inductance[mxConstants.STYLE_FONTSIZE] = '8';
    // inductance[mxConstants.STYLE_FONTCOLOR] = '#000';
    // inductance[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/inductance.png';
    // graph.getStylesheet().putCellStyle('inductance_image4gray', inductance);
    //
    //
    // //1-13.两侧变压器3
    // var twosidetransformer3 = new Object();
    // twosidetransformer3[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // twosidetransformer3[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // twosidetransformer3[mxConstants.STYLE_FONTSIZE] = '8';
    // twosidetransformer3[mxConstants.STYLE_FONTCOLOR] = '#000';
    // twosidetransformer3[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/two-side-transformer-3.png';
    // graph.getStylesheet().putCellStyle('twosidetransformer3_image4gray', twosidetransformer3);
    //
    //
    // //1-14.两侧变压器1
    // var twosidetransformer1 = new Object();
    // twosidetransformer1[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // twosidetransformer1[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // twosidetransformer1[mxConstants.STYLE_FONTSIZE] = '8';
    // twosidetransformer1[mxConstants.STYLE_FONTCOLOR] = '#000';
    // twosidetransformer1[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/two-side-transformer-1.png';
    // graph.getStylesheet().putCellStyle('twosidetransformer1_image4gray', twosidetransformer1);
    //
    //
    //
    // //1-15.两侧变压器2
    // var twosidetransformer2 = new Object();
    // twosidetransformer2[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // twosidetransformer2[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // twosidetransformer2[mxConstants.STYLE_FONTSIZE] = '8';
    // twosidetransformer2[mxConstants.STYLE_FONTCOLOR] = '#000';
    // twosidetransformer2[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/two-side-transformer-2.png';
    // graph.getStylesheet().putCellStyle('twosidetransformer2_image4gray', twosidetransformer2);
    //
    //
    //
    //
    // //1-16.开关-2(关)
    // var onoffclose2 = new Object();
    // onoffclose2[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // onoffclose2[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // onoffclose2[mxConstants.STYLE_FONTSIZE] = '8';
    // onoffclose2[mxConstants.STYLE_FONTCOLOR] = '#000';
    // onoffclose2[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/on-off-close-2.png';
    // graph.getStylesheet().putCellStyle('onoffclose2_image4gray', onoffclose2);
    //
    //
    //
    // //1-17.开关-2(开)
    // var onoffopen2 = new Object();
    // onoffopen2[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // onoffopen2[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // onoffopen2[mxConstants.STYLE_FONTSIZE] = '8';
    // onoffopen2[mxConstants.STYLE_FONTCOLOR] = '#000';
    // onoffopen2[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/on-off-open-2.png';
    // graph.getStylesheet().putCellStyle('onoffopen2_image4gray', onoffopen2);
    //
    //
    //
    // //1-18.互感器-三相五线PT
    // var TransformerThreephaseFivewireP = new Object();
    // TransformerThreephaseFivewireP[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // TransformerThreephaseFivewireP[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // TransformerThreephaseFivewireP[mxConstants.STYLE_FONTSIZE] = '8';
    // TransformerThreephaseFivewireP[mxConstants.STYLE_FONTCOLOR] = '#000';
    // TransformerThreephaseFivewireP[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Transformer-Three-phase-Five-wire-P.png';
    // graph.getStylesheet().putCellStyle('TransformerThreephaseFivewireP_image4gray', TransformerThreephaseFivewireP);
    //
    //
    // //1-19.互感器-三相CT
    // var TransformerThreePhaseCT = new Object();
    // TransformerThreePhaseCT[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // TransformerThreePhaseCT[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // TransformerThreePhaseCT[mxConstants.STYLE_FONTSIZE] = '8';
    // TransformerThreePhaseCT[mxConstants.STYLE_FONTCOLOR] = '#000';
    // TransformerThreePhaseCT[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Transformer-Three-Phase-CT.png';
    // graph.getStylesheet().putCellStyle('TransformerThreePhaseCT_image4gray', TransformerThreePhaseCT);
    //
    //
    //
    //
    //
    // //1-20.互感器-三相CT
    // var TransformerTwoPhaseCT = new Object();
    // TransformerTwoPhaseCT[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // TransformerTwoPhaseCT[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // TransformerTwoPhaseCT[mxConstants.STYLE_FONTSIZE] = '8';
    // TransformerTwoPhaseCT[mxConstants.STYLE_FONTCOLOR] = '#000';
    // TransformerTwoPhaseCT[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Transformer-Two-Phase-CT.png';
    // graph.getStylesheet().putCellStyle('TransformerTwoPhaseCT_image4gray', TransformerTwoPhaseCT);
    //
    //
    //
    // //1-21.TransformerSinglePhaseCT 互感器-单相CT
    // var TransformerSinglePhaseCT = new Object();
    // TransformerSinglePhaseCT[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // TransformerSinglePhaseCT[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // TransformerSinglePhaseCT[mxConstants.STYLE_FONTSIZE] = '8';
    // TransformerSinglePhaseCT[mxConstants.STYLE_FONTCOLOR] = '#000';
    // TransformerSinglePhaseCT[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Transformer-Single-Phase-CT.png';
    // graph.getStylesheet().putCellStyle('TransformerSinglePhaseCT_image4gray', TransformerSinglePhaseCT);
    //
    //
    //
    // //1-22.capacitance 电容
    // var capacitance = new Object();
    // capacitance[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // capacitance[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // capacitance[mxConstants.STYLE_FONTSIZE] = '8';
    // capacitance[mxConstants.STYLE_FONTCOLOR] = '#000';
    // capacitance[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/capacitance.png';
    // graph.getStylesheet().putCellStyle('capacitance_image4gray', capacitance);
    //
    //
    //
    // //1-23.KnifeGate3open 刀闸-3(分)
    // var KnifeGate3open = new Object();
    // KnifeGate3open[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // KnifeGate3open[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // KnifeGate3open[mxConstants.STYLE_FONTSIZE] = '8';
    // KnifeGate3open[mxConstants.STYLE_FONTCOLOR] = '#000';
    // KnifeGate3open[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Knife-Gate-3-open.png';
    // graph.getStylesheet().putCellStyle('KnifeGate3open_image4gray', KnifeGate3open);
    //
    //
    // /**
    //  *
    //  * 24.KnifeGate3close 刀闸-3(合)
    //  *
    //  **/
    // var KnifeGate3close = new Object();
    // KnifeGate3close[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // KnifeGate3close[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // KnifeGate3close[mxConstants.STYLE_FONTSIZE] = '8';
    // KnifeGate3close[mxConstants.STYLE_FONTCOLOR] = '#000';
    // KnifeGate3close[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Knife-Gate-3-close.png';
    // graph.getStylesheet().putCellStyle('KnifeGate3close_image4gray', KnifeGate3close);
    //
    //
    //
    // /**
    //  *
    //  * 25.Threephasetransformer2 三相变压器2
    //  *
    //  **/
    // var Threephasetransformer2 = new Object();
    // Threephasetransformer2[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // Threephasetransformer2[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // Threephasetransformer2[mxConstants.STYLE_FONTSIZE] = '8';
    // Threephasetransformer2[mxConstants.STYLE_FONTCOLOR] = '#000';
    // Threephasetransformer2[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Three-phase-transformer-2.png';
    // graph.getStylesheet().putCellStyle('Threephasetransformer2_image4gray', Threephasetransformer2);
    //
    //
    //
    //
    //
    // /**
    //  *
    //  * 26.Threephasetransformer3 三相变压器3
    //  *
    //  **/
    // var Threephasetransformer3 = new Object();
    // Threephasetransformer3[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // Threephasetransformer3[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // Threephasetransformer3[mxConstants.STYLE_FONTSIZE] = '8';
    // Threephasetransformer3[mxConstants.STYLE_FONTCOLOR] = '#000';
    // Threephasetransformer3[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Three-phase-transformer-3.png';
    // graph.getStylesheet().putCellStyle('Threephasetransformer3_image4gray', Threephasetransformer2);
    //
    //
    //
    // /**
    //  *
    //  * 27.Threephasetransformer1 三相变压器1
    //  *
    //  **/
    // var Threephasetransformer1 = new Object();
    // Threephasetransformer1[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // Threephasetransformer1[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // Threephasetransformer1[mxConstants.STYLE_FONTSIZE] = '8';
    // Threephasetransformer1[mxConstants.STYLE_FONTCOLOR] = '#000';
    // Threephasetransformer1[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Three-phase-transformer-1.png';
    // graph.getStylesheet().putCellStyle('Threephasetransformer1_image4gray', Threephasetransformer1);
    //
    //
    //
    // /**
    //  *
    //  * 28.Knifegate1close 刀闸-1(合)
    //  *
    //  **/
    // var Knifegate1close = new Object();
    // Knifegate1close[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // Knifegate1close[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // Knifegate1close[mxConstants.STYLE_FONTSIZE] = '8';
    // Knifegate1close[mxConstants.STYLE_FONTCOLOR] = '#000';
    // Knifegate1close[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Knife-gate-1-close.png';
    // graph.getStylesheet().putCellStyle('Knifegate1close_image4gray', Knifegate1close);
    //
    //
    // /**
    //  *
    //  * 29.Knifegate1open 刀闸-1(分)
    //  *
    //  **/
    // var Knifegate1open = new Object();
    // Knifegate1open[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // Knifegate1open[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // Knifegate1open[mxConstants.STYLE_FONTSIZE] = '8';
    // Knifegate1open[mxConstants.STYLE_FONTCOLOR] = '#000';
    // Knifegate1open[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/Knife-gate-1-open.png';
    // graph.getStylesheet().putCellStyle('Knifegate1open_image4gray', Knifegate1close);
    //
    //
    //
    // /**
    //  *
    //  * 30.transformer 互感器
    //  *
    //  **/
    // var transformer = new Object();
    // transformer[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // transformer[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // transformer[mxConstants.STYLE_FONTSIZE] = '8';
    // transformer[mxConstants.STYLE_FONTCOLOR] = '#000';
    // transformer[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/transformer.png';
    // graph.getStylesheet().putCellStyle('transformer_image4gray', transformer);
    //
    // /**
    //  *
    //  * 31.electricreactor 电抗器
    //  *
    //  **/
    // var electricreactor = new Object();
    // electricreactor[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // electricreactor[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // electricreactor[mxConstants.STYLE_FONTSIZE] = '8';
    // electricreactor[mxConstants.STYLE_FONTCOLOR] = '#000';
    // electricreactor[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/electric-reactor.png';
    // graph.getStylesheet().putCellStyle('electricreactor_image4gray', electricreactor);
    //
    //
    // /**
    //  *
    //  * 31.capacity 电容
    //  *
    //  **/
    // var capacity = new Object();
    // capacity[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    // capacity[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    // capacity[mxConstants.STYLE_FONTSIZE] = '8';
    // capacity[mxConstants.STYLE_FONTCOLOR] = '#000';
    // capacity[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/capacity.png';
    // graph.getStylesheet().putCellStyle('capacity_image4gray', capacity);

}