//创建节点模板 暂时未使用
function insertEdgeTemplate(panel, graph, name, icon, style, width, height, value, parentNode){
		var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), style)];
		cells[0].geometry.setTerminalPoint(new mxPoint(0, height), true);
		cells[0].geometry.setTerminalPoint(new mxPoint(width, 0), false);
		cells[0].edge = true;
		var funct = function(graph, evt, target){
			cells = graph.getImportableCells(cells);
			if(cells.length > 0){
				var validDropTarget = (target != null) ? graph.isValidDropTarget(target, cells, evt) : false;
				var select = null;
				if(target != null && !validDropTarget){
					target = null;
				}
				var pt = graph.getPointForEvent(evt);
				var scale = graph.view.scale;
				pt.x -= graph.snap(width / 2);
				pt.y -= graph.snap(height / 2);
				select = graph.importCells(cells, pt.x, pt.y, target);
				GraphEditor.edgeTemplate = select[0];
				graph.scrollCellToVisible(select[0]);
				graph.setSelectionCells(select);
			}
		};
		var node = panel.addTemplate(name, icon, parentNode, cells);
		var installDrag = function(expandedNode){
			if (node.ui.elNode != null){
				var dragPreview = document.createElement('div');
				dragPreview.style.border = 'dashed black 1px';
				dragPreview.style.width = width+'px';
				dragPreview.style.height = height+'px';
				mxUtils.makeDraggable(node.ui.elNode, graph, funct, dragPreview, -width / 2, -height / 2,graph.autoscroll, true);
			}
		};
		if(!node.parentNode.isExpanded()){
			panel.on('expandnode', installDrag);
		}else{
			installDrag(node.parentNode);
		}
		return node;
};
// 添加元素右上角的删除图标  
function addOverlays(graph, cell, addDeleteIcon){  
    var overlay = new mxCellOverlay(new mxImage('images/add.png', 24, 24), 'Add child');  
    overlay.cursor = 'hand';  
    overlay.align = mxConstants.ALIGN_CENTER;  
    overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
        addChild(graph, cell);  
    }));  
    graph.addCellOverlay(cell, overlay);  
    if (addDeleteIcon){  
        overlay = new mxCellOverlay(new mxImage('images/close.png', 30, 30), 'Delete');  
        overlay.cursor = 'hand';  
        overlay.offset = new mxPoint(-4, 8);  
        overlay.align = mxConstants.ALIGN_RIGHT;  
        overlay.verticalAlign = mxConstants.ALIGN_TOP;  
        overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
            deleteSubtree(graph, cell);  
        }));  
        graph.addCellOverlay(cell, overlay);  
    }  
}; 
// 添加子元素  
function addChild(graph, cell){  
    var model = graph.getModel();  
    var parent = graph.getDefaultParent();  
    var vertex;  
    model.beginUpdate();  
    try {  
        vertex = graph.insertVertex(parent, null, 'Double click to set name');  
        var geometry = model.getGeometry(vertex);  
        var size = graph.getPreferredSizeForCell(vertex);  
        geometry.width = size.width;  
        geometry.height = size.height;  
        var edge = graph.insertEdge(parent, null, '', cell, vertex);  
        edge.geometry.x = 1;  
        edge.geometry.y = 0;  
        edge.geometry.offset = new mxPoint(0, -20);  
    }finally{  
        model.endUpdate();  
    }  
    return vertex;  
};

////导入流程
function imp(graph,history){
    graph_refresh = graph;
    var processSelectModalCount = 0 ;
    $('#processSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#processSelectModal').on("shown.bs.modal",function(){
        if(++processSelectModalCount == 1){
            $('#searchFormprocess')[0].reset();
            var opt = {
                searchformId:'searchForprocess'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,rest_url_prefix_lc+'/lcProcess/blist',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"lc_process_id",
                        width:"50px"
                    },
                    {
                        data:'xt_userinfo_realName'
                    },
                    {
                        data:'lc_process_title'
                    },
                    {
                        data:'lc_process_status',
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "待发布";
                            }
                            if(data == 1){
                                return "发布中";
                            }
                            if(data == 2){
                                return "已关闭";
                            }
                        }
                    },
                    {
                        data:"lc_process_id",
                        render:function(data, type, row, meta) {
                            var lc_process_flag = row.lc_process_flag;
                            var lc_process_title = row.lc_process_title;
                            var xt_attachment = row.xt_attachment;
                            var btn = '<button class="btn btn-default" onclick=doImpl("'+data+'")><i class="glyphicon glyphicon-eye-open"></i>导入流程</button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#processDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('processDatatables');
        }
    })
}
function doImpl(id){
    msgTishCallFnBoot("确定导入该流程数据？",function(){
        ajaxBRequestCallFn(rest_url_prefix_lc+'/lcProcess/get/'+id,null,function(result){
            if(typeof(result.success) != "undefined" && result.success == true){
                var lc_process_title = result.data.lc_process_title;
                var lc_process_mxgraph_style = result.data.lc_process_mxgraph_style;
                var lc_process_mxgraphxml = result.data.lc_process_mxgraphxml;
                var processId = result.data.lc_process_uid;
                var candidateStarterUsers = result.data.candidateStarterUsers;
                var candidateStarterGroups = result.data.candidateStarterGroups;
                var lc_process_remark = result.data.lc_process_remark;
                var lc_process_id = result.data.lc_process_id;
                var xt_constant_id = result.data.xt_constant_id;
                //////////////////////////////////开始导入XML文件///////////////////////////
                var graph = graph_refresh;
                graph.getModel().beginUpdate();
                try{
                    /*
                     * 直接读取流程图的xml, 并展示流程图
                    */
                    if(lc_process_mxgraphxml != null && lc_process_mxgraphxml.length > 0){
                        var doc = mxUtils.parseXml(lc_process_mxgraphxml);
                        var dec = new mxCodec(doc);
                        dec.decode(doc.documentElement, graph.getModel());
                    }
                    validatePOOL(graph);
                }catch (e){
                    console.log("---导入流程出现异常---",e)
                }finally{
                    graph_refresh.getModel().endUpdate();
                    linetostyle(lc_process_mxgraph_style,graph);
                    graph_refresh.refresh();
                    $('#processId').val(processId);
                    $('#processName').val(lc_process_title);
                    $('#mxgraphxml').val(lc_process_mxgraphxml);
                    $('#lc_process_mxgraph_style').val(lc_process_mxgraph_style);
                    $('#lc_process_id').val(lc_process_id);
                    $('#candidateStarterUsers').val(candidateStarterUsers);
                    $('#remark').val(lc_process_remark);
                    $('#xt_constant_id').val(xt_constant_id);
                    $('#processSelectModal').modal('hide');
                }
                //////////////////////////////////结束导入XML文件///////////////////////////
            }
        },null,"GET");
    })
}

//连线样式设置虚线
function connectEdge(editor){
	if (editor.defaultEdge != null){
		editor.defaultEdge.style = 'straightEdge';
	}
}

function importP(graph,history){
    //initScroll()
	console.log("importP---");
    imp(graph,history)
}


/**
 * 导出文件
 */
function exportP(graph){
    //获取mxgraph拓扑图数据
    //var enc = new mxCodec(mxUtils.createXmlDocument());
    //var node1 = enc.encode(graph.getModel());
    //var mxgraphxml = mxUtils.getXml(node1);
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var mxgraphxml = mxUtils.getPrettyXml(node);
    mxgraphxml = mxgraphxml.replace(/\"/g,"'");
    //mxgraphxml = encodeURIComponent(mxgraphxml);

    var xmlDoc = mxUtils.createXmlDocument();
    var root = xmlDoc.createElement('output');
    xmlDoc.appendChild(root);
    var xmlCanvas = new mxXmlCanvas2D(root);
    var imgExport = new mxImageExport();
    imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    var bounds = graph.getGraphBounds();
    var w = Math.round(bounds.x + bounds.width + 4);
    var h = Math.round(bounds.y + bounds.height + 4);
    var imgxml = mxUtils.getXml(root);
    //imgxml = "<output>"+imgxml+"</output>";
    //imgxml = encodeURIComponent(imgxml);

    doExportP(mxgraphxml,w,h,imgxml);
}


function doExportP(mxgraphxml,w,h,imgxml){
    msgTishCallFnBoot("确定导出？",function(){
        $('#mxgraphxml').val(mxgraphxml);
        $('#imgxml').val(imgxml);
        $('#w').val(w);
        $('#h').val(h);
        submitBFormCallFn('BaseForm',baseUrl+'create',function(result){
            try {
                console.log(result);
                downOrExportB(baseUrl+'export?name='+result);
            } catch (e) {

            }
        });
    })
}


function saveP(graph){

    //获取mxgraph拓扑图数据
    //var enc = new mxCodec(mxUtils.createXmlDocument());
    //var node1 = enc.encode(graph.getModel());
    //var mxgraphxml = mxUtils.getXml(node1);
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var mxgraphxml = mxUtils.getPrettyXml(node);
    mxgraphxml = mxgraphxml.replace(/\"/g,"'");
    //mxgraphxml = encodeURIComponent(mxgraphxml);

    var xmlDoc = mxUtils.createXmlDocument();
    var root = xmlDoc.createElement('output');
    xmlDoc.appendChild(root);
    var xmlCanvas = new mxXmlCanvas2D(root);
    var imgExport = new mxImageExport();
    imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    var bounds = graph.getGraphBounds();
    var w = Math.round(bounds.x + bounds.width + 4);
    var h = Math.round(bounds.y + bounds.height + 4);
    var imgxml = mxUtils.getXml(root);
    //imgxml = "<output>"+imgxml+"</output>";
    //imgxml = encodeURIComponent(imgxml);
    saveDesign(mxgraphxml,w,h,imgxml);

    // //initScroll()
    // //获取mxgraph拓扑图数据
    // //var enc = new mxCodec(mxUtils.createXmlDocument());
    // //var node1 = enc.encode(graph.getModel());
    // //var mxgraphxml = mxUtils.getXml(node1);
    // var enc = new mxCodec(mxUtils.createXmlDocument());
    // var node = enc.encode(graph.getModel());
    // var mxgraphxml = mxUtils.getPrettyXml(node);
    // mxgraphxml = mxgraphxml.replace(/\"/g,"'");
    // //mxgraphxml = encodeURIComponent(mxgraphxml);
    // try{
    //     var xmlDoc = mxUtils.createXmlDocument();
    //     var root = xmlDoc.createElement('output');
    //     xmlDoc.appendChild(root);
    //     var xmlCanvas = new mxXmlCanvas2D(root);
    //     var imgExport = new mxImageExport();
    //     imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    //     var bounds = graph.getGraphBounds();
    //     var w = Math.round(bounds.x + bounds.width + 4);
    //     var h = Math.round(bounds.y + bounds.height + 4);
    //     var imgxml = mxUtils.getXml(root);
    //     //imgxml = "<output>"+imgxml+"</output>";
    //     //imgxml = encodeURIComponent(imgxml);
    //
    //     saveDesign(mxgraphxml,w,h,imgxml);
    // }catch (e){
    //
    // }

}

function closeJehcLcWin(){
    $('#jehcLcModal').modal('hide');
}

function initScroll(){
    // $("#sidebarContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#diagramContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false}); // First scrollable DIV
}

function nodeScroll(){
    // $("#mportletId").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#mportletId1").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#TabCol").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
}



function linetostyle(flag,graph){
    // var line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/default-style.xml';
    // if(flag == 0){
    //     //如果为1直线
    //     line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/default-style.xml';
    // }else if(flag == 1){
    //     //如果为1曲线
    //     line_style = basePath+'/view/pc/lc-view/lc-design/archive/grapheditor/resources/bight-style.xml';
    // }
    // var history = new mxUndoManager();
    // //载入默认样式
    // var node = mxUtils.load(line_style).getDocumentElement();
    // var dec = new mxCodec(node.ownerDocument);
    // dec.decode(node, graph.getStylesheet());
    // var edgeStyle = graph.getStylesheet().getDefaultEdgeStyle();
    // //edgeStyle[mxConstants.STYLE_EDGE] = mxEdgeStyle.TopToBottom;
    // edgeStyle['gradientColor'] = '#c0c0c0';
    // edgeStyle['strokeColor'] = '#c0c0c0'; //更改连线默认样式此处为颜色
    // edgeStyle['dashed'] = '1'; //虚线
    // edgeStyle['strokeWidth'] = 0.1;
    // edgeStyle['fontSize'] = '8';
    // edgeStyle['fontColor'] = '#000';
    // edgeStyle['arrowWidth'] = 0.1;
    // graph.alternateEdgeStyle = 'elbow=vertical';
    // graph.refresh();
    // Ext.getCmp('lc_process_mxgraph_style').setValue(flag);
}
//载入XML流程图
function loadXml(graph,xml){
    graph.getModel().beginUpdate();
    try{
        if(xml != null && xml.length > 0){
            var doc = mxUtils.parseXml(xml);
            var dec = new mxCodec(doc);
            dec.decode(doc.documentElement, graph.getModel());
        }
    }finally{
        graph.getModel().endUpdate();
        graph.refresh();
    }
    setTimeout(function(){
        resetZxLine(graph);
    },100);
}
$(document).ready(function() {
    $('#BaseForm').bootstrapValidator({
        message: '此值不是有效的'
    });
});
//保存流程
function saveDesign(mxgraphxml,w,h,imgxml){
    msgTishCallFnBoot("确定保存该设计？",function(){
        $('#graph_site_mxgraph').val(mxgraphxml);
        $('#imgxml').val(imgxml);
        $('#w').val(w);
        $('#h').val(h);
        $('#gridColor').val(ThizViewPort.editorUi.editor.graph.view.gridColor);
        var gridBackGround = ThizViewPort.editorUi.editor.graph.background;
        if(undefined === gridBackGround || gridBackGround == null || gridBackGround == ""){
            gridBackGround = "#fff";
        }
        $('#gridBackGround').val(gridBackGround);
        $("#stationId").val("1");
        submitBFormCallFn('BaseForm',basePath+'/graphDesignController/saveGraphSite',function(result){
            try {
                result = eval("(" + result + ")");
                if(typeof(result.success) != "undefined"){
                    if(result.success){
                        window.parent.toastrBoot(3,result.msg);
                        $("#graph_site_id").val(result.data);
                    }else{
                        window.parent.toastrBoot(4,result.msg);
                    }
                }
            } catch (e) {

            }
        });
    })
}


//将字符串转换XML
function printString(xmlobj){
    var xmlDom;
    //IE
    if(document.all){
        xmlDom=new ActiveXObject("Microsoft.XMLDOM");
        xmlDom.loadXML(xmlobj);
    }
    //非IE
    else {
        xmlDom = new DOMParser().parseFromString(xmlobj, "text/xml");
    }
    return xmlDom;
}



function resetZxLine(graph){
    var allLines =graph.getModel().cells;
    for(var i in allLines){
        var cell = allLines[i];
        if(cell.nodeType == "zxLine"){
            // Adds animation to edge shape and makes "pipe" visible
            graph.getModel().beginUpdate();
            try
            {
                var state = graph.view.getState(cell);
                if(null != state){
                    state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
                    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
                    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
                    state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');
                }
            }
            finally
            {
                // Updates the display
                graph.getModel().endUpdate();
            }
        }
    }
}


function aminLine(graph,parent,x,y){
    var vertexStyle = 'shape=cylinder;strokeWidth=2;fillColor=#ffffff;strokeColor=black;' +
        'gradientColor=#a0a0a0;fontColor=black;fontStyle=1;spacingTop=14;';
    graph.getModel().beginUpdate();
    try
    {
        var v1 = graph.insertVertex(parent, null, 'Pump', x, y, 48, 48,vertexStyle);
        var v2 = graph.insertVertex(parent, null, 'Tank', x+160, y, 48, 48,vertexStyle);
        var edge = graph.insertEdge(parent, null, '', v1, v2,'strokeWidth=3;endArrow=block;endSize=2;endFill=1;strokeColor=black;rounded=1;');
        edge.geometry.points = [new mxPoint(x, y)];
        graph.orderCells(true, [edge]);
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }

    // Adds animation to edge shape and makes "pipe" visible
    var state = graph.view.getState(edge);
    // state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
    state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
    state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');

    graph.getModel().beginUpdate();
    try
    {
        graph.removeCells([v1], false);
        graph.removeCells([v2], false);
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }

}


function addAnimationEdge(graph,edge){
    // Adds animation to edge shape and makes "pipe" visible
    graph.getModel().beginUpdate();
    try
    {
        var state = graph.view.getState(edge);
        state.shape.node.getElementsByTagName('path')[0].removeAttribute('visibility');
        state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke-width', '6');
        state.shape.node.getElementsByTagName('path')[0].setAttribute('stroke', 'lightGray');
        state.shape.node.getElementsByTagName('path')[1].setAttribute('class', 'flow');
    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }
}

function openNewWindow(url) {
    var a = document.createElement('a');
    a.setAttribute('href', url);
    a.setAttribute('target', '_blank');
    var id = Math.random(10000, 99999);
    a.setAttribute('id', id);
    // 防止反复添加
    if (!document.getElementById(id)) {
        document.body.appendChild(a);
    }
    a.click();
}

function clearCELLText(cell,graph){
    if(null == cell || undefined === cell){
        return;
    }
    //如果节点是连线 开关，点，改变填充颜色
    if(cell.nodeType == 'onoffclose2' ||
        cell.nodeType == 'onoffopen2' ||
        cell.nodeType == 'onOffClose' ||
        cell.nodeType == 'onOffOpen' ||
        cell.nodeType == "point"){
        setTimeout(function(){
            graph.getModel().beginUpdate();
            try{
                // graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, cell.fills, [cell]);
                cell.value="";//清空节点值
            } finally {
                graph.getModel().endUpdate();
            }
        },"500");
        return;
    }
    setTimeout(function(){
        graph.getModel().beginUpdate();
        try{
            if(cell.nodeType == "Text"){
                cell.value="文本";
                return;
            }else{
                cell.value="";//清空节点值
            }
        } finally {
            graph.getModel().endUpdate();
        }
    },"500");
}

function svgToBase64(cell,graph){
    if(null == cell || undefined === cell){
        return;
    }

    //如果节点是连线 开关，点，改变填充颜色
    if(cell.nodeType == 'onoffclose2' ||
        cell.nodeType == 'onoffopen2' ||
        cell.nodeType == 'onOffClose' ||
        cell.nodeType == 'onOffOpen' ||
        cell.nodeType == "point"){
        setTimeout(function(){
            graph.getModel().beginUpdate();
            try{
                graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, cell.fills, [cell]);
                cell.value="";//清空节点值
            } finally {
                graph.getModel().endUpdate();
            }
        },"500");
        return;
    }
    // console.log("geometry",cell.geometry);
    // console.log("height",cell.geometry.height);
    // console.log("width",cell.geometry.width);

    // var oldImgHref = MXSOURCESPATH+"/"+cell.image;
    // var oldImg = document.createElement('img');
    // oldImg.src = oldImgHref;
    // oldImg.onload=function(){

        try {
            var svgs = cell.svgs;
            var svgtemplate = document.getElementById("svgtemplate");
            if(svgs == null || undefined === svgs){
                return;
            }
            $("#svgtemplatePath").attr("d",svgs);

            var fills = cell.fills;//填充
            if('' != fills && fills != null && undefined !== fills){
                $("#svgtemplatePath").attr("fill",fills);
            }else{
                $("#svgtemplatePath").attr("fill","#fff");
            }

            var svgHeight = cell.svgHeight;
            if('' != svgHeight && svgHeight != null && undefined !== svgHeight) {
                $("#svgtemplate").attr("height", svgHeight);
            }else{
                $("#svgtemplate").attr("height", 120);
            }

            var svgWidth = cell.svgWidth;
            if('' != svgWidth && svgWidth != null && undefined !== svgWidth) {
                $("#svgtemplate").attr("width", svgWidth);
            }else{
                $("#svgtemplate").attr("width", 120);
            }

            var stockFill = cell.stockFill;//线条颜色
            if('' != stockFill && stockFill != null && undefined !== stockFill){
                $("#svgtemplatePath").attr("stroke",stockFill);
            }else{
                $("#svgtemplatePath").attr("stroke","#fff");
            }

            var svgCon = svgtemplate.outerHTML;
            //svg转base64;
            var svgBase64 = 'data:image/svg+xml;base64,' + window.btoa(unescape(encodeURIComponent(svgCon)));
            var image = new Image(); //创建一个Image对象，实现图片的预下载
            image.src = svgBase64;
            var height = cell.svgHeight;
            var width = cell.svgWidth;
            image.onload =function(){
                image.width = width;
                image.height = height;
                var canvas = document.createElement('canvas');
                // var canvasWidth = width;
                // var canvasHeight = height;
                canvas.width = width;
                canvas.height = height;
                var context = canvas.getContext('2d');
                // context.scale(cWidth/2,cHeight/2);
                // var scale =1;
                // if(height > width){
                //     scale = width / height;
                //     context.drawImage(image, (canvasWidth - canvasHeight * scale) / 2, 0, canvasHeight * scale, canvasHeight);
                // }else if (width > height) {
                //     scale = height / width
                //     context.drawImage(image, 0, (canvasHeight - canvasWidth * scale) / 2, canvasWidth, canvasWidth * scale);
                // }else {
                //     context.drawImage(image, 0, 0,canvasWidth,canvasHeight);
                // }
                context.drawImage(image, 0, 0,cell.svgWidth,cell.svgHeight);
                graph.getModel().beginUpdate();
                try{
                    //svg转png的base64编码;
                    var imgDataUri = canvas.toDataURL("image/png");
                    // imgDataUri = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAJ10lEQVR4Xu2ce1BU1x3Hf2dXQBSV0jAxgsru4jihmqmmMYmdajqaRhzLKzLJ6GBwM0CEKvVBUTEuGKFSfFLBABNQGEwbHF7tSGxIO2RGa5JWWzU6juwFDRgzWIpKgEV2T+d3cJllA+7ufTvd+xew95z7O5+5d+/3nvO5EFDxFm0wxFJKSSPH1aq1TKLWwmL0+g1ASDGrj9LUeo47psZa1QhQE63X7yeEbB4FjNLD9Ry3BXGqCaSqAEaGh/v5UfpHAIj2nTgRdpaUMFZ5KSkwODCAPzZYCHmjqbXVohaIqgH4WmhokL+f358B4OXAp56C3eXloIuIYJzarl6FPUYj9Ny9i7/+vd9iWXWmo6NbDRBVATBm5kwD+PqeAQDDDJ0O9lRWwg+feWYUn/988w3sXrcObre14d/NGpttWW1b202lISoOMEqv/4kG4AwQEhTxwguQVVoKk6ZMGZNL34MH7Ey8fvEift5lo3RlI8f9Q0mIigKMDQuLtmk0fyCETFwcGQmbDx6ECT4+j+XxcHAQDm/dCueamoBSOqCx2d6sa29vUAqiYgAxplCAIkIIiU1KgnWZmW4zoJRCVUEB1JWWIkRKANKUijlKACQxBkMBAGwlhEBydjasWLvWbXiOO35cXQ2l2dkIEbPiwXqO2yZ3zJEVoGNM8fH1he3HjsHCpUt5wbM3utDSAvs2bAC8tJWIObIBdIwpU4OC4N0PPoDw+fMFwbM3br18Gd57+224382SjawxRxaAcTrdbJtG8ynGlOmzZsGeqioIDgkRBZ69k67OTtidkAB3bt2SNeZIDpDFFEJOA0Dw3AULIKusDKYEBooKz97Zg54eyE1KkjXmSAowxmCIpJTWYkxZtHw5bCssBPzuk3LD78L9mzbBF83NLOYQQuLqzeYmqY4pGUA2mwJwFAjR/DIxEdZnZQHedeXY8K5cajLBxydP4t3ZBgC/kirmSDEiEm0w/I4AYKSAlJwc3jFFKOw/VVRAeW4u64YCFDSYzb8R2qdze1EBPoopVQAQj08UGYWFsOjVV8Wu2aP+vvjkEyjYtAmGHj7EdjUWQhLEnM0RDWC8Xj/tISH4XfNywLRpsKusDOYuXOjRYKXa+fqFC7A3KQl6791jMceH0sgajmO/CN1EAYgxxUrIGULI3OAZM1hMmT57ttDaRG1/5+ZNFnO6bt/Gm8t1LaWviTGbIxigY0wxzJsH75aXw7SgIFEHL1Zn97q74T2jEcxXrmCXXWC1/qK+vf1fQvoXBNAxpuAjWWZxMfj6+QmpR/K2gxYL5KemAj4CUkr7CCGrhcQc3gAdY8qKNWsgKTsbNBqN5ADEOIDNZoPinTvh01OnWMyxASQ1clw5n775ACQxev0+IIRFgoSMDIhLSeFzbMXb1JaUsGkxFnMo3dfAcTs8LcojgI4xRTthApsA/enKlZ4eU1X7nz19Gg5t2QLWoSEWc742m9f+E4BlHnc2twE6xpRJAQFsxexHL77ozjFUv89Xn3/OVv76envxTPzMFyDK3ZjjFkDHmBL09NMspoTo9aoH40mBnRzHYk73t98OxxyLZXltZ2eHqz5cAowJC/sxaLV/wdkU3bPPspjyg+BgV/0+kZ//t6uLxZy2a9cQ4h1is0W6ijmPBfgoppwihEx6bvFidtn6+fs/kXDcLdrS388u50vnzrGYg4v8DRzXPF77cQFG6fVGDUAZzqYsW70aUnNzQaPVulvHE72fzWqF4qwse8yx2gCSx4s5YwHEmJILhLBb+pvp6fDGxo1PNBC+xX909Ch8ePjwcMwByGswm7Oc+xoF8HkAn5kGQzXOpmBM2ZifD0ujo/keX1A7nNMr37uX9WHctUu2uUTnolsaGqAwIwMwfFNKqzs4br1jzBkBiDFlEKCRELJk4uTJsKu0VLGYgo9bB9LT2awybjibvfXIEcUeE/999izsS02Fge++YzGH3r+/qvHu3QdYGwMYFxISavXza8bZFBR7cqqqYNacOYLOIL6NcV0jJzHR/sA/0g1OVJiOH5dsPcVVvbdu3ABTQgITnCjAV9qBgRUYcwjGFKrRNBFCps+cMwdM5eXfE3tcdS7W5zjllLN+PVtZQ8nokUg08jOu6JkqKhSbKkPBKcdohK9v3GAxx6bRLCdROt0SrVb7V11ERG/OiROXAwIDrWIB8aSfy+fPT/ntO+8819/bO8EuGa1dsIB1UX3xIuQmJ8PVL78E/4CAoR3vv39p/ksvsUtI7q23p0dreuut+W3Xrk22Dg0tY5dwtE73s462tvOePAOKWfh4klFseDg7TF1rK5uSx2dWNUhFeLMNMRgWNZrNZ10+iYgJaqy+HCUjnNXB2R375ggQ/6Ymqcheo5IAMW/uB0K24HJnal4eLI+PH8XYGaD9w+aaGjafp6RUpChAZxc6s6hoTMloPIBYvNJSkWIAnSWj7OPHR1xo50v8cQBxXyWlIkUAeioZuQKIg1BKKpIdIB/JyB2AOBAlpCJZAfKVjNwFiIORWyqSDaDj6l2U0QiJO3a4PTHgCUB7zKnIywN0YqSWiuQAKFgy8hSgfVDoTpeYTOxXqaQiSQE6rt6hD7jtyBFekhFfgDg4lIr2p6fb3WnRpSLJADrGFJSMcAaFrwstBCAOUEqpSBKAzjFF6MyJUIA4SKmkItEBSiEZiQEQByqFVCQqQKkkI7EA4mDFlopEAyilZCQmQBwwrmuUZWePuNNCpCIxAI6SjPBdN3znTcxNbID22urKyqAyP3845vCUigQBdHahf33ggCSSkVQAcfAoFeFbn3Z32lOpiDdAZ8kI3yyXyoWWEqA95uD7x3ykIl4AnV1ozHhSSkZSA0QIfKUijwE6S0amEyckd6HlAIgg+EhFHgFUSjKSCyDC8FQqchugkpKRnABZzPFAKnIHIInW6/MIIdtx5zWbN0N8WpqYKcVlX3IDtBdUU1QEJw8dGo4540hFjwWoFslIKYAIB6Wi32dmunSnv7esqSbJSEmACBHd6b3JySNS0Vju9CiAzi40xhSlJCMcgNIAsQaUilB2Gs+dHgHoGFNQMsqprFTchVYDQIToLBU5utMMoHNM2V5cjBKPyy95qXdQC0AcZ39vL3vjEwUnR3caJwReB4CP0IVeEhXFrFRX/z1IanD2/tUEEGvC52a8sXzW2IizEFYbpXFkVWhoiP/UqVeef+WVS5lFRX+TC447x4kND2crQ3WtrTnu7C/XPvlpaT+/0NIyb6CvL2L4Eg4LC6xvb++RqwB3jxNjMLB/tlhvNispQY1ZLqYVfJtJdYU5VqtmgO48ibh7oki2nxegQLRegF6AAgkIbO49A70ABRIQ2Nx7BnoBCiQgsLn3DPQCFEhAYHPvGfh/APB/5gR7rwDtHTkAAAAASUVORK5CYII="
                    // console.log("----------------",imgDataUri);
                    graph.setCellStyles(mxConstants.STYLE_IMAGE, imgDataUri.replace(';',''), [cell]);//方法一 设置图片 注意需要替换';'号
                    if(cell.nodeType != "Text"){
                        cell.value="";//清空节点值
                    }

                    //方法二也可以
                    //onOffClose[mxConstants.STYLE_IMAGE] =imgDataUri;
                    //graph.getStylesheet().putCellStyle('onOffClose_image4gray', onOffClose);

                } finally {
                    graph.getModel().endUpdate();
                }
            }
        }catch (e){
            console.log("e",e);
        }

    // }
}

//更改线条颜色
function ChStockFill(cell,graph) {
    if (null == cell || undefined === cell) {
        return;
    }
    //如果节点是连线 开关，点，改变填充颜色
    if (cell.nodeType == 'onoffclose2' ||
        cell.nodeType == 'onoffopen2' ||
        cell.nodeType == 'onOffClose' ||
        cell.nodeType == 'onOffOpen' ||
        cell.nodeType == "point") {
        setTimeout(function () {
            graph.getModel().beginUpdate();
            try {
                graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, cell.fills, [cell]);
                cell.value = "";//清空节点值
            } finally {
                graph.getModel().endUpdate();
            }
        }, "500");
        return;
    }

    try {
        var stockFill = cell.stockFill;//线条颜色
        graph.getModel().beginUpdate();
        try{
            graph.setCellStyles("stockFill", stockFill, [cell]);
            if(cell.nodeType != "Text"){
                cell.value="";//清空节点值
            }
        } finally {
            graph.getModel().endUpdate();
        }
    }catch (e){
        console.log("e",e);
    }
}

//填充颜色
function ChFills(cell,graph){
    if (null == cell || undefined === cell) {
        return;
    }
    //如果节点是连线 开关，点，改变填充颜色
    if (cell.nodeType == 'onoffclose2' ||
        cell.nodeType == 'onoffopen2' ||
        cell.nodeType == 'onOffClose' ||
        cell.nodeType == 'onOffOpen' ||
        cell.nodeType == "point") {
        setTimeout(function () {
            graph.getModel().beginUpdate();
            try {
                graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, cell.fills, [cell]);
                cell.value = "";//清空节点值
            } finally {
                graph.getModel().endUpdate();
            }
        }, "500");
        return;
    }

    try {
        var fills = cell.fills;//填充
        graph.getModel().beginUpdate();
        try{
            graph.setCellStyles("fills", fills, [cell]);
            if(cell.nodeType != "Text"){
                cell.value="";//清空节点值
            }
        } finally {
            graph.getModel().endUpdate();
        }
    }catch (e){
        console.log("e",e);
    }
}


// function chCellColor(cell,graph){
//     if(null == cell || undefined === cell){
//         return;
//     }
//     //如果节点是连线 开关，点，改变填充颜色
//     if(cell.nodeType == 'onoffclose2' ||
//         cell.nodeType == 'onoffopen2' ||
//         cell.nodeType == 'onOffClose' ||
//         cell.nodeType == 'onOffOpen' ||
//         cell.nodeType == "point"){
//         setTimeout(function(){
//             graph.getModel().beginUpdate();
//             try{
//                 graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, cell.fills, [cell]);
//                 cell.value="";//清空节点值
//             } finally {
//                 graph.getModel().endUpdate();
//             }
//         },"500");
//         return;
//     }
//
//
//     var svgPath;
//     var stockFill = cell.stockFill;//线条颜色
//     var fills = cell.fills;//填充
//     var width = 32;
//     var height =32;
//     if(undefined != cell.svg && null != cell.svg){
//         svgPath = cell.svg;
//         // width = cell.width;
//         // height = cell.height;
//     }
//     console.log("svgPath",svgPath);
//     var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
//     svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
//     svg.setAttribute('xml:space', 'preserve')
//     svg.setAttribute('width', width)
//     svg.setAttribute('height', height)
//     svg.setAttribute('version', '1.1')
//
//     svg.setAttribute('viewBox', '0 0 '+width+' '+height)
//     svg.setAttribute('preserveAspectRatio', 'none')
//     // svg.setAttribute('style', 'shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd')
//     svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink')
//
//     var g = document.createElementNS("http://www.w3.org/2000/svg", "g")
//     g.setAttribute('id', 'G_x0020_1')
//     var metadata = document.createElementNS("http://www.w3.org/2000/svg", "metadata")
//     metadata.setAttribute('id', 'CorelCorpID_0Corel-Layer')
//     var cong = document.createElementNS("http://www.w3.org/2000/svg", "g")
//     cong.setAttribute('id', '_1828549179696')
//
//     // var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect")
//     // rect.setAttribute('class', 'fil0 str0')
//     // rect.setAttribute('x', '17')
//     // rect.setAttribute('y', '17')
//     // rect.setAttribute('width', '32')
//     // rect.setAttribute('height', '32')
//     var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
//     // path.setAttribute('class', 'fil1')
//     if(null != svgPath){
//         path.setAttribute('d', svgPath)
//         path.setAttribute('stroke', stockFill)
//         path.setAttribute('fill', fills)
//     }
//
//
//     // var defs = document.createElementNS("http://www.w3.org/2000/svg", "defs")
//     // var style = document.createElementNS("http://www.w3.org/2000/svg", "style")
//     // style.setAttribute('type', 'text/css')
//     // style.innerHTML =
//     //     ".str0 {" +
//     //     "stroke: #fff; stroke-width:3;}" +
//     //     ".fil0 {fill:none }" +
//     //     ".fil1 {" +
//     //     "fill: #FF0000;}"
//     g.appendChild(metadata)
//     g.appendChild(cong)
//     // cong.appendChild(rect)
//     cong.appendChild(path)
//     // defs.appendChild(style)
//     // svg.appendChild(defs)
//     svg.appendChild(g)
//
//     try {
//         graph.getModel().beginUpdate();
//         try{
//             // var CELLStyle = new Object();
//             // CELLStyle[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
//             // CELLStyle[mxConstants.STYLE_FONTSIZE] = '8';
//             // CELLStyle[mxConstants.STYLE_FONTCOLOR] = '#000';
//             // CELLStyle[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
//             // CELLStyle[mxConstants.STYLE_IMAGE] = MXSOURCESPATH+'/images/power/point.png';
//             // graph.getStylesheet().putCellStyle('CELLStyle', CELLStyle);
//             // graph.getStylesheet().putCellStyle('CELLStyle', CELLStyle);
//             graph.setCellStyles("stockFill", stockFill, [cell]);//方法一 设置图片 注意需要替换';'号
//             // graph.setCellStyles(mxConstants.STYLE_IMAGE, "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(new XMLSerializer().serializeToString(svg)))), [cell]);//方法一 设置图片 注意需要替换';'号
//             if(cell.nodeType != "Text"){
//                 cell.value="";//清空节点值
//             }
//         } finally {
//             graph.getModel().endUpdate();
//         }
//     }catch (e){
//         console.log("e",e);
//     }
// }

function imgBase64(){}

function initSidebarSvg(cell,graph){
    svgToBase64(cell,graph);
}

//
// function svgToBase64(graph,onOffClose){
//     var svgtemplate = document.querySelector(".svgtemplate");
//     var svgCon = svgtemplate.outerHTML;
//
//     //svg转base64;
//     var svgBase64 = 'data:image/svg+xml;base64,' + window.btoa(unescape(encodeURIComponent(svgCon)));
//
//     var image = document.createElement('img');
//     image.style.position = 'absolute';
//     image.style.marginLeft = '25px';
//     image.style.marginTop = '25px';
//     image.setAttribute('width', 50);
//     image.setAttribute('height',50);
//     image.src = svgBase64;
//     image.onload=function(){
//         //svg转png的base64编码;
//         var imgDataUri = image2Base64(image);
//         onOffClose[mxConstants.STYLE_IMAGE] = imgDataUri;
//         console.log("imgDataUri",imgDataUri);
//         graph.getStylesheet().putCellStyle('onOffClose_image4gray', onOffClose);
//     }
// }

function image2Base64(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, img.width, img.height);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
}




/**
 * method:上传操作
 * fieldid:附件编号
 * picid:附件上传后回显图片对象编号
 * validateparameter:校验非法参数组装字符串
 * validateSize:校验大小
 * xt_path_absolutek:平台路径配置中心键（自定义上传对绝路径使用）
 * xt_path_urlk:平台路径配置中心键（自定义上传路径 自定义URL地址）
 * xt_path_relativek:平台路径配置中心键（自定义上传相对路径）
 * llowedFileExtensions:['jpg','gif','png']
 **/
var fileUploadResultArray = [];
function initGraphFile(graph){
    fileUploadResultArray.push('0');
    var allowedFileExtensions_ = ['xml'];
    var maxFileSize_ = 0;
    $('.graphXml').fileinput('clear');
    $("#graphXml").attr("class","file-loading")
    $("#graphXml").fileinput({
        showUpload:true, //是否显示上传按钮
        autoReplace : true,
        showCaption:true,
        showPreview:true,
        overwriteInitial:true,//覆盖已存在的文件
        uploadUrl:baseUrl+'import',
        enctype:'multipart/form-data',
        language:'zh',
        allowedFileExtensions:allowedFileExtensions_,//接收的文件后缀
        minFileCount:1,
        uploadAsync:true,/**默认异步上传**/
        showCaption:true,/**是否显示标题**/
        maxFileSize:maxFileSize_,/**单位为kb，如果为0表示不限制文件大小**/
        maxFileCount:1,/**表示允许同时上传的最大文件个数**/
        enctype:'multipart/form-data',
        validateInitialCount:true,
        browseClass : "btn btn-info m-btn m-btn--custom m-btn--icon", //按钮样式
        msgFilesTooMany:"选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        uploadExtraData:function (previewId, index) {
            var data = {
            };
            return data;
        }
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
        for(var i = 0; i < fileUploadResultArray.length; i++){
            if(i == 0){
                var obj = data.response;
                loadXml(graph,obj.mxgraphxml);
                resetZxLine(graph);
                //关闭上传窗口
                $('#jehcImportModal').modal('hide');
                $('#graphXml').val('');
                $("#graphXml").fileinput('reset'); //重置上传控件（清空已文件）
                $('.graphXml').fileinput('destroy');
                $("#graphXml").removeAttr("class","file-loading")

                // if(obj.data.jsonID != 0){
                //     //关闭上传窗口
                //     $('#jehcImportModal').modal('hide');
                //     //并清空上传控件内容
                //     $('#graphXml').val('');
                //     $("#graphXml").fileinput('reset'); //重置上传控件（清空已文件）
                //     window.parent.toastrBoot(3,obj.data.msg);
                // }else{
                //     window.parent.toastrBoot(4,obj.data.msg);
                // }
                fileUploadResultArray.splice(0,fileUploadResultArray.length);
                i--;
                break;
            }
        }
    });
}

////导入流程
function imp(graph,history){
    graph_refresh = graph;
    var jehcImportModalCount = 0 ;
    $('#jehcImportModal').modal({backdrop: 'static', keyboard: false});
    $('#jehcImportModalLabel').html("选择要导入的文件");
    $('#jehcImportModal').on("shown.bs.modal",function(){
        $("#jehcImportForm")[0].reset();
        if(++jehcImportModalCount == 1){
            initGraphFile(graph);
        }

    })
}
function doImpl(id){
    msgTishCallFnBoot("确定导入该文件？",function(){
        // ajaxBRequestCallFn(rest_url_prefix_lc+'/lcProcess/get/'+id,null,function(result){
        //     if(typeof(result.success) != "undefined" && result.success == true){
        //         var lc_process_title = result.data.lc_process_title;
        //         var lc_process_mxgraph_style = result.data.lc_process_mxgraph_style;
        //         var lc_process_mxgraphxml = result.data.lc_process_mxgraphxml;
        //         var processId = result.data.lc_process_uid;
        //         var candidateStarterUsers = result.data.candidateStarterUsers;
        //         var candidateStarterGroups = result.data.candidateStarterGroups;
        //         var lc_process_remark = result.data.lc_process_remark;
        //         var lc_process_id = result.data.lc_process_id;
        //         var xt_constant_id = result.data.xt_constant_id;
        //         //////////////////////////////////开始导入XML文件///////////////////////////
        //         var graph = graph_refresh;
        //         graph.getModel().beginUpdate();
        //         try{
        //             /*
        //              * 直接读取流程图的xml, 并展示流程图
        //             */
        //             if(lc_process_mxgraphxml != null && lc_process_mxgraphxml.length > 0){
        //                 var doc = mxUtils.parseXml(lc_process_mxgraphxml);
        //                 var dec = new mxCodec(doc);
        //                 dec.decode(doc.documentElement, graph.getModel());
        //             }
        //             validatePOOL(graph);
        //         }catch (e){
        //             console.log("---导入流程出现异常---",e)
        //         }finally{
        //             graph_refresh.getModel().endUpdate();
        //             linetostyle(lc_process_mxgraph_style,graph);
        //             graph_refresh.refresh();
        //             $('#processId').val(processId);
        //             $('#processName').val(lc_process_title);
        //             $('#mxgraphxml').val(lc_process_mxgraphxml);
        //             $('#lc_process_mxgraph_style').val(lc_process_mxgraph_style);
        //             $('#lc_process_id').val(lc_process_id);
        //             $('#candidateStarterUsers').val(candidateStarterUsers);
        //             $('#remark').val(lc_process_remark);
        //             $('#xt_constant_id').val(xt_constant_id);
        //             $('#processSelectModal').modal('hide');
        //         }
        //         //////////////////////////////////结束导入XML文件///////////////////////////
        //     }
        // },null,"GET");
    })
}

function initData(graph){
    var params = {graph_site_id:$('#graph_site_id').val()};
    ajaxBRequestCallFn(basePath+'/graphSiteController/getGraphSiteById',params,function(result){
        result = eval("(" + result + ")");
        loadXml(graph,result.data.graph_site_mxgraph);
    });
}