//////////////数据属性事件子流程 子流程 事物流程 流程信息等地方使用///////////
//数据属性表格
var datamainProperties_grid;

/**
 * 创建数据属性数据源Grid
 */
function createDatamainPropertiesGrid(){
    datamainProperties_grid =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" onclick=\"addDatamainPropertiesRow()\">新一行</button>"+
					"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='datamainProperties_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_datamainProperties_form\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='datamainProperties_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f8f8f8;'>"+
						"<div class=\"col-md-3\">编号</div>"+
						"<div class=\"col-md-3\">名称</div>"+
        				"<div class=\"col-md-2\">类型</div>"+
						"<div class=\"col-md-3\">值</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return datamainProperties_grid;
}


/**
 * 新一行
 * @param ID
 * @param name
 * @param type
 * @param value
 */
function addDatamainPropertiesRow(ID,name,type,value){
    if(ID === undefined){
        ID = "";
    }
    if(name === undefined){
        name = "";
    }
    if(type === undefined){
        type = "";
    }
    if(value === undefined){
        value = "";
    }

    var uuid = guid();
    validatorDestroy('node_datamainProperties_form');
    data:[["string",""],["","boolean"],["","datetime"],["","double"],["","int"],["","long"]]
    var typeOption =  "<option value=''>请选择</option>" +
        "<option value='string'>string</option>" +
        "<option value='boolean'>boolean</option>"+
        "<option value='datetime'>datetime</option>"+
        "<option value='double'>double</option>"+
        "<option value='int'>int</option>"+
        "<option value='long'>long</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_datamainProperties_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='ID"+uuid+"' name='ID' value='"+ID+"' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='name"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"name\" value='"+name+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//类型
			"<div class=\"col-md-2\">"+
				"<select class=\"form-control\" id='type"+uuid+"' name=\"type\">" +
					typeOption+
				"</select>"+
			"</div>"+

			//值
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='value"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"value\" value='"+value+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delDatamainPropertiesRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#datamainProperties_grid").append(rows);
    $("#type"+uuid).val(type);
    reValidator('node_datamainProperties_form');
}

/**
 * 删除
 * @param rowID
 */
function delDatamainPropertiesRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_datamainProperties_form');
        $("#node_datamainProperties_rows_"+rowID).remove();
        reValidator('node_datamainProperties_form');
    });
}

/**
 * 初始化表单及数据
 */
function initDatamainProperties_grid(){
    //表单配置
    $('#node_datamainProperties_form').bootstrapValidator({
        message:'此值不是有效的'
    });
    initDatamainProperties_data();
}


/**
 * 初始化列数据
 */
function initDatamainProperties_data(){
    var datamainProperties_node_value = $("#datamainProperties_node_value").val();
    if(null != datamainProperties_node_value && "" != datamainProperties_node_value){
        datamainProperties_node_value = eval('(' + datamainProperties_node_value + ')');
        var IDList = $.makeArray(datamainProperties_node_value["ID"]);
        var nameList = $.makeArray(datamainProperties_node_value["name"]);
        var typeList = $.makeArray(datamainProperties_node_value["type"]);
        var valueList = $.makeArray(datamainProperties_node_value["value"]);
        for(var i = 0; i < nameList.length; i++){
            var name = nameList[i];
            var ID = IDList[i];
            var type = typeList[i];
            var value = valueList[i];
            addDatamainPropertiesRow(ID,name,type,value);
        }
    }
}

/**
 * 点击确定按钮
 * @param cell
 */
function datamainProperties_setvalue(){
    var bootform = $('#node_datamainProperties_form');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#datamainProperties_node_value").val(JSON.stringify($("#node_datamainProperties_form").serializeObject()))
}