var messageCatchingEventForm;

/**
 * 消息捕捉事件
 * @param cell
 * @param graph_refresh
 * @private
 */
function messageCatchingEventWin_(cell,graph_refresh){
    messageCatchingEventPanel(cell,graph_refresh);
}

/**
 *
 * @returns {string|*}
 */
function createMessageCatchingEventForm(){
    messageCatchingEventForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+

					//消息依附
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"control-label\" >消息依附</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" id=\"messageRef\" name=\"messageRef\" placeholder=\"请输入消息依附\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return messageCatchingEventForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initMessageCatchingEventData(cell){
    var messageRef = cell.messageRef;
    $("#messageRef").val(messageRef);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function messageCatchingEventPanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,2);
    event_grid = creatEventGrid(cell);
    messageCatchingEventForm = createMessageCatchingEventForm(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本配置</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

				"<a href='javascript:setMessageCatchingEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+messageCatchingEventForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:400px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(formInfo);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    //基本属性
    initMessageCatchingEventData(cell);
    nodeScroll();
}

/**
 * 设置内容
 */
function setMessageCatchingEventValue(){
    var messageRef = $('#messageRef').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本配置并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本配置
        if(null != messageRef && "" != messageRef){
            JehcClickCell.messageRef = messageRef;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}