//返回r
function goback(){
    tlocation("../graphPelController/loadGraphPel");
}
$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});
//保存
function updateGraphPel(){
    submitBForm('defaultForm','../graphPelController/updateGraphPel','../graphPelController/loadGraphPel');
}


function setVal(thiz,type,numbers){
    if(type == 1){
        $("#form_graphPelElement_"+numbers+"_fills").val(thiz.value);
    }else{
        $("#form_graphPelElement_"+numbers+"_stock").val(thiz.value);
    }
    preImages();

}

//$(document).ready(function(){
//    $("#file-input").change(function(e){
//        var file = e.target.files[0];
//        var reader = new FileReader();
//        reader.readAsDataURL(file); //读出 base64
//        reader.onloadend = function () {
//            var data_64= "data:image/png;base64,"+reader.result.substring(reader.result.indexOf(",")+1);
//            $("#image").val(data_64);
//            $("#imgCav").attr("src",data_64);
//        };
//    });
//});



function preImages(){
    try{
        var svgPath = $('#svg').val();
        var stockFill = $('#pickColors').val();//线条颜色
        var fills = $('#fillColors').val();//填充
        var width=32;//图元宽度
        var height =32;//图元高度
        if(null == svgPath || '' ==svgPath){
            return;

        }
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
        svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
        svg.setAttribute('xml:space', 'preserve')
        svg.setAttribute('width', width)
        svg.setAttribute('height', height)
        svg.setAttribute('version', '1.1')

        svg.setAttribute('viewBox', '0 0 '+width+' '+height)
        svg.setAttribute('preserveAspectRatio', 'none')
        // svg.setAttribute('style', 'shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd')
        svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink')

        var g = document.createElementNS("http://www.w3.org/2000/svg", "g")
        g.setAttribute('id', 'G_x0020_1')
        var metadata = document.createElementNS("http://www.w3.org/2000/svg", "metadata")
        metadata.setAttribute('id', 'CorelCorpID_0Corel-Layer')
        var cong = document.createElementNS("http://www.w3.org/2000/svg", "g")
        cong.setAttribute('id', '_1828549179696')

        // var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect")
        // rect.setAttribute('class', 'fil0 str0')
        // rect.setAttribute('x', '17')
        // rect.setAttribute('y', '17')
        // rect.setAttribute('width', '32')
        // rect.setAttribute('height', '32')
        var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
        // path.setAttribute('class', 'fil1')
        if(null != svgPath){
            path.setAttribute('d', svgPath)
            path.setAttribute('stroke', stockFill)
            if(null == fills || '' == fills || '#ffffff' == fills || '#fff' == fills){
                path.setAttribute('fill', "none")
            }else{
                path.setAttribute('fill', fills)
            }

        }


        // var defs = document.createElementNS("http://www.w3.org/2000/svg", "defs")
        // var style = document.createElementNS("http://www.w3.org/2000/svg", "style")
        // style.setAttribute('type', 'text/css')
        // style.innerHTML =
        //     ".str0 {" +
        //     "stroke: #fff; stroke-width:3;}" +
        //     ".fil0 {fill:none }" +
        //     ".fil1 {" +
        //     "fill: #FF0000;}"
        g.appendChild(metadata)
        g.appendChild(cong)
        // cong.appendChild(rect)
        cong.appendChild(path)
        // defs.appendChild(style)
        // svg.appendChild(defs)
        svg.appendChild(g);
        var data_64 = "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(new XMLSerializer().serializeToString(svg))));
        $("#imgCav").attr("src",data_64);
        $("#image").val(data_64);
    }catch(e){

    }
}


function addGraphPelElementItems(){
    validatorDestroy('defaultForm');
    var numbers = $('#graphPelElementFormNumber').val();
    numbers = parseInt(numbers);
    $('#graphPelElementFormNumber').val(numbers);
    //点击添加新一行
    var removeBtn = '<a class="btn btn-secondary m-btn m-btn--custom m-btn--icon" href="javascript:delGraphPelElementItems(this,'+numbers+')" >删除</a>';
    var form =
        '<div id="form_graphPelElement_'+numbers+'">'+
            '<fieldset>'+
                '<legend><h5>No.'+(numbers+1)+'</h5></legend>'+
                '<div class="form-group m-form__group row"><div class="col-md-1">'+removeBtn+'</div></div>'+
                '<div class="form-group m-form__group row">'+
                    '<label class="col-md-1 col-form-label">节点类型</label>'+
                '<div class="col-md-1">'+
                    '<select class="form-control" id="form_graphPelElement_'+numbers+'_graph_pel_element_categray" name="graphPelElements['+numbers+'].graph_pel_element_categray"  placeholder="请选择"><option value="">请选择</option><option value="path">path</option><option value="ellipse">ellipse</option></select>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">填充颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" value="#ffffff" readOnly id="form_graphPelElement_'+numbers+'_fills" name="graphPelElements['+numbers+'].fills"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#ffffff" onchange="setVal(this,1,'+numbers+')" id="fillColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线条颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" readOnly value="#FF0000" id="form_graphPelElement_'+numbers+'_stock" name="graphPelElements['+numbers+'].stock"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#FF0000" onchange="setVal(this,2,'+numbers+')" id="strokeColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线框宽度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+numbers+'_stroke_width" name="graphPelElements['+numbers+'].stroke_width"  placeholder="请输入"/>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">排序号</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+numbers+'_graph_pel_element_sort" name="graphPelElements['+numbers+'].graph_pel_element_sort"  placeholder="请输入"/>'+
                '</div>'+
                '</div>'+
                '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">属性代码</label>'+
                '<div class="col-md-7">'+
                    '<textarea class="form-control" rows="1" id="form_graphPelElement_'+numbers+'_graph_pel_element_attrs" name="graphPelElements['+numbers+'].graph_pel_element_attrs"  placeholder="请输入"/>'+
                '</div>'+
                '</div>'+
            '</fieldset>'+
        '</div>'
    $(".form_graphPelElement").append(form);
    reValidator('defaultForm');
}


function delGraphPelElementItems(thiz,numbers){
    validatorDestroy('defaultForm');
    $("#form_graphPelElement_"+numbers).remove();
    var graphPelElement_removed_flag = $('#graphPelElement_removed_flag').val()
    if(null == graphPelElement_removed_flag || '' == graphPelElement_removed_flag){
        $('#graphPelElement_removed_flag').val(','+numbers+',');
    }else{
        $('#graphPelElement_removed_flag').val(graphPelElement_removed_flag+numbers+',')
    }
    reValidator('defaultForm');
}
