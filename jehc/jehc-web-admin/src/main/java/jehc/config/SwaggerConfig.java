package jehc.config;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import com.google.common.base.Predicate;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(){
        StringBuilder basePackage = new StringBuilder("jehc.bmodules.bweb");
        basePackage.append(",jehc.cmsmodules.cmsweb");
        basePackage.append(",jehc.crmmodules.crmweb");
        basePackage.append(",jehc.lcmodules.lcweb");
        basePackage.append(",jehc.oamodules.oaweb");
        basePackage.append(",jehc.paymentmodules.paymentweb");
        basePackage.append(",jehc.solrmodules.solrweb");
        basePackage.append(",jehc.xtmodules.xtweb");
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(true)
                .apiInfo(apiInfo()).select()
                .apis(SwaggerConfig.basePackage(basePackage.toString()))
                .paths(PathSelectors.any()).build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("jehc", "", "");
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("jEhc构建API文档")//大标题
                .description("REST风格API")//小标题
                .contact(contact)//作者
                .termsOfServiceUrl("")
                .version("V1.0.0")//版本
                .license("主页")//链接显示文字
                .licenseUrl("")//网站链接
                .build();
        return apiInfo;
    }

    public static Predicate<RequestHandler> basePackage(final String basePackage) {
        return new Predicate<RequestHandler>() { 
            @Override
            public boolean apply(RequestHandler input) {
                return declaringClass(input).transform(handlerPackage(basePackage)).or(true);
            }
        };
    }
    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage) {
        return new Function<Class<?>, Boolean>() {

            @Override
            public Boolean apply(Class<?> input) {
                for (String strPackage : basePackage.split(",")) {
                    boolean isMatch = input.getPackage().getName().startsWith(strPackage);
                    if (isMatch) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    /**
     * @param input RequestHandler
     * @return Optional
     */
    private static Optional<? extends Class<?>> declaringClass(RequestHandler input) {
        return Optional.fromNullable(input.declaringClass());
    }
}
