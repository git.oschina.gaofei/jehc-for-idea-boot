//package jehc.config;
//
//import jehc.xtmodules.xtcore.interceptor.AuthHandler;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
///**
// * 注册拦截器
// */
//@Configuration
//public class RegisterAdapter implements WebMvcConfigurer {
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 添加一个拦截器，连接以/** url路径
//        registry.addInterceptor(new AuthHandler()).addPathPatterns("/**");
//    }
//}
