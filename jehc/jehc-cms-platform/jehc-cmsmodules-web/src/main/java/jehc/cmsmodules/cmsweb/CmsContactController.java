package jehc.cmsmodules.cmsweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.cmsmodules.cmsmodel.CmsAbout;
import jehc.cmsmodules.cmsmodel.CmsContact;
import jehc.cmsmodules.cmsservice.CmsContactService;

/**
* 内容发布平台联系我们 
* 2018-06-10 14:42:49  邓纯杰
*/
@Api(value = "内容发布平台联系我们", description = "内容发布平台联系我们")
@Controller
@RequestMapping("/cmsContactController")
public class CmsContactController extends BaseAction{
	@Autowired
	private CmsContactService cmsContactService;
	/**
	* 列表页面
	* @param cmsContact
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCmsContact(CmsContact cmsContact,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-contact/cms-contact-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCmsContactListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsContactListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<CmsContact> cmsContactList = cmsContactService.getCmsContactListByCondition(condition);
		String jehcimg_base_url = callBaseFileUrl();
		for(int i = 0; i < cmsContactList.size(); i++){
			cmsContactList.get(i).setXt_attachmentPath(jehcimg_base_url+cmsContactList.get(i).getXt_attachmentPath());
		}
		PageInfo<CmsContact> page = new PageInfo<CmsContact>(cmsContactList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param cms_contact_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCmsContactById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsContactById(String cms_contact_id,HttpServletRequest request){
		CmsContact cmsContact = cmsContactService.getCmsContactById(cms_contact_id);
		return outDataStr(cmsContact);
	}
	/**
	* 添加
	* @param cmsContact
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public String addCmsContact(CmsContact cmsContact,HttpServletRequest request){
		int i = 0;
		if(null != cmsContact){
			cmsContact.setCms_contact_id(UUID.toUUID());
			cmsContact.setCtime(getDate());
			cmsContact.setXt_userinfo_id(getXtUid());
			i=cmsContactService.addCmsContact(cmsContact);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param cmsContact
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCmsContact(CmsContact cmsContact,HttpServletRequest request){
		int i = 0;
		if(null != cmsContact){
			cmsContact.setMtime(getDate());
			i=cmsContactService.updateCmsContact(cmsContact);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param cms_contact_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public String delCmsContact(String cms_contact_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(cms_contact_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("cms_contact_id",cms_contact_id.split(","));
			i=cmsContactService.delCmsContact(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param cms_contact_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public String copyCmsContact(String cms_contact_id,HttpServletRequest request){
		int i = 0;
		CmsContact cmsContact = cmsContactService.getCmsContactById(cms_contact_id);
		if(null != cmsContact){
			cmsContact.setCms_contact_id(UUID.toUUID());
			i=cmsContactService.addCmsContact(cmsContact);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCmsContact",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCmsContact(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCmsContactAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsContactAdd(CmsContact cmsContact,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-contact/cms-contact-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCmsContactUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsContactUpdate(String cms_contact_id,HttpServletRequest request, Model model){
		CmsContact cmsContact = cmsContactService.getCmsContactById(cms_contact_id);
		model.addAttribute("cmsContact", cmsContact);
		return new ModelAndView("pc/cms-view/cms-contact/cms-contact-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCmsContactDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsContactDetail(String cms_contact_id,HttpServletRequest request, Model model){
		CmsContact cmsContact = cmsContactService.getCmsContactById(cms_contact_id);
		model.addAttribute("cmsContact", cmsContact);
		return new ModelAndView("pc/cms-view/cms-contact/cms-contact-detail");
	}
}
