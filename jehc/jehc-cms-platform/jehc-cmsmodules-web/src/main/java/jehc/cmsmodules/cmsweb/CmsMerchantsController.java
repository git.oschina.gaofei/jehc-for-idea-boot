package jehc.cmsmodules.cmsweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.cmsmodules.cmsmodel.CmsMerchants;
import jehc.cmsmodules.cmsservice.CmsMerchantsService;

/**
* 内容发布平台招商加盟 
* 2018-06-10 14:47:12  邓纯杰
*/
@Api(value = "内容发布平台招商加盟", description = "内容发布平台招商加盟")
@Controller
@RequestMapping("/cmsMerchantsController")
public class CmsMerchantsController extends BaseAction{
	@Autowired
	private CmsMerchantsService cmsMerchantsService;
	/**
	* 列表页面
	* @param cmsMerchants
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCmsMerchants(CmsMerchants cmsMerchants,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-merchants/cms-merchants-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCmsMerchantsListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsMerchantsListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<CmsMerchants> cmsMerchantsList = cmsMerchantsService.getCmsMerchantsListByCondition(condition);
		String jehcimg_base_url = callBaseFileUrl();
		for(int i = 0; i < cmsMerchantsList.size(); i++){
			cmsMerchantsList.get(i).setXt_attachmentPath(jehcimg_base_url+cmsMerchantsList.get(i).getXt_attachmentPath());
		}
		PageInfo<CmsMerchants> page = new PageInfo<CmsMerchants>(cmsMerchantsList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param cms_merchants_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCmsMerchantsById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsMerchantsById(String cms_merchants_id,HttpServletRequest request){
		CmsMerchants cmsMerchants = cmsMerchantsService.getCmsMerchantsById(cms_merchants_id);
		return outDataStr(cmsMerchants);
	}
	/**
	* 添加
	* @param cmsMerchants
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public String addCmsMerchants(CmsMerchants cmsMerchants,HttpServletRequest request){
		int i = 0;
		if(null != cmsMerchants){
			cmsMerchants.setCtime(getDate());
			cmsMerchants.setXt_userinfo_id(getXtUid());
			cmsMerchants.setCms_merchants_id(UUID.toUUID());
			i=cmsMerchantsService.addCmsMerchants(cmsMerchants);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param cmsMerchants
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCmsMerchants(CmsMerchants cmsMerchants,HttpServletRequest request){
		int i = 0;
		if(null != cmsMerchants){
			cmsMerchants.setMtime(getDate());
			i=cmsMerchantsService.updateCmsMerchants(cmsMerchants);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param cms_merchants_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public String delCmsMerchants(String cms_merchants_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(cms_merchants_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("cms_merchants_id",cms_merchants_id.split(","));
			i=cmsMerchantsService.delCmsMerchants(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param cms_merchants_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public String copyCmsMerchants(String cms_merchants_id,HttpServletRequest request){
		int i = 0;
		CmsMerchants cmsMerchants = cmsMerchantsService.getCmsMerchantsById(cms_merchants_id);
		if(null != cmsMerchants){
			cmsMerchants.setCms_merchants_id(UUID.toUUID());
			i=cmsMerchantsService.addCmsMerchants(cmsMerchants);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCmsMerchants",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCmsMerchants(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCmsMerchantsAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsMerchantsAdd(CmsMerchants cmsMerchants,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-merchants/cms-merchants-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCmsMerchantsUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsMerchantsUpdate(String cms_merchants_id,HttpServletRequest request, Model model){
		CmsMerchants cmsMerchants = cmsMerchantsService.getCmsMerchantsById(cms_merchants_id);
		model.addAttribute("cmsMerchants", cmsMerchants);
		return new ModelAndView("pc/cms-view/cms-merchants/cms-merchants-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCmsMerchantsDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsMerchantsDetail(String cms_merchants_id,HttpServletRequest request, Model model){
		CmsMerchants cmsMerchants = cmsMerchantsService.getCmsMerchantsById(cms_merchants_id);
		model.addAttribute("cmsMerchants", cmsMerchants);
		return new ModelAndView("pc/cms-view/cms-merchants/cms-merchants-detail");
	}
}
