package jehc.cmsmodules.cmsweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.cmsmodules.cmsmodel.CmsCase;
import jehc.cmsmodules.cmsservice.CmsCaseService;

/**
* 内容发布平台案例 
* 2018-06-10 18:01:22  邓纯
*/
@Api(value = "内容发布平台案例", description = "内容发布平台案例")
@Controller
@RequestMapping("/cmsCaseController")
public class CmsCaseController extends BaseAction{
	@Autowired
	private CmsCaseService cmsCaseService;
	/**
	* 列表页面
	* @param cmsCase
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCmsCase(CmsCase cmsCase,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-case/cms-case-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCmsCaseListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsCaseListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<CmsCase> cmsCaseList = cmsCaseService.getCmsCaseListByCondition(condition);
		String jehcimg_base_url = callBaseFileUrl();
		for(int i = 0; i < cmsCaseList.size(); i++){
			cmsCaseList.get(i).setXt_attachmentPath(jehcimg_base_url+cmsCaseList.get(i).getXt_attachmentPath());
		}
		PageInfo<CmsCase> page = new PageInfo<CmsCase>(cmsCaseList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param cms_case_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCmsCaseById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsCaseById(String cms_case_id,HttpServletRequest request){
		CmsCase cmsCase = cmsCaseService.getCmsCaseById(cms_case_id);
		return outDataStr(cmsCase);
	}
	/**
	* 添加
	* @param cmsCase
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public String addCmsCase(CmsCase cmsCase,HttpServletRequest request){
		int i = 0;
		if(null != cmsCase){
			cmsCase.setCms_case_id(UUID.toUUID());
			cmsCase.setCtime(getDate());
			cmsCase.setXt_userinfo_id(getXtUid());
			i=cmsCaseService.addCmsCase(cmsCase);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param cmsCase
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCmsCase(CmsCase cmsCase,HttpServletRequest request){
		int i = 0;
		if(null != cmsCase){
			cmsCase.setMtime(getDate());
			i=cmsCaseService.updateCmsCase(cmsCase);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param cms_case_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public String delCmsCase(String cms_case_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(cms_case_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("cms_case_id",cms_case_id.split(","));
			i=cmsCaseService.delCmsCase(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param cms_case_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public String copyCmsCase(String cms_case_id,HttpServletRequest request){
		int i = 0;
		CmsCase cmsCase = cmsCaseService.getCmsCaseById(cms_case_id);
		if(null != cmsCase){
			cmsCase.setCms_case_id(UUID.toUUID());
			i=cmsCaseService.addCmsCase(cmsCase);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCmsCase",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCmsCase(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCmsCaseAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsCaseAdd(CmsCase cmsCase,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-case/cms-case-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCmsCaseUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsCaseUpdate(String cms_case_id,HttpServletRequest request, Model model){
		CmsCase cmsCase = cmsCaseService.getCmsCaseById(cms_case_id);
		model.addAttribute("cmsCase", cmsCase);
		return new ModelAndView("pc/cms-view/cms-case/cms-case-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCmsCaseDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsCaseDetail(String cms_case_id,HttpServletRequest request, Model model){
		CmsCase cmsCase = cmsCaseService.getCmsCaseById(cms_case_id);
		model.addAttribute("cmsCase", cmsCase);
		return new ModelAndView("pc/cms-view/cms-case/cms-case-detail");
	}
}
