package jehc.cmsmodules.cmsweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.CommonUtils;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.cmsmodules.cmsmodel.CmsProduct;
import jehc.cmsmodules.cmsservice.CmsProductService;

/**
* 内容发布平台产品
* 2018-06-10 15:05:11  邓纯杰
*/
@Api(value = "内容发布平台产品", description = "内容发布平台产品")
@Controller
@RequestMapping("/cmsProductController")
public class CmsProductController extends BaseAction{
	@Autowired
	private CmsProductService cmsProductService;
	/**
	* 列表页面
	* @param cmsProduct
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCmsProduct(CmsProduct cmsProduct,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-product/cms-product-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCmsProductListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsProductListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<CmsProduct> cmsProductList = cmsProductService.getCmsProductListByCondition(condition);
		String jehcimg_base_url = callBaseFileUrl();
		for(int i = 0; i < cmsProductList.size(); i++){
			cmsProductList.get(i).setXt_attachmentPath(jehcimg_base_url+cmsProductList.get(i).getXt_attachmentPath());
		}
		PageInfo<CmsProduct> page = new PageInfo<CmsProduct>(cmsProductList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param cms_product_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCmsProductById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsProductById(String cms_product_id,HttpServletRequest request){
		CmsProduct cmsProduct = cmsProductService.getCmsProductById(cms_product_id);
		return outDataStr(cmsProduct);
	}
	/**
	* 添加
	* @param cmsProduct
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String addCmsProduct(CmsProduct cmsProduct,HttpServletRequest request){
		int i = 0;
		if(null != cmsProduct){
			cmsProduct.setCtime(getDate());
			cmsProduct.setXt_userinfo_id(getXtUid());
			cmsProduct.setCms_product_id(UUID.toUUID());
			i=cmsProductService.addCmsProduct(cmsProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param cmsProduct
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCmsProduct(CmsProduct cmsProduct,HttpServletRequest request){
		int i = 0;
		if(null != cmsProduct){
			cmsProduct.setMtime(getDate());
			i=cmsProductService.updateCmsProduct(cmsProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param cms_product_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String delCmsProduct(String cms_product_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(cms_product_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("cms_product_id",cms_product_id.split(","));
			i=cmsProductService.delCmsProduct(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param cms_product_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public String copyCmsProduct(String cms_product_id,HttpServletRequest request){
		int i = 0;
		CmsProduct cmsProduct = cmsProductService.getCmsProductById(cms_product_id);
		if(null != cmsProduct){
			cmsProduct.setCms_product_id(UUID.toUUID());
			i=cmsProductService.addCmsProduct(cmsProduct);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCmsProduct",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCmsProduct(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCmsProductAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsProductAdd(CmsProduct cmsProduct,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-product/cms-product-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCmsProductUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsProductUpdate(String cms_product_id,HttpServletRequest request, Model model){
		CmsProduct cmsProduct = cmsProductService.getCmsProductById(cms_product_id);
		model.addAttribute("cmsProduct", cmsProduct);
		return new ModelAndView("pc/cms-view/cms-product/cms-product-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCmsProductDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsProductDetail(String cms_product_id,HttpServletRequest request, Model model){
		CmsProduct cmsProduct = cmsProductService.getCmsProductById(cms_product_id);
		model.addAttribute("cmsProduct", cmsProduct);
		return new ModelAndView("pc/cms-view/cms-product/cms-product-detail");
	}
}
