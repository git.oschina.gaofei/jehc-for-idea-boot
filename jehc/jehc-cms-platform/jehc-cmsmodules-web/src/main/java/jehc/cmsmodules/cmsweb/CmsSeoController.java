package jehc.cmsmodules.cmsweb;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.xtmodules.xtcore.allutils.StringUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import jehc.xtmodules.xtcore.base.BaseAction;
import jehc.xtmodules.xtcore.base.BaseSearch;
import jehc.xtmodules.xtcore.util.excel.poi.ExportExcel;
import jehc.xtmodules.xtcore.util.UUID;
import jehc.cmsmodules.cmsmodel.CmsSeo;
import jehc.cmsmodules.cmsservice.CmsSeoService;

/**
* 内容发布平台SEO配置 
* 2018-06-10 15:15:07  邓纯杰
*/
@Api(value = "内容发布平台SEO配置", description = "内容发布平台SEO配置")
@Controller
@RequestMapping("/cmsSeoController")
public class CmsSeoController extends BaseAction{
	@Autowired
	private CmsSeoService cmsSeoService;
	/**
	* 列表页面
	* @param cmsSeo
	* @param request 
	* @return
	*/
	@ApiOperation(value="列表页面", notes="列表页面")
	@RequestMapping(value="/loadCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView loadCmsSeo(CmsSeo cmsSeo,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-seo/cms-seo-list");
	}
	/**
	* 查询并分页
	* @param baseSearch
	* @param request 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@ResponseBody
	@RequestMapping(value="/getCmsSeoListByCondition",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsSeoListByCondition(BaseSearch baseSearch,HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(condition,request);
		List<CmsSeo> cmsSeoList = cmsSeoService.getCmsSeoListByCondition(condition);
		PageInfo<CmsSeo> page = new PageInfo<CmsSeo>(cmsSeoList);
		return outPageBootStr(page,request);
	}
	/**
	* 查询单条记录
	* @param cms_seo_id 
	* @param request 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@ResponseBody
	@RequestMapping(value="/getCmsSeoById",method={RequestMethod.POST,RequestMethod.GET})
	public String getCmsSeoById(String cms_seo_id,HttpServletRequest request){
		CmsSeo cmsSeo = cmsSeoService.getCmsSeoById(cms_seo_id);
		return outDataStr(cmsSeo);
	}
	/**
	* 添加
	* @param cmsSeo
	* @param request 
	*/
	@ApiOperation(value="添加", notes="添加")
	@ResponseBody
	@RequestMapping(value="/addCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public String addCmsSeo(CmsSeo cmsSeo,HttpServletRequest request){
		int i = 0;
		if(null != cmsSeo){
			cmsSeo.setCms_seo_id(UUID.toUUID());
			cmsSeo.setXt_userinfo_id(getXtUid());
			cmsSeo.setCtime(getDate());
			i=cmsSeoService.addCmsSeo(cmsSeo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param cmsSeo
	* @param request 
	*/
	@ApiOperation(value="修改", notes="修改")
	@ResponseBody
	@RequestMapping(value="/updateCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public String updateCmsSeo(CmsSeo cmsSeo,HttpServletRequest request){
		int i = 0;
		if(null != cmsSeo){
			cmsSeo.setMtime(getDate());
			i=cmsSeoService.updateCmsSeo(cmsSeo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param cms_seo_id 
	* @param request 
	*/
	@ApiOperation(value="删除", notes="删除")
	@ResponseBody
	@RequestMapping(value="/delCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public String delCmsSeo(String cms_seo_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(cms_seo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("cms_seo_id",cms_seo_id.split(","));
			i=cmsSeoService.delCmsSeo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 复制一行并生成记录
	* @param cms_seo_id 
	* @param request 
	*/
	@ApiOperation(value="复制一行并生成记录", notes="复制一行并生成记录")
	@ResponseBody
	@RequestMapping(value="/copyCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public String copyCmsSeo(String cms_seo_id,HttpServletRequest request){
		int i = 0;
		CmsSeo cmsSeo = cmsSeoService.getCmsSeoById(cms_seo_id);
		if(null != cmsSeo){
			cmsSeo.setCms_seo_id(UUID.toUUID());
			i=cmsSeoService.addCmsSeo(cmsSeo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 导出
	* @param excleData 
	* @param excleHeader 
	* @param excleText 
	* @param request 
	* @param request 
	*/
	@ApiOperation(value="导出", notes="导出")
	@RequestMapping(value="/exportCmsSeo",method={RequestMethod.POST,RequestMethod.GET})
	public void exportCmsSeo(String excleData,String excleHeader,String excleText,HttpServletRequest request,HttpServletResponse response){
		ExportExcel exportExcel = new ExportExcel();
		exportExcel.exportExcel(excleData, excleHeader,excleText,response);
	}
	/**
	* 新增页面
	* @param request 
	*/
	@ApiOperation(value="新增页面", notes="新增页面")
	@RequestMapping(value="/toCmsSeoAdd",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsSeoAdd(CmsSeo cmsSeo,HttpServletRequest request){
		return new ModelAndView("pc/cms-view/cms-seo/cms-seo-add");
	}
	/**
	* 编辑页面
	* @param request 
	*/
	@ApiOperation(value="编辑页面", notes="编辑页面")
	@RequestMapping(value="/toCmsSeoUpdate",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsSeoUpdate(String cms_seo_id,HttpServletRequest request, Model model){
		CmsSeo cmsSeo = cmsSeoService.getCmsSeoById(cms_seo_id);
		model.addAttribute("cmsSeo", cmsSeo);
		return new ModelAndView("pc/cms-view/cms-seo/cms-seo-update");
	}
	/**
	* 详情页面
	* @param request 
	*/
	@ApiOperation(value="详情页面", notes="详情页面")
	@RequestMapping(value="/toCmsSeoDetail",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toCmsSeoDetail(String cms_seo_id,HttpServletRequest request, Model model){
		CmsSeo cmsSeo = cmsSeoService.getCmsSeoById(cms_seo_id);
		model.addAttribute("cmsSeo", cmsSeo);
		return new ModelAndView("pc/cms-view/cms-seo/cms-seo-detail");
	}
}
