### 演示地址：
http://49.233.42.59
或
http://www.jehc.top

普通账号及密码：jehc/123456


 **各版本如下：** 

1.Eclipse开发工具，mvc版本，多工程依赖，Maven版本:https://gitee.com/jehc/jehc

2.Eclipse开发工具，MVC版本，单工程，非Maven版本:https://gitee.com/jehc/jehc-none-maven

3.Eclipse开发工具，Mvc版本，单工程，maven版本:https://gitee.com/jehc/jehc-admin	

4.Eclipse开发工具，Mvc版本，单工程，工作流平台，Maven版本:https://gitee.com/jehc/jehc-activiti	

5.Idea开发工具，Mvc版本，多工程依赖，Maven版本:https://gitee.com/jehc/jehc_for_idea_mvc	

6.Eclipse开发工具，Boot版本，多工程依赖，Maven版本:https://gitee.com/jehc/jehc-for-eclipse-boot	

 **7.Idea开发工具,BOOT版本，多工程依赖，Maven版本：https://gitee.com/jehc/jehc-for-idea-boot** 

8.Idea开发工具,MVC版本，单工程，Maven版本：https://gitee.com/jehc/jehc_admin-for_idea_mvc.git

 **9.Idea开发工具，基于SpringCloud2.0版本，微服务版本：https://gitee.com/jehc/jehc-cloud-base-version.git** 


###本项目主打工作流系统包含重写Activiti工作流设计器支持IE，360 ，谷歌，火狐等浏览器


### 交流群：
673790569
### 邮箱：hxtkdcj@163.com 或 244831954@qq.com

### 特别提示：
### 微服务版本（已开源）
 **下载地址：** 
https://gitee.com/jehc/jehc-cloud-base-version.git

![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/111907_fe81c37c_1341290.jpeg "cloud.jpg")

### 全部采用模块化（零耦合 高可用）


 **开发工具：** 
 **eclipse-jee-mars-1
或eclipse-jee-mars-2
或eclipse-juno
或STS
或IDEA** 


采用的技术如下：

 **框架基础后端技术：** Spring+SpringMVC+Mybatis

 **框架基础前端技术：** Jquery+Bootstrap4.0+Extjs6.2.1+Mxgraph(流程设计器)等

 **框架其它技术：** 接口采用Swagger2，全文检索：solr4.10，工作流引擎Activiti5.22，缓存框架：【Redis，Ehcache】，Logback，FTP，hessian，FastJSON,GZIP(TK技术)，quartz，消息中间件RabbitMq+Kafka，及时通讯NETTY,分页插件PageHelper,SpringPool

框架数据库支持：Mysql5.6++，Oracle11g


**工业设计器**
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/111959_ba6cc0b7_1341290.png "tpt.png")

 **首页** 
![输入图片说明](https://gitee.com/uploads/images/2018/0624/083133_c76f44f1_1341290.png "首页.png")

 **流程模块** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164022_57141690_1341290.png "流程排他.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164041_9eabe1e7_1341290.png "排他.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164048_b0445dde_1341290.png "泳道.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164055_be96c2e4_1341290.png "配置表单字段.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164032_550cbdea_1341290.png "流程中心.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/163959_0fe6e2d5_1341290.png "发起流程实例.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164008_b29786a7_1341290.png "分配用户.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/164015_dc7516f2_1341290.png "流程监控图.png")

 **全文检索配置** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/165046_b96b70a5_1341290.png "2PYIIK998WJ]02}]O{[[`}Y.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0824/165226_11d49b04_1341290.png "I%7$_(X{U`CD_2GB}W@4B07.png")

### Eclipse MVC版本开发前部署可能出现问题
一般出现环境问题（主要是编译问题较多）
建议如下操作：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0903/121008_95886692_1341290.png "clean.png")


 **捐赠：** 

![输入图片说明](https://gitee.com/uploads/images/2018/0607/151148_2ecb3136_1341290.jpeg "微信.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0607/151155_ddbbd81d_1341290.jpeg "支付宝.jpg")

### 演示地址：新版本流程设计器演示地址（暂不开源）：

http://www.jehc.top/jehc-cloud-front/view/pc/lc-view/lc-design/grapheditor/index.html
